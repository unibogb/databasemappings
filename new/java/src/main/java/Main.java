import it.giacomobergami.filemaps.BufferReaderIterator;
import it.giacomobergami.filemaps.OffsetIterator;
import it.giacomobergami.filemaps.VertexEntry;
import it.giacomobergami.filemaps.buffers.BufferPointer;
import it.giacomobergami.filemaps.buffers.LongMMFile;
import it.giacomobergami.filemaps.formats.EdgesOUTH;
import it.giacomobergami.filemaps.formats.HashListEntryFormat;

import java.io.File;
import java.io.IOException;

/**
 * Created by vasistas on 05/09/16.
 */
public class Main {

    public static void readHashFile() throws IOException {
        String pathLeft = "/Users/vasistas/cgraph2/data/left/10";
        LongMMFile hashLeft = new LongMMFile(new File(pathLeft, "Hash.bin"));
        BufferReaderIterator<HashListEntryFormat> i = new BufferReaderIterator<>(HashListEntryFormat.class, hashLeft);
        while (i.hasNext()) {
            HashListEntryFormat n = i.next();
            System.out.println("Hash="+(n.hash)+", Offset="+(n.offset)+" bbb="+n.bbb);
        }
        hashLeft.close();
    }

    public static void main(String args[]) throws IOException {

        String pathLeft = "/Users/vasistas/cgraph2/data/left/5";
        LongMMFile hashLeft = new LongMMFile(new File(pathLeft, "Hash.bin"));
        BufferReaderIterator<HashListEntryFormat> hIt = new BufferReaderIterator<>(HashListEntryFormat.class, hashLeft);
        LongMMFile VALeft = new LongMMFile(new File(pathLeft, "VAOffset.bin"));
        int count = 0;
        int limit = 8;

        long currOffset = 0, nextOffset = 0;
        HashListEntryFormat nextHash = null, curHash = null;
        while (hIt.hasNext() && count<limit) {
            if (nextHash==null && curHash==null) {
                currOffset =  0;
                curHash = hIt.next();
                currOffset = curHash.offset;
            } else {
                curHash = nextHash;
                currOffset = nextOffset;
            }
            if (hIt.hasNext()) {
                nextHash = hIt.next();
                nextOffset = nextHash.offset;
            } else {
                nextHash = null;
                nextOffset = hashLeft.getSize();
            }
            OffsetIterator<VertexEntry> i = new OffsetIterator<>(VertexEntry.class, VALeft, currOffset, nextOffset);

            while (i.hasNext() && count<limit) {
                VertexEntry ve = i.next();
                System.out.println("Len=" + ve.vertexHead.size + " Hash=" + curHash.hash + " offset=" + curHash.offset);
                BufferPointer ptr = ve.getString(2);
                System.out.println(new String(ptr.memory, ptr.offsetBegin, ptr.length));
                ptr = ve.getString(0);
                System.out.println(new String(ptr.memory, ptr.offsetBegin, ptr.length));
                ptr = ve.getString(1);
                System.out.println(new String(ptr.memory, ptr.offsetBegin, ptr.length));
                for (EdgesOUTH e : ve.outgoing) {
                    System.out.println("\t id=" + e.adjacentID + " hash=" + e.adjacentHash);
                }
                count++;
            }
        }

        VALeft.close();
        hashLeft.close();
    }

}
