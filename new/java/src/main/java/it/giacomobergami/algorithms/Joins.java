/*
 * Joins.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.algorithms;

import it.giacomobergami.filemaps.BufferReaderIterator;
import it.giacomobergami.filemaps.OffsetIterator;
import it.giacomobergami.filemaps.VertexEntry;
import it.giacomobergami.filemaps.buffers.BufferArray;
import it.giacomobergami.filemaps.buffers.LongMMFile;
import it.giacomobergami.filemaps.formats.EdgesOUTH;
import it.giacomobergami.filemaps.formats.HashListEntryFormat;
import it.giacomobergami.filemaps.formats.IndexHashPos;
import it.giacomobergami.filemaps.queries.QueryAndChain;
import it.giacomobergami.filemaps.queries.QueryAttributes;
import it.giacomobergami.utils.I;
import it.giacomobergami.utils.datastructures.Pair;
import it.giacomobergami.utils.datastructures.iterators.MapIterator;
import it.giacomobergami.utils.datastructures.sets.UnidirectionalQueue;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by vasistas on 04/05/16.
 */
public class Joins {

    /**
     * Stores the result of the hash pointers, with the offset for starting and ending the iteration
     */
    public static class HashPointers implements Comparable<HashPointers> {
        public final int hash;
        public final long offsetLeftStart;
        public final long offsetLeftEnd;
        public final long offsetRightStart;
        public final long offsetRightEnd;

        public HashPointers(int hash, long offsetLeftStart, long offsetLeftEnd, long offsetRightStart, long offsetRightEnd) {
            this.hash = hash;
            this.offsetLeftStart = offsetLeftStart;
            this.offsetLeftEnd = offsetLeftEnd;
            this.offsetRightStart = offsetRightStart;
            this.offsetRightEnd = offsetRightEnd;
        }

        @Override
        public int compareTo(HashPointers o) {
            return Integer.compare(hash,o.hash);
        }
    }

    public static class JoinedAttributes {
        public final UnidirectionalQueue<Integer> leftPos, sharedLeft, sharedRight, rightPos;

        public JoinedAttributes() {
            rightPos = new UnidirectionalQueue<>();
            leftPos = new UnidirectionalQueue<>();
            sharedLeft = new UnidirectionalQueue<>();
            sharedRight = new UnidirectionalQueue<>();
        }
    }

    /**
     * This algorithm supposes that the vertices have always the same
     * @param pathLeft                  Path to the folder where the left operand is stored
     * @param pathRight                 Path to the folder where the right operand is stored
     * @param qRaw                      Query expressed as <AttributeLeft,AttributeRight> where an equijoin between the two is performed
     * @param leftattributeToPosition   The Left Graph's schema definition (mapping each attribute into a value positioned in a specific vertex)
     * @param rightattributeToPosition  The Right Graph's schema definition (as above)
     * @throws IOException
     */
    public static void basicJoin(File pathLeft, File pathRight, Collection<Pair<String,String>> qRaw, TreeMap<String,Integer> leftattributeToPosition, TreeMap<String,Integer> rightattributeToPosition) throws IOException {

        //// Step Alpha: converting a query (Steps 1--2)
        QueryAndChain q = null, qp = null;
        {
            //// Step 1: Converting the query into a integer-field check
            ArrayList<QueryAttributes> qt1 = new ArrayList<>(), qt2 = new ArrayList<>();
            qRaw.forEach(x -> {
                qt1.add(new QueryAttributes(leftattributeToPosition.get(x.first), rightattributeToPosition.get(x.value)));
                qt2.add(new QueryAttributes(leftattributeToPosition.get(x.first), rightattributeToPosition.get(x.value)));
            });

            //// Step 2: checking which are the common share attributes, and which are not. Adding such pars in the predicate to check
            Iterator<Map.Entry<String, Integer>>
                    lit = leftattributeToPosition.entrySet().iterator(),
                    rit = rightattributeToPosition.entrySet().iterator();

            //JoinedAttributes ja = new JoinedAttributes();
            Map.Entry<String, Integer>
                    a = lit.hasNext() ? lit.next() : null,
                    b = rit.hasNext() ? rit.next() : null;
            do {
                int cmp = a.getKey().compareTo(b.getKey());
                if (cmp == 0) {
                    qt1.add(new QueryAttributes(a.getValue(), b.getValue()));
                    qt2.add(new QueryAttributes(a.getValue(), b.getValue()));
                    //ja.sharedLeft.add(a.getValue());
                    //ja.sharedRight.add(b.getValue());
                    a = lit.hasNext() ? lit.next() : null;
                    b = rit.hasNext() ? rit.next() : null;
                } else if (cmp < 0) {
                    //ja.leftPos.add(a.getValue());
                    a = lit.hasNext() ? lit.next() : null;
                } else {
                    //ja.rightPos.add(b.getValue());
                    b = rit.hasNext() ? rit.next() : null;
                }
            } while (lit.hasNext() && rit.hasNext());

            //if (a!=null) ja.leftPos.add(a.getValue());
            //if (b!=null) ja.rightPos.add(b.getValue());
            //lit.forEachRemaining(x->ja.leftPos.add(x.getValue()));
            //rit.forEachRemaining(x->ja.rightPos.add(x.getValue()));

            q = new QueryAndChain(1,qt1.toArray(new QueryAttributes[]{}));
            qp = new QueryAndChain(2,qt2.toArray(new QueryAttributes[]{}));
        }

        //// Step 4: performing the join
        LongMMFile vLeft = new LongMMFile(new File(pathLeft, "VAOffset.bin")),
                vRight = new LongMMFile(new File(pathRight, "VAOffset.bin"));

        //// Step 3: Hash-Zone intersection. Storing the result in a fast access structure
        //UnidirectionalQueue<HashPointers> qit = new UnidirectionalQueue<>();

        LongMMFile lmfLeft = new LongMMFile(new File(pathLeft,"Index.bin")),
                lmfRight = new LongMMFile(new File(pathRight,"Index.bin"));
        BufferArray<IndexHashPos> baLeft = new BufferArray<>(IndexHashPos.class,lmfLeft),
                baRight = new BufferArray<>(IndexHashPos.class,lmfRight);

        //UnidirectionalQueue<NewBulkNode> bulkGraph = new UnidirectionalQueue<>();

        double mult = 0.0;
        double countMult = 0.0;

        //TreeSet<Integer> hashes =new TreeSet<>();
        {
            //Reading the indices files
            LongMMFile hashLeft = new LongMMFile(new File(pathLeft, "Hash.bin")),
                    hashRight = new LongMMFile(new File(pathRight, "Hash.bin"));

            //Iterating over an array of fixed size
            BufferReaderIterator<HashListEntryFormat> i = new BufferReaderIterator<>(HashListEntryFormat.class, hashLeft),
                    j = new BufferReaderIterator<>(HashListEntryFormat.class, hashRight);

            //Performing the intersection
            HashListEntryFormat in = null, jn = null;
            while (i.hasNext() && j.hasNext()) {
                if (in == null) in = i.next();
                if (jn == null) jn = j.next();
                if (in.hash < jn.hash) in = i.next();
                else if (in.hash > jn.hash) jn = j.next();
                else {
                    int hash = in.hash;
                    //System.out.println("HASH="+hash);
                    long leftStart = in.offset;
                    long rightStart = jn.offset;
                    in = i.next();
                    jn = j.next();
                    long leftEnd = in == null ? vLeft.getSize() : in.offset;
                    if (leftEnd-leftStart<0)
                        throw new RuntimeException("Error: start="+leftStart+" end="+leftEnd);
                    long rightEnd = jn == null ? vRight.getSize() : jn.offset;
                    if (rightEnd-rightStart<0)
                        throw new RuntimeException("Error: start="+leftStart+" end="+leftEnd);
                    //Match found: storing the left and right offset into the result
                    //qit.add(new HashPointers(in.hash, leftStart, leftEnd, rightStart, rightEnd));
                    //hashes.add(in.hash);
                    //OffsetIterator<VertexEntry> leftIterator = new OffsetIterator<>(VertexEntry.class,vLeft,h.offsetLeftStart,h.offsetLeftEnd);
                    for (VertexEntry u : new I<>(new OffsetIterator<>(VertexEntry.class,vLeft,leftStart,leftEnd))) {
                        q.compileOverLeftVertex(u);
                        boolean hasMult = false;

                        for (VertexEntry v : new I<>(new OffsetIterator<>(VertexEntry.class,vRight,rightStart,rightEnd))) {
                            if (!q.compareOverRightVertex(v)) continue;
                            //NewBulkNode bn = new NewBulkNode(u,v); // I do not have to calculate the solution: I just keep the holders
                            if (!hasMult) {
                                hasMult = true;
                                countMult+=1.0;
                            }
                            mult+=1.0;

                            int NuSize = u.ougoingSize;
                            int NvSize = v.ougoingSize;
                            if (NuSize >0 && NvSize>0) {
                                int index = 0, jndex = 0;
                                int hL = u.outgoing.get(index).adjacentHash, hR = v.outgoing.get(jndex).adjacentHash;
                                //assert(Nu[index].adjacentHash==);
                                while ((index<NuSize) && (jndex<NvSize)) {
                                    if (hL< hR) {
                                        index++;
                                        if (index<NuSize) hL = u.outgoing.get(index).adjacentHash;
                                    } else if (hL>hR) {
                                        jndex++;
                                        if (jndex<NvSize) hR = v.outgoing.get(jndex).adjacentHash;
                                    } else {
                                        //std::cout << "Hash = " << hL << std::endl;
                                        int ii = index, jj = jndex;
                                        EdgesOUTH eoii = u.outgoing.get(ii);
                                        while (ii<NuSize && eoii.adjacentHash == hL) {
                                            jj = jndex;
                                            if (eoii.adjacentID==775172146) {
                                                System.err.println("Current index="+ii+"// source id="+u.vertexHead.id+"// eoii id="+eoii.adjacentID+"// eoii hash="+eoii.adjacentHash+"// outSize="+NuSize);
                                                for (EdgesOUTH x : u.outgoing) {
                                                    System.err.println("Hash="+x.adjacentHash+" id="+x.adjacentID);
                                                }
                                            }
                                            IndexHashPos ptr = baLeft.get(eoii.adjacentID);
                                            VertexEntry t = new VertexEntry(vLeft,ptr.pos);
                                            qp.compileOverLeftVertex(t);
                                            EdgesOUTH eojj = v.outgoing.get(jj);
                                            while (jj<NvSize && eojj.adjacentHash == hR) {
                                                IndexHashPos ptr2 = baRight.get(eojj.adjacentID);
                                                VertexEntry t2 = new VertexEntry(vRight,ptr2.pos);
                                                if (qp.compareOverRightVertex(t2)) {
                                                    //solution->storeVertexInternal(up.vertexHeader->id,vp.vertexHeader->id);
                                                    //solution->storeEdge(u.vertexHeader->id,v.vertexHeader->id,up.vertexHeader->id,vp.vertexHeader->id);
                                                }

                                                //std::cout << "\t" << Nu[ii].adjacentID << ", " << Nv[jj].adjacentID << std::endl;
                                                jj++;
                                                if (jj<NvSize) eojj = v.outgoing.get(jj);
                                            }
                                            ii++;
                                            if (ii<NuSize) eoii = u.outgoing.get(ii);
                                        }
                                        index = ii;
                                        jndex = jj;
                                        if ((index<NuSize) && (jndex<NvSize)) {
                                            hL = u.outgoing.get(index).adjacentHash;
                                            hR = v.outgoing.get(jndex).adjacentHash;
                                        }
                                    }
                                }
                                //bulkGraph.add(bn);
                            }
                        }

                    }
                }
            }
            hashLeft.close();
            hashRight.close();
        }


        //System.out.println("Mult="+(mult/countMult));
        //System.out.println("Size="+bulkGraph.size);

        lmfLeft.close();
        lmfRight.close();
        vLeft.close();
        vRight.close();

    }

    /**
     * This algorithm supposes that the vertices have always the same
     * @param pathLeft                  Path to the folder where the left operand is stored
     * @param pathRight                 Path to the folder where the right operand is stored
     * @param x                      Query expressed as <AttributeLeft,AttributeRight> where an equijoin between the two is performed
     * @param leftattributeToPosition   The Left Graph's schema definition (mapping each attribute into a value positioned in a specific vertex)
     * @param rightattributeToPosition  The Right Graph's schema definition (as above)
     * @throws IOException
     */
    /*public static void leqJoin(File pathLeft, File pathRight, Pair<String,String> x, TreeMap<String,Integer> leftattributeToPosition, TreeMap<String,Integer> rightattributeToPosition) throws IOException {

        //// Step Alpha: converting a query (Steps 1--2)
        QueryAndChain q, qp;
        {
            //// Step 1: Converting the query into a integer-field check
            ArrayList<QueryAttributes> qt1 = new ArrayList<>(), qt2 = new ArrayList<>();
            qt1.add(new QueryAttributes(leftattributeToPosition.get(x.first), rightattributeToPosition.get(x.value)));
            qt2.add(new QueryAttributes(leftattributeToPosition.get(x.first), rightattributeToPosition.get(x.value)));

            //// Step 2: checking which are the common share attributes, and which are not. Adding such pars in the predicate to check
            Iterator<Map.Entry<String, Integer>>
                    lit = leftattributeToPosition.entrySet().iterator(),
                    rit = rightattributeToPosition.entrySet().iterator();

            JoinedAttributes ja = new JoinedAttributes();
            Map.Entry<String, Integer>
                    a = lit.hasNext() ? lit.next() : null,
                    b = rit.hasNext() ? rit.next() : null;
            do {
                int cmp = a.getKey().compareTo(b.getKey());
                if (cmp == 0) {
                    qt1.add(new QueryAttributes(a.getValue(), b.getValue()));
                    qt2.add(new QueryAttributes(a.getValue(), b.getValue()));
                    ja.sharedLeft.add(a.getValue());
                    ja.sharedRight.add(b.getValue());
                    a = lit.hasNext() ? lit.next() : null;
                    b = rit.hasNext() ? rit.next() : null;
                } else if (cmp < 0) {
                    ja.leftPos.add(a.getValue());
                    a = lit.hasNext() ? lit.next() : null;
                } else {
                    ja.rightPos.add(b.getValue());
                    b = rit.hasNext() ? rit.next() : null;
                }
            } while (lit.hasNext() && rit.hasNext());

            if (a!=null) ja.leftPos.add(a.getValue());
            if (b!=null) ja.rightPos.add(b.getValue());
            lit.forEachRemaining(y->ja.leftPos.add(y.getValue()));
            rit.forEachRemaining(y->ja.rightPos.add(y.getValue()));

            q = new QueryAndChain(0,qt1.toArray(new QueryAttributes[]{}));
            qp = new QueryAndChain(1,qt2.toArray(new QueryAttributes[]{}));
        }

        //// Step 3: Hash-Zone intersection. Storing the result in a fast access structure
        UnidirectionalQueue<HashPointers> qit = new UnidirectionalQueue<>();

        //// Step 4: performing the join
        LongMMFile vLeft = new LongMMFile(new File(pathLeft, "VAOffset.bin")),
                vRight = new LongMMFile(new File(pathRight, "VAOffset.bin"));

        //Reading the indices files
        LongMMFile hashLeft = new LongMMFile(new File(pathLeft, "Hash.bin")),
                hashRight = new LongMMFile(new File(pathRight, "Hash.bin"));

        LongMMFile lmfLeft = new LongMMFile(new File(pathLeft,"Index.bin")),
                lmfRight = new LongMMFile(new File(pathRight,"Index.bin"));
        BufferArray<IndexHashPos> baLeft = new BufferArray<>(IndexHashPos.class,lmfLeft),
                baRight = new BufferArray<>(IndexHashPos.class,lmfRight);

        UnidirectionalQueue<NewBulkNode> bulkGraph = new UnidirectionalQueue<>();

        double mult = 0.0;
        double countMult = 0.0;


        //Iterating over an array of fixed size
        BufferReaderIterator<HashListEntryFormat> i = new BufferReaderIterator<>(HashListEntryFormat.class, hashLeft);
        BufferBackwardReaderIterator<HashListEntryFormat> j;
        HashListEntryFormat in = i.hasNext() ? i.next() : null;
        while (in != null) {
            //Getting the informations from the left graph
            int leftHash = in.hash;
            if (leftHash==2007)
                System.out.println("Break");
            long leftOffset = in.offset;
            in = (i.hasNext() ? i.next() : null);
            long leftEnd = in == null ? vLeft.getSize() : in.offset;

            for (VertexEntry u : new I<>(new OffsetIterator<>(VertexEntry.class,vLeft,leftOffset,leftEnd))) {
                u.hash = leftHash;
                int leftYear = Integer.valueOf(u.values.get(2));
                q.compileOverLeftVertex(u);
                boolean hasMult = false;

                // Allocating all the neighbours in order not to scan the file coninuously
                Iterable<VertexEntry> Nu = new I<>(new MapIterator<Integer,VertexEntry>(u.outgoingA.iterator()) {
                    @Override
                    public VertexEntry apply(Integer o) {
                        IndexHashPos ptr = baLeft.get(o);
                        VertexEntry t = (VertexEntry)u.unserialize(vLeft,ptr.pos);
                        t.hash = ptr.hash;
                        return t;
                    }
                });

                //getting the informations from the right graph
                j = new BufferBackwardReaderIterator<>(HashListEntryFormat.class, hashRight);
                long rightEnd = vRight.getSize();
                HashListEntryFormat jn = j.hasNext() ? j.next() : null;
                while (jn != null) {
                    int rightHash = jn.hash;
                    //System.out.println(leftHash+" < "+rightHash+"...");
                    if (leftHash>rightHash) { break;}  // stop to analyse the right graph if I overcome with right matches
                    //System.out.println("Ok");
                    long rightOffset = jn.offset;
                    jn = (j.hasNext() ? j.next() : null);

                    for (VertexEntry v : new I<>(new OffsetIterator<>(VertexEntry.class,vRight,rightOffset,rightEnd))) {
                        v.hash = rightHash;
                        //System.out.println("Here");
                        if (!q.leqOverRightVertex(v)) continue;
                        //NewBulkNode bn = new NewBulkNode(u,v); // I do not have to calculate the solution: I just keep the holders
                        if (!hasMult) {
                            hasMult = true;
                            countMult+=1.0;
                        }
                        mult+=1.0;


                        Iterable<VertexEntry> Nv = new I<>(new MapIterator<Integer,VertexEntry>(v.outgoingA.iterator()) {
                            @Override
                            public VertexEntry apply(Integer o) {
                                IndexHashPos ptr = baRight.get(o);
                                VertexEntry t = u.unserialize(vRight,ptr.pos);
                                t.hash = ptr.hash;
                                return t;
                            }
                        });

                        for (VertexEntry up : Nu) {
                            qp.compileOverLeftVertex(up);
                            for (VertexEntry vp : Nv) {
                                if (up.hash>vp.hash) continue;
                                if (!qp.leqOverRightVertex(vp)) continue;
                                bn.jointNeighbours.add(new Pair(up.id,vp.id));
                            }
                        }
                        bulkGraph.add(bn);
                    }
                    rightEnd = rightOffset;
                }
            }
        }

        System.out.println("Mult="+(mult/countMult));
        System.out.println("Size="+bulkGraph.size);

        lmfLeft.close();
        vLeft.close();
        vRight.close();
    }*/

}
