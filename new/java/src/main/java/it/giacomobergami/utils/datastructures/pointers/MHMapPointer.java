/*
 * MHMapPointer.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.utils.datastructures.pointers;

import it.giacomobergami.utils.MultiMapValuePointer;
import it.giacomobergami.utils.datastructures.HashableHashSet;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

/**
 * Creates a fictious entry to the map. Due to Java's implementation, when I have to remove the node, the
 * tree scan is unavoidable
 */
public class MHMapPointer<K extends Comparable<K>, V extends Comparable<V>> implements MultiMapValuePointer<K,V> {

    private Map<K,Collection<V>> master;
    private Map.Entry<K,Collection<V>> l;
    private K rememberOldKey;

    public MHMapPointer(Map<K, Collection<V>> master, Map.Entry<K, Collection<V>> l) {
        this.master = master;
        this.l = l;
        this.rememberOldKey = l.getKey();
    }

    @Override
    public K getKey() {
        return rememberOldKey;
    }

    private void sanityCheck() {
        if (l==null) {
            // this means that there is no entry in master
            master.put(rememberOldKey,new HashableHashSet<V>());
            //Create the new pointer
            l = new Map.Entry<K, Collection<V>>() {
                Collection<V> ll = master.get(rememberOldKey);
                @Override
                public K getKey() {
                    return rememberOldKey;
                }
                @Override
                public Collection<V> getValue() {
                    return ll;
                }
                @Override
                public Collection<V> setValue(Collection<V> value) {
                    return master.put(rememberOldKey,value);
                }
            };
        }
    }

    @Override
    public Optional<V> get(int position) {
        if (l==null) return Optional.empty(); else {
            Iterator<V> it = l.getValue().iterator();
            int pos = position;
            while (it.hasNext() && pos>0) {
                it.next(); pos--;
            }
            return !it.hasNext() ? Optional.<V>empty() : Optional.of(it.next());
        }
    }

    @Override
    public boolean add(V value) {
        return l.getValue().add(value);
    }

    @Override
    public Iterator<V> iterator() {
        return l.getValue().iterator();
    }

    @Override
    public boolean contains(V src) {
        return l.getValue().contains(src);
    }

    @Override
    public int getSize() {
        return l.getValue().size();
    }

    @Override
    public void update(Collection<V> newValues) {
        master.put(rememberOldKey,newValues);
    }

    @Override
    public void removeNode() {
        master.remove(rememberOldKey);
        l = null;
    }
}
