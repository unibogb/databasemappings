package it.giacomobergami.filemaps.formats;

import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.buffers.IBufferReader;
import it.giacomobergami.filemaps.buffers.PointerBuffer;
import it.giacomobergami.filemaps.formats.interfaces.IWriteSerializable;
import it.giacomobergami.filemaps.rw.Writer;

/**
 * Created by vasistas on 04/09/16.
 */
public class EdgesOUTH extends IWriteSerializable<EdgesOUTH> {

    public int adjacentHash;
    public int adjacentID;

    public EdgesOUTH() {
        super(null);
        adjacentHash = adjacentID = 0;
    }

    public EdgesOUTH(IBufferReader br, long offset) {
        super(null);
        adjacentHash = FileMapsUtils.toInt(br.get(offset , 4), 0);
        adjacentID = FileMapsUtils.toInt(br.get(offset + 4, 4), 0);
    }

    @Override
    public int getStorageSize() {
        return 8;
    }

    @Override
    public EdgesOUTH unserialize(IBufferReader br, long offset) {
        return new EdgesOUTH(br,offset);
    }
    public EdgesOUTH unserialize(PointerBuffer br, long offset) {
        return new EdgesOUTH(br,offset);
    }

    public long serialize(Writer w) {
        w.writeRaw(adjacentHash);
        w.writeRaw(adjacentID);
        return 8;
    }

}
