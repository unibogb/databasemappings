/*
 * Writer.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */


package it.giacomobergami.filemaps.rw;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by vasistas on 25/04/16.
 */
public class Writer {

    private final ByteArrayOutputStream baos;
    private final DataOutputStream writer;

    public Writer() {
        baos = new ByteArrayOutputStream();
        writer = new DataOutputStream(baos);
    }

    public Writer(FileOutputStream fos) {
        baos = null;
        writer = new DataOutputStream(fos);
    }

    public boolean writeRaw(Long elem) {
        try {
            writer.writeLong(elem);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean write(String elem) {
        try {
            writer.writeInt(elem.length());
            writer.write(elem.getBytes());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean writeRaw(String elem) {
        try {
            writer.write(elem.getBytes());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean writeRaw(int v) {
        try {
            //Writing integers in a C fashion
            writer.write((v >>> 0) & 0xFF);
            writer.write((v >>> 8) & 0xFF);
            writer.write((v >>> 16) & 0xFF);
            writer.write((v >>> 24) & 0xFF);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean writeRaw(long v) {
        try {
            byte writeBuffer[] = new byte[8];
            writeBuffer[7] = (byte)(v >>> 56);
            writeBuffer[6] = (byte)(v >>> 48);
            writeBuffer[5] = (byte)(v >>> 40);
            writeBuffer[4] = (byte)(v >>> 32);
            writeBuffer[3] = (byte)(v >>> 24);
            writeBuffer[2] = (byte)(v >>> 16);
            writeBuffer[1] = (byte)(v >>>  8);
            writeBuffer[0] = (byte)(v >>>  0);
            writer.write(writeBuffer, 0, 8);
            //writer.writeLong(l);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean write(byte[] w) {
        try {
            if (w.length<0) throw new RuntimeException("Error: array has negative size");
            writer.writeInt(w.length);
            writer.write(w,0,w.length);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public ClosedWriter close() {
        return new ClosedWriter(baos);
    }

    public boolean writeRaw(byte[] b) {
        try {
            writer.write(b);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static class ClosedWriter {
        private byte[] toret;
        public ClosedWriter(ByteArrayOutputStream b) {
            if (b==null) toret = null;
            else
            this.toret = b.toByteArray();
            try {
                b.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        public byte[] getBytes() {
            return toret;
        }
    }

}
