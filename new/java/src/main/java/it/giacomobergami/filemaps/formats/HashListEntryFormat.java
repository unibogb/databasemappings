/*
 * HashListEntryFormat.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.formats;

import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.buffers.IBufferReader;
import it.giacomobergami.filemaps.formats.interfaces.IWriteSerializable;
import it.giacomobergami.filemaps.rw.Writer;

/**
 * Defines and creates a new entry of
 */
public class HashListEntryFormat extends IWriteSerializable<HashListEntryFormat> {

    public int hash;
    public int bbb;
    public long offset;

    public HashListEntryFormat(int hash, long offset) {
        super(null);
        this.hash = hash;
        this.bbb = 0;
        this.offset = offset;
    }

    public HashListEntryFormat() {
        super(null);
        this.hash = -1;
        this.bbb = 0;
        this.offset = -1;
    }

    public HashListEntryFormat(IBufferReader br, long offset) {
        super(null);
        hash = FileMapsUtils.toInt(br.get(offset , 4), 0);
        bbb = FileMapsUtils.toInt(br.get(offset + 4, 4), 0);
        this.offset = FileMapsUtils.toLong(br.get(offset + 8, 8), 0);
    }

    @Override
    public HashListEntryFormat unserialize(IBufferReader br, long offset) {
        return new HashListEntryFormat(br,offset);
    }

    public long serialize(Writer w) {
        w.writeRaw(hash);
        w.writeRaw(bbb);
        w.writeRaw(offset);
        return 16;
    }

    @Override
    public int getStorageSize() {
        return 16;
    }
}
