/*
 * QueryAndChain.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */


package it.giacomobergami.filemaps.queries;


import it.giacomobergami.filemaps.VertexEntry;

import java.util.Map;

/**
 * Gives an and condition over different values, where the left vertex is fixed in memory (reduce the access to getters).
 * Uses the command chain pattern Chain of Responsability. It implements a chain of conjunctive conditions
 */
public class QueryAndChain {

    public final QueryAttributes[] array;
    public final int i;
    public QueryAndChain(int i,QueryAttributes... array) {
        this.array = array;
        this.i = i;
    }

    public QueryAndChain(QueryAttributes... array) {
        this(0,array);
    }

    /*public void copileOverLeftGraphPosition(ReadonlyGraph left, ReadonlyGraph right) {
        for (int i= 0; i<array.length; i++) array[i].compileOverLeftGraphPosition(left, right);
    }*/

    public void copileOverLeftGraphPosition(Map<String,Integer> left, Map<String,Integer> right) {
        for (int i= 0; i<array.length; i++) array[i].compileOverLeftGraphPosition(left, right);
    }

    public void compileOverLeftVertex(VertexEntry vertex) {
        for (int i= 0; i<array.length; i++) array[i].compileOverLeftVertex(vertex);
    }

    public boolean compareOverRightVertex (VertexEntry vertex) {
        for (int i=0; i<array.length; i++) if (!array[i].compareEqOverRightVertex(vertex)) return false;
        return true;
    }

    public boolean leqOverRightVertex (VertexEntry vertex) {
        //System.out.println("Case of:" +i);
        for (int i=0; i<array.length; i++) if (!array[i].compareLeqOverRightVertex(vertex)) return false;
        return true;
    }

}
