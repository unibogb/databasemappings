/*
 * Reader.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */

package it.giacomobergami.filemaps.rw;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;

/**
 * Created by vasistas on 25/04/16.
 */
public class Reader /*implements AutoCloseable*/ {

    private final DataInputStream reader;

    public Reader(InputStream writer) {
        this.reader = new DataInputStream(writer);
    }
    public Reader(byte[] b) {
        this(new ByteArrayInputStream(b));
    }

    public String read() {
        try {
            int i = this.reader.readInt();
            byte[] a = new byte[i];
            this.reader.read(a,0,i);
            return new String(a);
        } catch (Exception e) {
            return null;
        }
    }

    public byte[] readRaw() {
        try {
            int i = this.reader.readInt();
            byte[] a = new byte[i];
            this.reader.read(a,0,i);
            return a;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /*@Override*/
    public void close() throws Exception {
        reader.close();
    }

}
