/*
 * QueryAttributes.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */


package it.giacomobergami.filemaps.queries;


import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.VertexEntry;
import it.giacomobergami.filemaps.buffers.BufferPointer;

import java.util.Map;

/**
 * Defines a query over two graphs' attributes
 */
public class QueryAttributes {

    //QueryState.QueryAttributes
    String attributeLeft, attributeRight;
    QueryPredicate pred;

    //QueryState.QueryPositionOverAttribute
    int leftPosition, rightPosition;

    //QueryState.QueryPointerOverAttribute
    BufferPointer leftPointer;

    //Current State
    QueryState qs;

    /**
     * Definition of the predicate indipendently from the two graphs (that is, where the attribute is stored)
     * @param attributeLeft
     * @param attributeRight
     * @param pred
     */
    public QueryAttributes(String attributeLeft, String attributeRight, QueryPredicate pred) {
        this.attributeLeft = attributeLeft;
        this.attributeRight = attributeRight;
        this.pred = pred;
        this.qs = QueryState.QueryAttributes;

        //Null for the other constants
        this.leftPosition = this.rightPosition = -1;
        this.leftPointer = null;
    }

    // First step to conversion: retrieves the position of the left graph parameter
    /*public void compileOverLeftGraphPosition(ReadonlyGraph left, ReadonlyGraph right) {
        this.leftPosition = left.attributeToPosition.get(this.attributeLeft);
        this.rightPosition = right.attributeToPosition.get(this.attributeRight);
        this.qs = QueryState.QueryPositionOverAttribute;
    }*/

    public void compileOverLeftGraphPosition(Map<String,Integer> leftattributeToPosition, Map<String,Integer> rightattributeToPosition) {
        this.leftPosition = leftattributeToPosition.get(this.attributeLeft);
        this.rightPosition = rightattributeToPosition.get(this.attributeRight);
        this.qs = QueryState.QueryPositionOverAttribute;
    }

    public QueryAttributes(int leftPosition, int rightPosition) {
        this.leftPosition = leftPosition;
        this.rightPosition = rightPosition;
        this.qs = QueryState.QueryPositionOverAttribute;
    }

    // Returns the specifications for the left vertex (memory place)
    public void compileOverLeftVertex(VertexEntry vertex) {
        this.leftPointer = vertex.getString(this.leftPosition);
        this.qs = QueryState.QueryPointerOverAttribute;
    }

    //Evaluates the extracted interface with the stored one
    public boolean compareEqOverRightVertex(VertexEntry vertex) {
        return FileMapsUtils.equals(leftPointer,vertex.getString(rightPosition));
    }

    public boolean compareLeqOverRightVertex(VertexEntry vertex) {
        return FileMapsUtils.leqInteger(leftPointer,vertex.getString(rightPosition));
    }

}
