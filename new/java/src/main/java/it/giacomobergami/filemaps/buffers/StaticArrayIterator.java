/*
 * StaticArrayIterator.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */


package it.giacomobergami.filemaps.buffers;


import it.giacomobergami.filemaps.formats.interfaces.IStaticArray;

import java.util.Iterator;

/**
 * Implements an iterator over a Static Array
 */
public class StaticArrayIterator<K> implements Iterator<K> {

    private final IStaticArray<K> elem;
    private int currentPtr;
    private int size;

    public StaticArrayIterator(IStaticArray<K> elem) {
        this.elem = elem;
        this.size = (int)elem.size();
        currentPtr = 0;
    }

    @Override
    public boolean hasNext() {
        return size>currentPtr;
    }

    @Override
    public K next() {
        return elem.get(currentPtr++);
    }
}
