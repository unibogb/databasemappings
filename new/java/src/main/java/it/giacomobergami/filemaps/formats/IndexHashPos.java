/*
 * IndexHashPos.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.formats;

import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.buffers.IBufferReader;
import it.giacomobergami.filemaps.buffers.LongMMFile;
import it.giacomobergami.filemaps.formats.interfaces.IWriteSerializable;
import it.giacomobergami.filemaps.rw.Writer;

import java.io.File;
import java.io.IOException;

/**
     * Defines the pointer to the hashmap memorization
     */
public class IndexHashPos extends IWriteSerializable<IndexHashPos> implements Comparable<IndexHashPos> {
    public final int index, hash;
    public final long pos;

    public IndexHashPos() {
        super(null);
        this.index = this.hash = -1; this.pos = -1L;
    }

    public IndexHashPos(int index, int hash, long pos) {
        super(null);
        this.index = index;
        this.hash = hash;
        this.pos = pos;
    }

    @Override
    public int compareTo(IndexHashPos o) {
            return Integer.compare(index,o.index);
        }

    @Override
    public long serialize(Writer w) {
        w.writeRaw((int)index);
        w.writeRaw((int)hash);
        w.writeRaw((long)pos);
        return 16;
    }

    @Override
    public IndexHashPos unserialize(IBufferReader br, long offset) {
        byte sameMem[] = br.get(offset,16);
        int index = FileMapsUtils.toInt(sameMem,0);
        int hash = FileMapsUtils.toInt(sameMem,4);
        long pos = FileMapsUtils.toLong(sameMem,8);
        return new IndexHashPos(index,hash,pos);
    }

    @Override
    public int getStorageSize() {
        return 16;
    }

}
