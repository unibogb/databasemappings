/*
 * QueryPredicate.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */


package it.giacomobergami.filemaps.queries;

import it.giacomobergami.filemaps.FileMapsUtils;

/**
 * Defines a predicate over the byte representation
 */
public enum QueryPredicate {
    /**
     * Defines the equality between two vertices' attributes
     */
    AttributeEquals((a, lOff, lLen, a2, rOff, rLen) -> FileMapsUtils.equals(a,lOff,lLen,a2,rOff,rLen)),
    AttributeIntLeq((a, lOff, lLen, a2, rOff, rLen) -> FileMapsUtils.leqInteger(a, lOff, lLen, a2, rOff, rLen));

    /**
     * Interface of a predicate over the byte representation
     */
    public interface Interface {
        boolean prop(byte[] a, int leftOffset, int leftLen, byte[] a2, int rightOffset, int rightLen);
    }

    public final Interface predicate;
    QueryPredicate(Interface p) {
        this.predicate = p;
    }
}
