package it.giacomobergami.filemaps;

import it.giacomobergami.filemaps.buffers.BufferArray;
import it.giacomobergami.filemaps.buffers.BufferPointer;
import it.giacomobergami.filemaps.buffers.IBufferReader;
import it.giacomobergami.filemaps.formats.*;
import it.giacomobergami.filemaps.formats.interfaces.IWriteSerializable;
import it.giacomobergami.filemaps.rw.Writer;

import java.util.Iterator;

/**
 * Created by vasistas on 05/09/16.
 */
public class VertexEntry extends IWriteSerializable<VertexEntry> {

    public final Header vertexHead;
    public int stringArrayHeader[];
    //public final StaticIntegerArray stringArrayHeader;
    //public final StaticStringArray stringArray;
    public final int noElems;
    //public final int lastElemSize;
    public final byte[] stringArray;
    public final int ougoingSize;
    public final BufferArray<EdgesOUTH> outgoing;

    public VertexEntry() {
        super(null);
        vertexHead = null;
        stringArrayHeader = null;
        noElems =ougoingSize =-1;
        stringArray = null;
        outgoing = null;
    }

    public VertexEntry(IBufferReader buff, long offset) {
        super(null);
        long tmpOffset;
        vertexHead = new Header(buff,offset);
        tmpOffset = offset + vertexHead.getStorageSize();
        noElems = FileMapsUtils.toInt(buff.get(tmpOffset,4),0);
        tmpOffset+=4;
        stringArrayHeader = new int[noElems];
        for (int i=0; i<noElems-1; i++) {
            stringArrayHeader[i] = FileMapsUtils.toInt(buff.get(tmpOffset, 4), 0);
            tmpOffset += 4;
        }
        //stringArrayHeader = new StaticIntegerArray(buff.get(tmpOffset, 4*(noElems)));
        //tmpOffset += (noElems*4);
        long basics = vertexHead.offsetOUT - vertexHead.getStorageSize() - (noElems * 4);
        stringArray = buff.get(tmpOffset, (int) basics);
        stringArrayHeader[noElems-1] = (int) (basics);
        ougoingSize = FileMapsUtils.toInt(buff.get(offset+vertexHead.offsetOUT,4),0);
        int buffLen = new EdgesOUTH().getStorageSize() * ougoingSize;
        long outOff = offset+vertexHead.offsetOUT+4;
        outgoing = new BufferArray<>(EdgesOUTH.class,buff.getSubBuffer(outOff,buffLen));
    }

    public BufferPointer getString(int i) {
        return new BufferPointer(stringArray,
                                    (i==0) ? 0 : stringArrayHeader[i-1],
                               (i==0) ? stringArrayHeader[0]-1 : (stringArrayHeader[i]-stringArrayHeader[i-1])-1);
    }

    @Override
    public long serialize(Writer w) {
        throw new UnsupportedOperationException("serialize in VertexEntry");
    }

    @Override
    public int getStorageSize() {
        return (int) vertexHead.size;
    }

    @Override
    public VertexEntry unserialize(IBufferReader br, long offset) {
        return new VertexEntry(br,offset);
    }
}
