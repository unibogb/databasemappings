/*
 * IWriteSerializable.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.formats.interfaces;

import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.buffers.IBufferReader;
import it.giacomobergami.filemaps.rw.Writer;

import java.lang.reflect.Field;

/**
 * Created by vasistas on 28/04/16.
 */
public abstract class IWriteSerializable<T> {

    public enum nativetypes {
        INT(4), LONG(8);
        public final int size;
        nativetypes(int i) {
            size = i;
        }
    };

    private Class<T> clazz;
    private final int size;
    private final Field[] fs;
    private final nativetypes[] nt;
    public IWriteSerializable(Class<T> claass) {
        clazz = claass;
        if (claass!=null) {
            fs = clazz.getDeclaredFields();
            nt = new nativetypes[fs.length];
            for (int i = 0; i < fs.length; i++) {
                nt[i] = FileMapsUtils.typeEnum(fs[i].getGenericType());
            }
            size = (int) FileMapsUtils.getSerializedSize(clazz);
        } else {
            size = 0;
            fs = null;
            nt = null;
        }
    }

    /**
     * Serialize the class into a writer
     *
     * @param w Where to save the serialization
     * @return  the number of the bytes that have been written (hence, returns the)
     */
    public long serialize(Writer w) {
        if (clazz!=null) {
            try {
                for (int i = 0; i < nt.length; i++) {
                    switch (nt[i]) {
                        case INT:
                            w.writeRaw(fs[i].getInt(this));
                            break;
                        case LONG:
                            w.writeRaw(fs[i].getLong(this));
                            break;
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return getStorageSize();
    }

    /**
     * Creates a new element from a reader buffer r
     * @param br        where to read the data
     * @param offset    at which point read the data
     * @return
     */
    public IWriteSerializable(Class<T> claass,IBufferReader br, long offset) {
        this(claass);
        if (claass!=null) {
            try {
                long offsetAdd = 0;
                for (int i = 0; i < fs.length; i++) {
                    System.out.println(fs[i].getName());
                    switch (nt[i]) {
                        case INT:
                            fs[i].set(this, FileMapsUtils.toInt(br.get(offset + offsetAdd, nt[i].size), 0));
                            offsetAdd += 4;
                            break;
                        case LONG:
                            fs[i].set(this, FileMapsUtils.toLong(br.get(offset + offsetAdd, nt[i].size), 0));
                            offsetAdd += 8;
                            break;
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Returns the number of bytes that are associated to the structure represented in the memory buffer
     * @return
     */
    public int getStorageSize() {
        return size;
    }

    /**
     * Creates a new element from a reader buffer r
     * @param br        where to read the data
     * @param offset    at which point read the data
     * @return
     */
    public abstract T unserialize(IBufferReader br, long offset);

}
