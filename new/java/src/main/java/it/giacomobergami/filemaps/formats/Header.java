package it.giacomobergami.filemaps.formats;

import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.buffers.IBufferReader;
import it.giacomobergami.filemaps.formats.interfaces.IWriteSerializable;

/**
 * Created by vasistas on 05/09/16.
 */
public class Header extends IWriteSerializable<Header> {

    public long size;
    public long offsetOUT;
    public int  id;
    public int  bbb;

    public Header() {
        super(Header.class);
        size = offsetOUT = 0;
        id = bbb = 0;
    }

    public Header(IBufferReader br, long offset) {
        super(null);
        size = FileMapsUtils.toLong(br.get(offset , 8), 0);
        offsetOUT = FileMapsUtils.toLong(br.get(offset + 8, 8), 0);
        id = FileMapsUtils.toInt(br.get(offset + 16, 4), 0);
        bbb = FileMapsUtils.toInt(br.get(offset + 20, 4), 0);
    }

    @Override
    public int getStorageSize() {
        return 24;
    }

    @Override
    public Header unserialize(IBufferReader br, long offset) {
        return new Header(br,offset);
    }
}
