package it.giacomobergami.filemaps.buffers;

import com.sun.jna.Pointer;

/**
 * Created by vasistas on 05/09/16.
 */
public class PointerBuffer implements IBufferReader {

    private final Pointer ptr;
    private final int size;
    private final long offset;

    /**
     * If the pointer is a memory mapped file, I cannot obtain a pointer within a pointer. An error is going to be
     * returned. On the other hand, if I want some
     * @param ptr
     * @param offset
     * @param size
     */
    public PointerBuffer(Pointer ptr, long offset, int size) {
        this.ptr = ptr;
        this.size = size;
        this.offset = offset;
    }
    @Override
    public byte[] get(long offset, int size) {
        return ptr.getByteArray(this.offset+offset,size);
    }

    @Override
    public IBufferReader getSubBuffer(long offset, int size) {
        return new PointerBuffer(ptr,this.offset+offset,size);
    }

    @Override
    public long getSize() {
        return size;
    }

    @Override
    public  Class<PointerBuffer> getClazz() {
        return PointerBuffer.class;
    }
}
