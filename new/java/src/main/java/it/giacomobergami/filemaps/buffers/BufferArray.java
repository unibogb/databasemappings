/*
 * BufferArray.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.buffers;


import it.giacomobergami.filemaps.formats.interfaces.IStaticArray;
import it.giacomobergami.filemaps.formats.interfaces.IWriteSerializable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;

/**
 * Wraps a buffer as an array (given the data that has to be mapped)
 */
public class BufferArray<F extends IWriteSerializable<F>> implements IStaticArray<F> {

    private final IBufferReader buf;
    private int sizeBlock;
    private Method creator;
    private F element;

    public BufferArray(Class<F> clazz, IBufferReader buf) {
        this.buf = buf;
        try {
            element = clazz.newInstance();
            sizeBlock = (Integer)clazz.getDeclaredMethod("getStorageSize").invoke(element);
            creator = clazz.getDeclaredMethod("unserialize",buf.getClazz(),long.class);
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    public long size() {
        return buf.getSize() / sizeBlock;
    }


    @Override
    public F get(int pos) {
        //if (pos>=size()) throw new RuntimeException("Pos greater than actual pos: "+pos+">="+size());
        try {
            return (F)creator.invoke(element,buf,pos*sizeBlock);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Iterator<F> iterator() {
        return new StaticArrayIterator<>(this);
    }
}
