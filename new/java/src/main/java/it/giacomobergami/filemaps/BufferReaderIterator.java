/*
 * BufferReaderIterator.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps;

import it.giacomobergami.filemaps.buffers.IBufferReader;
import it.giacomobergami.filemaps.formats.interfaces.IWriteSerializable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;

/**
 * Implements an iterator over the serializable content through our raw interface
 */
public class BufferReaderIterator<T extends IWriteSerializable<T>> implements Iterator<T> {

    private final IBufferReader buff;
    private final long maxBufferSize;
    private final long maxTSize;
    private long currentPointer;
    T check;

    public BufferReaderIterator(Class<T> clazz, IBufferReader buff) {
        this.buff = buff;
        check = null;
        try {
            check = clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        this.maxBufferSize = buff.getSize();
        this.maxTSize = check==null ? 0 : check.getStorageSize();
        currentPointer = 0L;
    }

    public void reset() {
        currentPointer = 0L;
    }

    @Override
    public boolean hasNext() {
        return Long.compareUnsigned(currentPointer,maxBufferSize)<0;
    }

    @Override
    public T next() {
        T toret = (T)check.unserialize(buff,currentPointer);
        currentPointer+=maxTSize;
        return toret;
    }
}
