package it.giacomobergami.relational;

import org.hibernate.annotations.Entity;

import javax.persistence.SequenceGenerator;

/**
 * Created by vasistas on 07/09/16.
 */
@Entity
@SequenceGenerator(initialValue = 0, name = "idgen", sequenceName = "entityaseq")
public class Left implements Vertex {
    public int getId1() {
        return Id1;
    }

    public void setId1(int id1) {
        Id1 = id1;
    }

    private int Id1;
    private String Ip1;
    private String Organization1;
    private String Year1;

    public Left() {}

    public Left(String ip1, String organization1, String year1) {
        Ip1 = ip1;
        Organization1 = organization1;
        Year1 = year1;
    }


    public String getIp1() {
        return Ip1;
    }

    public void setIp1(String ip1) {
        Ip1 = ip1;
    }

    public String getOrganization1() {
        return Organization1;
    }

    public void setOrganization1(String organization1) {
        Organization1 = organization1;
    }

    public String getYear1() {
        return Year1;
    }

    public void setYear1(String year1) {
        Year1 = year1;
    }


    @Override
    public int id() {
        return Id1;
    }

    @Override
    public String Ip() {
        return Ip1;
    }

    @Override
    public String Organization() {
        return Organization1;
    }

    @Override
    public String Year() {
        return Year1;
    }
}
