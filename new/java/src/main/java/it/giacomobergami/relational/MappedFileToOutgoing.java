package it.giacomobergami.relational;

import it.giacomobergami.filemaps.OffsetIterator;
import it.giacomobergami.filemaps.VertexEntry;
import it.giacomobergami.filemaps.buffers.BufferArray;
import it.giacomobergami.filemaps.buffers.BufferPointer;
import it.giacomobergami.filemaps.buffers.LongMMFile;
import it.giacomobergami.filemaps.formats.EdgesOUTH;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by vasistas on 07/09/16.
 */
public class MappedFileToOutgoing implements Iterator<Iterator<Integer>>, AutoCloseable {

    OffsetIterator<VertexEntry> i;
    LongMMFile VALeft;
    private final boolean isLeft;

    public static class IteratorWithId implements Iterator<Integer> {
        public Iterator<Integer> it;
        public int id;

        public IteratorWithId(Iterator<Integer> it, int id) {
            this.it = it;
            this.id = id;
        }

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public Integer next() {
            return it.next();
        }
    }

    public static String fromPointer(BufferPointer ptr) {
        return new String(ptr.memory, ptr.offsetBegin, ptr.length);
    }


    public MappedFileToOutgoing(File pathLeft, boolean isLeft) throws IOException {
        this.isLeft = isLeft;
        VALeft = new LongMMFile(new File(pathLeft, "VAOffset.bin"));
        i = new OffsetIterator<>(VertexEntry.class, VALeft, 0, VALeft.getSize());
    }

    @Override
    public boolean hasNext() {
        return i.hasNext();
    }

    @Override
    public IteratorWithId next() {
        VertexEntry curr = i.next();
        Iterator<Integer> it = new Iterator<Integer>() {
            Iterator<EdgesOUTH> it = curr.outgoing.iterator();

            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public Integer next() {
                return it.next().adjacentID;
            }
        };
        return new IteratorWithId(it,curr.vertexHead.id);
    }

    @Override
    public void close() throws Exception {
        VALeft.close();
    }
}
