package it.giacomobergami.relational;

/**
 * Created by vasistas on 07/09/16.
 */
public interface Vertex {
    int id();
    String Ip();
    String Organization();
    String Year();
}
