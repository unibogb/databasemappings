package it.giacomobergami.relational;

/**
 * Created by vasistas on 07/09/16.
 */
public interface Edge<T extends Vertex> {
    T getSrc();
    void setSrc(T src);
    T getDst();
    void setDst(T dst);
    int id();
}
