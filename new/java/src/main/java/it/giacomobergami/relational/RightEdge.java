package it.giacomobergami.relational;

/**
 * Created by vasistas on 07/09/16.
 */
public class RightEdge implements Edge<Right> {

    public RightEdge() {
    }

    @Override
    public Right getSrc() {
        return src;
    }

    @Override
    public void setSrc(Right src) {
        this.src = src;
    }

    @Override
    public Right getDst() {
        return dst;
    }

    @Override
    public void setDst(Right dst) {
        this.dst = dst;
    }

    @Override
    public int id() {
        return righteid;
    }

    public int getRighteid() {
        return righteid;
    }

    public void setRighteid(int lefteid) {
        this.righteid = lefteid;
    }

    private int righteid;
    private Right src;
    private Right dst;

    public RightEdge(Right src, Right dst) {
        this.src = src;
        this.dst = dst;
    }
}
