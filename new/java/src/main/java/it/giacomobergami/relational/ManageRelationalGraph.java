package it.giacomobergami.relational;

import org.hibernate.JDBCException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import java.io.File;
import java.util.Iterator;

/**
 * Created by vasistas on 07/09/16.
 */
public class ManageRelationalGraph {

    private static ManageRelationalGraph self;
    private static SessionFactory factory = null;
    private ManageRelationalGraph() {
        factory = HibernateFactory.createSessionFactory();
    }
    public static ManageRelationalGraph manager() {
        if (self==null) {
            self = new ManageRelationalGraph();
        }
        return self;
    }

    public static Left getLeftVertexByIdInDB(int id) {
        Session session = factory.openSession();
        Left v =  (Left) session.get(Left.class, id+1);
        return v;
    }

    public static Right getRightVertexByIdInDB(int id) {
        Session session = factory.openSession();
        Right v =  (Right) session.get(Right.class, id+1);
        return v;
    }

    public double loadOperator(String basPath, boolean isLeft, int pace) {
        double toret = 0;
        try(MappedFileToVertices mf = new MappedFileToVertices(new File(basPath,(isLeft ? "left" : "right")+"/"+pace),isLeft)) {
            Session session =factory.openSession();
            Transaction tx = session.beginTransaction();
            double d = 0;
            int count = 0;
            while (mf.hasNext()) {
                if (isLeft) {
                    Left vertex = (Left) mf.next();
                    d = System.currentTimeMillis();
                    session.save(vertex);
                    toret += (System.currentTimeMillis() - d);
                } else {
                    Right vertex = (Right) mf.next();
                    d = System.currentTimeMillis();
                    session.save(vertex);
                    toret += (System.currentTimeMillis() - d);
                }
                count++;
                if (count%10000 ==0) {
                    session.flush();
                    session.clear();
                    count = 0;
                }
            }
            d = System.currentTimeMillis();
            tx.commit();
            toret += (System.currentTimeMillis() - d);
            session.close();
            return toret;
        } catch (JDBCException jdbce) {
            jdbce.printStackTrace();
            jdbce.getSQLException().getNextException().printStackTrace();
            System.exit(1);
            return -1;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }


    public double createEdges(String basPath, boolean isLeft, int pace) {
        double toret = 0;
        try(MappedFileToOutgoing mf = new MappedFileToOutgoing(new File(basPath,(isLeft ? "left" : "right")+"/"+pace),isLeft)) {
            Session session =factory.openSession();
            Transaction tx = session.beginTransaction();
            double d = 0;
            int count = 0;
            while (mf.hasNext()) {
                MappedFileToOutgoing.IteratorWithId v = mf.next();
                if (isLeft) {
                    while (v.hasNext()) {
                        LeftEdge le = new LeftEdge(getLeftVertexByIdInDB(v.id),getLeftVertexByIdInDB(v.next()));
                        d = System.currentTimeMillis();
                        session.save(le);
                        toret += (System.currentTimeMillis() - d);
                        count++;
                        if (count%10000 ==0) {
                            session.flush();
                            session.clear();
                            count = 0;
                        }
                    }
                } else {
                    while (v.hasNext()) {
                        RightEdge re = new RightEdge(getRightVertexByIdInDB(v.id), getRightVertexByIdInDB(v.next()));
                        d = System.currentTimeMillis();
                        session.save(re);
                        toret += (System.currentTimeMillis() - d);
                        count++;
                        if (count%10000 ==0) {
                            session.flush();
                            session.clear();
                            count = 0;
                        }
                    }
                }
            }
            d = System.currentTimeMillis();
            tx.commit();
            toret += (System.currentTimeMillis() - d);
            session.close();
            return toret;
        } catch (JDBCException jdbce) {
            jdbce.printStackTrace();
            jdbce.getSQLException().getNextException().printStackTrace();
            System.exit(1);
            return -1;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
