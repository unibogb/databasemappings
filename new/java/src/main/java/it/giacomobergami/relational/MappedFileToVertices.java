package it.giacomobergami.relational;

import it.giacomobergami.filemaps.OffsetIterator;
import it.giacomobergami.filemaps.VertexEntry;
import it.giacomobergami.filemaps.buffers.BufferPointer;
import it.giacomobergami.filemaps.buffers.LongMMFile;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by vasistas on 07/09/16.
 */
public class MappedFileToVertices implements Iterator<Vertex>, AutoCloseable {

    OffsetIterator<VertexEntry> i;
    LongMMFile VALeft;
    private final boolean isLeft;

    public static String fromPointer(BufferPointer ptr) {
        return new String(ptr.memory, ptr.offsetBegin, ptr.length);
    }
    public Vertex asVertex(VertexEntry ve) {
        String ip, org, year;
        ip = fromPointer(ve.getString(0));
        org =  fromPointer(ve.getString(1));
        year =  fromPointer(ve.getString(2));
        return isLeft ? new Left(ip,org,year) : new Right(ip,org,year);
    }

    public MappedFileToVertices(File pathLeft, boolean isLeft) throws IOException {
        this.isLeft = isLeft;
        VALeft = new LongMMFile(new File(pathLeft, "VAOffset.bin"));
        i = new OffsetIterator<>(VertexEntry.class, VALeft, 0, VALeft.getSize());
    }

    @Override
    public boolean hasNext() {
        return i.hasNext();
    }

    @Override
    public Vertex next() {
        return asVertex(i.next());
    }

    @Override
    public void close() throws Exception {
        VALeft.close();
    }
}
