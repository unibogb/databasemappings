package it.giacomobergami.relational;

import org.hibernate.annotations.Entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by vasistas on 07/09/16.
 */
@Entity
public class Right implements Vertex {
    public Right(String ip2, String organization2, String year2) {
        Ip2 = ip2;
        Organization2 = organization2;
        Year2 = year2;
    }

    public Right() {
    }

    public int getId2() {
        return Id2;
    }

    public void setId2(int id2) {
        Id2 = id2;
    }

    public String getIp2() {
        return Ip2;
    }

    public void setIp2(String ip2) {
        Ip2 = ip2;
    }

    public String getOrganization2() {
        return Organization2;
    }

    public void setOrganization2(String organization2) {
        Organization2 = organization2;
    }

    public String getYear2() {
        return Year2;
    }

    public void setYear2(String year2) {
        Year2 = year2;
    }

    private int Id2;

    private String Ip2;

    private String Organization2;

    private String Year2;


    @Override
    public int id() {
        return Id2;
    }

    @Override
    public String Ip() {
        return Ip2;
    }

    @Override
    public String Organization() {
        return Organization2;
    }

    @Override
    public String Year() {
        return Year2;
    }

}
