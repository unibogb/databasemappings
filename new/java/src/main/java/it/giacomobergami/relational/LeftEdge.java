package it.giacomobergami.relational;

/**
 * Created by vasistas on 07/09/16.
 */
public class LeftEdge implements Edge<Left> {

    public LeftEdge() {
    }

    @Override
    public Left getSrc() {
        return src;
    }

    @Override
    public void setSrc(Left src) {
        this.src = src;
    }

    @Override
    public Left getDst() {
        return dst;
    }

    @Override
    public void setDst(Left dst) {
        this.dst = dst;
    }

    @Override
    public int id() {
        return lefteid;
    }

    public int getLefteid() {
        return lefteid;
    }

    public void setLefteid(int lefteid) {
        this.lefteid = lefteid;
    }

    private int lefteid;
    private Left src;
    private Left dst;

    public LeftEdge(Left src, Left dst) {
        this.src = src;
        this.dst = dst;
    }
}
