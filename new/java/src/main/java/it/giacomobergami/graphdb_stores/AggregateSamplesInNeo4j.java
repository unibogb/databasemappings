/*
 * AggregateSamplesInNeo4j.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */

package it.giacomobergami.graphdb_stores;


import it.giacomobergami.filemaps.VertexEntry;
import it.giacomobergami.filemaps.buffers.BufferArray;
import it.giacomobergami.filemaps.buffers.BufferPointer;
import it.giacomobergami.filemaps.buffers.LongMMFile;
import it.giacomobergami.filemaps.formats.EdgesOUTH;
import it.giacomobergami.filemaps.formats.IndexHashPos;
import it.giacomobergami.graphdb_stores.Neo4J.Neo4JGraph;
import it.giacomobergami.graphdb_stores.general.AbstractGraph;
import it.giacomobergami.graphdb_stores.general.AbstractNode;
import it.giacomobergami.graphdb_stores.general.GoogleGraphVertex;
import it.giacomobergami.graphdb_stores.pointers.GraphIdPointerLong;
import it.giacomobergami.graphdb_stores.pointers.Pointer;
import it.giacomobergami.utils.I;
import it.giacomobergami.utils.datastructures.iterators.MapIterator;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Stores the left and right operands that have been sampled in the raw format in Neo4J
 */
public class AggregateSamplesInNeo4j {

    //public static final BSONHolder4 unserializer = new BSONHolder4();
    public static final HashSet<String> blankSet = new HashSet<>();
    public static final Pointer ptr = new GraphIdPointerLong(0L);

    /**
     * Returns the node as a vertex
     * @param ve
     * @param inTransaction
     * @return
     */
    public static AbstractNode getOrCreateVertex(VertexEntry ve, AbstractGraph inTransaction, int side, int hop) {
        //BSONHolder4 bulkVertexL = unserializer.unserialize(file,left.pos);
        GoogleGraphVertex vL = new GoogleGraphVertex("User",blankSet,ptr);
        vL.setAttribute("UID",(ve.vertexHead.id+hop)+"");
        vL.setAttribute("graph",side==1 ? "L" : "R");
        BufferPointer b = ve.getString(0);
        new String(b.memory,b.offsetBegin,b.length);
        vL.setAttribute("Ip"+side,new String(b.memory,b.offsetBegin,b.length));
        b = ve.getString(1);
        vL.setAttribute("Organization"+side,new String(b.memory,b.offsetBegin,b.length));
        b = ve.getString(2);
        vL.setAttribute("Year"+side,new String(b.memory,b.offsetBegin,b.length));
        return inTransaction.addVertex(vL);
    }

    public static void delete(File file)
            throws IOException{

        if(file.isDirectory()){

            //directory is empty, then delete it
            if(file.list().length==0){

                file.delete();

            }else{

                //list all the directory contents
                String files[] = file.list();

                for (String temp : files) {
                    //construct the file structure
                    File fileDelete = new File(file, temp);

                    //recursive delete
                    delete(fileDelete);
                }

                //check the directory again, if empty then delete it
                if(file.list().length==0){
                    file.delete();
                }
            }

        }else{
            //if file, then delete it
            file.delete();
        }
    }

    public static void main(String args[]) throws IOException {
        //Paths p = Paths.getInstance();
        int sizes[] = new int[]{2,5,10,100,1000,10000,100000,1000000};
        for (int pos=6; pos<7; pos++) {
            File torem = new File(""+(pos));
            if (torem.exists()) {
                delete(torem);
            }
            System.out.println("Storing graph i-th:"+pos);
            File pathLeft = new File("/Users/vasistas/cgraph2/data/left/"+sizes[pos]);
            File pathRight = new File("/Users/vasistas/cgraph2/data/right/"+sizes[pos]);
            Map<Set<String>,Set<String>> vertexLabelToAttributesToIndex = new HashMap<>(1);
            Set<String> type = new HashSet<>(1);
            type.add("User");
            Set<String> hashing = new HashSet<>(4);
            hashing.add("Organization1");
            hashing.add("Organization2");
            hashing.add("Year1");
            hashing.add("Year2");
            vertexLabelToAttributesToIndex.put(type,hashing);
            long t = System.currentTimeMillis();
            Neo4JGraph g = new Neo4JGraph("UID","r",false,vertexLabelToAttributesToIndex,(pos)+""); // graphs are automatically stored in the current directory, with increasing numbers
            t = System.currentTimeMillis() - t;
            //BSONHolder4 unserializer = new BSONHolder4();

            LongMMFile vLeft = new LongMMFile(new File(pathLeft, "VAOffset.bin")),
                    vRight = new LongMMFile(new File(pathRight, "VAOffset.bin"));

            LongMMFile lmfLeft = new LongMMFile(new File(pathLeft,"Index.bin")),
                    lmfRight = new LongMMFile(new File(pathRight,"Index.bin"));

            BufferArray<IndexHashPos> baLeft = new BufferArray<>(IndexHashPos.class,lmfLeft),
                    baRight = new BufferArray<>(IndexHashPos.class,lmfRight);

            long tt = System.currentTimeMillis();
            int max = g.startTransaction(() -> {
                int maxUID = -1;
                for (IndexHashPos left : baLeft) {
                    IndexHashPos ptr2 = baRight.get(left.index);
                    VertexEntry bulkVertexL = new VertexEntry(vRight,ptr2.pos);
                    //BSONHolder4 bulkVertexL = unserializer.unserialize(vLeft,left.pos);
                    //System.out.println("id="+left.index+" idB="+bulkVertexL.id);
                    AbstractNode anLeft = getOrCreateVertex(bulkVertexL,g,1,0);
                    if (maxUID<left.index) maxUID = left.index;
                    // Allocating all the neighbours in order not to scan the file coninuously
                    Iterable<AbstractNode> Nu = new I<>(new MapIterator<EdgesOUTH,AbstractNode>(bulkVertexL.outgoing.iterator()) {
                        @Override
                        public AbstractNode apply(EdgesOUTH o) {
                            IndexHashPos ptr = baLeft.get(o.adjacentID);
                            VertexEntry ptrbulk = new VertexEntry(vLeft,ptr.pos);
                            return getOrCreateVertex(ptrbulk,g,1,0);
                        }
                    });
                    for (AbstractNode anLeftp : Nu) {
                        g.addEdge(anLeft,anLeftp);
                    }
                }
                return maxUID;
            }).get();
            g.startTransaction(()-> {
                for (IndexHashPos right : baRight) {
                    IndexHashPos ptr2 = baRight.get(right.index);
                    VertexEntry bulkVertexR = new VertexEntry(vRight,ptr2.pos);
                    //BSONHolder4 bulkVertexR = unserializer.unserialize(vRight,right.pos);
                    AbstractNode anRight = getOrCreateVertex(bulkVertexR,g,2,max);
                    // Allocating all the neighbours in order not to scan the file coninuously
                    Iterable<AbstractNode> Nu = new I<>(new MapIterator<EdgesOUTH,AbstractNode>(bulkVertexR.outgoing.iterator()) {
                        @Override
                        public AbstractNode apply(EdgesOUTH o) {
                            IndexHashPos ptr = baRight.get(o.adjacentID);
                            VertexEntry ptrbulk = new VertexEntry(vRight,ptr.pos);
                            return getOrCreateVertex(ptrbulk,g,1,0);
                        }
                    });
                    for (AbstractNode anRightp : Nu) {
                        g.addEdge(anRight,anRightp);
                    }
                }
                return 1;
            });
            tt = System.currentTimeMillis() - tt;
            g.close();
            System.out.println(pos+" in "+tt);
        }
    }

}
