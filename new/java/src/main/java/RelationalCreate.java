import it.giacomobergami.filemaps.BufferReaderIterator;
import it.giacomobergami.filemaps.OffsetIterator;
import it.giacomobergami.filemaps.VertexEntry;
import it.giacomobergami.filemaps.buffers.BufferPointer;
import it.giacomobergami.filemaps.buffers.LongMMFile;
import it.giacomobergami.filemaps.formats.EdgesOUTH;
import it.giacomobergami.filemaps.formats.HashListEntryFormat;
import it.giacomobergami.relational.ManageRelationalGraph;

import java.io.File;
import java.io.IOException;

/**
 * Created by vasistas on 07/09/16.
 */
public class RelationalCreate {
    public static void main(String args[]) throws IOException {
        ManageRelationalGraph m = ManageRelationalGraph.manager();
        String basePath = "/Users/vasistas/cgraph2/data/";
        int pace = 4000000;
        double creatTime = 0;
        creatTime += m.loadOperator(basePath,true,pace);
        System.out.println("IntTime1="+creatTime);
        creatTime += m.loadOperator(basePath,false,pace);
        System.out.println("IntTime2="+creatTime);
        creatTime += m.createEdges(basePath,true,pace);
        System.out.println("IntTime3="+creatTime);
        creatTime += m.createEdges(basePath,false,pace);
        System.out.println(pace+"="+creatTime);
    }
}
