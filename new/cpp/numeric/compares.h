//
// Created by Giacomo Bergami on 18/08/16.
//

#pragma once

#ifdef __cplusplus
extern "C" {
#endif
void compareU_Int(unsigned int left, unsigned int right, int *toret);
void  compareUL_Int(unsigned long, unsigned long, int *);
#ifdef __cplusplus
}
#endif

