//
// Created by Giacomo Bergami on 20/08/16.
//

unsigned long dovetail(unsigned long left, unsigned long right) {
    unsigned long c = left+right;
    if (c%2 == 0) {
        c = c/2;
        c = c*(left+right+1)+left;
        return c;
    } else {
        c = (c+1)/2;
        c = c*(left+right)+left;
        return c;
    }
}