//
// Created by Giacomo Bergami on 20/08/16.
//

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

unsigned long dovetail(unsigned long left, unsigned long right);

#ifdef __cplusplus
};
#endif