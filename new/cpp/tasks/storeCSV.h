//
// Created by Giacomo Bergami on 16/08/16.
//

#pragma once

#include "csv.h"

void storeCSV(std::string& valueCSVFile,
              std::string& edgeFile,
              unsigned int maxSize,
              int numHashes,
              int *pos);

#ifdef __cplusplus
extern "C" {
#endif
void compareU_Int(unsigned int left, unsigned int right, int *toret);
#ifdef __cplusplus
}
#endif

