//
// Created by Giacomo Bergami on 28/08/16.
//

#include "graph_sparksee.h"
#include "../../../fromJava/System.h"

graph_sparksee::graph_sparksee(std::string acquaio) {
    db = Init::sparksee->Open(System::ws(acquaio+".snap"),true);
    sess = db->NewSession();
    graph = sess->GetGraph();
    sparksee::gdb::TypeList *edges = graph->FindEdgeTypes();
    sparksee::gdb::TypeListIterator *iteratore = edges->Iterator();
    while (iteratore->HasNext()) {
        edgeType = iteratore->Next();
        break;
    }
    delete iteratore;
    delete edges;
}

graph_sparksee::graph_sparksee(const graph_sparksee &cpy) {
    db = cpy.db;
    sess = cpy.sess;
    graph = cpy.graph;
}

sparksee::gdb::Graph *graph_sparksee::operator()() {
    return graph;
}


sparksee::gdb::Objects *graph_sparksee::newObjects() {
    return sess->NewObjects();
}

graph_sparksee::~graph_sparksee() {
    delete graph;
    delete sess;
    delete db;
}

graph_sparksee::graph_sparksee() {
    db = nullptr;
    sess = nullptr;
    graph = nullptr;
}

void graph_sparksee::init(std::string acquaio) {
    //std::cerr << acquaio.substr(0,acquaio.size()-1)+".snap" << std::endl;
    db = Init::sparksee->Open(System::ws(acquaio.substr(0,acquaio.size()-1)+".snap"),true);
    sess = db->NewSession();
    graph = sess->GetGraph();
    sparksee::gdb::TypeList *edges = graph->FindEdgeTypes();
    sparksee::gdb::TypeListIterator *iteratore = edges->Iterator();
    while (iteratore->HasNext()) {
        edgeType = iteratore->Next();
        break;
    }
    delete iteratore;
    delete edges;
}


