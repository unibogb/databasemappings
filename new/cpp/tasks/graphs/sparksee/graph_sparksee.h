//
// Created by Giacomo Bergami on 28/08/16.
//

#pragma once

#include <gdb/Query.h>
#include <gdb/Sparksee.h>
#include <gdb/Database.h>
#include <gdb/Session.h>
#include <gdb/Graph.h>
#include <string>
#include "../../generalization/config/Init.h"

class graph_sparksee {
    sparksee::gdb::Database * db;
    sparksee::gdb::Session *sess;
    sparksee::gdb::Graph *graph;
public:
    graph_sparksee();
    graph_sparksee(std::string acquaio);
    graph_sparksee(const graph_sparksee& cpy);


    void init(std::string acquaio);
    sparksee::gdb::Graph* operator()();

    sparksee::gdb::Objects* newObjects();

    ~graph_sparksee();

    sparksee::gdb::type_t edgeType;
};