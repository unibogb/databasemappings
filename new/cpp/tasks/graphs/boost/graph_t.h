//
// Created by Giacomo Bergami on 28/08/16.
//

#pragma once
#include "VFull.h"

//Defining the graph data structure. Using the vecS specification
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,VFull> graph_t;
typedef boost::graph_traits<graph_t>::vertex_descriptor result_vertex_descriptor;
typedef boost::graph_traits<graph_t>::edge_descriptor result_edge_descriptor;