//
// Created by Giacomo Bergami on 28/08/16.
//

#pragma once
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/directed_graph.hpp>
#include <boost/graph/adj_list_serialize.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/binary_object.hpp>

class VFull {
public:
    VFull();
    unsigned int id, hash, year;
    std::string ip, organization;
    VFull(unsigned int id, unsigned int hash, unsigned int year, std::string ip, std::string organization);


    template<class Archive> void serialize(Archive &ar, const unsigned int version) {
        ar &  id;
        ar & hash;
        ar & ip;
        ar & organization;
        ar & year;
    }

};

