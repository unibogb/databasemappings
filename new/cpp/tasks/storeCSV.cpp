//
// Created by Giacomo Bergami on 16/08/16.
//

#define _GNU_SOURCE
#define  _SVID_SOURCE
#define _BSD_SOURCE


#include <iostream>
#include <vector>
#include <algorithm>

#include "storeCSV.h"
#include "../rbtree/rbtree.h"
#include "../fromJava/javaUtils.h"
#include "../io/serializers.h"
#include "../numeric/compares.h"
#include "../random/RandomFileLine.h"


#include <map>
#include <fstream>
#include <set>
#include <sstream>
#include <array>
#include <sys/fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <unordered_map>

unsigned int doHash3(std::vector<std::string>& stringarray, int numHashes, int* pos) {
    unsigned int prime = 17;
    unsigned int result = 3;
    int i = 0;
    int c = 0;
    char* tok;
    for (int i=0; i<numHashes; i++) {
        result = result * prime + hashCode((char*)stringarray.at(i).c_str());
    }
    return result;
}

unsigned int doHash2(std::vector<char*>& stringarray, int numHashes, int* pos) {
    unsigned int prime = 17;
    unsigned int result = 3;
    int i = 0;
    int c = 0;
    char* tok;
    for (int i=0; i<numHashes; i++) {
        result = result * prime + hashCode(stringarray.at(i));
    }
    return result;
}

class entry {
public:
    unsigned int id;
    unsigned int hash;
    unsigned long offsetInOutgoingFile;
    bool hasFoundMatch;
    entry(unsigned int i, unsigned int h, unsigned long o, bool hasMatch) : id{i}, hash{h}, offsetInOutgoingFile{o}, hasFoundMatch{hasMatch} {}
    entry(const entry& cp) {
        id = cp.id;
        hash = cp.hash;
        if (hash==0) {
            std::cerr << id << std::endl;
            exit(1);
        }
        offsetInOutgoingFile = cp.offsetInOutgoingFile;
        hasFoundMatch = cp.hasFoundMatch;
    };
    entry() {};
} ;

void storeCSV(std::string& valueCSVFile,
              std::string& edgeFile,
              unsigned int maxSize,
              int numHashes,
              int *pos) {

    //std::ifstream outgoingFile(edgeFile);
    std::string line;
    clock_t start;

    RandomFileLine rfl_ip{valueCSVFile+"ip.txt",true};
    RandomFileLine rfl_org{valueCSVFile+"all_companies.txt",true};
    RandomFileLine rfl_out{edgeFile};

    RBTree<unsigned int,entry> inTree{&compareU_Int};
    std::unordered_map<unsigned int,unsigned int> vertexToHash;
    unsigned int inSrc = 0, prevSrc = 0, inDst = 0;
    unsigned int i = 0;

    //Getting the line zero
    unsigned long offsetInOutgoingFile = 0, prevOffset = 0;
    line = rfl_out.getLine(0);
    {
        std::istringstream ss(line);
        ss >> inSrc >> inDst;
    }

    //Reading the values
    for (unsigned int i = 0; i<maxSize; i++){
        if (i % 1000 == 0) std::cout << i<< std::endl;
        //I have to copy and allocate the elements, since the std::string have
        //A fixed memory that will be overwritten at the next step
        std::vector<std::string> valueArray;
        valueArray.push_back(rfl_ip.getRandomLine(i));
        valueArray.push_back(rfl_ip.getRandomLine(i));
        valueArray.push_back(rfl_ip.getRandomLine(i));
        unsigned int h = doHash3(valueArray,numHashes,pos);//2331829
        vertexToHash[i] = h;
        bool hasEndOut = false;
        if (i==inSrc) {
            inTree.insertKey(h)->add(i,h,offsetInOutgoingFile,true);
            while ((i== inSrc)&&(!hasEndOut)) {
                //e->outEdges.emplace_back(0,inDst); // I do not have all the hashes per ID yet.
                hasEndOut = !rfl_out.hasNext();
                if (!hasEndOut) {
                    long getCurrOffset = rfl_out.getCurrentFilePos();
                    line = rfl_out.readNext();
                    std::istringstream ss(line);
                    prevSrc = inSrc;
                    ss >> inSrc >> inDst;
                    if (i!=inSrc) {
                        prevOffset = offsetInOutgoingFile;
                        offsetInOutgoingFile = getCurrOffset;
                    }
                }
            }
        } else {
            inTree.insertKey(h)->add(i,h,0,false);
        }
        //TODO: XXX
        /*bool hasEndOut = false;
        while ((i== inSrc)&&(!hasEndOut)) {
            e->outEdges.emplace_back(0,inDst); // I do not have all the hashes per ID yet.
            hasEndOut = !getline(outgoingFile, line);
            if (!hasEndOut) {
                std::istringstream ss(line);
                prevSrc = inSrc;
                ss >> inSrc >> inDst;
            }
        }*/

        //e->hash = doHash2(valueArray,numHashes,pos);
        //e->outEdges.insert(e->outEdges.begin(),e->outEdges.size());
        //hasEndOut = false;
        /*while ((i == outDst)&&(!hasEndOut)) {
            e->inEdges.emplace_back(0,outSrc);
            hasEndOut = !getline(ingoingFile,iLine);
            if (!hasEndOut) {
                std::istringstream ss(iLine);
                prevDst = outDst;
                ss >> outDst >> outSrc;
            }
        }*/
        //e->inEdges.emplace_back()
        //e->inEdges.insert(e->inEdges.begin(),e->inEdges.size());

        //e->id = i;
        ////inTree.insertKey(e->hash)->add(e);
        //vertexToHash[e->id] = e;
        /*if (e->id == 271892)
            std::cout << "BReak" << std::endl;*/

    }


    FILE *h = fopen("/Users/vasistas/cgraph2/Hash.bin","w");
    FILE *b = fopen("/Users/vasistas/cgraph2/VAOffset.bin","w");
    FILE *j = fopen("/Users/vasistas/cgraph2/Index.bin","w");
    unsigned long VAOffset = 0;
    unsigned long HashOffset = 0;
    unsigned int cnt = (unsigned int) 0;
    clock_t  c = std::clock();
    RBTree<unsigned int, entry>::rbiterator it = inTree.iterator();
    while (it.hasNext()) {
        cnt++;
        if (cnt % 10000 == 1) std::cout << cnt << "-th Hash" << std::endl;
        Node<unsigned int, entry> *ptr = it.next();
        unsigned int hash = it.getCurrentK();
        if (hash==0) {
            std::cerr << hash << " at pos " << cnt;
            exit(2);
        }

        serialize_hash(h,hash,HashOffset);
        for (const entry& vId : ptr->overflowList) {
            i = vId.id;
            std::vector<EDGES_OUTIN> out;
            std::string ip = rfl_ip.getRandomLine(i);
            std::string org = rfl_org.getRandomLine(i);
            int year = (RanHash::doub(i)*37)+1970;
            if (vId.hasFoundMatch) {
                line = rfl_out.setCurrentFilePos(vId.offsetInOutgoingFile);
                {
                    std::istringstream ss(line);
                    ss >> inSrc >> inDst;
                }
                bool hasEndOut = false;
                if (i!=inSrc) {
                    assert(i == inSrc);
                }
                while ((i== inSrc)&&(!hasEndOut)) {

                    out.emplace_back(vertexToHash[inDst],inDst);
                    //e->outEdges.emplace_back(0,inDst); // I do not have all the hashes per ID yet.
                    hasEndOut = !rfl_out.hasNext();
                    offsetInOutgoingFile++;
                    if (!hasEndOut) {
                        line = rfl_out.readNext();
                        std::istringstream ss(line);
                        prevSrc = inSrc;
                        ss >> inSrc >> inDst;
                    }
                }
                std::sort(out.begin(),out.end());
            }

            char *ipArray, *orgArray, *yearArray;
            asprintf(&ipArray,"%s",ip.data());
            asprintf(&orgArray,"%s",org.data());
            asprintf(&yearArray,"%d",year);
            std::vector<char*> valueArray;
            valueArray.push_back(ipArray);
            valueArray.push_back(orgArray);
            valueArray.push_back(yearArray);
            unsigned long thiOffset = VAOffset;
            serialize_vertex_values(b, &VAOffset, &HashOffset, i, 3, valueArray, out);
            free(ipArray);
            free(orgArray);
            free(yearArray);
            serialize_vertex_id(j,i,hash,thiOffset);
        }
    }

    c = clock() - c;
    double d = ((double)c) / CLOCKS_PER_SEC;
    fclose(b);
    fclose(h);
    fclose(j);
    std::cout << d << std::endl;
    //THROWS an error:: free(vertices);

}