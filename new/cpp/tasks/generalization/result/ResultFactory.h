//
// Created by Giacomo Bergami on 20/08/16.
//

#pragma once

#include <string>
#include "resultActions.h"

class ResultFactory {
public:
    static resultActions* generateInstance(std::string name, std::string path);
};
