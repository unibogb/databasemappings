//
// Created by Giacomo Bergami on 20/08/16.
//

#include "ResultFactory.h"
#include "concrete/resultProposed.h"
#include "concrete/resultSnap.h"
#include "concrete/resultSparksee.h"
#include "concrete/resultBoost.h"
#include "concrete/resultMimic.h"

resultActions *ResultFactory::generateInstance(std::string name, std::string path) {
    if (!name.compare("proposed")) {
        return new resultProposed(path);
    } else if (!name.compare("snap")) {
        return new resultSnap(path);
    } else if (!name.compare("sparksee")) {
        return new resultSparksee(path);
    } else if (!name.compare("boost")) {
        return new resultBoost(path);
    } else if (!name.compare("mimic")) {
        return new resultMimic();
    } return nullptr;
}
