//
// Created by Giacomo Bergami on 29/08/16.
//

#include "resultMimic.h"

void resultMimic::storeVertex(unsigned int left, unsigned int right) {}

void resultMimic::storeVertexInternal(unsigned int left, unsigned int right) {}

void resultMimic::storeEdge(unsigned int leftSrc, unsigned int rightSrc, unsigned int leftDst, unsigned int rightDst) {}

clock_t resultMimic::save() { return 0; }
