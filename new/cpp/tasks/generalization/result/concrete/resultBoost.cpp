//
// Created by Giacomo Bergami on 24/08/16.
//

#include "resultBoost.h"


resultBoost::resultBoost(std::string n) : name{n}, file{n, std::ios_base::binary}{
}

void resultBoost::storeVertex(unsigned int left, unsigned int right) {
    clock_t  t = clock();
    unsigned long dt = dovetail(left,right);
    const std::unordered_map<unsigned long, unsigned long>::iterator &it = id_to_dovetail.find(dovetail(left, right));
    if (id_to_dovetail.find(dt)==id_to_dovetail.end()){
        VResult v{left,right};
        outer = boost::add_vertex(v,g);
        id_to_dovetail[dt] = outer;
    } else outer = it->first;
    time += (clock()-t);
}

void resultBoost::storeVertexInternal(unsigned int left, unsigned int right) {
    clock_t  t = clock();
    unsigned long dt = dovetail(left,right);
    const std::unordered_map<unsigned long, unsigned long>::iterator &it = id_to_dovetail.find(dovetail(left, right));
    if (id_to_dovetail.find(dt)==id_to_dovetail.end()){
        VResult v{left,right};
        inner = boost::add_vertex(v,g);
        id_to_dovetail[dt] = inner;
    } else inner = it->first;
    time += (clock()-t);
}

void resultBoost::storeEdge(unsigned int leftSrc, unsigned int rightSrc, unsigned int leftDst, unsigned int rightDst) {
    clock_t  t = clock();
    storeVertexInternal(leftDst,rightDst);
    boost::add_edge(outer,inner,g);
    time += (clock()-t);
}

clock_t resultBoost::save() {
    clock_t  t = clock();
    boost::archive::binary_oarchive toStore{file};
    toStore << g;
    time += (clock()-t);
    file.close();
    return time;
}
