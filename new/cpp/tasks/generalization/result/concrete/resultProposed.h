//
// Created by Giacomo Bergami on 20/08/16.
//

#pragma once

#include "../resultActions.h"
#include <string>

class resultProposed : public resultActions {
    FILE* result;
    clock_t cont = 0;

public:
    resultProposed(std::string resultFileName);
    void storeVertexInternal(unsigned int left, unsigned int right);
    void storeVertex(unsigned int left, unsigned int right);
    /**
     * Given my algorithm, with this implementation I only store the outgoing element
     * @param leftSrc
     * @param rightSrc
     * @param leftDst
     * @param rightDst
     */
    void storeEdge(unsigned int leftSrc, unsigned int rightSrc, unsigned int leftDst, unsigned int rightDst);
    clock_t save();
};

