//
// Created by Giacomo Bergami on 24/08/16.
//

#pragma once


#include <ctime>
#include <string>


#include "../resultActions.h"
#include <gdb/Sparksee.h>
#include <gdb/Database.h>
#include <gdb/Session.h>
#include <gdb/Graph.h>

class resultSparksee : public resultActions {
    sparksee::gdb::Database * db;
    sparksee::gdb::Session *sess;
    sparksee::gdb::Graph *graph;
    sparksee::gdb::type_t vertex, edge;
    sparksee::gdb::oid_t currentOuter, currentInner;
    int dovetailAttribute;
    clock_t  t;
public:
    resultSparksee(std::string name);
    void storeVertex(unsigned int left, unsigned int right);;
    void storeVertexInternal(unsigned int left, unsigned int right);;
    void storeEdge(unsigned int leftSrc, unsigned int rightSrc, unsigned int leftDst, unsigned int rightDst);;
    clock_t save();;
    ~resultSparksee();
};