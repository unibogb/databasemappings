//
// Created by Giacomo Bergami on 24/08/16.
//

#include <ctime>
#include "resultSnap.h"
#include "../../../../numeric/dovetail.h"

void resultSnap::storeVertex(unsigned int left, unsigned int right) {
    unsigned int id = dovetail(left,right);
    clock_t delta = std::clock();
    if (!g.IsNode(id)) {
        g.AddNode(id);
    }
    time += (std::clock() - delta);
}

void resultSnap::storeEdge(unsigned int leftSrc, unsigned int rightSrc, unsigned int leftDst, unsigned int rightDst) {
    unsigned int idSrc = dovetail(leftSrc,rightSrc);
    unsigned int idDst = dovetail(leftDst,rightDst);
    clock_t delta = std::clock();
    g.AddEdge(idSrc,idDst);
    time += (std::clock() - delta);
}

void resultSnap::storeVertexInternal(unsigned int left, unsigned int right) {
    unsigned int idDst = dovetail(left,right);
    clock_t delta = std::clock();
    if (!g.IsNode(idDst)) {
        g.AddNode(idDst);
    }
    time += (std::clock() - delta);
}

unsigned long resultSnap::save() {
    clock_t c = std::clock();
    TFOut FOut((outputFile+"result.snap").c_str());
    g.Save(FOut);
    time += (std::clock() - c);
    return time;
}

resultSnap::~resultSnap() {
}

resultSnap::resultSnap(std::string resultFileName) : outputFile{resultFileName} {}
