//
// Created by Giacomo Bergami on 24/08/16.
//

#include <gdb/Objects.h>
#include <gdb/ObjectsIterator.h>
#include <gdb/Handler.h>
#include "resultSparksee.h"
#include "../../config/Init.h"
#include "../../../../fromJava/System.h"
#include "../../../../numeric/dovetail.h"


resultSparksee::resultSparksee(std::string name) {
    t = 0;
    clock_t  loc = std::clock();
    db = Init::sparksee->Create(System::ws(name),System::ws(name));
    sess = db->NewSession();
    graph = sess->GetGraph();
    vertex = graph->NewNodeType(System::ws("Vertex"));
    edge = graph->NewEdgeType(System::ws("Edge"),true,true);
    dovetailAttribute = graph->NewAttribute(vertex,System::ws("dovetail"),sparksee::gdb::Long,sparksee::gdb::AttributeKind::Indexed);
    t += (std::clock()-loc);
}

void resultSparksee::storeVertex(unsigned int left, unsigned int right) {
    unsigned long id = dovetail(left,right);
    clock_t  loc = std::clock();
    sparksee::gdb::Value u; u.SetLong(id);
    sparksee::gdb::Objects* onLeft = graph->Select(dovetailAttribute, sparksee::gdb::Condition::Equal, u);
    sparksee::gdb::ObjectsIterator* it = onLeft->Iterator();
    if (it->HasNext()) {
        currentOuter = it->Next();
    } else {
        currentOuter = graph->NewNode(vertex);
    }
    delete it;
    graph->SetAttribute(currentOuter,dovetailAttribute,u.SetLong(id));
    t += (std::clock()-loc);
}

void resultSparksee::storeVertexInternal(unsigned int left, unsigned int right) {
    unsigned long id = dovetail(left,right);
    clock_t  loc = std::clock();
    sparksee::gdb::Value u; u.SetLong(id);
    sparksee::gdb::Objects* onLeft = graph->Select(dovetailAttribute, sparksee::gdb::Condition::Equal, u);
    sparksee::gdb::ObjectsIterator* it = onLeft->Iterator();
    if (it->HasNext()) {
        currentInner = it->Next();
    } else {
        currentInner = graph->NewNode(vertex);
    }
    delete it;
    t += (std::clock()-loc);
}

void resultSparksee::storeEdge(unsigned int leftSrc, unsigned int rightSrc, unsigned int leftDst, unsigned int rightDst) {
    clock_t  loc = std::clock();
    graph->NewEdge(edge,currentOuter,currentInner);
    t += (std::clock()-loc);
}

unsigned long resultSparksee::save() {
    return t;
}

resultSparksee::~resultSparksee() {
    delete graph;
    delete sess;
    delete db;
}
