//
// Created by Giacomo Bergami on 29/08/16.
//

#pragma once


#include <ctime>
#include "../resultActions.h"

class resultMimic : public resultActions {
public:
    void storeVertex(unsigned int left, unsigned int right);
    void storeVertexInternal(unsigned int left, unsigned int right);
    void storeEdge(unsigned int leftSrc, unsigned int rightSrc, unsigned int leftDst, unsigned int rightDst);
    clock_t save();
};

