//
// Created by Giacomo Bergami on 24/08/16.
//

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "resultProposed.h"

typedef struct {
    unsigned int isVertex;
    unsigned int isEdge;
    unsigned int left;
    unsigned int right;
} JOINRESULT;

resultProposed::resultProposed(std::string resultFileName) {
    result = fopen(resultFileName.c_str(),"w");
    if (!result) {
        std::cerr << "Error while opening result file \"" << resultFileName <<"\": " << strerror(errno) << std::endl;
        exit(1);
    }
    cont = 0;
}

void resultProposed::storeVertexInternal(unsigned int left, unsigned int right) {

}

void resultProposed::storeVertex(unsigned int left, unsigned int right) {
    clock_t c = std::clock();
    JOINRESULT vertex;
    vertex.isVertex = 1;
    vertex.isEdge = 0;
    vertex.left = left;
    vertex.right = right;
    fwrite(&vertex, sizeof(JOINRESULT),1,result);
    cont += (std::clock() - c);
}

void resultProposed::storeEdge(unsigned int leftSrc, unsigned int rightSrc, unsigned int leftDst, unsigned int rightDst) {
    clock_t c = std::clock();
    JOINRESULT edge;
    edge.isEdge = 1;
    edge.isVertex = 0;
    edge.left = leftDst;
    edge.right = rightDst;
    fwrite(&edge, sizeof(JOINRESULT),1,result);
    cont += (std::clock() - c);
}

clock_t resultProposed::save() {
    fclose(result);
    return cont;
}
