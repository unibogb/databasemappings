//
// Created by Giacomo Bergami on 24/08/16.
//

#pragma once

#include <unordered_map>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/directed_graph.hpp>
#include <boost/graph/adj_list_serialize.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/binary_object.hpp>
#include <iostream>
#include <fstream>

#include "../resultActions.h"
#include "../../../../numeric/dovetail.h"
#include <string>


class VResult {
public:
    unsigned int left, right;

    //Serializers
    template<class Archive> void serialize(Archive &ar, const unsigned int version) {
        ar & left;
        ar & right;
    }

    VResult(unsigned int l, unsigned int r) : left{l}, right{r} {}
    VResult() {}
};

//Defining the graph data structure. Using the vecS specification
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,VResult> graph_t;
typedef boost::graph_traits<graph_t>::vertex_descriptor result_vertex_descriptor;
typedef boost::graph_traits<graph_t>::edge_descriptor result_edge_descriptor;


class resultBoost : public resultActions {
    std::ofstream file;
    std::unordered_map<unsigned long,unsigned long> id_to_dovetail;
    graph_t g;
    unsigned long outer, inner;
    std::string name;
    clock_t time = 0;
public:
    resultBoost(std::string n);
    void storeVertex(unsigned int left, unsigned int right);
    void storeVertexInternal(unsigned int left, unsigned int right);
    void storeEdge(unsigned int leftSrc, unsigned int rightSrc, unsigned int leftDst, unsigned int rightDst);
    clock_t save();
};

