//
// Created by Giacomo Bergami on 20/08/16.
//

#pragma once

#include <string>
#include "../../../../lib-headers/snap-core/Snap.h"
#include "../resultActions.h"

class resultActions;



class resultSnap : public resultActions {
    TNodeNet<TAttr> g;
    std::string outputFile;
    clock_t time = 0;

public:
    resultSnap(std::string resultFileName);
    void storeVertex(unsigned int left, unsigned int right);
    /**
     * Given my algorithm, with this implementation I only store the outgoing element
     * @param leftSrc
     * @param rightSrc
     * @param leftDst
     * @param rightDst
     */
    void storeEdge(unsigned int leftSrc, unsigned int rightSrc, unsigned int leftDst, unsigned int rightDst);

    void storeVertexInternal(unsigned int left, unsigned int right);

    clock_t save();

    ~resultSnap();
};