//
// Created by Giacomo Bergami on 20/08/16.
//

#pragma once

#include <ctime>

class resultActions {
public:
    virtual void storeVertex(unsigned int left, unsigned int right) = 0;
    virtual void storeVertexInternal(unsigned int left, unsigned int right) = 0;
    virtual void storeEdge(unsigned int leftSrc, unsigned int rightSrc, unsigned int leftDst, unsigned int rightDst) = 0;
    virtual clock_t save() = 0;
};