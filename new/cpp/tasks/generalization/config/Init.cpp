//
// Created by Giacomo Bergami on 24/08/16.
//

extern "C" {
    #include <getopt.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <fcntl.h>
    #include <unistd.h>
}
#include <fstream>
#include <sstream>
#include "Init.h"
#include "../../../fromJava/evaluateHashes.h"
#include "../../storeCSV.h"
#include "../sample/Sample.h"

#include "../sample/Sample.h"
#include "../../../fromJava/javaUtils.h"
#include "../../storeCSV.h"
#include "../../generalization/join/Join.h"
#include "../../generalization/join/concrete/joinProposed.h"
#include "../../generalization/join/joinFactory.h"
#include "../../generalization/config/Init.h"


#include "../../../io/mappers.h"
#include "../../../io/serializers.h"
#include "../../../io/file/HashIndexArrayField.h"
#include "../../../io/file/VertexIndexArrayField.h"

#define _XOPEN_SOURCE 500
#include <ftw.h>
static int remove_cb(const char *fpath, const struct stat *sb, int typeFlag, struct FTW *ftwbuf)
{
    if (ftwbuf->level)
        remove(fpath);
    return 0;
}

void printElem(VAEntry* elem,INDEXFILE *f,char* vaoffset) {
    std::cout << "ID = " << elem->vertexHeader->id << std::endl;
    std::cout << "BBB = " << elem->vertexHeader->bbb << std::endl;
    //std::cout << "VIN SIZE=" << VIN_SIZE(elem) << std::endl;
    std::cout << "VOUT SIZE=" << VOUT_SIZE(elem);
    unsigned int size2 = VOUT_SSIZE(elem->vertexHeader->id,vaoffset,f);
    std::cout << "SIZE= " <<  size2 << std::endl;
    //unsigned int* array = VOUT_ARRAY(f[i].id,vaoffset,f);
    for (unsigned int j=0; j<size2; j++)
        std::cout << VOUT_ARRAY(elem->vertexHeader->id,vaoffset,f)[j].adjacentID << std::endl;
    /*std::cout << "VOUT SIZE=" << size2 << std::endl;*/

    char* att = ATTRIBUTE(elem,0);
    std::cout << "Attribute 0 = " << att << std::endl;
    att = ATTRIBUTE(elem,1);
    std::cout << "Attribute 1 = " << att << std::endl;
    att = ATTRIBUTE(elem,2);
    std::cout << "Attribute 2 = " <<att << std::endl << std::endl;
}

void printElem2(VAEntry_updating* elem,INDEXFILE *f,char* vaoffset) {
    std::cout << "ID = " << elem->vertexHeader->id << std::endl;
    std::cout << "BBB = " << elem->vertexHeader->bbb << std::endl;
    unsigned int size2 = VOUT_SSIZE(elem->vertexHeader->id,vaoffset,f);
    std::cout << "SIZE= " <<  size2 << std::endl;
    //unsigned int* array = VOUT_ARRAY(f[i].id,vaoffset,f);
    for (unsigned int j=0; j<size2; j++)
        std::cout << VOUT_ARRAY(elem->vertexHeader->id,vaoffset,f)[j].adjacentID << std::endl;
    /*std::cout << "VOUT SIZE=" << size2 << std::endl;*/

    char* att = ATTRIBUTE(elem,0);
    std::cout << "Attribute 0 = " << att << std::endl;
    att = ATTRIBUTE(elem,1);
    std::cout << "Attribute 1 = " << att << std::endl;
    att = ATTRIBUTE(elem,2);
    std::cout << "Attribute 2 = " <<att << std::endl << std::endl;
}


void k() {



    //// XXX 38+41


    /* std::vector<EDGES_OUTIN> testLeft, testRight;
     testLeft.emplace_back(0,13); testLeft.emplace_back(2,14); testLeft.emplace_back(4,50); testLeft.emplace_back(5,10); testLeft.emplace_back(5,20);
     testRight.emplace_back(1,24); testRight.emplace_back(1,10); testRight.emplace_back(4,20); testRight.emplace_back(5,30); testRight.emplace_back(5,20); testRight.emplace_back(5,70);

     int i = 0; int j = 0;
     int sL = testLeft.size(), sR = testRight.size();
     unsigned int hL = testLeft[i].adjacentHash,  hR = testRight[j].adjacentHash;
     while ((i<sL) && (j<sR)) {
         if (hL< hR) {
             i++;
             hL = testLeft[i].adjacentHash;
         } else if (hL>hR) {
             j++;
             hR = testRight[j].adjacentHash;
         } else {
             std::cout << "Hash = " << hL << std::endl;
             int ii = i, jj = j;
             while (testLeft[ii].adjacentHash == hL) {
                 jj = j;
                 while (testRight[jj].adjacentHash == hR) {
                     std::cout << "\t" << testLeft[ii].adjacentID << ", " << testRight[jj].adjacentID << std::endl;
                     jj++;
                 }
                 ii++;
             }
             i = ii;
             j = jj;
             hL = testLeft[i].adjacentHash;
             hR = testRight[j].adjacentHash;
         }
     }

     exit(1);*/
    unsigned int sizee = 100;
    std::string dir = "left";


    std::cout <<sizeof(EDGES_OUTIN) << " " << sizeof(unsigned int)*2<< std::endl;
    unsigned long size = 0;
    int fd;
    INDEXFILE *f = (INDEXFILE *)mmapFile("/Users/vasistas/cgraph2/data/"+dir+"/"+std::to_string(sizee)+"/Index.bin",&size,&fd);
    unsigned int id = 0;
    unsigned int hash = 0;
    unsigned long offset = 0, limitOffset = 0;
    int i = 0;
    while(f[i++].offset!=0);
    i-=1;

    unsigned long sized = 0;
    int vafd = 0;
    HASHFILE *h = (HASHFILE *)mmapFile("/Users/vasistas/cgraph2/data/"+dir+"/"+std::to_string(sizee)+"/Hash.bin",&sized,&fd);

    unsigned long ssize = 0;
    int sfd = 0;
    char* vaoffset = (char*)mmapFile("/Users/vasistas/cgraph2/data/"+dir+"/"+std::to_string(sizee)+"/VAOffset.bin",&ssize,&sfd);

    VAEntry va;
    //INDEXFILE l = f[6593];
    //std::cout << "ID=" << l.id << " hash=" << l.hash << std::endl;
    //descriptorFromOffset(&va,l.offset,vaoffset);

    /*std::cout << "EHSIZE=" << sizeof(EDGES_OUTIN) << std::endl;
    std::cout << "VOUT SIZE=" << VOUT_SIZE(&va);
    int sz = VOUT_SIZE(&va);
    for (int i=0; i<sz; i++) {
        EDGES_OUTIN eo = VOUT(&va)[i];
        std::cout << "\t\t hash=" << eo.adjacentHash << " id=" << eo.adjacentID << std::endl;
    }*/


    /*for (int i=0; i<size / sizeof(INDEXFILE); i++) {
        VAEntry_updating elem;
        partialdescriptorFromVertexID(&elem,i,f,vaoffset);
        printElem2(&elem,f,vaoffset);
    }*/

    for (int i= 0; i<sized/sizeof(HASHFILE); i++) {
        hash = h[i].hash;
        limitOffset = (i==(sized/sizeof(HASHFILE))-1) ? (ssize) : h[i+1].offset;
        offset = h[i].offset;
        std::cout << "Hash=" << hash << " + Offset = " << offset << " ==> LimitOffset " << limitOffset << std::endl;
        assert (offset < limitOffset);
        /*while (offset<limitOffset) {
            VAEntry elem;
            descriptorFromOffset(&elem,offset,vaoffset);
            printElem(&elem,f,vaoffset);
            std::cout << std::endl;
            offset += elem.vertexHeader->size;
        }*/
        //std::getchar();
    }
    //quit(fd)

    close(sfd);
    close(fd);
    close(vafd);

}

////////////////////////////////

/** parse:
 * Parsing the configuration file
 */
void Init::parse(char* file) {
    int lineno = 0;
    std::ifstream cfgfile(file);
    std::string line;
    while( std::getline(cfgfile, line) )
    {
        std::istringstream is_line(line);
        std::string key;
        if(line.find("#") && std::getline(is_line, key, '=') )
        {
            std::string value;
            if( std::getline(is_line, value) )
                options[key] = value;
        }
    }
}


void Init::init(int argc, char**argv) {
    //// parsing the program arguments: that is where to read the configuration file
    int doConfig = 0;
    char* confFile = NULL;
    int c;
    std::cerr << "Reading binary cgraph2 parameters from conf file" << std::endl;
    while (1)  {
        static struct option long_options[] = {
                {"config",    required_argument,  0,  'c'},
                {0,0,0,0}
        };

        int option_index = 0;

        c = getopt_long(argc,argv,"c",long_options, &option_index);

        if (c==-1) break;
        switch (c) {
            case 'c': {
                asprintf(&confFile,"%s",optarg);
                doConfig = 1;
                break;
            }
            default: {
                abort();
            }
        }

    }
    char* config_file;
    if (doConfig) {
        parse(confFile);
        free(confFile);
    } else {
        parse((char*)"my.conf");
    }

    //Parsing the configuration file
    std::cerr << "Parsing the configuration file" << std::endl;

    //Setting the current work directory
    if (options.find("chdir")!=options.end()) {
        std::cerr << "Setting current directory to = " << options["chdir"] << std::endl;
        chdir(options["chdir"].c_str());
    }

    cmd = options["do.command"];
    library = options["with.library"];
    std::cerr << "Performing \"" << cmd << "\" with library \"" << library << "\"" << std::endl;

    std::cerr << "Loading Sparksee Configurations" << std::endl;
    std::wstring ws;
    ws.assign(options["sparksee.conf"].begin(), options["sparksee.conf"].end());
    sparksee::gdb::SparkseeProperties::Load(ws);
    sparksee::gdb::SparkseeConfig cfg;
    sparksee = new sparksee::gdb::Sparksee(cfg);
}


void Init::run() {

    int out, save_out=-1;
    int times = 1;
    //number of iterations per sample
    if (options.find("for.times")!=options.end()) {
        times = std::stoi(options["for.times"]);
    }

    //redirect stdOut
    if (options.find("redirect.stdout")!=options.end()) {
        int out = open(options["redirect.stdout"].c_str(), O_CREAT|O_APPEND|O_WRONLY, 0600);
        if (-1 == out) { perror("opening stdout redirection file"); exit(255); }
        save_out = dup(fileno(stdout));
        if (-1 == dup2(out, fileno(stdout))) { perror("cannot redirect stdout"); exit(255); }
    }

    //Setting if the directory has to be cleared
    bool doClear = (options.find("do.clear")!=options.end())
                   && ((!options["do.clear"].compare("T")) || (!options["do.clear"].compare("true")));

    std::set<std::string> toCout;
    if (options.find("to.cout")!=options.end()) {
        std::istringstream is_line(options["to.cout"]);
        std::string left;
        while(std::getline(is_line, left, ',') ) {
            toCout.emplace(left);
        }
    }

    if ((!cmd.compare("evaluateHashes"))||(!cmd.compare("storeCSVInMemory"))) {
        std::string hashFile = options["csv"];
        std::string attributes = options["hashables"];
        char* arrayOfHashables = (char*)options["hashables"].data();

        int countargs = 0;
        char* tok;
        for (tok = strtok(arrayOfHashables, ",");
             tok && *tok;
             tok = strtok(NULL, ",")) {
            countargs++;
        }
        int* array = (int*)malloc(sizeof(int)*countargs);
        tok = arrayOfHashables;
        for (int i = 0; i < countargs; ++i) {
            array[i] = atoi(tok);
            tok += strlen(tok) + 1;  // get the next token by skipping past the '\0'
            tok += strspn(tok, ","); //   then skipping any starting delimiters
        }
        if ((!cmd.compare("evaluateHashes")))
            evaluateHashes((char *) hashFile.data(), countargs, array);
        else if (!cmd.compare("storeCSVInMemory"))
            storeCSV(options["csv"],options["edgesFile"],std::stoul(options["csv.Size"]), countargs, array);
    } else if (!cmd.compare("sample")) {

        std::string sampleOperator = options["sample.operator"];
        std::cerr << "Reading " + sampleOperator + " configuration" << std::endl;
        std::string nChoice = "sampler."+ sampleOperator + ".nChoice";
        std::string doSkip = "sampler."+ sampleOperator + ".doSkip";

        int samplerVertex = std::stoi(options["sampler.vertex"]);
        int samplerSkip = std::stoi(options[doSkip]);
        int samplerNext = std::stoi(options[nChoice]);
        double jumpProb = std::stod(options["sampler.jumpProb"]);
        std::map<int,std::vector<int>> neigh;
        std::stringstream ss(options["components"]);
        std::vector<unsigned int> steps;
        while (ss.good()) {
            std::string substr;
            getline(ss, substr, ',');
            steps.emplace_back(std::stoi(substr));
        }
        for (int i=0; i<times; i++) {

            std::string folder = options["store.sample.in"]+"/"+sampleOperator+"/";
            if (doClear) {
                std::cerr << "Clearing folder" << std::endl;
                nftw(folder.c_str(), remove_cb, 10, FTW_DEPTH);
            }
            /*
             * int samplerVertex, int samplerSkip, int samplerNext, double jumpProb, std::string folder, std::string library, std::vector<unsigned int> steps
             */

            clock_t timeDeltaSum;

            unsigned long size = 0;
            int fd,vafd;
            INDEXFILE *f = (INDEXFILE *)mmapFile("/Users/vasistas/cgraph2/Index.bin",&size,&fd);
            unsigned int nVert = size / sizeof(INDEXFILE);
            //std::cout << nVert << std::endl;
            //exit(50);
            char* vaoffset = (char*)mmapFile("/Users/vasistas/cgraph2/VAOffset.bin",&size,&vafd);
            Sample s(samplerVertex,samplerSkip,samplerNext,jumpProb,nVert,vaoffset,f,library);
            s.init(folder,steps);

            //int steps[] = {2,5,10,100,1000,10000,100000,1000000,4000000};
            for (unsigned int step : steps) {
                double timeIndexing = s.next(step);
                if (toCout.find("indexing")!=toCout.end()) {
                    std::cout<< step << ",indexing," << library << "," << std::fixed << timeIndexing << std::endl;
                }
                std::string  k = folder;
                k += std::to_string(step);
                double timeSerialize = s.serializeToFolder(step,k,library);
                if (toCout.find("storing")!=toCout.end()) {
                    std::cout<< step << ",storing," << library << "," << std::fixed << timeSerialize << std::endl;
                }
                if (toCout.find("indexing+storing")!=toCout.end()) {
                    std::cout<< step << ",indexing+storing," << library << "," << std::fixed << (timeIndexing+timeSerialize) << std::endl;

                }
            }


            //sample(samplerVertex, samplerSkip, samplerNext, jumpProb, , library, steps);
        }
    } else if (!cmd.compare("debug")) {
        k();
    } else if (!cmd.compare("join")) {

        //parsing the line containing the join predicate
        std::stringstream join(options["Join"]);

        //Storing the predicate (eq/leq) as a ::map
        std::map<std::string,std::string> theta_predicate;
        while (join.good()) {
            std::string substr;
            getline(join,substr,'&');

            std::istringstream is_line(substr);
            std::string left;
            if(std::getline(is_line, left, ':') )
            {
                std::string right;
                if( std::getline(is_line, right) )
                    theta_predicate[left] = right;
            }
        }



        // Either use the proposed storage for the result, or use the specified library one
        std::string storage;
        if (!options["with.useProposedStorage"].compare("T")) {
            storage = "proposed";
        } else {
            storage = options["with.useProposedStorage"];
        }
        std::cerr<< "Proposed Storage: \"" << storage << "\"" << std::endl;


        //Subdirectories
        std::stringstream ss(options["components"]);

        while (ss.good()) {
            std::string substr;
            getline(ss,substr,',');
            int current = std::stoi(substr);
            //std::cout << "Join n°=" << substr << std::endl;

            std::string graphFirstOperand = options["operand.left"]+substr+"/";
            std::string graphSecondOperand = options["operand.right"]+substr+"/";
            std::string graphResultFolder = options["store.result.in"]+substr+"/";
            for (int i=0; i<times; i++) {
                Join* algo = joinFactory::generateInstance(library);
                if (doClear) {
                    std::cerr << "Clearing folder" << std::endl;
                    nftw(graphResultFolder.c_str(), remove_cb,  10, FTW_DEPTH);
                }
                algo->EqJoin(graphFirstOperand,
                                                graphSecondOperand,
                                                graphResultFolder,
                                                theta_predicate, storage,
                                                library) ;

                for (const std::string& x : toCout) {
                    if (!x.compare("storeResult")) {
                        std::cout << substr << ",storeResult," << storage << "," << std::fixed << algo->storageTime << std::endl;
                        fflush(stdout);
                    } else if (!x.compare("join")) {
                        std::cout << substr << ",join," << library << "," << std::fixed << algo->algorithmTime << std::endl;
                        fflush(stdout);
                    } else if (!x.compare("load")) {
                        std::cout << substr << ",load," << library << "," << std::fixed << algo->loadingTime << std::endl;
                        fflush(stdout);
                    }
                }
                delete algo;
            }
        }

    } else if (!cmd.compare("virt")) {

    }

    fflush(stdout);

    if (save_out!=-1) {
        close(out);
        dup2(save_out, fileno(stdout));
        close(save_out);
        puts("back to normal output");
    }
}

void Init::quit() {
    //if (sparksee) delete sparksee;
}

sparksee::gdb::Sparksee* Init::sparksee = nullptr;