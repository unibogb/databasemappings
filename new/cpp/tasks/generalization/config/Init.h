//
// Created by Giacomo Bergami on 24/08/16.
//

#pragma once
#include <string>
#include <gdb/Sparksee.h>
#include <map>

class Init {
    std::map<std::string, std::string> options;
    void parse(char* file);
public:
    std::string cmd, library;
    static sparksee::gdb::Sparksee* sparksee;
    void init(int argc, char**argv);
    void quit();
    void run();
};


