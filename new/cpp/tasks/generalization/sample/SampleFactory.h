//
// Created by Giacomo Bergami on 20/08/16.
//

#pragma once

#include "sampleActions.h"

class SampleFactory {
public:
    static sampleActions* generateInstance(std::string ak, int vName);
};
