//
// Created by Giacomo Bergami on 16/08/16.
//

#pragma once

#include <map>
#include <vector>
#include <set>
#include <string>
#include <random>
#include <memory>
#include <iostream>
#include <array>
#include "../../../io/serializers.h"
#include "../../../io/mappers.h"
#include "../../../rbtree/rbtree.h"
#include "../../storeCSV.h"
#include "../../../numeric/compares.h"
#include "SampleFactory.h"
#include "../../../io/file/VertexIndexArrayField.h"

class Sample {
    std::mt19937 rvGen, rjGen, nchoiceGen;
    std::uniform_int_distribution<int> rv;
    std::uniform_real_distribution<double> rj;
    char* vaoffset;
    INDEXFILE *f;
    double jumpProb;
    int vRange;
    int u;
    unsigned int hashU;
    sampleActions* algorithm;

public:
    std::vector<int> idConversion;
    int counter;

    ~Sample();

    Sample(int samplerVertex, int samplerSkip, int samplerNext, double jumpProb,const int nVert,char* vaoff,INDEXFILE *idx, std::string solution);

    double next(int maxStep);
    double serializeToFolder(int pace,std::string s,std::string lib);
    void init(std::string initialPath, std::vector<unsigned int>& vec);
};

