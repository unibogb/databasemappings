//
// Created by Giacomo Bergami on 20/08/16.
//

#include "SampleFactory.h"
#include "concrete/sampleProposed.h"
#include "concrete/sampleSnap.h"
#include "concrete/sampleSparksee.h"
#include "concrete/sampleBoost.h"
#include "concrete/sampleVirtuoso.h"

sampleActions *SampleFactory::generateInstance(std::string name, int nVert) {
    if (!name.compare("proposed")) {
        return new sampleProposed(nVert);
    } else if (!name.compare("snap")) {
        return new sampleSnap();
    } else if (!name.compare("sparksee")) {
        return new sampleSparksee();
    } else if (!name.compare("boost")) {
        return new sampleBoost();
    } else if (!name.compare("virtuoso")) {
        return new sampleVirtuoso();
    } else return nullptr;
}
