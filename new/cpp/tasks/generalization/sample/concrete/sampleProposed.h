//
// Created by Giacomo Bergami on 20/08/16.
//

#pragma once
#include <algorithm>

#include <unordered_set>
#include <unordered_map>
#include <map>
#include "../../../../io/mappers.h"
#include "../../../../rbtree/rbtree.h"
#include "../sampleActions.h"

class sampleProposed : public sampleActions {
    //How the graph is represented in main memory
    std::vector<VAEntry_updating*> vertices;
    RBTree<unsigned int,int> inTree;
public:

    sampleProposed(int nVert);
    void addNewVertex(int cc_newcounter, int oldid, unsigned int hash, VAEntry_updating* myData);
    void addNewEdge(unsigned int src, unsigned int srcHash, unsigned int dst, unsigned int dstHash);
    void serialize(std::string s, int maxId);
    unsigned int getVertexOutSizeByNewId(int cc_newcounter);
    void prepareSerialization(std::string basePath, std::vector<unsigned int> prepareSizes);
};


