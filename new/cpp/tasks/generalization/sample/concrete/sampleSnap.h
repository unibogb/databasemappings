//
// Created by Giacomo Bergami on 20/08/16.
//

#pragma once


#include <map>
#include "../sampleActions.h"
#include "../../../../lib-headers/snap-core/Snap.h"

class sampleSnap : public sampleActions {
    TNodeNet<TAttr> g;
    std::map<int,std::string> posToAttribute;

public:
    sampleSnap();
    void addNewVertex(int cc_newcounter, int oldid,unsigned int hash,  VAEntry_updating* myData);;
    void addNewEdge(unsigned int src, unsigned int srcHash, unsigned int dst, unsigned int dstHash);;
    void serialize(std::string path, int maxId);;
    unsigned int getVertexOutSizeByNewId(int cc_newcounter);
    void prepareSerialization(std::string basePath, std::vector<unsigned int> prepareSizes);
};
