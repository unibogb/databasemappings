//
// Created by Giacomo Bergami on 26/08/16.
//

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/directed_graph.hpp>
#include <boost/graph/adj_list_serialize.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/binary_object.hpp>
#include <iostream>
#include <fstream>
#include "sampleBoost.h"


void sampleBoost::addNewVertex(int cc_newcounter, int oldid, unsigned int hash, VAEntry_updating *myData) {
    char *ip = ATTRIBUTE(myData, 0);
    char *org = ATTRIBUTE(myData, 1);
    char *year = ATTRIBUTE(myData, 2);
    std::string organization{org};
    std::string IP{ip};
    VFull v{(unsigned int) cc_newcounter, hash, (unsigned int) std::atoi(year), IP, organization};
    id_to_dovetail[boost::add_vertex(v,g)] = cc_newcounter;
}

void sampleBoost::addNewEdge(unsigned int src, unsigned int srcHash, unsigned int dst, unsigned int dstHash) {
    boost::add_edge(id_to_dovetail[src],id_to_dovetail[dst],g);
}

void sampleBoost::serialize(std::string s, int maxId) {
    std::ofstream file{s+".boost",std::ios_base::binary};
    boost::archive::binary_oarchive toStore{file};
    toStore << g;
    file.close();
}

unsigned int sampleBoost::getVertexOutSizeByNewId(int cc_newcounter) {
    return (unsigned int) g.out_edge_list(id_to_dovetail[cc_newcounter]).size();
}

void sampleBoost::prepareSerialization(std::string basePath, std::vector<unsigned int> prepareSizes) {
    //Not needed, since the binary graph is serialized each time
}

sampleBoost::sampleBoost() {}

//VERTICES

///////
