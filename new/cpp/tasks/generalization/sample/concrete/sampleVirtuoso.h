//
// Created by Giacomo Bergami on 10/09/16.
//

#ifndef CGRAPH2_SAMPLEVIRTUOSO_H
#define CGRAPH2_SAMPLEVIRTUOSO_H

//#define BASIC_VALUE "http://jackbergus.alwaysdata.net/values/"
#define BASIC_EDGE  "http://jackbergus.alwaysdata.net/edges/"
#define BASIC_PROPERTY "http://jackbergus.alwaysdata.net/property/"

#include <map>
#include "../sampleActions.h"
#include "../../../graphs/virtuosoredland/VirtuosoGraph.h"

class sampleVirtuoso  : public sampleActions {
    Virtuoso* connection;
    std::set<unsigned int> toHandle;
    std::map<unsigned int,VirtuosoGraph> handlers;
    std::map<unsigned int,clock_t> timing;
    bool isLeft;
public:
    ~sampleVirtuoso();
    sampleVirtuoso();
    void addNewVertex(int cc_newcounter, int oldid, unsigned int hash, VAEntry_updating* myData);
    void addNewEdge(unsigned int src, unsigned int srcHash, unsigned int dst, unsigned int dstHash);
    void serialize(std::string s, int maxId);
    unsigned int getVertexOutSizeByNewId(int cc_newcounter);
    void prepareSerialization(std::string basePath, std::vector<unsigned int> prepareSizes);
};


#endif //CGRAPH2_SAMPLEVIRTUOSO_H
