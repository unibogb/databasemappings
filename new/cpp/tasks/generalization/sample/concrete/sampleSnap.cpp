//
// Created by Giacomo Bergami on 25/08/16.
//
#include "sampleSnap.h"
#include <iostream>

void sampleSnap::addNewVertex(int cc_newcounter, int oldid, unsigned int hash, VAEntry_updating *myData) {
    //TInt id{cc_newcounter};

    TAttr attribute;
    int count = 0;
    char* ptr = myData->stringArray;
    int no = 0;
    //The first attribute is the hash value

    TStr hashAttribute{"hash"};
    TInt Boris_3_13;
    TStr tHash{std::to_string(hash).c_str()};
    attribute.AddSAttr(hashAttribute,atStr,Boris_3_13);
    attribute.AddSAttrDat(cc_newcounter,Boris_3_13,tHash);

    //all the other values are the actual values
    while (count < myData->stringArraySize) {

        TStr attributeName{posToAttribute[no++].c_str()};
        TStr attributeValue{ptr};
        TInt Duccio_smarmella;
        attribute.AddSAttr(attributeName,atStr,Duccio_smarmella);
        attribute.AddSAttrDat(cc_newcounter,Duccio_smarmella,attributeValue);

        int c = (int) (std::strlen(ptr) + 1);
        ptr = &ptr[c];
        count += c;
    }
    //Storing the association between the id and its attribute
    g.AddNode(cc_newcounter,attribute);
}

void sampleSnap::addNewEdge(unsigned int src, unsigned int srcHash, unsigned int dst, unsigned int dstHash) {
    g.AddEdge(src,dst);
}

void sampleSnap::serialize(std::string path, int maxId) {
    if (maxId>=0) {
        TFOut FOut((path+".snap").c_str());
        g.Save(FOut);
    }
}

unsigned int sampleSnap::getVertexOutSizeByNewId(int cc_newcounter) {
    return (unsigned int) g.GetNI(cc_newcounter).GetOutDeg();
}

void sampleSnap::prepareSerialization(std::string basePath, std::vector<unsigned int> prepareSizes) {
    //No need to prepare it
}

sampleSnap::sampleSnap() {
    posToAttribute[0] = "Ip";
    posToAttribute[1] = "Organization";
    posToAttribute[2] = "Year";
}
