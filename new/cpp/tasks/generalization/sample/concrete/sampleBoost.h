//
// Created by Giacomo Bergami on 26/08/16.
//

#pragma once

#include "../sampleActions.h"
#include "../../../graphs/boost/graph_t.h"


class sampleBoost : public sampleActions {
    graph_t g;
    std::unordered_map<unsigned long,unsigned int> id_to_dovetail;
public:
    sampleBoost();
    void addNewVertex(int cc_newcounter, int oldid, unsigned int hash, VAEntry_updating* myData);
    void addNewEdge(unsigned int src, unsigned int srcHash, unsigned int dst, unsigned int dstHash);
    void serialize(std::string s, int maxId);
    unsigned int getVertexOutSizeByNewId(int cc_newcounter);
    void prepareSerialization(std::string basePath, std::vector<unsigned int> prepareSizes);
};
