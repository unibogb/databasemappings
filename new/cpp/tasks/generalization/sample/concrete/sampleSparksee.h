//
// Created by Giacomo Bergami on 25/08/16.
//

#pragma once
#include "../sampleActions.h"
#include <map>
#include <set>

#include <gdb/Query.h>
#include <gdb/Sparksee.h>
#include <gdb/Database.h>
#include <gdb/Session.h>
#include <gdb/Graph.h>

class graphHandler {
public:
    sparksee::gdb::Database * db;
    sparksee::gdb::Session *sess;
    sparksee::gdb::Graph *graph;
    sparksee::gdb::type_t vertex, edge;
    sparksee::gdb::attr_t  hashAttribute, ipAttribute, orgAttribute, yearAttribute, idAttribute;
    graphHandler() {}
    ~graphHandler() {
        delete graph;
        delete sess;
        delete db;
    }
    graphHandler(const graphHandler& gh) {
        db = gh.db;
        sess = gh.sess;
        graph = gh.graph;
        vertex = gh.vertex;
        edge = gh.vertex;
        hashAttribute = gh.hashAttribute;
        ipAttribute = gh.ipAttribute;
        orgAttribute = gh.orgAttribute;
        yearAttribute = gh.yearAttribute;
        idAttribute = gh.idAttribute;
    }
};

class sampleSparksee : public sampleActions {
    std::set<unsigned int> toHandle;
    std::map<unsigned int,graphHandler> handlers;
    std::map<unsigned int,clock_t> timing;
public:
    ~sampleSparksee();
    sampleSparksee();
    void addNewVertex(int cc_newcounter, int oldid, unsigned int hash, VAEntry_updating* myData);
    void addNewEdge(unsigned int src, unsigned int srcHash, unsigned int dst, unsigned int dstHash);
    void serialize(std::string s, int maxId);
    unsigned int getVertexOutSizeByNewId(int cc_newcounter);
    void prepareSerialization(std::string basePath, std::vector<unsigned int> prepareSizes);
};