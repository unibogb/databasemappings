//
// Created by Giacomo Bergami on 25/08/16.
//

#include "sampleSparksee.h"
#include "../../config/Init.h"
#include "../../../../fromJava/System.h"
#include <gdb/Objects.h>
#include <gdb/ObjectsIterator.h>
#include <map>


sampleSparksee::sampleSparksee() {
    // I cannot do very much with the only initial dimension. This is a graph database,
    // and hence I need to know which is the path where I'm storing my data. So serialize is called with
    // a negative valueip
}

void sampleSparksee::addNewVertex(int cc_newcounter, int oldid, unsigned int hash, VAEntry_updating *myData) {
    char *ip = ATTRIBUTE(myData, 0);
    char *org = ATTRIBUTE(myData, 1);
    char *year = ATTRIBUTE(myData, 2);
    for (unsigned int x : toHandle) {
        clock_t  t = clock();
        //sparksee::gdb::Graph* graph = handlers[x].graph;
        sparksee::gdb::oid_t v = handlers[x].graph->NewNode(handlers[x].vertex);
        sparksee::gdb::Value ipV, idV, orgV, yV, hV;
        handlers[x].graph->SetAttribute(v, handlers[x].hashAttribute, hV.SetInteger(hash));
        handlers[x].graph->SetAttribute(v, handlers[x].idAttribute, idV.SetInteger(cc_newcounter));
        std::string ipS{ip};
        handlers[x].graph->SetAttribute(v, handlers[x].ipAttribute, ipV.SetString(System::ws(ipS)));
        std::string tmp{org};
        std::wstring tmp2 = System::ws(tmp);
        handlers[x].graph->SetAttribute(v, handlers[x].orgAttribute, orgV.SetString(tmp2));
        handlers[x].graph->SetAttribute(v, handlers[x].yearAttribute, yV.SetInteger(std::stoi(year)));
        timing[x] += (clock()-t);
    }
}

void sampleSparksee::addNewEdge(unsigned int src, unsigned int srcHash, unsigned int dst, unsigned int dstHash) {
    for (unsigned int x : toHandle) {
        clock_t  t = clock();
        sparksee::gdb::Graph* graph = handlers[x].graph;
        sparksee::gdb::Value srcId;
        sparksee::gdb::Objects *resultLeft = graph->Select(handlers[x].idAttribute, sparksee::gdb::Condition::Equal,
                                                           srcId.SetInteger(src));
        sparksee::gdb::oid_t leftId = resultLeft->Any();
        delete resultLeft;
        resultLeft = graph->Select(handlers[x].idAttribute, sparksee::gdb::Condition::Equal, srcId.SetInteger(dst));
        sparksee::gdb::oid_t rightId = resultLeft->Any();
        delete resultLeft;
        graph->NewEdge(handlers[x].edge, leftId, rightId);
        timing[x] += (clock()-t);
    }
}

void sampleSparksee::serialize(std::string s, int maxId) {
    maxId = *toHandle.begin();
    std::cout << maxId << ",indexing+storing,sparksee," << ((double)timing[maxId])/(CLOCKS_PER_SEC/1000.0)  << std::endl;
    timing.erase(maxId);
    handlers.erase(maxId);
    toHandle.erase(maxId);
}

unsigned int sampleSparksee::getVertexOutSizeByNewId(int cc_newcounter) {
    unsigned int x = *toHandle.begin();
    clock_t t = clock();
    sparksee::gdb::Value srcId;
    sparksee::gdb::Objects *resultLeft = handlers[x].graph->Select(handlers[x].idAttribute, sparksee::gdb::Condition::Equal,
                                                       srcId.SetInteger(cc_newcounter));
    sparksee::gdb::oid_t leftId = resultLeft->Any();
    delete resultLeft;
    resultLeft = handlers[x].graph->Neighbors(leftId, handlers[x].edge, sparksee::gdb::EdgesDirection::Outgoing);
    unsigned int toret = resultLeft->Count();
    delete resultLeft;
    timing[x] += (clock() - t);
    return toret;
}

void sampleSparksee::prepareSerialization(std::string basePath, std::vector<unsigned int> prepareSizes) {
    for (unsigned int x : prepareSizes) {
        toHandle.emplace(x);
        clock_t t = clock();
        handlers[x].db = Init::sparksee->Create(System::ws(basePath+std::to_string(x)+".snap"), System::ws(basePath+std::to_string(x)+".snap"));
        handlers[x].sess = handlers[x].db->NewSession();
        handlers[x].graph = handlers[x].sess->GetGraph();
        handlers[x].vertex = handlers[x].graph->NewNodeType(System::ws("Vertex"));
        handlers[x].edge = handlers[x].graph->NewEdgeType(System::ws("Edge"), true, true);
        handlers[x].hashAttribute = handlers[x].graph->NewAttribute(handlers[x].vertex, System::ws("hash"), sparksee::gdb::Integer,
                                            sparksee::gdb::AttributeKind::Indexed);
        handlers[x].idAttribute = handlers[x].graph->NewAttribute(handlers[x].vertex, System::ws("id"), sparksee::gdb::Integer,
                                          sparksee::gdb::AttributeKind::Indexed);
        handlers[x].ipAttribute = handlers[x].graph->NewAttribute(handlers[x].vertex, System::ws("Ip"), sparksee::gdb::String,
                                          sparksee::gdb::AttributeKind::Basic);
        handlers[x].orgAttribute = handlers[x].graph->NewAttribute(handlers[x].vertex, System::ws("Organization"), sparksee::gdb::String,
                                           sparksee::gdb::AttributeKind::Basic);
        handlers[x].yearAttribute = handlers[x].graph->NewAttribute(handlers[x].vertex, System::ws("Year"), sparksee::gdb::Integer,
                                            sparksee::gdb::AttributeKind::Indexed);

        timing[x] = clock() - t;
    }
}

sampleSparksee::~sampleSparksee() {

}

