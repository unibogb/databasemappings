//
// Created by Giacomo Bergami on 25/08/16.
//

#include "sampleProposed.h"

#include <iostream>

#include "../../../../io/serializers.h"
#include "../../../storeCSV.h"

sampleProposed::sampleProposed(int nVert) : inTree{&compareU_Int}  {
    vertices = std::vector<VAEntry_updating*>(nVert);
}

void sampleProposed::addNewVertex(int cc_newcounter, int oldid, unsigned int hash, VAEntry_updating *myData) {
    if (hash==0)
        std::cerr << "There is a zero hash = " << cc_newcounter << " " << oldid << std::endl;
    vertices[cc_newcounter] = myData;
    vertices[cc_newcounter]->hash = hash;
    vertices[cc_newcounter]->new_id = cc_newcounter;
    inTree.insertKey(hash)->add(cc_newcounter);
}

void sampleProposed::addNewEdge(unsigned int src, unsigned int srcHash, unsigned int dst, unsigned int dstHash) {
    vertices[src]->outEdges.emplace_back(dstHash,dst);
    //vertices[dst]->inEdges.emplace_back(dstHash,dst);
}

void sampleProposed::serialize(std::string s, int maxId) {
    if (maxId>=0) {
        std::string vao = s + "/VAOffset.bin";
        FILE *b = fopen(vao.c_str(), "w");
        if (b == nullptr) {
            std::cerr << "ERROR OPENING FILE: " << vao << " cause of " << strerror(errno) << std::endl;
            exit(19);
        }
        vao = s + "/Hash.bin";
        FILE *h = fopen(vao.c_str(), "w");
        vao = s + "/Index.bin";
        FILE *j = fopen(vao.c_str(), "w");
        unsigned long VAOffset = 0;
        unsigned long HashOffset = 0;
        //int ccc = 0;
        RBTree<unsigned int, int>::rbiterator it = inTree.iterator();

        while (it.hasNext()) {
            Node<unsigned int, int> *ptr = it.next();
            unsigned int hash = it.getCurrentK();

            //std::cout << "hash=" << hash << " offset="<< HashOffset << std::endl;
            serialize_hash(h, hash, HashOffset);

            for (int kk : ptr->overflowList) {
                VAEntry_updating *e = vertices[kk];
                e->offset = VAOffset;
                //std::sort(e->inEdges.begin(),e->inEdges.end());
                std::sort(e->outEdges.begin(), e->outEdges.end());
                serialize_vertex_values2(b, &VAOffset, &HashOffset, e); /// TODO XXX
                //ccc++;
            }
        }
        //std::cerr << ccc << std::endl;
        fclose(b);
        fclose(h);

        for (int p = 0; p < maxId; p++) {
            serialize_vertex_id(j, vertices[p]->new_id, vertices[p]->hash, vertices[p]->offset);
        }
        fclose(j);
    }
}

unsigned int sampleProposed::getVertexOutSizeByNewId(int cc_newcounter) {
    return vertices[cc_newcounter]->outEdges.size();
}

void sampleProposed::prepareSerialization(std::string basePath, std::vector<unsigned int> prepareSizes) {
    // No need to do that
}