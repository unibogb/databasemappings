//
// Created by Giacomo Bergami on 10/09/16.
//

#include <string>
#include <iostream>
#include "../../../graphs/virtuosoredland/utils/SchemaValues.h"
#include "sampleVirtuoso.h"



sampleVirtuoso::~sampleVirtuoso() {
    delete connection;
}

sampleVirtuoso::sampleVirtuoso()  {
    connection = new Virtuoso();
}

void sampleVirtuoso::addNewEdge(unsigned int src, unsigned int srcHash, unsigned int dst, unsigned int dstHash) {
    for (unsigned int x : toHandle) {
        std::string baseIRI = handlers[x].getGraphIRI();
        clock_t  t = clock();
        handlers[x].addTriplet(baseIRI+"values/"+std::to_string(src),BASIC_EDGE,baseIRI+"values/"+std::to_string(dst));
        timing[x] += (clock()-t);
    }
}

void sampleVirtuoso::serialize(std::string s, int maxId) {
    maxId = *toHandle.begin();
    std::cout << maxId << ",indexing+storing,virtuoso," << ((double)timing[maxId])/(CLOCKS_PER_SEC/1000.0)  << std::endl;
    timing.erase(maxId);
    handlers.erase(maxId); //Closes the graph (as node)
    toHandle.erase(maxId);
}

void sampleVirtuoso::prepareSerialization(std::string basePath, std::vector<unsigned int> prepareSizes) {
    isLeft = (basePath.find("left") != std::string::npos);
    for (unsigned int x : prepareSizes) {
        toHandle.emplace(x);
        clock_t t = clock();
        std::string res = basePath + "/" + std::to_string(x) + "/";
        handlers.emplace(std::piecewise_construct, std::make_tuple(x), std::make_tuple(connection,res));
        timing[x] = clock() - t;
    }
}

void sampleVirtuoso::addNewVertex(int cc_newcounter, int oldid, unsigned int hash, VAEntry_updating *myData) {
    char *ip = ATTRIBUTE(myData, 0);
    char *org = ATTRIBUTE(myData, 1);
    char *year = ATTRIBUTE(myData, 2);
    std::string sIp{ip};
    std::string sOrg{org};
    std::string sYear{year};

    for (unsigned int x : toHandle) {
        std::string baseIRI = handlers[x].getGraphIRI();
        std::string valueIRI = baseIRI+"values/"+std::to_string(cc_newcounter);
        clock_t  t = clock();
        handlers[x].addValueTriplet(valueIRI,BASIC_PROPERTY "Id",STRING_VALUE(std::to_string(cc_newcounter)));
        if (isLeft) {
            handlers[x].addValueTriplet(valueIRI,BASIC_PROPERTY "Ip1",STRING_VALUE(sIp));
            handlers[x].addValueTriplet(valueIRI,BASIC_PROPERTY "Organization1",STRING_VALUE(sOrg));
            handlers[x].addValueTriplet(valueIRI,BASIC_PROPERTY "Year1",STRING_VALUE(sYear));
        } else {
            handlers[x].addValueTriplet(valueIRI,BASIC_PROPERTY "Ip2",STRING_VALUE(sIp));
            handlers[x].addValueTriplet(valueIRI,BASIC_PROPERTY "Organization2",STRING_VALUE(sOrg));
            handlers[x].addValueTriplet(valueIRI,BASIC_PROPERTY "Year2",STRING_VALUE(sYear));
        }
        timing[x] += (clock()-t);
    }
}

unsigned int sampleVirtuoso::getVertexOutSizeByNewId(int cc_newcounter) {
    clock_t  t = clock();
    unsigned int minId = *toHandle.begin();
    std::string valueIRI = handlers[minId].getGraphIRI()+"values/"+std::to_string(cc_newcounter);
    int toret = handlers[minId].countOutgoingEdges(valueIRI,BASIC_EDGE);
    timing[minId] += (clock()-t);
    return (unsigned int) toret;
}
