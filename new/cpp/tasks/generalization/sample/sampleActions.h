//
// Created by Giacomo Bergami on 20/08/16.
//

#pragma once

#include "../../../io/mappers.h"

class sampleActions {
public:
    /**
     * Stores a graph within the choosed library
     * @param cc_newcounter     Vertex ID
     * @param oldid             Old Vertex ID
     * @param hash              Current vertex's hash
     * @param myData            Data associated to the vertex
     */
    virtual void addNewVertex(int cc_newcounter, int oldid,unsigned int hash,  VAEntry_updating* myData) = 0;

    /**
     * Adds an edge between two vertices that were already stored in the library/graph database
     * through the addNewVertex method
     * @param src
     * @param srcHash
     * @param dst
     * @param dstHash
     */
    virtual void addNewEdge(unsigned int src, unsigned int srcHash, unsigned int dst, unsigned int dstHash) = 0;

    /**
     * This function tells the serializer that the process of sampling is finished for the
     * graph of size maxID. In this case a graph database closes the connection witht the
     * current graph, while a graph library serializes the content to secondary memory
     * @param path     If the class samples with a graph library, where to store the graph
     * @param maxId    If the class stores a graph into a database, tells which connection has
     *                 to terminate
     */
    virtual void serialize(std::string path, int maxId) = 0;

    /**
     * Returns the current outgoing vertices size
     * @param cc_newcounter
     * @return
     */
    virtual unsigned int getVertexOutSizeByNewId(int cc_newcounter) = 0;

    /**
     * This function is designed for graph databases.
     * In this case I'm telling to the sampler:
     * @param basePath      In which folder (or uri) I want to store my graph
     * @param prepareSizes  Which are the graph sizes that are going to be used
     */
    virtual void prepareSerialization(std::string basePath, std::vector<unsigned int> prepareSizes) = 0;
};


