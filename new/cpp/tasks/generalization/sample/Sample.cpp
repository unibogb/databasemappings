//
// Created by Giacomo Bergami on 16/08/16.
//

#include "Sample.h"





Sample::Sample(int samplerVertex, int samplerSkip, int samplerNext, double jumpProb, const int nVert, char *vaoff,
               INDEXFILE *idx, std::string solution)

        : jumpProb{jumpProb}, rj{0.0,1.0}, rv{0,nVert}, algorithm{SampleFactory::generateInstance(solution, nVert)}, vaoffset{vaoff}, f{idx}
{
    idConversion = std::vector<int>(nVert, -1);
    //vertices = std::vector<VAEntry_updating*>(nVert);
    rvGen.seed(samplerVertex);
    rjGen.seed(samplerSkip);
    nchoiceGen.seed(samplerVertex);
    counter = 0;

}

double Sample::next(int maxStep) {
    clock_t creation = 0;
    do {
        int v;
        unsigned int hashV;
        int step;
        unsigned int Nusize = VOUT_SSIZE(f[u].id,vaoffset,f);
        EDGES_OUTIN* Nu = VOUT_ARRAY(f[u].id,vaoffset,f);
        //hasToJump
        if ((Nusize>0)&&(!(rj(rjGen)<=jumpProb))) {
            step = 0;

            std::uniform_int_distribution<> nuNext(0, Nusize-1);

            do {
                int idGen = nuNext(nchoiceGen);
                v = Nu[idGen].adjacentID;
                hashV = Nu[idGen].adjacentHash;

                bool wasPrev = false;
                if (idConversion[v]==-1) {
                    int cc = counter++;
                    idConversion[v] = cc;
                    VAEntry_updating *w = new VAEntry_updating();
                    partialdescriptorFromVertexID(w, v, f, vaoffset);
                    clock_t t = std::clock();
                    algorithm->addNewVertex(cc,v,hashV,w);
                    creation += (std::clock()-t);
                    /* vertices[cc] = new VAEntry_updating();
                     partialdescriptorFromVertexID(vertices[cc], v, f, vaoffset);
                     vertices[cc]->hash = f[v].hash;
                     vertices[cc]->new_id = cc;
                     inTree.insertKey(f[v].hash)->add(cc);*/
                }

                //neighs[u].add(v); //E(u,v)
                clock_t t = std::clock();
                algorithm->addNewEdge(idConversion[u],hashU,idConversion[v],hashV);
                creation += (std::clock()-t);
                //vertices[idConversion[u]]->outEdges.emplace_back(hashV,idConversion[v]);
                //vertices[idConversion[v]]->inEdges.emplace_back(hashU,idConversion[u]);

                ///XXXX
            } while ((algorithm->getVertexOutSizeByNewId(idConversion[v]) == VOUT_SSIZE(f[v].id,vaoffset,f))&&(step++<Nusize));

            if (step <= Nusize) {
                u = v;
                hashU = hashV;
            } else {
                u = rv(rvGen);
                hashU = f[u].hash;
                bool hasCheck = false;
                if (idConversion[u]==-1) {
                    int cc = counter++;
                    idConversion[u] = cc;
                    //vertices[cc] = new VAEntry_updating();
                    //partialdescriptorFromVertexID(vertices[cc], u, f, vaoffset);
                    //inTree.insertKey(f[u].hash)->add(cc);
                    //vertices[cc]->hash = f[u].hash;
                    //vertices[cc]->new_id = cc;
                    VAEntry_updating *w = new VAEntry_updating();
                    partialdescriptorFromVertexID(w, u, f, vaoffset);
                    hashU = f[u].hash;
                    clock_t t = std::clock();
                    algorithm->addNewVertex(cc,u,hashU,w);
                    creation += (std::clock()-t);

                    hasCheck = true;
                }
            }
        } else {
            u = rv(rvGen);
            if (idConversion[u]==-1) {
                int cc = counter++;
                idConversion[u] = cc;

                VAEntry_updating *w = new VAEntry_updating();
                partialdescriptorFromVertexID(w, u, f, vaoffset);
                hashU = f[u].hash;
                clock_t t = std::clock();
                algorithm->addNewVertex(cc,u,hashU,w);
                creation += (std::clock()-t);
            }
        }
    } while (counter<maxStep);
    return ((double)creation) / (CLOCKS_PER_SEC / 1000.0);
}

double Sample::serializeToFolder(int pace, std::string s, std::string lib) {
    clock_t  c = std::clock();
    algorithm->serialize(s,pace);
    return ((double)(std::clock() - c))/(CLOCKS_PER_SEC / 1000.0);
}

void Sample::init(std::string initialPath, std::vector<unsigned int> &vec) {
    u = rv(rvGen);
    int cc = counter++;

    idConversion[u] = cc;
    VAEntry_updating *v = new VAEntry_updating();
    partialdescriptorFromVertexID(v, u, f, vaoffset);
    hashU = f[cc].hash;

    algorithm->prepareSerialization(initialPath,vec);
    algorithm->addNewVertex(cc,u,hashU,v);
    //inTree.insertKey(f[cc].hash)->add(cc);
    //vertices[cc]->hash = f[cc].hash;

    //vertices[cc]->new_id = cc;
}

Sample::~Sample() {
    delete algorithm;
}

