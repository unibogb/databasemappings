//
// Created by Giacomo Bergami on 24/08/16.
//

#include "Join.h"
#include "joinFactory.h"
#include "concrete/joinProposed.h"
#include "concrete/snapJoinOthers.h"
#include "concrete/boostJoinOthers.h"
#include "concrete/sparkseeJoinOthers.h"
#include "concrete/virtuosoJoinSPARQL.h"

Join *joinFactory::generateInstance(std::string name) {
    if (!name.compare("proposed")) {
        return new Proposed();
    } else if (!name.compare("snap")) {
        return new snapJoinOthers();
    } else if (!name.compare("boost")) {
        return new boostJoinOthers();
    } else if (!name.compare("sparksee")) {
        return new sparkseeJoinOthers();
    } else if (!name.compare("virtuoso")) {
        return new virtuosoJoinSPARQL();
    } else return nullptr;
}
