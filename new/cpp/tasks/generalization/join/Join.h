//
// Created by Giacomo Bergami on 24/08/16.
//

#pragma once
#include <string>
#include <map>

    class Join {
    public:
        double storageTime, algorithmTime, loadingTime;
        virtual double EqJoin(std::string leftPath, std::string rightPath, std::string resultPath,
                               std::map<std::string, std::string> join,
                               std::string solutionStorage, std::string solutionAccess) = 0;
    };

