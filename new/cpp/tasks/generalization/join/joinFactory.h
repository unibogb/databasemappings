//
// Created by Giacomo Bergami on 24/08/16.
//

#pragma once
#include "Join.h"


#include <string>

class joinFactory {
public:
    static Join* generateInstance(std::string name);
};

