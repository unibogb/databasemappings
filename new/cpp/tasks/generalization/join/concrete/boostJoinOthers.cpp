//
// Created by Giacomo Bergami on 28/08/16.
//

#include "boostJoinOthers.h"

boostJoinOthers::boostJoinOthers() {}

void boostJoinOthers::initVertexIteration(bool isLeft) {
    currentGraph = isLeft;
    std::pair<graph_t::vertex_iterator ,graph_t::vertex_iterator> cp = boost::vertices((isLeft ? left : right));
    ciaoni = cp.first;
    endni = cp.second;
}

bool boostJoinOthers::hasNextVertex()       { return ciaoni != endni; }

void boostJoinOthers::next()                { ciaoni++; }

unsigned long boostJoinOthers::current()    { return (unsigned int) *ciaoni; }

unsigned int boostJoinOthers::currentHash() { return (currentGraph ? left : right)[*ciaoni].hash;  }

void boostJoinOthers::initVertexInnerIteration(unsigned long ptr, bool isLeft) {
    if (isLeft) {
        std::pair<graph_t::adjacency_iterator ,graph_t::adjacency_iterator> cp = boost::adjacent_vertices(ptr, left);
        outerleftCURR = cp.first;
        outerleftMAX = cp.second;
    } else {
        std::pair<graph_t::adjacency_iterator ,graph_t::adjacency_iterator> cp =  boost::adjacent_vertices(ptr, right);
        outerrightCURR = cp.first;
        outerrightMAX = cp.second;
    }
}

bool boostJoinOthers::hasNextInnerVertex(bool isLeft) {
    return (isLeft ? outerleftCURR!=outerleftMAX : outerrightCURR!=outerrightMAX);
}

void boostJoinOthers::nextInner(bool isLeft) {
    if (isLeft) outerleftCURR++; else outerrightCURR++;
}

unsigned long boostJoinOthers::currentInner(bool isLeft) {
    return (isLeft ? *outerleftCURR : *outerrightCURR);
}

unsigned int boostJoinOthers::currentHashInner(bool isLeft) {
    return (currentGraph ? left : right)[(isLeft ? *outerleftCURR : *outerrightCURR)].hash;
}

bool boostJoinOthers::doTheyMatch(unsigned long left, unsigned long right) {
    for (std::pair<std::string,std::string> x : join) {
        std::string leftValue{""}, rightValue{""};
        if (!x.first.compare(0,2,"Id")) {
            leftValue = this->left[left].ip;
        } else if (!x.first.compare(0,12,"Organization")) {
            leftValue = this->left[left].organization;
        } else if (!x.first.compare(0,4,"year")) {
            leftValue = std::to_string(this->left[left].year);
        }
        if (!x.second.compare(0,2,"Id")) {
            leftValue = this->right[right].ip;
        } else if (!x.second.compare(0,12,"Organization")) {
            leftValue = this->right[right].organization;
        } else if (!x.second.compare(0,4,"year")) {
            leftValue = std::to_string(this->right[right].year);
        }

        if (leftValue.compare(rightValue)) return false;
    }
    return true;
}

void boostJoinOthers::init(std::string leftPath, std::string rightPath, std::string resultPath,
                           std::map<std::string, std::string> join, std::string solutionStorage,
                           std::string solutionAccess) {

    {
        leftPath = leftPath.substr(0, leftPath.size()-1);
        std::ifstream file(leftPath+".boost",std::ios_base::binary);
        boost::archive::binary_iarchive toStore{file};
        toStore >> left;
    }
    {
        rightPath = rightPath.substr(0, rightPath.size()-1);
        std::ifstream file(rightPath+".boost",std::ios_base::binary);
        boost::archive::binary_iarchive toStore{file};
        toStore >> right;
    }


    for (std::pair<std::string,std::string> x : join) {
        this->join.emplace(x.first.substr(0, x.first.size()-1),x.second.substr(0, x.second.size()-1));
    }
}

