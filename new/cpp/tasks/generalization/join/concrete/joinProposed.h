//
// Created by Giacomo Bergami on 17/08/16.
//

#pragma once

#include <string>
#include <map>

    class Join;
    class Proposed : public Join {
    public:
        double EqJoin(std::string leftPath, std::string rightPath, std::string resultPath,
                       std::map<std::string, std::string> join,
                       std::string solutionStorage, std::string solutionAccess);

    };
