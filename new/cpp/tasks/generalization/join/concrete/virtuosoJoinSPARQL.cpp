//
// Created by Giacomo Bergami on 11/09/16.
//

#include <iostream>
#include "virtuosoJoinSPARQL.h"
#include "../../../graphs/virtuosoredland/Virtuoso.h"

double virtuosoJoinSPARQL::EqJoin(std::string leftPath, std::string rightPath, std::string resultPath,
                                  std::map<std::string, std::string> join, std::string solutionStorage,
                                  std::string solutionAccess) {

    std::string qFirst{"CONSTRUCT { \n"
                               "\t?newSrc <http://jackbergus.alwaysdata.net/graph> \"Result\";\n"
                               "\t      <http://jackbergus.alwaysdata.net/edges/result> ?newDst; \n"
                               "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip1;\n"
                               "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org1;\n"
                               "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y1;\n"
                               "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip2;\n"
                               "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org2;\n"
                               "\t      <http://jackbergus.alwaysdata.net/property/Year2> ?y2.\n"
                               "\t?newDst <http://jackbergus.alwaysdata.net/graph> \"Result\";\n"
                               "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip3;\n"
                               "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org3;\n"
                               "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y3;\n"
                               "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip4;\n"
                               "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org4;\n"
                               "\t      <http://jackbergus.alwaysdata.net/property/Year2> ?y4.\n"
                               "} \n"
                               "FROM NAMED <"};
    qFirst += leftPath;
    qFirst += ">\n"
            "FROM NAMED <";
    qFirst += rightPath;
    qFirst += ">\n"
            "WHERE\n"
            "{\n"
            "  GRAPH ?g { \n"
            "  \t\t?src1 <http://jackbergus.alwaysdata.net/property/Id> ?id1;\n"
            "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip1;\n"
            "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org1;\n"
            "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y1.\n"
            "  \t}.\n"
            "  GRAPH ?h { \n"
            "  \t\t?src2 <http://jackbergus.alwaysdata.net/property/Id> ?id2;\n"
            "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip2;\n"
            "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org2;\n"
            "\t      <http://jackbergus.alwaysdata.net/property/Year2> ?y2.\n"
            "  \t}\n"
            "  filter(?g=<";
    qFirst += leftPath;
    qFirst += "> && \n"
            "         ?h=<";
    qFirst += rightPath;
    qFirst += "> &&\n"
            "         ( ?org1 = ?org2 ) && ( ?y1 =  ?y2 ))\n"
            "         \n"
            "  BIND (URI(CONCAT(\"http://jackbergus.alwaysdata.net/values/\",?id1,\"-\",?id2)) AS ?newSrc)\n"
            "  \n"
            "  OPTIONAL {\n"
            "  \t\tGRAPH ?g { \n"
            "  \t\t\t?src1 <http://jackbergus.alwaysdata.net/edges/edge> ?dst1.\n"
            "  \t\t\t?dst1 <http://jackbergus.alwaysdata.net/property/Id> ?id3;\n"
            "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip3;\n"
            "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org3;\n"
            "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y3.\n"
            "  \t\t}.\n"
            "\t\tGRAPH ?h { \n"
            "\t\t\t?src2 <http://jackbergus.alwaysdata.net/edges/edge> ?dst2.\n"
            "\t\t\t?dst2 <http://jackbergus.alwaysdata.net/property/Id> ?id4;\n"
            "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip4;\n"
            "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org4;\n"
            "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y4.\n"
            "  \t\t}\n"
            "\t\tFILTER ( ( ?org3 = ?org4 ) && ( ?y3 = ?y4 ) )\n"
            "\t\tBIND (URI(CONCAT(\"http://jackbergus.alwaysdata.net/values/\",?id3,\"-\",?id4)) AS ?newDst)\n"
            "\t}\n"
            "}";

    Virtuoso db{};
    VirtuosoQuery* q = db.compileQuery(qFirst);
    clock_t t = clock();
    q->operator()();
    t = clock() - t;
    std::cout << ((double)t)/(((double)CLOCKS_PER_SEC)/1000.0) << std::endl;
}