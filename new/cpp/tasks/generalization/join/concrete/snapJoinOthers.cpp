//
// Created by Giacomo Bergami on 26/08/16.
//


#include <iostream>
#include "snapJoinOthers.h"
#include "../../result/ResultFactory.h"

void snapJoinOthers::init(std::string leftPath, std::string rightPath, std::string resultPath,
                          std::map<std::string, std::string> join, std::string solutionStorage,
                          std::string solutionAccess) {

    leftPath = leftPath.substr(0, leftPath.size()-1);
    rightPath = rightPath.substr(0, rightPath.size()-1);
    {
        std::string lp = leftPath+".snap";
        TFIn fin{lp.c_str()};
        std::unique_ptr<TNodeNet<TAttr>> p1(new TNodeNet<TAttr>(fin));
        left = std::move(p1);
        if (left->Empty()) {
            std::cerr << "Empty" << std::endl;
            exit(23);
        }
    }
    {
        std::string rp = rightPath+".snap";
        TFIn fin{rp.c_str()};
        std::unique_ptr<TNodeNet<TAttr>> p1(new TNodeNet<TAttr>(fin));
        right = std::move(p1);
    }
    solution = ResultFactory::generateInstance(solutionStorage,resultPath+"bulk_result.bin");
    for (std::pair<std::string,std::string> x : join) {
        this->join.emplace(x.first.substr(0, x.first.size()-1),x.second.substr(0, x.second.size()-1));
    }

}

void snapJoinOthers::initVertexIteration(bool isLeft) {
    ciaoni = (isLeft ? left : right)->BegNI();
    endni = (isLeft ? left : right)->EndNI();
}

bool snapJoinOthers::hasNextVertex() {
    return ciaoni != endni;
}

void snapJoinOthers::next() {
    ciaoni++;
}

unsigned int snapJoinOthers::current() {
    return (unsigned int) ciaoni.GetId();
}

unsigned int snapJoinOthers::currentHash() {
    TStr hashAttribute{"hash"},val;
    ciaoni.GetDat().GetSAttrDat(ciaoni.GetId(),hashAttribute,val);
    //std::cerr << "nl=" << val() << std::endl;
    //std::cerr << "val=" << std::stoul(val()) << std::endl;
    return (unsigned int) std::stoul(val());
}

void snapJoinOthers::initVertexInnerIteration(unsigned int ptr, bool isLeft) {
    if (isLeft) {
        currLeft = left->GetNI(ptr);
        outerleftMAX = currLeft.GetOutDeg();
        outerleftCURR = 0;
    } else {
        currRight = right->GetNI(ptr);
        outerrightMAX = currRight.GetOutDeg();
        outerrightCURR = 0;
    }
}

bool snapJoinOthers::hasNextInnerVertex(bool isLeft) {
    return (isLeft ? (outerleftCURR<outerleftMAX) : (outerrightCURR<outerrightMAX));
}

void snapJoinOthers::nextInner(bool isLeft) {
    if (isLeft) outerleftCURR++; else outerrightCURR++;
}

unsigned int snapJoinOthers::currentInner(bool isLeft) {
    return (unsigned int) (isLeft ? outerleftCURR : outerrightCURR);
}

unsigned int snapJoinOthers::currentHashInner(bool isLeft) {
    TStr tHash;
    (isLeft ? currLeft : currRight).GetDat().GetSAttrDat((isLeft ? currLeft : currRight).GetId(),"hash",tHash);
    return (unsigned int) std::stoul(tHash());
}

bool snapJoinOthers::doTheyMatch(unsigned int left, unsigned int right) {
    TAttr &leftDat = this->left->GetNI(left)(),
          &righDat = this->right->GetNI(right)();
    for (std::pair<std::string,std::string> x : join) {
        TStr leftValue, rightValue;
        leftDat.GetSAttrDat(left,x.first.c_str(),leftValue);
        righDat.GetSAttrDat(right,x.second.c_str(),rightValue);
        if (!leftValue.EqI(rightValue)) return false;
    }
    return true;
}

snapJoinOthers::snapJoinOthers() : leftMap{&compareU_Int}, rightMap{&compareU_Int} {}
