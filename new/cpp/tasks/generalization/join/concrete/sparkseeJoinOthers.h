//
// Created by Giacomo Bergami on 28/08/16.
//

#include <gdb/Query.h>
#include <gdb/Sparksee.h>
#include <gdb/Database.h>
#include <gdb/Session.h>
#include <gdb/Graph.h>
#include <gdb/Objects.h>
#include <gdb/ObjectsIterator.h>
#include <unordered_map>
#include "JoinOthers.h"
#include "../../config/Init.h"
#include "../../../graphs/sparksee/graph_sparksee.h"


class sparkseeJoinOthers : public JoinOthers<sparksee::gdb::oid_t> {
    graph_sparksee left, right;
    std::unordered_map<std::string,std::string> join;
    bool currentParsing;
    sparksee::gdb::attr_t hashLeftAttribute, hashRightAttribute;
    bool firstIteration;

    sparksee::gdb::Objects *vertexIterationBase, *leftObjects, *rightObjects;
    sparksee::gdb::ObjectsIterator* vertexIterator, *leftIterator, *rightIterator;
    sparksee::gdb::oid_t curr,currentLeft,currentRight;

public:
    sparkseeJoinOthers();

/// Initial Linear Scan
    // Iteration that has to be used while linearly scanning the graph
    void initVertexIteration(bool isLeft) ;

    // Checking whether there is another vertex while linearly scanning all the vertices
    bool hasNextVertex() ;

    // Getting the current vertex while linearly scanning all the vertices
    void next();

    sparksee::gdb::oid_t current() ;

    unsigned int currentHash() ;

    /// Iteration over the outgoing edges
    // Iteration that has to be used while checking the outgoing edges
    void initVertexInnerIteration(sparksee::gdb::oid_t ptr, bool isLeft) ;

    // Checking whether there is another vertex while linearly scanning all the vertices
    bool hasNextInnerVertex(bool isLeft) ;

    void nextInner(bool isLeft) ;

    sparksee::gdb::oid_t currentInner(bool isLeft) ;

    unsigned int currentHashInner(bool isLeft) ;

    bool doTheyMatch(sparksee::gdb::oid_t left, sparksee::gdb::oid_t right) ;

    void init(std::string leftPath, std::string rightPath, std::string resultPath,
                      std::map<std::string, std::string> join,
                      std::string solutionStorage, std::string solutionAccess) ;

};

