//
// Created by Giacomo Bergami on 11/09/16.
//

#ifndef CGRAPH2_VIRTUOSOJOINOTHERS_H
#define CGRAPH2_VIRTUOSOJOINOTHERS_H

#include "../Join.h"



class virtuosoJoinSPARQL : public Join {
public:
    double EqJoin(std::string leftPath, std::string rightPath, std::string resultPath,
                          std::map<std::string, std::string> join,
                          std::string solutionStorage, std::string solutionAccess);
};


#endif //CGRAPH2_VIRTUOSOJOINOTHERS_H
