//
// Created by Giacomo Bergami on 27/08/16.
//

#pragma once

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/graph/adj_list_serialize.hpp>
#include "JoinOthers.h"
#include "../../sample/concrete/sampleBoost.h"
#include "../../../graphs/boost/graph_t.h"

#include <iomanip>
#include <iostream>
#include <fstream>

class boostJoinOthers : public JoinOthers<unsigned long> {
    graph_t left, right;
    std::unordered_map<std::string,std::string> join;
    graph_t::vertex_iterator endni, ciaoni;
    bool currentGraph;
    graph_t::adjacency_iterator outerleftMAX, outerleftCURR, outerrightMAX, outerrightCURR;
public:

    boostJoinOthers();

    /// Initial Linear Scan
    // Iteration that has to be used while linearly scanning the graph
    void initVertexIteration(bool isLeft);
    // Checking whether there is another vertex while linearly scanning all the vertices
    bool hasNextVertex();

    // Getting the current vertex while linearly scanning all the vertices
    void next();
    unsigned long current();
    unsigned int currentHash();

    /// Iteration over the outgoing edges
    // Iteration that has to be used while checking the outgoing edges
    void initVertexInnerIteration(unsigned long ptr, bool isLeft);

    // Checking whether there is another vertex while linearly scanning all the vertices
    bool hasNextInnerVertex(bool isLeft);
    void nextInner(bool isLeft);
    unsigned long currentInner(bool isLeft);
    unsigned int currentHashInner(bool isLeft);

    bool doTheyMatch(unsigned long left, unsigned long right);

    void init(std::string leftPath, std::string rightPath, std::string resultPath,
              std::map<std::string, std::string> join,
              std::string solutionStorage, std::string solutionAccess);

};
