//
// Created by Giacomo Bergami on 26/08/16.
//

#pragma once

#include <unordered_map>
#include <memory>
#include "../../../../rbtree/rbtree.h"
#include "JoinOthers.h"
#include "../../../../lib-headers/snap-core/Snap.h"
#include "../../result/resultActions.h"

class snapJoinOthers : public JoinOthers<unsigned int> {
    std::unique_ptr<TNodeNet<TAttr>> left,right;
    resultActions* solution;
    RBTree<unsigned int,unsigned int> leftMap, rightMap;
    std::unordered_map<std::string,std::string> join;
    TNodeNet<TAttr>::TNodeI endni, currRight, currLeft, ciaoni;
    int outerleftMAX, outerleftCURR, outerrightMAX, outerrightCURR;
public:

    snapJoinOthers();

    /// Initial Linear Scan
    // Iteration that has to be used while linearly scanning the graph
    void initVertexIteration(bool isLeft);
    // Checking whether there is another vertex while linearly scanning all the vertices
    bool hasNextVertex();
    // Getting the current vertex while linearly scanning all the vertices
    void next();
    unsigned int current();
    unsigned int currentHash();

    /// Iteration over the outgoing edges
    // Iteration that has to be used while checking the outgoing edges
    void initVertexInnerIteration(unsigned int ptr, bool isLeft);
    // Checking whether there is another vertex while linearly scanning all the vertices
    bool hasNextInnerVertex(bool isLeft);
    void nextInner(bool isLeft);
    unsigned int currentInner(bool isLeft);
    unsigned int currentHashInner(bool isLeft);

    bool doTheyMatch(unsigned int left, unsigned int right);

    void init(std::string leftPath, std::string rightPath, std::string resultPath,
                      std::map<std::string, std::string> join,
                      std::string solutionStorage, std::string solutionAccess);

};
