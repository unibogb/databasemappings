//
// Created by Giacomo Bergami on 25/08/16.
//

#pragma once

#include <vector>
#include <iostream>
#include "../Join.h"
#include "../../../../rbtree/rbtree.h"
#include "../../../../numeric/compares.h"
#include "../../result/resultActions.h"
#include "../../result/ResultFactory.h"

template<typename VertexType>
class JoinOthers : public Join {
public:
    /// Initial Linear Scan
    // Iteration that has to be used while linearly scanning the graph
    virtual void initVertexIteration(bool isLeft) = 0;

    // Checking whether there is another vertex while linearly scanning all the vertices
    virtual bool hasNextVertex() = 0;

    // Getting the current vertex while linearly scanning all the vertices
    virtual void next() =0;

    virtual VertexType current() = 0;

    virtual unsigned int currentHash() = 0;

    /// Iteration over the outgoing edges
    // Iteration that has to be used while checking the outgoing edges
    virtual void initVertexInnerIteration(VertexType ptr, bool isLeft) = 0;

    // Checking whether there is another vertex while linearly scanning all the vertices
    virtual bool hasNextInnerVertex(bool isLeft) = 0;

    virtual void nextInner(bool isLeft) =0;

    virtual VertexType currentInner(bool isLeft) = 0;

    virtual unsigned int currentHashInner(bool isLeft) = 0;

    virtual bool doTheyMatch(VertexType left, VertexType right) = 0;

    virtual void init(std::string leftPath, std::string rightPath, std::string resultPath,
                      std::map<std::string, std::string> join,
                      std::string solutionStorage, std::string solutionAccess) =0;

    double EqJoin(std::string leftPath, std::string rightPath, std::string resultPath,
                  std::map<std::string, std::string> join,
                  std::string solutionStorage, std::string solutionAccess) {

        clock_t t = clock();
        init(leftPath, rightPath, resultPath, join, solutionStorage, solutionAccess);
        t = clock()-t;
        loadingTime = ((double) (clock() - t)) / (((double) CLOCKS_PER_SEC) / 1000.0);;

        RBTree<unsigned int, VertexType> hashSorterLeft{&compareU_Int};
        RBTree<unsigned int, VertexType> hashSorterRight{&compareU_Int};

        //Hashing
        t = clock();

        initVertexIteration(true);
        do {
            hashSorterLeft.insertKey(currentHash())->add(current());
            next();
        } while (hasNextVertex());

        initVertexIteration(false);
        do {
            hashSorterRight.insertKey(currentHash())->add(current());
            next();
        } while (hasNextVertex());

        std::set<unsigned int> hashes;
        {
            typename RBTree<unsigned int, VertexType>::rbiterator itLeft = hashSorterLeft.iterator();
            typename RBTree<unsigned int, VertexType>::rbiterator itRight = hashSorterRight.iterator();


            //Creating the hash for pruning the outgoing elements
            Node<unsigned int, VertexType> *currentLeftBlock = itLeft.next();
            Node<unsigned int, VertexType> *currentRightBlock = itRight.next();
            while (itLeft.hasNext() && itRight.hasNext()) {
                if (itLeft.getCurrentK() < itRight.getCurrentK()) {
                    currentLeftBlock = itLeft.next();
                } else if (itLeft.getCurrentK() > itRight.getCurrentK()) {
                    currentRightBlock = itRight.next();
                } else {
                    hashes.emplace(itLeft.getCurrentK());
                    itLeft.next();
                    itRight.next();
                }
            }
        }

        resultActions* solution = ResultFactory::generateInstance(solutionStorage,resultPath+"bulk_result.bin");
        typename RBTree<unsigned int, VertexType>::rbiterator itLeft = hashSorterLeft.iterator();
        typename RBTree<unsigned int, VertexType>::rbiterator itRight = hashSorterRight.iterator();
        Node<unsigned int, VertexType> *currentLeftBlock = itLeft.next();
        Node<unsigned int, VertexType> *currentRightBlock = itRight.next();

        //Now I perform the Join
        currentLeftBlock = itLeft.next();
        currentRightBlock = itRight.next();
        while (itLeft.hasNext() && itRight.hasNext()) {
            if (itLeft.getCurrentK() < itRight.getCurrentK()) {
                currentLeftBlock = itLeft.next();
            } else if (itLeft.getCurrentK() > itRight.getCurrentK()) {
                currentRightBlock = itRight.next();
            } else {
                // I have an hash match
                typename std::vector<VertexType>::iterator itouterLeft = currentLeftBlock->overflowList.begin();
                while (itouterLeft != currentLeftBlock->overflowList.end()) {
                    typename std::vector<VertexType>::iterator itouterRight = currentRightBlock->overflowList.begin();
                    while (itouterRight != currentRightBlock->overflowList.end()) {
                        if (doTheyMatch(*itouterLeft, *itouterRight)) {
                            //create vertex
                            solution->storeVertex(*itouterLeft,*itouterRight);
                            initVertexInnerIteration(*itouterLeft, true);
                            while (hasNextInnerVertex(true)) {
                                nextInner(true);
                                unsigned int innerLeft = currentInner(true);
                                if (hashes.find(currentHashInner(true)) != hashes.end()) {
                                    initVertexInnerIteration(*itouterRight, false);
                                    while (hasNextInnerVertex(false)) {
                                        nextInner(false);
                                        if (currentHashInner(true) == currentHashInner(false)) {
                                            unsigned int innerRIght = currentInner(false);
                                            if (doTheyMatch(innerLeft,innerRIght)) {
                                                solution->storeVertexInternal(innerLeft,innerRIght);
                                                solution->storeEdge(*itouterLeft,*itouterRight,innerLeft,innerRIght);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        itouterRight++;
                    }
                    itouterLeft++;
                }

                itLeft.next();
                itRight.next();

            }
        }

        solution->save();
        algorithmTime = ((double) (clock() - t)) / (((double) CLOCKS_PER_SEC) / 1000.0);

        return algorithmTime;
    };

};