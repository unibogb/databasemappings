//
// Created by Giacomo Bergami on 28/08/16.
//

#include <gdb/Objects.h>
#include <gdb/ObjectsIterator.h>
#include "sparkseeJoinOthers.h"
#include "../../../../fromJava/System.h"

void sparkseeJoinOthers::initVertexIteration(bool isLeft) {
    currentParsing = isLeft;
    firstIteration = true;
    sparksee::gdb::Graph* g = (isLeft ? left() : right());
    sparksee::gdb::TypeList *typeList = g->FindNodeTypes();
    sparksee::gdb::TypeListIterator *iterator = typeList->Iterator();
    if (vertexIterator) {
        delete vertexIterator;
    }
    vertexIterator = nullptr;
    if (vertexIterationBase) {
        delete vertexIterationBase;
    }
    vertexIterationBase = nullptr;

    while (iterator->HasNext()) {
        sparksee::gdb::Objects *objects = g->Select(iterator->Next());
        if (!vertexIterationBase) {
            vertexIterationBase = objects;
        } else {
            vertexIterationBase = sparksee::gdb::Objects::CombineUnion(vertexIterationBase,objects);
        }
    }
    vertexIterator = vertexIterationBase->Iterator();
    delete iterator;
    delete typeList;
    curr = vertexIterator->Next();
    if (currentParsing) {
        hashLeftAttribute = left()->FindAttribute(left()->GetObjectType(curr), System::ws("hash"));
    } else {
        hashRightAttribute = right()->FindAttribute(right()->GetObjectType(curr),System::ws("hash"));
    }
}

sparkseeJoinOthers::sparkseeJoinOthers() : vertexIterationBase(nullptr), vertexIterator{nullptr}, firstIteration{true} {}

bool sparkseeJoinOthers::hasNextVertex() {
    bool toret = vertexIterator->HasNext();
    if (!toret) {
        delete vertexIterator;
        delete vertexIterationBase;
        vertexIterator = nullptr;
        vertexIterationBase = nullptr;
    }

    return toret;
}

void sparkseeJoinOthers::next() {
    curr = vertexIterator->Next();
    if (currentParsing) {
        hashLeftAttribute = left()->FindAttribute(left()->GetObjectType(curr), System::ws("hash"));
    } else {
        hashRightAttribute = right()->FindAttribute(right()->GetObjectType(curr),System::ws("hash"));
    }
}

sparksee::gdb::oid_t sparkseeJoinOthers::current() {
    return curr;
}

unsigned int sparkseeJoinOthers::currentHash() {
    sparksee::gdb::Graph* g = currentParsing ? left() : right();
    sparksee::gdb::Value v;
    g->GetAttribute(curr,currentParsing ? hashLeftAttribute : hashRightAttribute,v);
    return (unsigned int)v.GetInteger();
}

void sparkseeJoinOthers::initVertexInnerIteration(sparksee::gdb::oid_t ptr, bool isLeft) {
    sparksee::gdb::Graph* g = isLeft ? left() : right();
    if (isLeft) {
        leftObjects = left()->Neighbors(ptr,left.edgeType,sparksee::gdb::EdgesDirection::Outgoing);
        leftIterator = leftObjects->Iterator();
    } else {
        rightObjects = right()->Neighbors(ptr,right.edgeType,sparksee::gdb::EdgesDirection::Outgoing);
        rightIterator = rightObjects->Iterator();
    }
}

bool sparkseeJoinOthers::hasNextInnerVertex(bool isLeft) {
    bool toret = isLeft ? leftIterator->HasNext() : rightIterator->HasNext();
    if (!toret) {
        if (isLeft) {
            delete leftIterator;
            delete leftObjects;
            leftIterator = nullptr;
            leftObjects = nullptr;
        } else {
            delete  rightIterator;
            delete  rightObjects;
            rightIterator = nullptr;
            rightObjects = nullptr;
        }
    }
    return toret;
}

void sparkseeJoinOthers::nextInner(bool isLeft) {
    if (isLeft) {
        currentLeft = leftIterator->Next();
    } else {
        currentRight = rightIterator->Next();
    }
}

sparksee::gdb::oid_t sparkseeJoinOthers::currentInner(bool isLeft) {
    return isLeft ? currentLeft : currentRight;
}

unsigned int sparkseeJoinOthers::currentHashInner(bool isLeft) {
    sparksee::gdb::Graph* g = isLeft ? left() : right();
    sparksee::gdb::Value v;
    g->GetAttribute(isLeft ? currentLeft : currentRight,currentParsing ? hashLeftAttribute : hashRightAttribute,v);
    return (unsigned int)v.GetInteger();
}

bool sparkseeJoinOthers::doTheyMatch(sparksee::gdb::oid_t left, sparksee::gdb::oid_t right) {
    for (std::pair<std::string,std::string> cp : join) {
        sparksee::gdb::attr_t leftAttrId = this->left()->FindAttribute(this->left()->GetObjectType(left), System::ws(cp.first));
        sparksee::gdb::attr_t rightAttrId = this->right()->FindAttribute(this->right()->GetObjectType(right), System::ws(cp.second));
        sparksee::gdb::Value leftValue, rightValue;
        this->left()->GetAttribute(left,leftAttrId,leftValue);
        this->right()->GetAttribute(right,rightAttrId,rightValue);
        if (!leftValue.Equals(rightValue)) return false;
    }
    return true;
}

void sparkseeJoinOthers::init(std::string leftPath, std::string rightPath, std::string resultPath,
                              std::map<std::string, std::string> join, std::string solutionStorage,
                              std::string solutionAccess) {

    left.init(leftPath);
    right.init(rightPath);
    for (std::pair<std::string,std::string> x : join) {
        this->join.emplace(x.first.substr(0, x.first.size()-1),x.second.substr(0, x.second.size()-1));
    }

}

