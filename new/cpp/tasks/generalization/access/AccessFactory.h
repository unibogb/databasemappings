//
// Created by Giacomo Bergami on 21/08/16.
//

#pragma once

#include "accessActions.h"

class AccessFactory {
public:
    static accessActions* generateInstance(std::string type);
};

