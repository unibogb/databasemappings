//
// Created by Giacomo Bergami on 21/08/16.
//

#include "AccessFactory.h"
#include "concrete/accessProposed.h"


accessActions* AccessFactory::generateInstance(std::string type) {
    if (!type.compare("proposed")) {
        return new accessProposed();
    } else return nullptr;
}