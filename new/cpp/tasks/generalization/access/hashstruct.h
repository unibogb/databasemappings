//
// Created by Giacomo Bergami on 21/08/16.
//

#pragma once

class hashstruct {
public:
    unsigned int hash;
    unsigned long offsetLeftStart;
    unsigned long offsetLeftEnd;
    unsigned long offsetRightStart;
    unsigned long offsetRightEnd;
    hashstruct(unsigned int h, unsigned long lS, unsigned long lE, unsigned long rS, unsigned long rE) :
            hash{h}, offsetLeftStart{lS}, offsetLeftEnd{lE}, offsetRightStart{rS}, offsetRightEnd{rE} {};

    hashstruct(const hashstruct& hs) {
        hash = hs.hash;
        offsetLeftEnd = hs.offsetLeftEnd;
        offsetLeftStart = hs.offsetLeftStart;
        offsetRightEnd = hs.offsetRightEnd;
        offsetRightStart = hs.offsetRightStart;
    }

    //Assuming that an hash value only appears once
    bool operator<( const hashstruct& other ) const { return (hash) < (other.hash); }
    bool operator==( const hashstruct& other ) const { return (hash == other.hash); }
};

