//
// Created by Giacomo Bergami on 21/08/16.
//

#pragma once

#include <sstream>
#include <string>
#include <map>
#include <fstream>
#include <vector>
#include <zconf.h>
#include <iostream>
#include "../../../io/file/VAOffsetStructures.h"
#include "../../../io/mappers.h"
#include "../result/resultActions.h"
#include "../../../rbtree/RBTreeSet.h"
#include "../../../numeric/compares.h"
#include "hashstruct.h"
#include "../../../io/file/HashIndexArrayField.h"

class query_chunk {
public:
    char *ptrLeftVal;

    int posLeft, posRight;
    query_chunk(int l, int r) : posLeft{l}, posRight{r}, ptrLeftVal{nullptr} {};
    void compileOverLeftVertex(VAEntry* left) {
        ptrLeftVal = ATTRIBUTE(left,posLeft);
    }
    bool compileOverRightVertex(VAEntry* right) {
        char *ptrRightVal = ATTRIBUTE(right,posRight);
        return (std::strcmp(ptrLeftVal,ptrRightVal) == 0);
    }
};

class query {
    std::vector<query_chunk> qcks;
public:
    void emplace_front(int l, int r) {
        qcks.emplace_back(l,r);
    }

    void compileOverLeftVertex(VAEntry* left) {
        for (int i = 0; i<qcks.size(); i++) {
            qcks[i].compileOverLeftVertex(left);
        }
    }

    bool compileOverRightVertex(VAEntry* right) {
        for (int i = 0; i<qcks.size(); i++) {
            if (!qcks[i].compileOverRightVertex(right)) return false;
        }
        return true;
    }
};

class accessActions {
protected:
    std::map<std::string,int> leftMap, rightMap;
    query        qt1,    qt2;
    RBTreeSet<unsigned long> hashes;     //Used for the edge pruning

public:
    std::vector<hashstruct> hashes_with_offset; // it is ordered because the hash file is ordered, and the search is ordered, too
    accessActions() : hashes{&compareUL_Int} {};

    void initEfficient(std::string leftPath, std::string rightPath,std::map<std::string,std::string>& join, unsigned long leftEnd, unsigned long rightEnd) {
        int count = 0;
        {
            std::ifstream schemaLeft(leftPath+"schema.txt");
            std::string line;
            while (schemaLeft >> line)
            {
                leftMap[line] = count++;
            }
        }
        count = 0;
        {
            std::ifstream schemaRight(rightPath+"schema.txt");
            std::string line;
            while (schemaRight >> line)
            {
                rightMap[line] = count++;
            }
        }
        {
            for (std::pair<std::string,std::string> p : join) {
                qt1.emplace_front(leftMap[p.first],rightMap[p.second]);
                qt2.emplace_front(leftMap[p.first],rightMap[p.second]);
            }
            std::map<std::string,int>::iterator lit = leftMap.begin(), rit = rightMap.begin();
            //std::list<int> leftPos, sharedLeft, sharedRight, rightPos;
            while (lit!=leftMap.end() && rit!=rightMap.end()) {
                int cmp = lit->first.compare(rit->first);
                if (cmp == 0) {
                    qt1.emplace_front(lit->second,rit->second);
                    qt2.emplace_front(lit->second,rit->second);
                    lit++;
                    rit++;
                } else if (cmp < 0 ) {
                    lit++;
                } else {
                    rit++;
                }
            }
        }
        {
            int fdLeft = 0, fdRight = 0;
            unsigned long sizeLeft = 0, sizeRight = 0;
            HASHFILE* left = (HASHFILE*)mmapFile(leftPath+"Hash.bin",&sizeLeft,&fdLeft);
            HASHFILE* right = (HASHFILE*)mmapFile(rightPath+"Hash.bin",&sizeRight,&fdRight);
            unsigned long i = 0, j = 0;
            sizeLeft = sizeLeft / sizeof(HASHFILE);
            sizeRight = sizeRight / sizeof(HASHFILE);

            //bool startLeft = true, startRight = true;
            while ((i < sizeLeft)&&(j < sizeRight)) {
                //std::cout << "hash = "<< left[i].hash << " offset=" << left[i].offset << " bbb="<< left[i].bbb << std::endl;
                if (left[i].hash < right[j].hash) i++;
                else if (left[i].hash > right[j].hash) j++;
                else {

                    unsigned long hash = left[i].hash;
                    if (hash==4150056173)
                        std::cout << "BREAK" << std::endl;
                    unsigned long leftStart = left[i].offset;
                    unsigned long rightStart = right[i].offset;
                    i++;
                    j++;
                    unsigned long leftEnd = (i== sizeLeft) ? sizeLeft*sizeof(HASHFILE) : left[i+1].offset;
                    unsigned long rightEnd = (j==sizeRight) ? sizeRight*sizeof(HASHFILE) : right[i+1].offset;

                    //std::cout << hash << " " << leftStart << " " << leftEnd << " " << rightStart << " " << rightEnd << std::endl;
                    hashes.add(hash);
                    hashes_with_offset.emplace_back(hash,leftStart,leftEnd,rightStart,rightEnd);
                }
            }
            // quit hashes file
            close(fdLeft);
            close(fdRight);
        }
    }

    void performSequentialInnerJoin() {

        unsigned int NuSize = getLeftSize();
        unsigned int NvSize = getRightSize();
        for (int i=0; i<NuSize; i++) {
            for (int j=0; j<NuSize; j++) {

            }
        }
    }

    void performBucketedInnerJoin(unsigned int uId, unsigned int vId, INDEXFILE *indexLeft, INDEXFILE *indexRight,char*  fileOffsetLeft, char* fileOffsetRight, resultActions* solution) {
        int i = 0; int j = 0;
        unsigned int hL = getIthHash(i,false),  hR = getIthHash(j,true);
        //assert(Nu[i].adjacentHash==);
        unsigned int NuSize = getLeftSize();
        unsigned int NvSize = getRightSize();
        while ((i<NuSize) && (j<NvSize)) {
            if (hL< hR) {
                i++;
                hL = getIthHash(i,false);
            } else if (hL>hR) {
                j++;
                hR = getIthHash(j,true);
            } else {
                //std::cout << "Hash = " << hL << std::endl;
                int ii = i, jj = j;
                while (getIthHash(ii,false) == hL) {
                    jj = j;
                    VAEntry up;
                    unsigned int upId = indexLeft[i].id;
                    descriptorFromVertexID(&up,indexLeft[i].id,indexLeft,fileOffsetLeft);
                    //qt2.compileOverLeftVertex(&up);
                    loadVertexById(&up,false,true);
                    while (getIthHash(jj,true) == hR) {
                        VAEntry vp;
                        unsigned int vpId = indexRight[j].id;
                        descriptorFromVertexID(&vp,indexRight[j].id,indexRight,fileOffsetRight);
                        loadVertexById(&vp,true,true);
                        if (testPasses(&vp,true)) {
                            solution->storeEdge(uId,vId,upId,vpId);
                        }
                        jj++;
                    }
                    ii++;
                }
                i = ii;
                j = jj;
                hL = getIthHash(i,false);
                hR = getIthHash(j,true);
            }
        }
    }
    
    virtual int loadVertexById(VAEntry* va, bool isRight, bool isInner) = 0;
    virtual bool testPasses(VAEntry* va, bool isInner) = 0;
    virtual unsigned int getLeftSize() = 0;
    virtual unsigned int getRightSize() = 0;
    virtual unsigned int getIthHash(int pos, bool isRight) = 0;
};