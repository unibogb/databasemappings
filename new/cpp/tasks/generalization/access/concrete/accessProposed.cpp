//
// Created by Giacomo Bergami on 24/08/16.
//

#include "accessProposed.h"
#include "../../../../io/file/VAOffsetStructures.h"
#include "../../../../io/mappers.h"

int accessProposed::loadVertexById(VAEntry *va, bool isRight, bool isInner) {
    if (!isInner) {
        if (! isRight) {
            NuSize = VOUT_SIZE(va);
            Nu = VOUT(va);
            qt1.compileOverLeftVertex(va);
        } else {
            NvSize = VOUT_SIZE(va);
            Nv = VOUT(va);
        }
    } else {
        if (!isRight) {
            qt2.compileOverLeftVertex(va);
        }
    }
    return va->vertexHeader->id;
}

bool accessProposed::testPasses(VAEntry *va, bool isInner) {
    return (isInner ?  qt2.compileOverRightVertex(va) : qt1.compileOverRightVertex(va));
}

unsigned int accessProposed::getLeftSize() {
    return NuSize;
}

unsigned int accessProposed::getRightSize() {
    return NvSize;
}

unsigned int accessProposed::getIthHash(int pos, bool isRight) {
    return (isRight ? Nv : Nu)[pos].adjacentHash;
}
