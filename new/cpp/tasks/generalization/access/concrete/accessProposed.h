//
// Created by Giacomo Bergami on 21/08/16.
//

#pragma once

#include "../accessActions.h"


class accessProposed : public accessActions {
    unsigned int NuSize, NvSize;
    EDGES_OUTIN  *Nu,    *Nv;

public:
    int loadVertexById(VAEntry* va, bool isRight, bool isInner);;

    bool testPasses(VAEntry* va, bool isInner);;
    unsigned int getLeftSize();
    unsigned int getRightSize();
    unsigned int getIthHash(int pos, bool isRight);
};
