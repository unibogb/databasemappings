#include <iostream>
#include <getopt.h>

#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <fstream>
#include <vector>
#include <set>

#include <gdb/Sparksee.h>
#include <gdb/Database.h>
#include <gdb/Session.h>
#include <gdb/Graph.h>
#include <gdb/Objects.h>
#include <gdb/ObjectsIterator.h>
#include <gdb/common.h>
#include <dlfcn.h>


#include "tasks/generalization/join/Join.h"
#include "fromJava/evaluateHashes.h"
#include "tasks/generalization/sample/Sample.h"
#include "fromJava/javaUtils.h"
#include "tasks/storeCSV.h"
#include "tasks/generalization/join/concrete/joinProposed.h"
#include "tasks/generalization/join/Join.h"
#include "tasks/generalization/join/joinFactory.h"
#include "tasks/generalization/config/Init.h"
#include "lib-headers/snap-core/Snap.h"
#include "tasks/generalization/sample/concrete/sampleVirtuoso.h"
#include "tasks/graphs/virtuosoredland/utils/SchemaValues.h"

#define _LIBCPP_DEBUG_LEVEL 2



int main(int argc, char**argv) {
    /*TFIn FIn("/Users/vasistas/cgraph2/data/left/5.snap");
    TNodeNet<TAttr>* ptr = new TNodeNet<TAttr>(FIn);
    for (TNodeNet<TAttr>::TNodeI x = ptr->BegNI(); x!=ptr->EndNI(); x++) {
        TStr hashAttribute{"hash"},val;
        x.GetDat().GetSAttrDat(x.GetId(),hashAttribute,val);
        std::cout << val.CStr() << std::endl;
    }
    exit(323);*/

    /*{
        TNodeNet<TAttr> g;
        TAttr v;
        TStr hashAttribute{"hash"};
        TInt Boris_3_13;
        TStr tHash{"123"}, w;
        v.AddSAttr(hashAttribute, atStr, Boris_3_13);
        v.AddSAttrDat(444, Boris_3_13, tHash);
        g.AddNode(444, v);

        TFOut FOut("/Users/vasistas/test.snap");
        g.Save(FOut);
    }

    TNodeNet<TAttr>* ptr = new TNodeNet<TAttr>(FIn);

    TStr w;
    ptr->GetNI(444).GetDat().GetSAttrDat(444,"hash",w);
    char* elem = w.CStr();
    std::cout << elem  << std::endl;
    delete  ptr;
    exit(39);*/

    //entryPoint(argc,argv);
    Init i;
    i.init(argc,argv);
    i.run();
    i.quit();
}

#include "rbtree/Node.h"
#include "rbtree/rbtree.h"


#include "io/mappers.h"
#include "io/serializers.h"
#include "io/file/HashIndexArrayField.h"

