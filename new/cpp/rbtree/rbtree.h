//
// http://cplusplus.kurttest.com/notes/llrb.html
//

#pragma once

#include "Node.h"

#define NODECOLOR(n) ((!(n)) ? Color::BLACK : ((n)->color));

template <typename K, typename V> class RBTree {



    //// INSERTION
    void insertCase1(Node<K,V>* n) {
        if (!(n->parent))
            n->color = Color::BLACK;
        else
            insertCase2(n);
    }
    void insertCase2(Node<K,V>* n) {
        Color c = NODECOLOR(n->parent);
        if (c == Color::BLACK)
            return; // Tree is still valid
        else insertCase3(n);
    }
    void insertCase3(Node<K,V>* n) {
        Color c = NODECOLOR(n->uncle());
        if (c == Color::RED) {
            n->parent->color = Color::BLACK;
            n->uncle()->color = Color::BLACK;
            n->grandparent()->color = Color::RED;
            insertCase1(n->grandparent());
        } else {
            insertCase4(n);
        }
    }
    void insertCase4(Node<K,V>* n) {
        if ((n == n->parent->right) &&
                (n->parent == n->grandparent()->left)) {
            rotateLeft(n->parent);
            n = n->left;
        } else if (n == n->parent->left && n->parent == n->grandparent()->right) {
            rotateRight(n->parent);
            n = n->right;
        }
        insertCase5(n);
    }
    void insertCase5(Node<K,V>* n) {
        n->parent->color = Color::BLACK;
        n->grandparent()->color = Color::RED;
        if (n == n->parent->left && n->parent == n->grandparent()->left) {
            rotateRight(n->grandparent());
        } else {
            rotateLeft(n->grandparent());
        }
    }


    /// DELETION
    void deleteCase1(Node<K, V>* n) {
        if (!(n->parent))
            return;
        else deleteCase2(n);
    }
    void deleteCase2( Node<K,V>* n) {
        if ((n->sibling()->color)->color == Color::RED) {
            n->parent->color = Color::RED;
            n->sibling()->color = Color::BLACK;
            if (n == n->parent->left)
                rotateLeft(n->parent);
            else
                rotateRight(n->parent);
        }
        deleteCase3(n);
    }
    void deleteCase3(Node<K,V> n) {
        if ((n->parent)->color == Color::BLACK &&
            (n->sibling())->color == Color::BLACK &&
            (n->sibling()->left)->color == Color::BLACK &&
            (n->sibling()->right)->color == Color::BLACK)
        {
            n->sibling()->color = Color::RED;
            deleteCase1(n->parent);
        }
        else
            deleteCase4(n);
    }
  void deleteCase4(Node<K,V>* n) {
        if ((n->parent)->color == Color::RED &&
            (n->sibling())->color == Color::BLACK &&
            (n->sibling()->left)->color == Color::BLACK &&
            (n->sibling()->right)->color == Color::BLACK)
        {
            n->sibling()->color = Color::RED;
            n->parent.color = Color::BLACK;
        }
        else
            deleteCase5(n);
    }
   void deleteCase5(Node<K,V>* n) {
        if (n == n->parent.left &&
            (n->sibling())->color == Color::BLACK &&
            (n->sibling()->left)->color == Color::RED &&
            (n->sibling()->right)->color == Color::BLACK)
        {
            n->sibling()->color = Color::RED;
            n->sibling()->left.color = Color::BLACK;
            rotateRight(n->sibling());
        }
        else if (n == n->parent.right &&
                 (n->sibling())->color == Color::BLACK &&
                 (n->sibling()->right)->color == Color::RED &&
                 (n->sibling()->left)->color == Color::BLACK)
        {
            n->sibling()->color = Color::RED;
            n->sibling()->right.color = Color::BLACK;
            rotateLeft(n->sibling());
        }
        deleteCase6(n);
    }
   void  deleteCase6(Node<K,V>* n) {
        n->sibling()->color = (n->parent);
        n->parent.color = Color::BLACK;
        if (n == n->parent.left) {
            n->sibling()->right.color = Color::BLACK;
            rotateLeft(n->parent);
        } else {
            n->sibling()->left.color = Color::BLACK;
            rotateRight(n->parent);
        }
    }




public:
    bool VERIFY_RBTREE = false;
    int count;
    Node<K,V>* root;
    void (*cmp)(K,K,int*);

    RBTree(void (*comp)(K,K,int*)): root{nullptr}, count{0}, cmp{comp} {}



    void rotateLeft(Node<K,V>* n) {
        Node<K,V>* r = n->right;
        replaceNode(n, r);
        n->right = r->left;
        if (r->left) {
            r->left->parent = n;
        }
        r->left = n;
        n->parent = r;
    }

    void rotateRight(Node<K,V>* n) {
        Node<K,V>* l = n->left;
        replaceNode(n, l);
        n->left = l->right;
        if (l->right) {
            l->right->parent = n;
        }
        l->right = n;
        n->parent = l;
    }
    void replaceNode( Node<K, V>* oldn, Node<K, V>* newn) {
        if (!(oldn->parent)) {
            root = newn;
        } else {
            if (oldn == oldn->parent->left)
                oldn->parent->left = newn;
            else
                oldn->parent->right = newn;
        }
        if (newn) {
            newn->parent = oldn->parent;
        }
    }


    Node<K,V>* lookupNode(K key) {
        Node<K,V>* n = root;
        while (n) {
            int compResult = 0;
            (*cmp)(key,n->key,&compResult);
            if (compResult == 0) {
                return n;
            } else if (compResult < 0) {
                n = n->left;
            } else {
                n = n->right;
            }
        }
        return n;
    }

    Node<K,V>* insertKey(K key) {
        Node<K,V>* insertedNode = new Node<K,V>(key, Color::RED, nullptr, nullptr);
        bool toret = false;
        if (!root) {
            root = insertedNode;
        } else {
            Node<K,V>* n = root;
            while (true) {
                int compResult = 0;
                (*cmp)(key,n->key,&compResult);
                if (compResult == 0) {
                    //n.add(value);
                    return n;
                } else if (compResult < 0) {
                    if (n->left == nullptr) {
                        n->left = insertedNode;
                        toret = true;
                        break;
                    } else {
                        n = n->left;
                    }
                } else {
                    if (n->right == nullptr) {
                        n->right = insertedNode;
                        toret = true;
                        break;
                    } else {
                        n = n->right;
                    }
                }
            }
            insertedNode->parent = n;
        }
        insertCase1(insertedNode);
        //verifyProperties();
        if (toret) count++;
        return insertedNode;
    }

    class rbiterator {

        Node<K,V>* current;
        RBTree<K, V>* root;
        bool justStarted;

    public:
        rbiterator(Node<K,V>* current, RBTree<K, V>* root) {
            this->current = current;
            this->root = root;
            if (this->current) this->current->visit = PositionIteratorEnum::Left;
            justStarted = true;
        }

        rbiterator(rbiterator& elem) {
            current = elem.current;
            root = elem.root;
            justStarted = elem.justStarted;
        }

        rbiterator(rbiterator&& elem) {
            current = elem.current;
            root = elem.root;
            justStarted = elem.justStarted;
        }

        K getCurrentK() {
            if (current && (!justStarted)) return current->key;
            else return (K)-1;
        }

        bool hasNext() {
            return current && current->hasNext();
        }

        Node<K,V>* next() {
            justStarted = false;
            if (!current) return nullptr;
            while (current) {
                switch (current->visit) {
                    case PositionIteratorEnum::Left:{
                        if (current->left) {
                            current->visit = PositionIteratorEnum::Center;
                            current = current->left;
                            current->visit = PositionIteratorEnum::Left; //Initialization of the left node's visit
                        } else {
                            current->visit=PositionIteratorEnum::Center;
                        }
                    } break;

                    case PositionIteratorEnum::Center: {
                        current->visit=PositionIteratorEnum::Right;
                        return current;
                    }

                    case PositionIteratorEnum::Right: {
                        current->visit=PositionIteratorEnum::None;
                        if (current->right) {
                            current = current->right;
                            current->visit = PositionIteratorEnum::Left;
                        } else {
                            current = current->parent;
                        }
                    } break;

                    case PositionIteratorEnum::None:{
                        current = current->parent;
                    } break;
                }
            }
            return nullptr;
        }

    };

    rbiterator iterator() {
        rbiterator toret{root,this};
        return toret;
    }


};
