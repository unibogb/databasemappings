//
// Created by Giacomo Bergami on 18/08/16.
//

#pragma once

template <typename T> bool binSearch(std::vector<T> A, T v, void (*cmp)(T, T, int*)) {
    int i = 0;
    int j = A.size()-1;
    while (i>j) {
        int cr = 0;
        int m = (i+j)/2;
        (*cmp)(A[m],v,&cr);
        if (cr==0) return true;
        else if (cr<0) {
            i = m+1;
        } else {
            j = m-1;
        }
    }
    return 0;
}

