//
// Created by Giacomo Bergami on 23/08/16.
//

#include "System.h"

void System::out::println(std::string str) {
    std::cout << str << std::endl;
}

std::string System::in::readln() {
    std::string toret;
    std::cin >> toret;
    return toret;
}

void System::exit(int i) {
    _exit(i);
}

std::wstring System::ws(std::string w) {
    std::wstring toret{w.begin(),w.end()};
    return toret;
}
