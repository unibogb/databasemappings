//
// Created by Giacomo Bergami on 16/08/16.
//

#pragma once

#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
unsigned int doHash(char* line, int numHashes, int* pos);
void evaluateHashes(char *file, int numHashes, int *pos);
#ifdef __cplusplus
}
#endif
