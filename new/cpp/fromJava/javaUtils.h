//
// Created by Giacomo Bergami on 16/08/16.
//

#pragma once

#ifdef __cplusplus
extern "C" {
#endif
unsigned int hashCode(char* string);
unsigned int hashCodeArray(char** arrayToHash);
#ifdef __cplusplus
}
#endif
