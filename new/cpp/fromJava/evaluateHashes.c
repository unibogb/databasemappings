//
// Created by Giacomo Bergami on 16/08/16.
//

#include "evaluateHashes.h"

#include "../fromJava/javaUtils.h"

unsigned int doHash(char* line, int numHashes, int* pos) {
    unsigned int prime = 17;
    unsigned int result = 3;
    int i = 0;
    int c = 0;
    char* tok;
    for (tok = strtok(line, ",");
         tok && *tok;
         tok = strtok(NULL, ",\n")) {
        if (i++ == pos[c]) {
            result = result * prime + hashCode(tok);
            c++;
        }
    }
    return result;
}



void evaluateHashes(char *file, int numHashes, int *pos) {
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen(file, "r");
    if (fp == NULL)
        exit(1);
    int first = 1;

    while ((read = getline(&line, &len, fp)) != -1) {
        if (first) {
            first = 0;
            printf("Hashes\n");
        } else
            printf("%u\n", doHash(line,numHashes,pos));
    }

    fclose(fp);
    if (line)
        free(line);
}