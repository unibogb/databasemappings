//
// Created by Giacomo Bergami on 23/08/16.
//

#pragma once

#include "Iterator.h"
#include "EndIterator.h"

template <typename T> class Iterable {
public:
    virtual Iterator<T> begin () const = 0;

    Iterator<T> end () const {
        return EndIterator<T>();
    }
};