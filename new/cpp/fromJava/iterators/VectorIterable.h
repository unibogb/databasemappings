//
// Created by Giacomo Bergami on 24/08/16.
//

#pragma once
#include <vector>

class Iterable;
class Iterator;

template <typename T>
class VectorIterable : public Iterable<T>  {

public:
    std::vector<T> vec;
    VectorIterable() {};
    Iterator<T> begin ();

    class Iteratorem : public Iterator<T> {
        long pos = 0;
        std::vector<T>::iterator it;
    public:
        Iteratorem();
        Iteratorem(std::vector<T>::iterator v) : it(v) {}

        long currentPosition() const { return pos; }
        const T peek() const;
        T next();
    };

};



template <typename T> VectorIterable::Iteratorem::Iteratorem() {
    pos = -1;
    it = nullptr;
}

template <typename T> const T VectorIterable::Iteratorem::peek() const {
    return it;
}

template <typename T> T VectorIterable::Iteratorem::next() {
    it++;
    return it;
}

template <typename T> Iterator<T> VectorIterable::begin() {
    return Iteratorem(vec.begin());
}

template <typename T>long VectorIterable::Iteratorem::currentPosition() const { return pos; }

