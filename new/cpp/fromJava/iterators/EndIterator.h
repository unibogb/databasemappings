//
// Created by Giacomo Bergami on 23/08/16.
//

#pragma once

class Iterator;

template <typename T> class EndIterator : public Iterator<T> {
    long currentPosition() const {
        return -1;
    };
    const T peek() const {};
    T next() {};
};
