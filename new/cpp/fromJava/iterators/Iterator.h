//
// Created by Giacomo Bergami on 23/08/16.
//

#pragma once

template <typename T>  class Iterator {
public:
    bool operator!= (const Iterator& other) const {
        return currentPosition() != other.currentPosition();
    }
    bool operator!= (const int& other) const {
        return currentPosition() != other;
    }
    T operator* () const {
        return peek();
    }
    const Iterator& operator++ () {
        next();
        return *this;
    }
    bool hasNext() {
        return *this!=-1;
    }
    virtual long currentPosition() const = 0;
    virtual const T peek() const = 0;
    virtual T next() = 0;

};

