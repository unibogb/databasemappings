//
// Created by Giacomo Bergami on 29/08/16.
//

#pragma once

#include <string>
#include <algorithm>

namespace String {
    template<typename TString>
    inline bool starts_with(const TString& str, const TString& start) {
        if (start.size() > str.size()) return false;
        return str.compare(0, start.size(), start) == 0;
    }

    template<typename TString>
    inline bool ends_with(const TString& str, const TString& end) {
        if (end.size() > str.size()) return false;
        return std::equal(end.rbegin(), end.rend(), str.rbegin());
    }
}