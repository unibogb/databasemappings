//
// Created by Giacomo Bergami on 23/08/16.
//

#pragma once

#include <string>
#include <iostream>
#include <zconf.h>

namespace System {

    class out {
    public:
        static void println(std::string str);
    };

    class in {
    public:
        static std::string readln();
    };

    std::wstring ws(std::string w);

    void exit(int i);

}
