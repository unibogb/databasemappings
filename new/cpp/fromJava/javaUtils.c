//
// Created by Giacomo Bergami on 16/08/16.
//

#include <string.h>
#include "javaUtils.h"

unsigned int hashCode(char* string) {
    unsigned int len = strlen(string), h = 0;
    if (h == 0 && len > 0) {
        for (int i = 0; i < len; i++) {
            h = 31 * h + (unsigned int)string[i];
        }
    }
    return h;
}

unsigned int hashCodeArray(char** arrayToHash) {
    unsigned int prime = 17;
    unsigned int result = 3;
    int i = 0;
    char* check = arrayToHash[i++];
    while (check!=NULL) {
        result = result * prime + hashCode(check);
        check = arrayToHash[i++];
    }
    return result;
}
