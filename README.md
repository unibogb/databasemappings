# databaseMappings
## Giacomo Bergami � 2016

This software is part of my Ph.D Thesis in Bologna University.
The code is released under the [GPL3 Licence](http://www.gnu.org/licenses/gpl-3.0.html),
even when is not explicitely stated within the source code.

### Upgraded version

> **G. Bergami**. �[On Efficiently Equi-Joining Graphs](https://doi.org/10.1145/3472163.3472269)". In Proceedings of 25th International Database Engineering & Applications Symposium ([IDEAS 2021](http://confsys.encs.concordia.ca/IDEAS/ideas21/ideas21.php)), Montreal, Canada, July 14–16, 2021. ACM, New York, NY, USA. ([**Source Code**](https://github.com/gyankos/graphjoin/releases/tag/1.0), [**Dataset**](https://osf.io/xney5/))

This contains the 2021 improved version of the algorithm, which contains the disjunctive semantics as well as the implementation of incremental algorithms for the conjunctive one. A better and different dataset is also considered. Please refer to this version for stable and maintained code.

### Current version

> **G. Bergami**, M. Magnani, D. Montesi. �[A Join Operator for Property Graphs](http://ceur-ws.org/Vol-1810/GraphQ_paper_04.pdf)�. Proceedings of Sixth International Workshop on Querying Graph Structured Data, Venice, Italy, March 2017 (GraphQ 2017). ([Web App](http://jackbergus.alwaysdata.net/joinapp/), [Slides](https://www.slideshare.net/jackbergus/a-join-operator-for-property-graphs), [Dataset](http://smartdata.cs.unibo.it/data/GRAPH/BolognaGraph2016.tar.gz), **[Source Code](https://bitbucket.org/unibogb/databasemappings/)**)

All the source code is released in the `new` folder. The java benchmarks for
Neo4J are placed in the `new/java` folder, while the C++11 tests for the other
benchmarks (Virtuoso, Boost, SNAP, Sparksee) are in `new/cpp`. Our graph join
implementations could be found on the same folders. The dataset used for the benchmarks is available at [this](http://smartdata.cs.unibo.it/data/GRAPH/BolognaGraph2016.tar.gz) link, which was sampled using [this](https://github.com/jackbergus/graphSampler) other project.

#### Java
Further details are provided inside the code: in order to generate the documentation, simply run `mvn javadoc:javadoc`.
In order to run and execute the project, please [learn Maven](https://maven.apache.org/guides/getting-started/) (it's worth it) or import this project in any IDE that supports Maven. The code for the Neo4J Benchmarks is provided in the
`it.giacomobergami.benchmarks`, while `it.giacomobergami.graphdb_stores.AggregateSamplesInNeo4j`
performs the benchmarks over the same data produced in the C++ solution.

#### C++
The C++11 code is compiled in debug mode, so no specific optimizations were
performed. This code was compiled using CMake. Please use CLion for better
user experience.

This solution does not come with all the dependencies, since Virtuoso,
SNAP 3.0 and Redland have to be installed separately. Moreover, all those
libraries have different user licences.

##### Sparksee configuration

* Move the Sparksee dynamic library for MacOS, `libsparksee.dylib`, to the `/new/cpp/libs` folder
 (libsparksee.dylib). Please nothe that the C++ solution was tested only for
 MAC. Eventually move into this folder even `SparkseecppScriptParser`.
* Set your Sparksee code licence into the `/new/cpp/confs/sparksee.conf` configuration
file. Please do not change the other configurations, as those are the default
configurations that we used for our tests.

##### SNAP 3.0

* Move all the header files belonging to the SNAP core headers into the `/new/cpp/lib-headers/snap-core` folder
* Move the Snap static library (`Snap.o`) into `/new/cpp/libs`.

##### Redland RDF & Virtuoso

* Move the files `rdf_hash_internal.h` and `rdf_heuristics.h` from the Redland RDF
  library into the `/new/cpp/lib-headers/missing-librdf` folder.
* The Virtuoso configuration we used for our tests could be found on the
  `/new/cpp/settings_virtuoso/` folder. This operation has to be set by the user,
  as well as the linking between virtuoso and iODBC.
* The graph join SPARQL query is provided in the `virtuosoJoinSPARQL::EqJoin` method.

### Old Draft paper verson

> **G. Bergami**, M. Magnani, D. Montesi. 2016. �[On Joining Graphs](https://arxiv.org/abs/1608.05594)�. CoRR abs/1608.05594 (2016) (**[Source Code](https://bitbucket.org/unibogb/databasemappings/src/graphq2017/draft_java/)**).

All the source code is released in the `draft_java/src` folder: the `main/java` subfolder contains all the algorithms, while the `test/java` one contains the "main" classes that both generate the data (either for our proposed graph database model, either for Neo4J or RDF4J) and query it.

The `generator.properties` file is used to instantiate the graph random walk sampling, the folders where to store the resulting data and where to read the SOC LiveJournal edges as an external reference. The SOC LiveJournal data is enriched through the LDBC protocol as described inside the paper: `main/resources` contains the IP addresses and the company names, while the year of employment is randomly generated. In order to use the YouTube dataset, you could simply specify a different edge file (since both graphs are in the same format)

Further details are provided inside the code: in order to generate the documentation, simply run `mvn javadoc:javadoc`.
In order to run and execute the project, please [learn Maven](https://maven.apache.org/guides/getting-started/) (it's worth it) or import this project in any IDE that supports Maven.
