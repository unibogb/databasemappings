#include <math.h>
#include <stdio.h>

typedef struct cpair {
	unsigned long L,R;
} cpair;


/** Evaluates the dove tailing index given two elements.
*/
unsigned long dt(unsigned long i, unsigned long j) {
	unsigned long r = i+j;
	unsigned long p = r;
	int b = r%2;
	if (!(b)) p = r/2;
	r+=1;
	if (b) r = r/2;
	return p*r+j;
}

cpair dt_inv(unsigned long dt) {
	unsigned long d = sqrtl(dt*8+1);
	unsigned long e = 0;
	unsigned long dold = 0;
	d = (d-1)/2;
	dold = d;
	e = d;
	int b = d%2;
	if (!(b)) d = d/2;
	e+=1;
	if (b) e = e/2;
	e = e*d;
	cpair cp;
	cp.R = dt - e;
	cp.L = dold - cp.R;
	return cp;
}

int main(void) {
	unsigned long res = dt(6,5);
	printf("%lu\n",res);
	cpair cp = dt_inv(res);
	printf("%lu,%lu\n",cp.L, cp.R);
	return 0;
}