/*
 * Paths.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */

package it.giacomobergami;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import it.giacomobergami.data.GeneratorConfiguration;

import java.io.File;

/**
 * This class generates the basic system paths for generating the datasets used for performing the queries
 */
public class Paths {

    public final String pgstore;
    /**
     * Returns the path where the raw graphs are stored
     */
    public final String store;
    public final String soccmel;
    public final String operand;

    @Inject
    public Paths(@Named("propertygraph.data")String pgstore, @Named("graph.proposed.store")String store, @Named("graph.soc.edges")String soccmel, @Named("graph.proposed.sampleOperandFolder")String operand) {
        this.pgstore = pgstore;
        this.store = store;
        this.soccmel = soccmel;
        this.operand = operand;
    }

    /**
     * Folder where all the
     * @return
     */
    public String leqTestRaw(String file) {
        return store + File.separator + "leqtest"+ File.separator + file;
    }

    public String eqTestRaw(String file) {
        return store + File.separator + "downtoearth" + File.separator + file;
    }

    private static Paths instance = null;
    public static Paths getInstance() {
        if (instance == null)
        instance = GeneratorConfiguration.getInstance().getInstance(Paths.class);
        return instance;
    }

}
