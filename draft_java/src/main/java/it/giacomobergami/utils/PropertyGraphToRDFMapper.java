/*
 * PropertyGraphToRDFMapper.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.utils;

import it.giacomobergami.graphs.abstractgraph.AbstractGraph;
import it.giacomobergami.graphs.abstractgraph.AbstractNode;
import it.giacomobergami.graphs.intermediateToRDF.AbstractRDFGraph;
import it.giacomobergami.graphs.nodes.RDFRawResourceVertex;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Converts a Kripke graph into a RDF graph
 */
public class PropertyGraphToRDFMapper {

    public static final String edgeUrl = "http://jackbergus.alwaysdata.net/edges/edge";

    public static <K extends AbstractNode> AbstractRDFGraph createAbstractRDFGraph(AbstractGraph<K> original, AbstractRDFGraph destination, boolean hasNodeId, String defaultNodeid) {
        return original.startTransaction(()-> {
            destination.startTransaction(() -> {
                for (AbstractNode u : original.getVertices()) {
                    String urlSrc = "http://jackbergus.alwaysdata.net/nodes/" + u.getAttribute("MyGraphLabel")+ "/" + original.joinschema(u) + "#" + (hasNodeId ? u.getAttribute(defaultNodeid) : u.hashCode());
                    try {
                        RDFRawResourceVertex urlSrcValue = destination.addVertex(new RDFRawResourceVertex("http://jackbergus.alwaysdata.net/values/" +(hasNodeId ? u.getAttribute(defaultNodeid) : u.hashCode())));
                        RDFRawResourceVertex uCopy = destination.addVertex(new RDFRawResourceVertex(new URI(urlSrc)));
                        for (String property : u.getVertexSchema()) {
                            String propertyUrl = "http://jackbergus.alwaysdata.net/property/" + property;

                            String objectUrl = "http://jackbergus.alwaysdata.net/values/" + u.getAttribute(property);
                            RDFRawResourceVertex pCopy = destination.addVertex(new RDFRawResourceVertex(objectUrl));

                            destination.addEdge(uCopy,propertyUrl,pCopy);
                        }
                        for (AbstractNode v : original.getNeighbours((K)u)) {
                            String urlDstValue = "http://jackbergus.alwaysdata.net/values/" +(hasNodeId ? u.getAttribute(defaultNodeid) : u.hashCode());

                            String urlDst = "http://jackbergus.alwaysdata.net/nodes/" + v.getAttribute("MyGraphLabel")+ "/" + original.joinschema(v) + "#" + (hasNodeId ? v.getAttribute(defaultNodeid) : v.hashCode());
                            RDFRawResourceVertex vCopy = destination.addVertex(new RDFRawResourceVertex(new URI(urlDst)));

                            destination.addEdge(uCopy,edgeUrl,vCopy);
                        }
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }
                return 1;
            });
            return destination;
        }).get();
    }

    public static <K extends AbstractNode> AbstractRDFGraph createAbstractRDFGraphForced(AbstractGraph<K> original, AbstractRDFGraph destination, boolean hasNodeId, String defaultNodeid, String forced) {
        return original.startTransaction(()-> {
            destination.startTransaction(() -> {
                for (AbstractNode u : original.getVertices()) {
                    String urlSrc = "http://jackbergus.alwaysdata.net/nodes/" + forced+ "/" + original.joinschema(u) + "#" + (hasNodeId ? u.getAttribute(defaultNodeid) : u.hashCode());
                    try {
                        RDFRawResourceVertex urlSrcValue = destination.addVertex(new RDFRawResourceVertex("http://jackbergus.alwaysdata.net/values/" +(hasNodeId ? u.getAttribute(defaultNodeid) : u.hashCode())));
                        RDFRawResourceVertex uCopy = destination.addVertex(new RDFRawResourceVertex(new URI(urlSrc)));
                        for (String property : u.getVertexSchema()) {
                            String propertyUrl = "http://jackbergus.alwaysdata.net/property/" + property;

                            String objectUrl = "http://jackbergus.alwaysdata.net/values/" + u.getAttribute(property);
                            RDFRawResourceVertex pCopy = destination.addVertex(new RDFRawResourceVertex(objectUrl));

                            destination.addEdge(uCopy,propertyUrl,pCopy);
                        }
                        for (AbstractNode v : original.getNeighbours((K)u)) {
                            String urlDstValue = "http://jackbergus.alwaysdata.net/values/" +(hasNodeId ? u.getAttribute(defaultNodeid) : u.hashCode());

                            String urlDst = "http://jackbergus.alwaysdata.net/nodes/" + forced+ "/" + original.joinschema(v) + "#" + (hasNodeId ? v.getAttribute(defaultNodeid) : v.hashCode());
                            RDFRawResourceVertex vCopy = destination.addVertex(new RDFRawResourceVertex(new URI(urlDst)));

                            destination.addEdge(uCopy,edgeUrl,vCopy);
                        }
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }
                return 1;
            });
            return destination;
        }).get();
    }


}
