/*
 * NewBulkNode.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.algorithms;

import it.giacomobergami.filemaps.formats.BSONHolder4;
import it.giacomobergami.utils.datastructures.Pair;
import it.giacomobergami.utils.datastructures.sets.UnidirectionalQueue;

/**
 * Simplifies the old Bulk Node
 */
public class NewBulkNode {
    public final UnidirectionalQueue<String> values; // Values defined as the result of the join operation
    public final UnidirectionalQueue<Pair<Integer,Integer>> jointNeighbours; //each neighbour is represented as a pair of the two graphs ids.
    public final BSONHolder4 leftHolder, rightHolder; //Holder to all the previous values (and hence, their neighbours)
    public final int leftId, rightId; // The id of the new vertex is a function of the two old vertices' ids
    public NewBulkNode(BSONHolder4 leftHolder, BSONHolder4 rightHolder) {
        this.leftHolder = leftHolder;
        this.rightHolder = rightHolder;
        values = new UnidirectionalQueue<>();
        leftId = leftHolder.id;
        rightId = rightHolder.id;
        jointNeighbours = new UnidirectionalQueue<>();
    }
}
