/*
 * RelationshipCache.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */




package it.giacomobergami.algorithms;


import it.giacomobergami.graphs.abstractgraph.AbstractNode;
import it.giacomobergami.graphs.nodes.GoogleGraphVertex;
import it.giacomobergami.graphs.pointers.GraphBulkBinPointer;

import java.util.HashMap;


/**
 * Created by vasistas on 18/04/16.
 */
public class RelationshipCache {

    private HashMap<GraphBulkBinPointer, Boolean> table;
    private int capacity;

    public RelationshipCache(int capacity) {
        this.capacity = capacity;
        table = new HashMap<>();
    }

    public void put(AbstractNode left, AbstractNode right) {
        if (table.size()<capacity) table.put(new GraphBulkBinPointer(left.id,right.id),true);
    }

    public boolean get(AbstractNode left, AbstractNode right) {
        Boolean toret = table.get(new GraphBulkBinPointer(left.id,right.id));
        if (toret!=null) return toret;
        else {
            if (left.hasNeighbour(right)) {
                put(left,right);
                return true;
            } else return false;
        }
    }

    public void remove(AbstractNode x, AbstractNode o) {
        table.remove(new GraphBulkBinPointer(x.id,o.id));
    }
}
