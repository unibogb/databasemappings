/*
 * Joins.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.algorithms;

import it.giacomobergami.filemaps.BufferBackwardReaderIterator;
import it.giacomobergami.filemaps.BufferReaderIterator;
import it.giacomobergami.filemaps.OffsetIterator;
import it.giacomobergami.filemaps.buffers.BufferArray;
import it.giacomobergami.filemaps.buffers.LongMMFile;
import it.giacomobergami.filemaps.formats.BSONHolder4;
import it.giacomobergami.filemaps.formats.HashListEntryFormat;
import it.giacomobergami.filemaps.formats.IndexHashPos;
import it.giacomobergami.Leq.QueryAndChain;
import it.giacomobergami.Leq.QueryAttributes;
import it.giacomobergami.utils.I;
import it.giacomobergami.utils.datastructures.Pair;
import it.giacomobergami.utils.datastructures.iterators.MapIterator;
import it.giacomobergami.utils.datastructures.sets.UnidirectionalQueue;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by vasistas on 04/05/16.
 */
public class Joins {

    /**
     * Stores the result of the hash pointers, with the offset for starting and ending the iteration
     */
    public static class HashPointers implements Comparable<HashPointers> {
        public final int hash;
        public final long offsetLeftStart;
        public final long offsetLeftEnd;
        public final long offsetRightStart;
        public final long offsetRightEnd;

        public HashPointers(int hash, long offsetLeftStart, long offsetLeftEnd, long offsetRightStart, long offsetRightEnd) {
            this.hash = hash;
            this.offsetLeftStart = offsetLeftStart;
            this.offsetLeftEnd = offsetLeftEnd;
            this.offsetRightStart = offsetRightStart;
            this.offsetRightEnd = offsetRightEnd;
        }

        @Override
        public int compareTo(HashPointers o) {
            return Integer.compare(hash,o.hash);
        }
    }

    public static class JoinedAttributes {
        public final UnidirectionalQueue<Integer> leftPos, sharedLeft, sharedRight, rightPos;

        public JoinedAttributes() {
            rightPos = new UnidirectionalQueue<>();
            leftPos = new UnidirectionalQueue<>();
            sharedLeft = new UnidirectionalQueue<>();
            sharedRight = new UnidirectionalQueue<>();
        }
    }

    /**
     * This algorithm supposes that the vertices have always the same
     * @param pathLeft                  Path to the folder where the left operand is stored
     * @param pathRight                 Path to the folder where the right operand is stored
     * @param qRaw                      Query expressed as <AttributeLeft,AttributeRight> where an equijoin between the two is performed
     * @param leftattributeToPosition   The Left Graph's schema definition (mapping each attribute into a value positioned in a specific vertex)
     * @param rightattributeToPosition  The Right Graph's schema definition (as above)
     * @throws IOException
     */
    public static void basicJoin(File pathLeft, File pathRight, Collection<Pair<String,String>> qRaw, TreeMap<String,Integer> leftattributeToPosition, TreeMap<String,Integer> rightattributeToPosition) throws IOException {

        //// Step Alpha: converting a query (Steps 1--2)
        QueryAndChain q = null, qp = null;
        {
            //// Step 1: Converting the query into a integer-field check
            ArrayList<QueryAttributes> qt1 = new ArrayList<>(), qt2 = new ArrayList<>();
            qRaw.forEach(x -> {
                qt1.add(new QueryAttributes(leftattributeToPosition.get(x.first), rightattributeToPosition.get(x.value)));
                qt2.add(new QueryAttributes(leftattributeToPosition.get(x.first), rightattributeToPosition.get(x.value)));
            });

            //// Step 2: checking which are the common share attributes, and which are not. Adding such pars in the predicate to check
            Iterator<Map.Entry<String, Integer>>
                    lit = leftattributeToPosition.entrySet().iterator(),
                    rit = rightattributeToPosition.entrySet().iterator();

            JoinedAttributes ja = new JoinedAttributes();
            Map.Entry<String, Integer>
                    a = lit.hasNext() ? lit.next() : null,
                    b = rit.hasNext() ? rit.next() : null;
            do {
                int cmp = a.getKey().compareTo(b.getKey());
                if (cmp == 0) {
                    qt1.add(new QueryAttributes(a.getValue(), b.getValue()));
                    qt2.add(new QueryAttributes(a.getValue(), b.getValue()));
                    ja.sharedLeft.add(a.getValue());
                    ja.sharedRight.add(b.getValue());
                    a = lit.hasNext() ? lit.next() : null;
                    b = rit.hasNext() ? rit.next() : null;
                } else if (cmp < 0) {
                    ja.leftPos.add(a.getValue());
                    a = lit.hasNext() ? lit.next() : null;
                } else {
                    ja.rightPos.add(b.getValue());
                    b = rit.hasNext() ? rit.next() : null;
                }
            } while (lit.hasNext() && rit.hasNext());

            if (a!=null) ja.leftPos.add(a.getValue());
            if (b!=null) ja.rightPos.add(b.getValue());
            lit.forEachRemaining(x->ja.leftPos.add(x.getValue()));
            rit.forEachRemaining(x->ja.rightPos.add(x.getValue()));

            q = new QueryAndChain(1,qt1.toArray(new QueryAttributes[]{}));
            qp = new QueryAndChain(2,qt2.toArray(new QueryAttributes[]{}));
        }

        //// Step 4: performing the join
        LongMMFile vLeft = new LongMMFile(new File(pathLeft, "vertices.bin")),
                vRight = new LongMMFile(new File(pathRight, "vertices.bin"));

        //// Step 3: Hash-Zone intersection. Storing the result in a fast access structure
        UnidirectionalQueue<HashPointers> qit = new UnidirectionalQueue<>();

        LongMMFile lmfLeft = new LongMMFile(new File(pathLeft,"id.index")),
                lmfRight = new LongMMFile(new File(pathRight,"id.index"));
        BufferArray<IndexHashPos> baLeft = new BufferArray<>(IndexHashPos.class,lmfLeft),
                baRight = new BufferArray<>(IndexHashPos.class,lmfRight);

        UnidirectionalQueue<NewBulkNode> bulkGraph = new UnidirectionalQueue<>();

        double mult = 0.0;
        double countMult = 0.0;

        //TreeSet<Integer> hashes =new TreeSet<>();
        {
            //Reading the indices files
            LongMMFile hashLeft = new LongMMFile(new File(pathLeft, "hashes.index")),
                    hashRight = new LongMMFile(new File(pathRight, "hashes.index"));

            //Iterating over an array of fixed size
            BufferReaderIterator<HashListEntryFormat> i = new BufferReaderIterator<>(HashListEntryFormat.class, hashLeft),
                    j = new BufferReaderIterator<>(HashListEntryFormat.class, hashRight);

            //Performing the intersection
            HashListEntryFormat in = null, jn = null;
            while (i.hasNext() && j.hasNext()) {
                if (in == null) in = i.next();
                if (jn == null) jn = j.next();
                if (in.hash < jn.hash) in = i.next();
                else if (in.hash > jn.hash) jn = j.next();
                else {
                    int hash = in.hash;
                    long leftStart = in.offset;
                    long rightStart = jn.offset;
                    in = i.next();
                    jn = j.next();
                    long leftEnd = in == null ? vLeft.getSize() : in.offset;
                    if (leftEnd-leftStart<0)
                        throw new RuntimeException("Error: start="+leftStart+" end="+leftEnd);
                    long rightEnd = jn == null ? vRight.getSize() : jn.offset;
                    if (rightEnd-rightStart<0)
                        throw new RuntimeException("Error: start="+leftStart+" end="+leftEnd);
                    //Match found: storing the left and right offset into the result
                    //qit.add(new HashPointers(in.hash, leftStart, leftEnd, rightStart, rightEnd));
                    //hashes.add(in.hash);
                    //OffsetIterator<BSONHolder4> leftIterator = new OffsetIterator<>(BSONHolder4.class,vLeft,h.offsetLeftStart,h.offsetLeftEnd);
                    for (BSONHolder4 u : new I<>(new OffsetIterator<>(BSONHolder4.class,vLeft,leftStart,leftEnd))) {
                        q.compileOverLeftVertex(u);
                        boolean hasMult = false;

                        // Allocating all the neighbours in order not to scan the file coninuously
                        Iterable<BSONHolder4> Nu = new I<>(new MapIterator<Integer,BSONHolder4>(u.outgoingA.iterator()) {
                            @Override
                            public BSONHolder4 apply(Integer o) {
                                IndexHashPos ptr = baLeft.get(o);
                                BSONHolder4 t = (BSONHolder4)u.unserialize(vLeft,ptr.pos);
                                t.hash = ptr.hash;
                                return t;
                            }
                        });

                        for (BSONHolder4 v : new I<>(new OffsetIterator<>(BSONHolder4.class,vRight,rightStart,rightEnd))) {
                            if (!q.compareOverRightVertex(v)) continue;
                            NewBulkNode bn = new NewBulkNode(u,v); // I do not have to calculate the solution: I just keep the holders
                            if (!hasMult) {
                                hasMult = true;
                                countMult+=1.0;
                            }
                            mult+=1.0;

                            Iterable<BSONHolder4> Nv = new I<>(new MapIterator<Integer,BSONHolder4>(v.outgoingA.iterator()) {
                                @Override
                                public BSONHolder4 apply(Integer o) {
                                    IndexHashPos ptr = baRight.get(o);
                                    BSONHolder4 t = (BSONHolder4)u.unserialize(vRight,ptr.pos);
                                    t.hash = ptr.hash;
                                    return t;
                                }
                            });

                            for (BSONHolder4 up : Nu) {
                                // XXX
                                /*if (!hashes.contains(up.hash)) continue;
                                qp.compileOverLeftVertex(up);
                                for (BSONHolder4 vp : Nv) {
                                    if (!hashes.contains(vp.hash)) continue;
                                    if (!qp.compareOverRightVertex(vp)) continue;
                                    bn.jointNeighbours.add(new Pair(up.id,vp.id));
                                }*/
                            }
                            bulkGraph.add(bn);
                        }

                    }
                }
            }
            hashLeft.close();
            hashRight.close();
        }



        for (HashPointers h : qit) {
            int hash = h.hash;

        }

        //System.out.println("Mult="+(mult/countMult));
        //System.out.println("Size="+bulkGraph.size);

        lmfLeft.close();
        vLeft.close();
        vRight.close();

    }

    /**
     * This algorithm supposes that the vertices have always the same
     * @param pathLeft                  Path to the folder where the left operand is stored
     * @param pathRight                 Path to the folder where the right operand is stored
     * @param x                      Query expressed as <AttributeLeft,AttributeRight> where an equijoin between the two is performed
     * @param leftattributeToPosition   The Left Graph's schema definition (mapping each attribute into a value positioned in a specific vertex)
     * @param rightattributeToPosition  The Right Graph's schema definition (as above)
     * @throws IOException
     */
    public static void leqJoin(File pathLeft, File pathRight, Pair<String,String> x, TreeMap<String,Integer> leftattributeToPosition, TreeMap<String,Integer> rightattributeToPosition) throws IOException {

        //// Step Alpha: converting a query (Steps 1--2)
        QueryAndChain q, qp;
        {
            //// Step 1: Converting the query into a integer-field check
            ArrayList<QueryAttributes> qt1 = new ArrayList<>(), qt2 = new ArrayList<>();
            qt1.add(new QueryAttributes(leftattributeToPosition.get(x.first), rightattributeToPosition.get(x.value)));
            qt2.add(new QueryAttributes(leftattributeToPosition.get(x.first), rightattributeToPosition.get(x.value)));

            //// Step 2: checking which are the common share attributes, and which are not. Adding such pars in the predicate to check
            Iterator<Map.Entry<String, Integer>>
                    lit = leftattributeToPosition.entrySet().iterator(),
                    rit = rightattributeToPosition.entrySet().iterator();

            JoinedAttributes ja = new JoinedAttributes();
            Map.Entry<String, Integer>
                    a = lit.hasNext() ? lit.next() : null,
                    b = rit.hasNext() ? rit.next() : null;
            do {
                int cmp = a.getKey().compareTo(b.getKey());
                if (cmp == 0) {
                    qt1.add(new QueryAttributes(a.getValue(), b.getValue()));
                    qt2.add(new QueryAttributes(a.getValue(), b.getValue()));
                    ja.sharedLeft.add(a.getValue());
                    ja.sharedRight.add(b.getValue());
                    a = lit.hasNext() ? lit.next() : null;
                    b = rit.hasNext() ? rit.next() : null;
                } else if (cmp < 0) {
                    ja.leftPos.add(a.getValue());
                    a = lit.hasNext() ? lit.next() : null;
                } else {
                    ja.rightPos.add(b.getValue());
                    b = rit.hasNext() ? rit.next() : null;
                }
            } while (lit.hasNext() && rit.hasNext());

            if (a!=null) ja.leftPos.add(a.getValue());
            if (b!=null) ja.rightPos.add(b.getValue());
            lit.forEachRemaining(y->ja.leftPos.add(y.getValue()));
            rit.forEachRemaining(y->ja.rightPos.add(y.getValue()));

            q = new QueryAndChain(0,qt1.toArray(new QueryAttributes[]{}));
            qp = new QueryAndChain(1,qt2.toArray(new QueryAttributes[]{}));
        }

        //// Step 3: Hash-Zone intersection. Storing the result in a fast access structure
        UnidirectionalQueue<HashPointers> qit = new UnidirectionalQueue<>();

        //// Step 4: performing the join
        LongMMFile vLeft = new LongMMFile(new File(pathLeft, "vertices.bin")),
                vRight = new LongMMFile(new File(pathRight, "vertices.bin"));

        //Reading the indices files
        LongMMFile hashLeft = new LongMMFile(new File(pathLeft, "hashes.index")),
                hashRight = new LongMMFile(new File(pathRight, "hashes.index"));

        LongMMFile lmfLeft = new LongMMFile(new File(pathLeft,"id.index")),
                lmfRight = new LongMMFile(new File(pathRight,"id.index"));
        BufferArray<IndexHashPos> baLeft = new BufferArray<>(IndexHashPos.class,lmfLeft),
                baRight = new BufferArray<>(IndexHashPos.class,lmfRight);

        UnidirectionalQueue<NewBulkNode> bulkGraph = new UnidirectionalQueue<>();

        double mult = 0.0;
        double countMult = 0.0;

        /*
        BufferReaderIterator<HashListEntryFormat> i = new BufferReaderIterator<>(HashListEntryFormat.class, hashLeft);
        BufferReaderIterator<HashListEntryFormat> j = new BufferReaderIterator<>(HashListEntryFormat.class, hashRight);
        while (i.hasNext()) System.out.print(i.next()+",");
        System.out.println("");
        while (j.hasNext()) System.out.print(j.next()+",");
        System.out.println("");
        System.exit(1);
         */

        //Iterating over an array of fixed size
        BufferReaderIterator<HashListEntryFormat> i = new BufferReaderIterator<>(HashListEntryFormat.class, hashLeft);
        BufferBackwardReaderIterator<HashListEntryFormat> j;
        HashListEntryFormat in = i.hasNext() ? i.next() : null;
        while (in != null) {
            //Getting the informations from the left graph
            int leftHash = in.hash;
            if (leftHash==2007)
                System.out.println("Break");
            long leftOffset = in.offset;
            in = (i.hasNext() ? i.next() : null);
            long leftEnd = in == null ? vLeft.getSize() : in.offset;

            for (BSONHolder4 u : new I<>(new OffsetIterator<>(BSONHolder4.class,vLeft,leftOffset,leftEnd))) {
                u.hash = leftHash;
                int leftYear = Integer.valueOf(u.values.get(2));
                /*if (leftYear!=leftHash) {
                    System.out.println("That is not possible (1): "+u.hash+" "+u.values.get(2));
                    System.exit(1);
                }*/
                q.compileOverLeftVertex(u);
                boolean hasMult = false;

                // Allocating all the neighbours in order not to scan the file coninuously
                Iterable<BSONHolder4> Nu = new I<>(new MapIterator<Integer,BSONHolder4>(u.outgoingA.iterator()) {
                    @Override
                    public BSONHolder4 apply(Integer o) {
                        IndexHashPos ptr = baLeft.get(o);
                        BSONHolder4 t = (BSONHolder4)u.unserialize(vLeft,ptr.pos);
                        t.hash = ptr.hash;
                        return t;
                    }
                });

                //getting the informations from the right graph
                j = new BufferBackwardReaderIterator<>(HashListEntryFormat.class, hashRight);
                long rightEnd = vRight.getSize();
                HashListEntryFormat jn = j.hasNext() ? j.next() : null;
                while (jn != null) {
                    int rightHash = jn.hash;
                    //System.out.println(leftHash+" < "+rightHash+"...");
                    if (leftHash>rightHash) { break;}  // stop to analyse the right graph if I overcome with right matches
                    //System.out.println("Ok");
                    long rightOffset = jn.offset;
                    jn = (j.hasNext() ? j.next() : null);

                    for (BSONHolder4 v : new I<>(new OffsetIterator<>(BSONHolder4.class,vRight,rightOffset,rightEnd))) {
                        v.hash = rightHash;
                        /*int rightYear = Integer.valueOf(v.values.get(2));
                        if (rightYear!=rightHash) {
                            System.out.println("That is not possible (2): "+v.hash+" "+v.values.get(2));
                            System.exit(2);
                        }*/
                        //System.out.println("Here");
                        if (!q.leqOverRightVertex(v)) continue;
                        NewBulkNode bn = new NewBulkNode(u,v); // I do not have to calculate the solution: I just keep the holders
                        if (!hasMult) {
                            hasMult = true;
                            countMult+=1.0;
                        }
                        mult+=1.0;


                        Iterable<BSONHolder4> Nv = new I<>(new MapIterator<Integer,BSONHolder4>(v.outgoingA.iterator()) {
                            @Override
                            public BSONHolder4 apply(Integer o) {
                                IndexHashPos ptr = baRight.get(o);
                                BSONHolder4 t = u.unserialize(vRight,ptr.pos);
                                t.hash = ptr.hash;
                                return t;
                            }
                        });

                        for (BSONHolder4 up : Nu) {
                            qp.compileOverLeftVertex(up);
                            for (BSONHolder4 vp : Nv) {
                                if (up.hash>vp.hash) continue;
                                if (!qp.leqOverRightVertex(vp)) continue;
                                bn.jointNeighbours.add(new Pair(up.id,vp.id));
                            }
                        }
                        bulkGraph.add(bn);
                    }
                    rightEnd = rightOffset;
                }
            }
        }

        System.out.println("Mult="+(mult/countMult));
        System.out.println("Size="+bulkGraph.size);

        lmfLeft.close();
        vLeft.close();
        vRight.close();
    }

}
