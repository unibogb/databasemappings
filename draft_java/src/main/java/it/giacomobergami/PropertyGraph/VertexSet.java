package it.giacomobergami.PropertyGraph;

import java.io.IOException;
import java.util.Optional;

/**
 * Creates a VertexSet that is associated to
 */
public class VertexSet<L extends Label> {

    //each vertex has a schema
    private final Schema<L> s;

    private VertexSet(L label) throws IOException {
        s = new Schema<>(label);
    }

     public static <L extends Label> Optional<VertexSet<L>> getVertices(L label) {
         try {
             return Optional.of(new VertexSet<L>(label));
         } catch  (IOException e) {
             return Optional.empty();
         }
     }

}
