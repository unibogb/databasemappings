package it.giacomobergami.PropertyGraph.examples;

import it.giacomobergami.PropertyGraph.Label;

/**
 * By doing this, you automatically search a vertex schema named "User"
 */
public class User extends Label {
    protected User() {
        super(User.class);
    }
}
