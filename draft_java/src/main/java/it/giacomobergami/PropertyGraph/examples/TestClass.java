package it.giacomobergami.PropertyGraph.examples;

import it.giacomobergami.PropertyGraph.Schema;

import java.util.Optional;

/**
 * Created by vasistas on 07/06/16.
 */
public class TestClass {


    public static void main(String args[]) {
        Optional<Schema<User>> s = Schema.loadSchema(new User());
        System.out.println(s.isPresent()); //returns false if the schema file is not loaded
    }

}
