package it.giacomobergami.PropertyGraph.indexing;

import com.sun.jna.Native;
import com.sun.jna.Structure;

/**
 * Uses the dovetailing implementation on native code (it handles unsigned longs, more efficiently than java's)
 */
public  class Dovetailing {

        public static class cpair extends Structure {
                public static class ByValue extends cpair implements Structure.ByValue {}
                public long L, R;
        }

    /**
     * Performs the Dovetailing over the i and j parameter
     * @param i
     * @param j
     * @return
     */
        public static native long dt(long i, long j);

    /**
     * Desplits the information in the pair value into
     * @param dt dovetailing value
     * @return  a pair (i,j) of the left and right value
     */
    public static native cpair.ByValue dt_inv(long dt);

        static {
            Native.register(("dt"));
        }

    /*
        public static void main(String args[]) {
                long i = 4000000, j = 7;
                System.out.println(dt(i,j));
                cpair cp = dt_inv(8000030000035L);
                System.out.println(cp.L+" "+cp.R);
        }*/

}