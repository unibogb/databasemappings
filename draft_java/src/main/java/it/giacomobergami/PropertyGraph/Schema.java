package it.giacomobergami.PropertyGraph;

import it.giacomobergami.Paths;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;
import java.util.function.Function;

/**
 * Defines the schema associated to a specific labelled vertex. Each schema has a label (the file name) and a set
 * of associated attributes (the schema). Since this information occupies little memory, it is all stored in main memory
 *
 * A schema maps an attribute into its position on raw data
 *
 * Nothing else could be a Schema
 */
public final class Schema<L extends Label> implements Function<String,Integer> {

    /**
     * The label of the vertex denotes its associated schema, that is defined over a s
     */
    public final String label;
    public final String[] attributes; //Maps an attribute position into an attribute name
    public final HashMap<String,Integer> conv;

    public static final Paths p = Paths.getInstance();

    /** Constructs the data from raw */
    public Schema(String label, String[] attributes) {
        this.label = label;
        this.attributes = attributes;
        conv = new HashMap<>();
        for (int i=0; i<attributes.length; i++) {
            conv.put(attributes[i],i);
        }
    }

    private Schema(File openFile) throws IOException {
        this(FilenameUtils.getBaseName(openFile.getName()),FileUtils.readLines(openFile).toArray(new String[]{}));
    }

    protected Schema(L clazz) throws IOException {
        this(new File(p.pgstore,clazz.name+".schema"));
    }

    /**
     * Instantiates a schema from a schema file
     * @param clazz  Label associated to a specific schema
     * @return      Returns a new schema
     */
    public static <L extends Label> Optional<Schema<L>> loadSchema(L clazz) {
        try {
            return Optional.of(new Schema(clazz));
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    @Override
    public Integer apply(String s) {
        return conv.getOrDefault(s,-1);
    }
}
