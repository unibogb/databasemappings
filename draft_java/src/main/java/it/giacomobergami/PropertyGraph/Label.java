package it.giacomobergami.PropertyGraph;

/**
 * Defines a label as its class name
 */
public abstract class Label {
    public final String name;
    protected Label (Class<? extends Label> clazz) { name =  clazz.getName(); }
}
