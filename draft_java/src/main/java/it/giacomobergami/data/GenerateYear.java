/*
 * GenerateYear.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.data;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import java.util.Iterator;
import java.util.Random;

/**
 * Generate a random affiliation year (to Organization)
 */
public class GenerateYear implements Iterator<String> {

    private Random r;
    private String[] elems;

    @Inject
    public GenerateYear(@Named("year.seed") int seed, @Named("year.lowYear") int from, @Named("year.upYear") int to) {
        r = new Random(seed);
        elems = new String[to-from+1];
        for (int i=0; i<to-from+1; i++) {
            elems[i] = from+i+"";
        }
    }

    public String year() {
        return elems[r.nextInt(elems.length)];
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public String next() {
        return year();
    }

}
