/*
 * GenerateIpLoc.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.data;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Random;

/**
 * Generates random locations
 */
public class GenerateIpLoc implements Iterator<GenerateIpLoc.IpLoc> {

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public IpLoc next() {
        return ipAndLocation();
    }

    public static class IpLoc {
        public String ip, loc;
        public IpLoc(String ip, String loc) {
            this.ip = ip;
            this.loc = loc;
        }
    }

    private Random r;
    private JSONArray a;

    @Inject
    public GenerateIpLoc(@Named("iploc.seed") int seed) throws IOException, ParseException {
            r = new Random(seed);
        ClassLoader classLoader = getClass().getClassLoader();
        File interestFile = new File(classLoader.getResource("ipgeo.json").getFile());
            a = (JSONArray)new JSONParser().parse(new FileReader(interestFile));
    }

    public IpLoc ipAndLocation() {
        JSONObject o = (JSONObject)a.get(r.nextInt(a.size()));
        return new IpLoc(o.get("ip").toString(),o.get("location").toString());
    }

    public static void main(String args[]) throws IOException, ParseException {
        GenerateIpLoc g = new GenerateIpLoc(0);
        for (int i=0; i<100; i++) {
            IpLoc l = g.next();
            System.out.println(l.ip+" in "+l.loc);
        }
    }

}
