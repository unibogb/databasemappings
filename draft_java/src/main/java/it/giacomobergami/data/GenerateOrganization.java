/*
 * GenerateOrganization.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.data;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Created by vasistas on 06/03/16.
 */
public class GenerateOrganization implements Iterator<String> {

    private Random r;
    private ArrayList<String> list;

    @Inject
    public GenerateOrganization(@Named("organization.seed") int seed) throws IOException {
        r = new Random(seed);
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("all_companies.txt").getFile());
        InputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
        BufferedReader br = new BufferedReader(isr);
        String line;
        list = new ArrayList<>();
        while ((line = br.readLine()) != null) {
            list.add(line);
        }
        br.close();
    }

    public String organization() {
        return list.get(r.nextInt(list.size()));
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public String next() {
        return organization();
    }
}
