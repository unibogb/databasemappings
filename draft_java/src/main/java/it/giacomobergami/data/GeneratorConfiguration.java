/*
 * GeneratorConfiguration.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.data;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by vasistas on 06/03/16.
 */
public class GeneratorConfiguration extends AbstractModule {

        @Override
        protected void configure() {
            Properties properties = new Properties();
            try {
                properties.load(new FileReader("generator.properties"));
                Names.bindProperties(binder(), properties);
            } catch (FileNotFoundException e) {
                System.out.println("The configuration file Test.properties can not be found");
            } catch (IOException e) {
                System.out.println("I/O Exception during loading configuration");
            }

        }

    private static Injector conf;
    public static Injector getInstance() {
        if (conf==null) {
            conf = Guice.createInjector( new GeneratorConfiguration());
        }
        return conf;
    }

}
