/*
 * CacheMediator.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.cache;

import it.giacomobergami.cache.retrievers.IRetriever;
import org.apache.jcs.access.behavior.ICacheAccess;
import org.apache.jcs.access.exception.CacheException;

/**
 * Handles the JSC Cache with specific policies
 */
public class CacheMediator<Key,Value> {

    private ICacheAccess neighCache;
    private IRetriever<Key,Value> golden;

    /**
     *
     * @param neighCache        Local store
     * @param golden            Object that remotely retrieves the elements
     */
    public CacheMediator(ICacheAccess neighCache, IRetriever<Key, Value> golden) {
        this.neighCache = neighCache;
        this.golden = golden;
    }


    /**
     * Cache is disabled
     * @param golden            Object that remotely retrieves the elements
     */
    public CacheMediator(IRetriever<Key, Value> golden) {
        this.neighCache = new NoCache();
        this.golden = golden;
    }

    /**
     * Retrieves the element associated with key, either in the cache or remotely
     * @param key
     * @return
     */
    public Value retrieve(Key key) {
        Object o = neighCache.get(key);
        if (o!=null) return (Value)o;
        else {
            try {
                Value got = golden.get(key);
                if (got!=null) neighCache.put(key,got); //Fills the cache iif. there is an element remotely.
                return got;
            } catch (CacheException e) {
                e.printStackTrace();
                System.exit(1);
                return null;
            }
        }
    }

    public Value getFromCache(Key key) {
        return (Value) neighCache.get(key);
    }

    /**
     * Remove the element only from the cache and
     * @param key
     * @return
     */
    public boolean removeFromCache(Key key) {
        try {
            neighCache.remove(key);
            return true;
        } catch (CacheException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Fully clears the local cache. This is the desired function when a new computation has to start
     * @return  Returns false iff. an error occurred
     */
    public boolean clearCache() {
        try {
            neighCache.destroy();
            return true;
        } catch (CacheException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean put(Key integer, Value neue) {
        try {
            golden.update(integer,neue);
            neighCache.put(integer,neue);
            return true;
        } catch (CacheException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean locallyUpdate(Key integer, Value neue) {
        try {
            neighCache.put(integer,neue);
            return true;
        } catch (CacheException e) {
            e.printStackTrace();
            return false;
        }
    }
}
