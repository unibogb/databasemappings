/*
 * NoCache.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.cache;

import org.apache.jcs.access.behavior.ICacheAccess;
import org.apache.jcs.access.exception.CacheException;
import org.apache.jcs.engine.behavior.ICompositeCacheAttributes;
import org.apache.jcs.engine.behavior.IElementAttributes;

/**
 * Created by vasistas on 24/04/16.
 */
public class NoCache implements ICacheAccess {
    @Override
    public Object get(Object o) {
        return null;
    }

    @Override
    public void putSafe(Object o, Object o1)  {

    }

    @Override
    public void put(Object o, Object o1)  {

    }

    @Override
    public void put(Object o, Object o1, IElementAttributes iElementAttributes)  {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void remove()  {

    }

    @Override
    public void destroy(Object o)  {

    }

    @Override
    public void remove(Object o)  {

    }

    @Override
    public void resetElementAttributes(IElementAttributes iElementAttributes)  {

    }

    @Override
    public void resetElementAttributes(Object o, IElementAttributes iElementAttributes) {

    }

    @Override
    public IElementAttributes getElementAttributes()  {
        return null;
    }

    @Override
    public IElementAttributes getElementAttributes(Object o) {
        return null;
    }

    @Override
    public ICompositeCacheAttributes getCacheAttributes() {
        return null;
    }

    @Override
    public void setCacheAttributes(ICompositeCacheAttributes iCompositeCacheAttributes) {

    }

    @Override
    public int freeMemoryElements(int i) throws CacheException {
        return 0;
    }
}
