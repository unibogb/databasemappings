/*
 * OffsetIterator.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps;

import it.giacomobergami.filemaps.buffers.IBufferReader;
import it.giacomobergami.filemaps.formats.IWriteSerializable;

import java.lang.reflect.Method;
import java.util.Iterator;

/**
 * Created by vasistas on 04/05/16.
 */
public class OffsetIterator<T extends IWriteSerializable<T>> implements Iterator<T> {

    private final IBufferReader buff;
    private final long start;
    private final long end;
    private long pos;
    private final T check;

    public OffsetIterator(Class<T> clazz, IBufferReader buff, long start, long end) {
        this.buff = buff;
        this.start = pos = start;
        this.end = end;
        T check = null;
        try {
            check = clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        this.check = check;
    }

    @Override
    public boolean hasNext() { return end-pos>0; }

    @Override
    public T next() {
        int n = FileMapsUtils.toInt(buff.get(pos,4),0);
        T toret = check.unserialize(buff,pos);
        pos+=n;
        return toret;
    }

}
