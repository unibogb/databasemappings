/*
 * ReadAdjacencyFile.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.rw;

import it.giacomobergami.utils.datastructures.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public  class ReadAdjacencyFile implements Iterator<Pair<Integer,Set<Integer>>> {
        final String regexSplit;
        final BufferedReader br;
        private String currentLine;

        public ReadAdjacencyFile(String regexSplit, String fileName)  {
            this.regexSplit = regexSplit;
            File file = new File(fileName);
            BufferedReader brl = null;
            try {
                FileReader fr = new FileReader(file);
                brl = new BufferedReader(fr);
                currentLine = brl.readLine();
            } catch (Exception e) {
                System.exit(1);
            }
            br = brl;
        }

        @Override
        public boolean hasNext() {
            return currentLine!=null;
        }

        @Override
        public Pair<Integer,Set<Integer>> next() {
            if (currentLine==null) return null;
            Integer src  = null;
            Set<Integer> dst = new HashSet<>();
            try {
                do {
                    String src_dst[] = currentLine.split(regexSplit);
                    if (src==null) {
                        src = Integer.valueOf(src_dst[0]);
                        dst.add(Integer.valueOf(src_dst[1]));
                    } if (!src.equals(Integer.valueOf(src_dst[0]))) {
                        return new Pair<>(src,dst);
                    } else dst.add(Integer.valueOf(src_dst[1]));
                } while((currentLine = br.readLine()) != null);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
                return null;
            }
            return null;
        }
    }
