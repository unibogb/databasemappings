/*
 * Sample.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import it.giacomobergami.data.GeneratorConfiguration;
import it.giacomobergami.filemaps.formats.BSONHolder3;
import it.giacomobergami.filemaps.formats.BSONHolder4;
import it.giacomobergami.graphs.abstractgraph.AbstractGraph;
import it.giacomobergami.graphs.readonly_graph.ReadonlyIntegerGraph;
import it.giacomobergami.utils.datastructures.trees.rbtree.RBTree;

import java.util.*;

/**
 * This class performs the random graphs sampling given the given configurations
 */
public class Sample {

    private Random rv, rj, nchoice;
    private int vRange;
    private ReadonlyIntegerGraph g;
    private int u;
    //private boolean isVisited[];
    //public Set<Integer> V;
    //public Set<Integer> neighs[];
    public RBTree<Integer, BSONHolder4> orderedTree;
    int idConversion[];
    private double jumpProb;
    int counter = 0;

    private static Sample self = null;
    public static Sample getInstance() {
        if (self==null)
            self = GeneratorConfiguration.getInstance().getInstance(Sample.class);
        return self;
    }

    @Inject
    protected Sample(@Named("sampler.vertex")int samplerVertex,
                     @Named("sampler.doSkip")int samplerSkip,
                     @Named("sampler.nChoice")int samplerNext,
                     @Named("sampler.jumpProb")double jumpProb) {
        rv = new Random(samplerVertex);
        rj = new Random(samplerSkip);
        nchoice = new Random(samplerNext);
        g = null;
        this.jumpProb = jumpProb;

    }

    public boolean hasToJump() {
        return rj.nextDouble()<=jumpProb;
    }

    public boolean hasNext() {
        return orderedTree.getSize()<g.getVertices().size();
    }

    /*public static boolean isPowerOf(int x, int y) {
        while (x%y == 0)  x = x / y;
        return x == 1;

    }*/

    public  BSONHolder4 createVertexFromPos(int u) {
        BSONHolder3 holder = g.getRawVertexById(u);
        String one,two,three;
        one = holder.values.get(0);
        two = holder.values.get(1);
        three = holder.values.get(2);
        return new BSONHolder4(new String[]{one, two, three}, idConversion[u], holder.hash);
    }

    public long next(int maxStep) {
        long creation = 0;
        do {
            int v;
            int step;
            Collection<Integer> Nu = g.getNeighbours(u);


            if ((!Nu.isEmpty())&&(!hasToJump())) {
                step = 0;

                BSONHolder4 eH = null,  newverrtex = null;
                {
                    BSONHolder3 uH = g.getRawVertexById(u);
                    Iterator<BSONHolder4> it = orderedTree.lookup(uH.hash).iterator();
                    while (it.hasNext()) {
                        BSONHolder4 h = it.next();
                        if (idConversion[uH.id] == h.id) {
                            eH = h;
                            break;
                        }
                    }
                }

                do {
                    v = Nu.<Integer>toArray(new Integer[Nu.size()])[nchoice.nextInt(Nu.size())];
                    boolean wasPrev = false;
                    if (idConversion[v]==-1) {
                        wasPrev = true;
                        idConversion[v] = counter++;
                    }

                    //neighs[u].add(v); //E(u,v)

                    long delta = System.currentTimeMillis();
                    newverrtex = createVertexFromPos(v);
                    eH.outgoing.add(idConversion[v]);
                    newverrtex.outgoing.add(idConversion[u]);
                    if (wasPrev) {
                        orderedTree.insert(newverrtex.hash,newverrtex);
                    }
                    delta = System.currentTimeMillis() - delta;
                    creation += delta;
                    ////System.out.println("E << ("+u+","+v+")");
                    //V.add(v);
                } while ((newverrtex.outgoing.size==g.getNeighbours(v).size())&&(step++<Nu.size()));
                if (step <= Nu.size()) {
                    //isVisited[v] = true;
                    u = v;
                    ////System.out.println("V << " + u);
                } else {
                    u = rv.nextInt(vRange);
                    boolean hasCheck = false;
                    if (idConversion[u]==-1) {
                        idConversion[u] = counter++;
                        hasCheck = true;
                    }
                    long delta = System.currentTimeMillis();
                    BSONHolder4 h = createVertexFromPos(u);
                    if (hasCheck) {
                        orderedTree.insert(h.hash,h);
                    }
                    delta = System.currentTimeMillis() - delta;
                    creation += delta;
                    //---> orderedTree.insert(h.hash,h);
                    //if (!isVisited[u]) V.add(u);
                    //isVisited[u] = true;
                    ////System.out.println("V <<(Ex) " + u);
                }
            } else {
                u = rv.nextInt(vRange);
                boolean hasCheck = false;
                if (idConversion[u]==-1) {
                    idConversion[u] = counter++;
                    hasCheck = true;
                }
                long delta = System.currentTimeMillis();
                BSONHolder4 h = createVertexFromPos(u);
                if (hasCheck) {
                    orderedTree.insert(h.hash,h);
                }
                delta = System.currentTimeMillis() - delta;
                creation += delta;
                //if (!isVisited[u]) V.add(u);
                //isVisited[u] = true;
                ////System.out.println("V <<(J) " + u);
            }
        } while (counter<maxStep);
        //System.out.println("Sampled size: "+orderedTree.getSize());
        return creation;
    }

    public ReadonlyIntegerGraph getG() {
        return g;
    }

    public void setG(ReadonlyIntegerGraph g) {
        this.g = g;
        vRange = g.getVertices().size();

        u = rv.nextInt(vRange);
        idConversion = new int[g.getVertices().size()];
        Arrays.fill(idConversion,-1);

        //isVisited = new boolean[vRange];
        //Arrays.fill(isVisited,false);
        //isVisited[u] = true;
        ////System.out.println("V << " + u);

        //neighs = new HashSet[vRange];
        //Arrays.fill(neighs,new HashSet<>());

        orderedTree = new RBTree<>(false); // Checks if the element already exists
        if (idConversion[u]==-1) idConversion[u] = counter++;
        BSONHolder4 h = createVertexFromPos(u);
        orderedTree.insert(h.hash,h);

        //V = new HashSet<>(vRange);
        //isVisited[u] = true;
        //V.add(u);
    }
}
