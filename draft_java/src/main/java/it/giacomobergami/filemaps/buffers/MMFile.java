/*
 * MMFile.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.buffers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Proof Of Concept: using java mmap, you can handle files that are only of a size of MAX_INTEGER
 */
public abstract class MMFile<T extends Comparable<T>> {

    public final MappedByteBuffer map;
    private final int structureSize;
    public abstract T getObjectFromPos(int pos);
    public int currentPos;
    public final long size;

    public int arrayPositionToMemoryPosition(int pos) {
        return (currentPos = pos * structureSize);
    }

    protected MMFile(String filename, int structureSize) {
        long size1;
        MappedByteBuffer map1;
        int structureSize1;
        currentPos = 0;
        //Create file object
        File file = new File(filename);

        //Get file channel in readonly mode
        FileChannel fileChannel = null;
        try {
            fileChannel = new RandomAccessFile(file, "r").getChannel();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //Get direct byte buffer access using channel.map() operation
        try {
            size1 = fileChannel.size();
            map1 = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
            structureSize1 = structureSize;
        } catch (IOException e) {
            size1 = 0;
            map1 = null;
            structureSize1 = 0;
        }
        size = size1;
        this.map = map1;
        this.structureSize = structureSize1;
    }

    protected MMFile(MappedByteBuffer map, int structureSize) {
        this.map = map;
        this.structureSize = structureSize;
        currentPos = map.position();
        size = map.limit();
    }

    public boolean contains(T element) {
        double L = 0, R = map.limit() / structureSize;
        while (true) {
            if (L>R) return false;
            int m = (int)Math.floor((L+R)/((double)2));
            int cmp = element.compareTo(getObjectFromPos(m));
            if (cmp==0) return true;
        }
    }


    public int getCurrentPosition() {
        return currentPos;
    }

    public long getFileSize() {
        return size;
    }

    public static void main(String args[]) {
        //does not store data that is bigger than Integer.MAX_VALUE
        new MMFile("/Users/vasistas/databaseMappings/mappingsGraph2.graph", 4) {
            @Override
            public Comparable getObjectFromPos(int pos) {
                return null;
            }
        };
    }

}
