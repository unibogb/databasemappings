/*
 * LongMMFile.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.buffers;

import com.sun.jna.Pointer;
import it.giacomobergami.filemaps.peregrine.mman;

import java.io.File;
import java.io.IOException;

/**
 * Wrapping the peregrine.os interface to MemoryMapped files
 */
public class LongMMFile implements AutoCloseable, IBufferReader {

    public final int fd;
    public final Pointer addr;
    public final long size;

    public LongMMFile(File path) throws IOException {
        size = path.length();
        fd = mman.open(path.getAbsolutePath(),mman.O_RDONLY);
        addr = mman.mmap( size, mman.PROT_READ, mman.MAP_SHARED | mman.MAP_LOCKED, fd, 0 );
    }

    /**
     * Allocating the elements as row as possible.
     * @param offset
     * @param size
     * @return
     */
    public byte[] get(long offset,int size) {
        return addr.getByteArray(offset,size);
    }

    @Override
    public long getSize() {
        return size;
    }

    @Override
    public Class<IBufferReader> getClazz() {
        return IBufferReader.class;
    }

    @Override
    public void close() {
        mman.close(fd);
    }

}
