/*
 * BSONHolder3.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.formats;

import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.buffers.BufferArraySize;
import it.giacomobergami.filemaps.buffers.IBufferReader;
import it.giacomobergami.filemaps.rw.Writer;
import it.giacomobergami.utils.I;
import it.giacomobergami.utils.datastructures.sets.UnidirectionalStack;

import java.util.Iterator;

/**
 * Defines a new way to implement the vertices.
 * Please note that elements are serialized on the fly
 */
public class BSONHolder3 implements IWriteSerializable<BSONHolder3> {

    public  int id;  // It has not to be final, since after the deserialization process we have to store the value
    public  int hash; //
    public VertexValues values;
    public StaticIntegerArray ingoingA;
    public StaticIntegerArray outgoingA;

    /**
     * TODO: yes, you can change the ingoing, but beware, you have even to change the ingoingA/outgoingA
     */
    public UnidirectionalStack<BSONHolder3> ingoing;
    public UnidirectionalStack<BSONHolder3> outgoing;

    public BSONHolder3(String[] values,int id, int hash) {
        this.id = id;
        this.hash = hash;
        ingoingA = null;
        outgoingA = null;
        this.values = new VertexValues(values);
        ingoing = new UnidirectionalStack<>();
        outgoing = new UnidirectionalStack<>();
    }

    public BSONHolder3(VertexValues gv, StaticIntegerArray ing, StaticIntegerArray oug, int id) {
        this.id = id;
        this.hash = -1;
        ingoingA = ing;
        outgoingA = oug;
        ingoing = null;
        outgoing = null;
        values = gv;
    }

    public static StaticIntegerArray serializeAdjacency(UnidirectionalStack<BSONHolder3> adj) {
        return serializeAdjacency(adj.iterator(),adj.getSize());
    }

    public static StaticIntegerArray serializeAdjacency(Iterator<BSONHolder3> it, int len) {
        int[] toStatic = new int[len];
        int i = 0;
        while (it.hasNext()) toStatic[i++] = it.next().id;
        return new StaticIntegerArray(toStatic);
    }

    public static StaticIntegerArray serializeAdjacencyAsPointers(Iterator<Integer> it, int len) {
        int[] toStatic = new int[len];
        int i = 0;
        while (it.hasNext()) toStatic[i++] = it.next();
        return new StaticIntegerArray(toStatic);
    }

    public void initSerial() {
        if (ingoingA == null && outgoingA == null) {
            ingoingA = serializeAdjacency(ingoing);
            outgoingA = serializeAdjacency(outgoing);
        }
    }

    @Override
    public long serialize(Writer w) {
        int count = 16;

        int ingoingOffset = 16 + values.getStorageSize();
        initSerial();
        int outgoingOffset = ingoingOffset + ingoingA.getStorageSize();
        int initialSize = outgoingOffset + outgoingA.getStorageSize();

        w.writeRaw(initialSize);
        w.writeRaw(ingoingOffset);
        w.writeRaw(outgoingOffset);
        w.writeRaw(id);
        long a = values.serialize(w);
        long b = ingoingA.serialize(w);
        long c = outgoingA.serialize(w);
        count += (a+b+c);
        if (count!=initialSize)
            throw new RuntimeException("Wrong vertex allocation logic. 16+"+values.getStorageSize()+"+"+ingoingA.getStorageSize()+"+"+outgoingA.getStorageSize()+"!=16+"+a+"+"+b+"+"+c);

        return count;
    }

    @Override
    public BSONHolder3 unserialize(IBufferReader br, long offset) {
        return unmarshall(br, offset);
    }

    public static BSONHolder3 unmarshall(IBufferReader br, long offset) {
        int initialSize, ingoingOffset, outgoingOffset, id;
        {
            byte[] getOffsets = br.get(offset,16);
            initialSize = FileMapsUtils.toInt(getOffsets,0);
            ingoingOffset = FileMapsUtils.toInt(getOffsets,4);
            outgoingOffset = FileMapsUtils.toInt(getOffsets,8);
            id = FileMapsUtils.toInt(getOffsets,12);
            //System.out.println("initial="+initialSize+" ingoing="+ingoingOffset+" outgoing="+outgoingOffset);
        }
        VertexValues gv = new VertexValues(br.get(offset+16,ingoingOffset-16));
        StaticIntegerArray ing = new StaticIntegerArray(br.get(offset+ingoingOffset,outgoingOffset-ingoingOffset));
        StaticIntegerArray oug = new StaticIntegerArray(br.get(offset+outgoingOffset,initialSize-outgoingOffset));
        return new BSONHolder3(gv,ing,oug,id);
    }

    @Override
    public int getStorageSize() {
        initSerial();
        return 16 + values.getStorageSize() + ingoingA.getStorageSize() + outgoingA.getStorageSize();
    }

    public static void main(String args[]) {
        BSONHolder3 bsh = new BSONHolder3(new String[]{"Elements","sgf"},1035,0);
        bsh.ingoing.add(new BSONHolder3(new String[]{},1,0));
        bsh.ingoing.add(new BSONHolder3(new String[]{},2,0));
        bsh.outgoing.add(new BSONHolder3(new String[]{},3,0));
        Writer w = new Writer();
        bsh.serialize(w);
        IBufferReader reader = new BufferArraySize(w.close().getBytes());
        BSONHolder3 vertex = BSONHolder3.unmarshall(reader,0);

        System.out.println("Id: "+vertex.id);
        System.out.print("Ingoing: ");
        for (int ingoing : new I<>(vertex.ingoingA.iterator())) {
            System.out.print(ingoing+", ");
        }
        System.out.println("");

        System.out.print("Outgoing: ");
        for (int outgoing : new I<>(vertex.outgoingA.iterator())) {
            System.out.print(outgoing+", ");
        }
        System.out.println("");

        System.out.print("Values: ");
        for (String val : new I<>(vertex.values.iterator())) {
            System.out.print(val+", ");
        }
        System.out.println("");
    }


}
