/*
 * StaticIntegerArray.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.formats;

import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.arrays.IStaticArray;
import it.giacomobergami.filemaps.buffers.IBufferReader;
import it.giacomobergami.filemaps.rw.Writer;

import java.util.Iterator;

/**
 * Created by vasistas on 28/04/16.
 */
public class StaticIntegerArray implements IWriteSerializable<StaticIntegerArray>, IStaticArray<Integer> {

    private int array[];
    private byte[] elems;

    public StaticIntegerArray(int[] array) {
        this.array = array;
        this.elems = null;
    }

    public StaticIntegerArray(byte[] elems) {
        this.array = null;
        this.elems = elems;
    }

    @Override
    public long serialize(Writer w) {
        if (elems!=null && array==null) {
            w.write(elems);
            return elems.length;
        } else {
            w.writeRaw(array.length);
            for (int i : array) w.writeRaw(i);
        }
        return (array.length+1)*4;
    }

    @Override
    public StaticIntegerArray unserialize(IBufferReader br, long offset) {
        return new StaticIntegerArray(br.get(offset,FileMapsUtils.toInt(br.get(offset,4),0)));
    }

    @Override
    public int getStorageSize() {
        return elems!=null ? elems.length : (array.length+1)*4;
    }

    @Override
    public Integer get(int pos) {
        return array!=null ? array[pos] : FileMapsUtils.toInt(elems,(pos+1)*4);
    }

    @Override
    public long size() {
        return array!=null ? array.length : (elems.length/4)-1;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            long length = size();
            long ptr = 1; //Skip the first integer: it is only the array's size
            @Override
            public boolean hasNext() {
                return ptr <= length;
            }
            @Override
            public Integer next() {
                return array!=null ? array[(int)ptr++] : FileMapsUtils.toInt(elems,((int)(ptr++))*4);
            }
        };
    }

}
