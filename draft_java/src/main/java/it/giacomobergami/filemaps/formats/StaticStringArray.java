/*
 * StaticStringArray.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.formats;

import it.giacomobergami.filemaps.BufferPointer;
import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.arrays.IStaticArray;
import it.giacomobergami.filemaps.buffers.IBufferReader;
import it.giacomobergami.filemaps.rw.Writer;

import java.util.Iterator;

/**
 * Implements a static string array, either if it is persisted into a array, or if it has been created in main memory
 */
public class StaticStringArray implements IWriteSerializable<StaticStringArray>, IStaticArray<String> {

    private final String[] array;
    public byte[] elems;
    private int size;
    private int offsets[];

    public StaticStringArray(String[] array) {
        this.array = array;
        size = array.length;
        elems = null;
        offsets = null;
    }

    /**
     * Creates a String Array from a byte array that is stored in (e.g.) main memory
     * @param elems
     */
    public StaticStringArray(byte[] elems) {
        if (elems.length>1000)
            System.err.println("Warning: elems is greater than 1000");
        array = null;
        this.elems = elems;
        this.size = FileMapsUtils.toInt(elems, 0);
        //if (this.size<0) System.out.println("Wat? "+this.size);
        offsets = new int[this.size];
        //if (elems.length<0) System.out.println("Wat? "+elems.length);
        for (int i=0; i<this.size; i++) {
            int j = (i+1)*4;
            if (j<0) { System.out.println(j+" <-- "); if (elems.length>1000)
                System.err.println("Warning: elems is greater than 1000");}

            offsets[i] = FileMapsUtils.toInt(elems,(i+1)*4);
        }
    }

    /**
     * Get an element from array, either if it is memorized from buffer, or
     * @param i
     * @return
     */
    @Override
    public String get(int i) {
        return array == null ? ((i<size-1) ? new String(elems,offsets[i],offsets[i+1]-offsets[i]) :  new String(elems,offsets[i],elems.length-offsets[i])) : array[i];
    }

    public BufferPointer getPointer(int i) {
        return array == null ?  ((i<size-1) ? new BufferPointer(elems,offsets[i],offsets[i+1]-offsets[i]):  new BufferPointer(elems,offsets[i],elems.length-offsets[i])) : null;
    }

    @Override
    public long size() {
        return size;
    }

    @Override
    public long serialize(Writer w) {
        if (array==null && elems!=null) {
            w.write(elems);
            return elems.length;
        } else {
            byte sarrayb[][] = new byte[array.length][];
            w.writeRaw(array.length);
            int i = 0;
            int offsetCount = (array.length + 1) * 4;
            for (String s : array) {
                byte b[] = s.getBytes();
                sarrayb[i++] = b;
                w.writeRaw(offsetCount);
                offsetCount += b.length;
            }
            for (byte b[] : sarrayb) {
                w.writeRaw(b);
            }
            return offsetCount;
        }
    }

    @Override
    public StaticStringArray unserialize(IBufferReader br, long offset) {
        int moveTo = FileMapsUtils.toInt(br.get(offset,0),0)*4;
        int size = FileMapsUtils.toInt(br.get(offset,moveTo),0);
        byte[] elems = br.get(offset,size);
        return new StaticStringArray(elems);
    }

    @Override
    public int getStorageSize() {
        if (elems!=null) {
            return elems.length;
        } else {
            int start = (array.length + 1) * 4;
            for (String s : array) start+=s.getBytes().length;
            return start;
        }
    }


    @Override
    public Iterator<String> iterator() {
        return new Iterator<String>() {
            long size = size();
            long ptr = 0;
            @Override
            public boolean hasNext() {
                return ptr < size;
            }
            @Override
            public String next() {
                return array!=null ? array[(int)ptr++] : get((int)ptr++);
            }
        };
    }

    /***
     * The following methods are used for querying the data
     ***/
    public boolean ithEqualsTo(BufferPointer leftPointer, int i) {
        return FileMapsUtils.equals(leftPointer.memory,leftPointer.offsetBegin,leftPointer.length,
                                    elems,             offsets[i],             (i < size-1) ? offsets[i+1]-offsets[i]:  elems.length-offsets[i]);
    }

    public boolean ithLeqsTo(BufferPointer leftPointer, int i) {
        return FileMapsUtils.leqInteger(leftPointer.memory,leftPointer.offsetBegin,leftPointer.length,
                elems,             offsets[i],             (i < size-1) ? offsets[i+1]-offsets[i]:  elems.length-offsets[i]);
    }
}
