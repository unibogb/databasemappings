/*
 * HashListEntryFormat.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.formats;

import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.buffers.IBufferReader;
import it.giacomobergami.filemaps.rw.Writer;

/**
 * Defines and creates a new entry of
 */
public class HashListEntryFormat implements IWriteSerializable<HashListEntryFormat> {

    public final int hash;
    public final long offset;

    public HashListEntryFormat(int hash, long offset) {
        this.hash = hash;
        this.offset = offset;
    }

    public HashListEntryFormat() {
        this.hash = -1;
        this.offset = -1;
    }

    @Override
    public long serialize(Writer w) {
        w.writeRaw(hash);
        w.writeRaw(offset);
        return 12;
    }

    @Override
    public HashListEntryFormat unserialize(IBufferReader br, long offset) {
        return new HashListEntryFormat(FileMapsUtils.toInt(br.get(offset,4),0),FileMapsUtils.toLong(br.get(offset+4L,8),0));
    }

    @Override
    public int getStorageSize() {
        return 12;
    }
}
