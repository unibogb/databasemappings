/*
 * IWriteSerializable.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.formats;

import it.giacomobergami.filemaps.buffers.IBufferReader;
import it.giacomobergami.filemaps.rw.Writer;

/**
 * Created by vasistas on 28/04/16.
 */
public interface IWriteSerializable<T> {

    /**
     * Serialize the class into a writer
     *
     * @param w Where to save the serialization
     * @return  the number of the bytes that have been written (hence, returns the)
     */
    long serialize(Writer w);

    /**
     * Creates a new element from a reader buffer r
     * @param br        where to read the data
     * @param offset    at which point read the data
     * @return
     */
    T unserialize(IBufferReader br, long offset);

    /**
     * Returns the number of bytes that are associated to the structure represented in the memory buffer
     * @return
     */
    int getStorageSize();

}
