/*
 * FileMapsUtils.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps;

import it.giacomobergami.filemaps.formats.IWriteSerializable;
import it.giacomobergami.filemaps.rw.Writer;

/**
 * Created by vasistas on 28/04/16.
 */
public class FileMapsUtils {

    public static int toInt(byte[] bytes, int offset) {
        int ret = 0;
        for (int i=offset; i<4+offset && i<bytes.length; i++) {
            ret <<= 8;
            ret |= (int)bytes[i] & 0xFF;
        }
        return ret;
    }


    public static long toLong(byte[] readBuffer, int offset) {
        //From Java8 Code
        return (((long)readBuffer[offset] << 56) +
                ((long)(readBuffer[offset+1] & 255) << 48) +
                ((long)(readBuffer[offset+2] & 255) << 40) +
                ((long)(readBuffer[offset+3] & 255) << 32) +
                ((long)(readBuffer[offset+4] & 255) << 24) +
                ((readBuffer[offset+5] & 255) << 16) +
                ((readBuffer[offset+6] & 255) <<  8) +
                ((readBuffer[offset+7] & 255) <<  0));
    }


    public static <T extends IWriteSerializable<T>> void writeIteration(Iterable<T> itbl, Writer w) {
        for (T it : itbl) it.serialize(w);
    }


    public static boolean leqInteger(byte[] a, int leftOffset, int leftLen, byte[] a2, int rightOffset, int rightLen) {
        //System.out.println(Integer.valueOf(new String(a,leftOffset,leftLen))+"<=?"+Integer.valueOf(new String(a2,rightOffset,rightLen)));
        return Integer.valueOf(new String(a,leftOffset,leftLen))<=Integer.valueOf(new String(a2,rightOffset,rightLen));
    }

    public static boolean equals(byte[] a, int leftOffset, int leftLen, byte[] a2, int rightOffset, int rightLen) {
        if (a==null || a2==null)
            return false;

        if (rightLen != leftLen)
            return false;

        for (int i=0; i<rightLen; i++)
            if (a[i+leftOffset] != a2[i+rightOffset])
                return false;

        return true;
    }

}
