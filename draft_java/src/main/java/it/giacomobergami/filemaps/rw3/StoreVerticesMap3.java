/*
 * StoreVerticesMap3.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.rw3;

import com.google.inject.Guice;
import com.google.inject.Injector;
import it.giacomobergami.data.GeneratorConfiguration;
import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.formats.HashListEntryFormat;
import it.giacomobergami.filemaps.formats.IndexHashPos;
import it.giacomobergami.filemaps.formats.BSONHolder3;
import it.giacomobergami.filemaps.rw.ReadAdjacencyFile;
import it.giacomobergami.filemaps.rw.Writer;
import it.giacomobergami.utils.I;
import it.giacomobergami.utils.MultiMapValuePointer;
import it.giacomobergami.utils.datastructures.Pair;
import it.giacomobergami.utils.datastructures.pointers.RBTreePointer;
import it.giacomobergami.utils.datastructures.trees.rbtree.RBTree;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by vasistas on 24/04/16.
 */
public class StoreVerticesMap3 {

    /**
     * Returns the whole vertex set
     * @return
     */
    public void load() {
        System.out.println("Using solution 3.");

        Injector injector = Guice.createInjector( new GeneratorConfiguration());
        VertexGenerator3 gen = VertexGenerator3.getInstance();
        Iterable<BSONHolder3> avsi = ()->gen;

        //Vertex Data Structure
        RBTree<Integer,BSONHolder3> orderedTree = new RBTree<>(true);
        RBTree<Integer,BSONHolder3> indexedTree = new RBTree<>(true);
        System.out.print("Generating vertices in two data structures…");
        long t = System.currentTimeMillis();
        for (BSONHolder3 v : avsi) {
            orderedTree.insert(v.hash,v);
            indexedTree.insert(v.id,v);
        }
        t = System.currentTimeMillis() - t;
        System.out.println(" done in "+(t/1000)+" seconds");

        System.out.print("Loading in/out edges… ");
        //Adding the edges to each vertex.
        ReadAdjacencyFile  outgoingFile = new ReadAdjacencyFile("\t","/Users/vasistas/Desktop/soc-LiveJournal1.txt/soc-LiveJournal1.txt"),
                ingoingFile = new ReadAdjacencyFile("\\s","/Users/vasistas/Desktop/soc-LiveJournal1.txt/incomingEdges.txt");
        Pair<Integer,Set<Integer>> left = null, right = null;
        t = System.currentTimeMillis();
        if (left==null && ingoingFile.hasNext()) left = ingoingFile.next();
        if (right==null && outgoingFile.hasNext()) right = outgoingFile.next();
        while (ingoingFile.hasNext() && outgoingFile.hasNext()) {
            int leftNow = left.first;
            int rightNow = right.first;
            if (leftNow==rightNow) {
                RBTreePointer<Integer, BSONHolder3> ptr = indexedTree.lookup(leftNow);
                if (ptr==null)
                    System.out.println("Break-ptr-null");
                else if (ptr.first()==null)
                    System.out.println("Break-firstofptr-null");
                else if (ptr.first().ingoing==null)
                    System.out.println("Break-first-null");

                int index = left.first;
                for (int ingoing : left.value) {
                    RBTreePointer<Integer, BSONHolder3> l = indexedTree.lookup(ingoing);
                    if (l!=null)
                    ptr.first().ingoing.add(l.first());
                }
                for (int outgoing : right.value) {
                    RBTreePointer<Integer, BSONHolder3> l = indexedTree.lookup(outgoing);
                    if (l!=null)
                    ptr.first().outgoing.add(l.first());
                }
                left = ingoingFile.next();
                right = outgoingFile.next();
            } else if (leftNow<rightNow){
                int index = leftNow;
                RBTreePointer<Integer, BSONHolder3> ptr = indexedTree.lookup(index);
                for (int ingoing : left.value) {
                    RBTreePointer<Integer, BSONHolder3> l = indexedTree.lookup(ingoing);
                    if (l!=null)
                    ptr.first().ingoing.add(l.first());
                }
                left = ingoingFile.next();
            } else {
                int index = rightNow;
                RBTreePointer<Integer, BSONHolder3> ptr = indexedTree.lookup(index);
                for (int outgoing : right.value) {
                    RBTreePointer<Integer, BSONHolder3> l = indexedTree.lookup(outgoing);
                    if (l!=null)
                    ptr.first().outgoing.add(l.first());
                }
                right = outgoingFile.next();
            }
        }
        while (ingoingFile.hasNext()) {
            int index = left.first;
            RBTreePointer<Integer, BSONHolder3> ptr = indexedTree.lookup(index);
            for (int ingoing : left.value) {
                RBTreePointer<Integer, BSONHolder3> l = indexedTree.lookup(ingoing);
                if (l!=null)
                ptr.first().ingoing.add(l.first());
            }
            left = ingoingFile.next();
        }
        while (outgoingFile.hasNext()) {
            int index = right.first;
            RBTreePointer<Integer, BSONHolder3> ptr = indexedTree.lookup(index);
            for (int outgoing : right.value) {
                RBTreePointer<Integer, BSONHolder3> l = indexedTree.lookup(outgoing);
                if (l!=null)
                ptr.first().outgoing.add(l.first());
            }
            right = outgoingFile.next();
        }
        t = System.currentTimeMillis() - t;
        System.out.println(" done in "+(t/1000)+" seconds");

        policyMultiFileWithIndex(orderedTree);

    }


    public static FileOutputStream createFile(String filename) {
        File file = new File(filename);
        if (file.exists()) file.delete();
        try {
            return new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }


    public void policyMultiFileWithIndex(RBTree<Integer,BSONHolder3> orderedTree) {
        System.out.print("Write MultiFile… ");
        long t = System.currentTimeMillis();
        Iterable<MultiMapValuePointer<Integer, BSONHolder3>> it = new I<>(orderedTree.treeIterator());
        TreeSet<IndexHashPos> indexFile = new TreeSet<>();

        FileOutputStream fop = createFile("/Users/vasistas/graphdb/test/vertices.bin");
        Writer vertexWriter = new Writer(fop);

        FileOutputStream hashIndexFile = createFile("/Users/vasistas/graphdb/test/hashes.index");
        Writer hashWriter = new Writer(hashIndexFile);

        try {
            //Counting the offset of the hashlist
            long hashCounter = 0;
            for (MultiMapValuePointer<Integer, BSONHolder3> ptr : new I<>(orderedTree.treeIterator())) {
                Integer hashValue = ptr.getKey();

                //Position of the single vertex locally to the hash offset
                //int vertexPositionLocallyToHash = 0;

                //Creating an entry on the hash list element
                new HashListEntryFormat(hashValue,hashCounter).serialize(hashWriter);
                hashIndexFile.flush();

                for (BSONHolder3 key : new I<>(ptr.iterator())) {
                    //Setting the position of the current vertex
                    indexFile.add(new IndexHashPos(key.id,hashValue,hashCounter));

                    //Serializing the vertices in the vertices file. Defining the offset of the next vertex
                    hashCounter += key.serialize(vertexWriter);
                }
                fop.flush();

                //Incrementing the offset globally for the hash element
                //hashCounter += (long)vertexPositionLocallyToHash;
            }
            hashIndexFile.close();
            fop.close();

            //Writing the index file using the inverse pointer strategy

            /**
             * TODO: alternative
             *
             */
            FileOutputStream indexHashPos = createFile("/Users/vasistas/graphdb/test/id.index");
            FileMapsUtils.writeIteration(indexFile,new Writer(indexHashPos));
            indexHashPos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String args[]) {
        new StoreVerticesMap3().load();
    }

}
