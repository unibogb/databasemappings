/*
 * VertexGenerator3.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.filemaps.rw3;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import it.giacomobergami.data.GenerateIpLoc;
import it.giacomobergami.data.GenerateOrganization;
import it.giacomobergami.data.GenerateYear;
import it.giacomobergami.data.GeneratorConfiguration;
import it.giacomobergami.filemaps.formats.BSONHolder3;

import java.util.Iterator;

/**
 * Created by vasistas on 23/04/16.
 */
public class VertexGenerator3 implements Iterator<BSONHolder3> {

    final GenerateIpLoc ip;
    final GenerateOrganization org;
    final GenerateYear ye;
    private final int upTo;
    private int count;

    public static int multipleHashString(String... stringsToHash) {
        final int prime = 31;
        int result = 1;
        for( String s : stringsToHash )
        {
            result = result * prime + s.hashCode();
        }
        return result;
    }

    @Inject
    public VertexGenerator3(@Named("vertices.upTo") int upTo)  {
        this.upTo = upTo;
        ip = GeneratorConfiguration.getInstance().getInstance(GenerateIpLoc.class);
        org = GeneratorConfiguration.getInstance().getInstance(GenerateOrganization.class);
        ye = GeneratorConfiguration.getInstance().getInstance(GenerateYear.class);
        count = 0;
    }

    private static VertexGenerator3 self = null;
    public static VertexGenerator3 getInstance() {
        if (self==null)
            self = GeneratorConfiguration.getInstance().getInstance(VertexGenerator3.class);
        return self;
    }

    public boolean hasNext() {
        return count < upTo;
    }

    public BSONHolder3 next() {
        if (!hasNext()) {
            return null;
        } else {
            String ORG = org.next();
            String Y = ye.next();
            String IpOrgYear[] = new String[]{ip.next().ip,ORG,Y};
            int id = count;
            int hash = multipleHashString(ORG,Y);
            BSONHolder3 v = new BSONHolder3(IpOrgYear,id,hash);
            count++;
            return v;
        }
    }

}
