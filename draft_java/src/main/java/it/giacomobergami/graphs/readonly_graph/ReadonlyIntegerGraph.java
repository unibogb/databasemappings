/*
 * ReadonlyIntegerGraph.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.readonly_graph;

import it.giacomobergami.filemaps.buffers.BufferArray;
import it.giacomobergami.filemaps.buffers.LongMMFile;
import it.giacomobergami.filemaps.formats.BSONHolder3;
import it.giacomobergami.filemaps.formats.IndexHashPos;
import it.giacomobergami.graphs.GoogleGraph.DoubleHashVertexSet;
import it.giacomobergami.graphs.abstractgraph.AbstractGraph;
import it.giacomobergami.graphs.abstractgraph.AbstractNode;
import it.giacomobergami.graphs.abstractgraph.GeneralEdge;
import it.giacomobergami.utils.datastructures.iterators.MapIterator;
import it.giacomobergami.utils.datastructures.trees.iterator.IteratorOfIterator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.function.Supplier;

/**
 * This class is used as a view for the secondary-memory graph when doing graph sampling
 */
public class ReadonlyIntegerGraph /*extends AbstractGraph<Integer>*/ {

    final LongMMFile lmf;
    final BufferArray<IndexHashPos> ba;
    final LongMMFile vertices;
    public final Hashtable<String,Integer> attributeToPosition;

    /**
     * Reading the graph schema from the folder
     * @param folder
     * @throws IOException
     */
    public ReadonlyIntegerGraph(File folder) throws IOException {
        this(folder, Files.readAllLines(new File(folder,"schema.txt").toPath()).toArray(new String[]{}));
    }

    /**
     * Dynamically sets the graph schema
     * @param path
     * @param lines
     * @throws IOException
     */
    public ReadonlyIntegerGraph(File path, String... lines) throws IOException {
        lmf = new LongMMFile(new File(path, "id.index"));
        vertices = new LongMMFile(new File(path,"vertices.bin"));
        ba = new BufferArray<>(IndexHashPos.class,lmf);
        attributeToPosition = new Hashtable<>();
        int count = 0;
        for (String line : lines) {
            attributeToPosition.put(line,count++);
        }
    }

    public BSONHolder3 getRawVertexById(int index) {
        IndexHashPos vertexPtr = ba.get(index);
        if (vertexPtr==null) return null;
        long actualPtr = vertexPtr.pos;
        BSONHolder3 holder = BSONHolder3.unmarshall(vertices,actualPtr);
        holder.hash = vertexPtr.hash;
        return holder;
    }

    
    public boolean hasVertex(Integer index) {
        return ba.get(index)!=null;
    }

    
    public Integer getVertex(Integer tmp) {
        return ba.get(tmp)!=null ? tmp : null;
    }

    
    public Collection<Integer> getNeighbours(Integer src) {
        ReadonlyIntegerGraph g = this;
        return new Collection<Integer>() {
            BSONHolder3 h = getRawVertexById(src);
            
            
            public int size() {
                return (int)h.outgoingA.size();
            }
            
            public boolean isEmpty() {
                return h.outgoingA.size()==0L;
            }
            
            public boolean contains(Object o) {
                return o instanceof Integer && hasVertex((Integer) o);
            }

            
            public Iterator<Integer> iterator() {
                return h.outgoingA.iterator();
            }

            
            public Object[] toArray() {
                Object toret[] = new Object[(int)h.outgoingA.size()];
                Iterator<Integer> eit = iterator();
                int i=0;
                while (eit.hasNext()) toret[i++] = eit.next();
                return toret;
            }

            
            public <T> T[] toArray(T[] toret) {
                Iterator<Integer> eit = iterator();
                int i=0;
                while (eit.hasNext()) toret[i++] = (T)eit.next();
                return toret;
            }

            
            public boolean containsAll(Collection<?> c) {
                for (Object x :c) if (!contains(x)) return false;
                return true;
            }

            /////
            
            public boolean add(Integer bsonNode) {
                throw new RuntimeException("Error: cannot add a vertex into a read-only vertex");
            }

            
            public boolean remove(Object o) {
                throw new RuntimeException("Error: cannot remove a vertex into a read-only vertex");
            }

            
            public boolean addAll(Collection<? extends Integer> c) {
                throw new RuntimeException("Error: cannot add a vertex into a read-only vertex");
            }

            
            public boolean removeAll(Collection<?> c) {
                throw new RuntimeException("Error: cannot remove a vertex into a read-only vertex");
            }

            
            public boolean retainAll(Collection<?> c) {
                throw new RuntimeException("Error: cannot remove a vertex into a read-only vertex");
            }

            
            public void clear() {
                throw new RuntimeException("Error: cannot remove a vertex into a read-only vertex");
            }
        };
    }

     @Deprecated
    public Iterator<GeneralEdge> getEdges() {
        throw new RuntimeException("Error: cannot render true edges as pair of integers");
    }
    
    public Set<Integer> getVertices() {
        return new Set<Integer>() {
            
            public int size() {
                //System.out.println(ba.size());
                return (int)ba.size();
            }

            
            public boolean isEmpty() {
                return ba.size()>0L;
            }

            
            public boolean contains(Object o) {
                return o instanceof Integer && hasVertex((Integer) o);
            }

            
            public boolean containsAll(Collection<?> c) {
                for (Object o : c) if (!contains(o)) return false;
                return true;
            }

            
            public Iterator<Integer> iterator() {
                return new MapIterator<IndexHashPos, Integer>(ba.iterator()){
                    
                    public Integer apply(IndexHashPos indexHashPos) {
                        return indexHashPos.index;
                    }
                };
            }

            
            public Object[] toArray() {
                int i= 0;
                Object a[] = new Object[(int)ba.size()];
                Iterator<Integer> it = iterator();
                while (it.hasNext()) a[i++] = it.next();
                return a;
            }

            
            public <T> T[] toArray(T[] a) {
                int i=0;
                Iterator<Integer> it = iterator();
                while (it.hasNext()) a[i++] = (T)it.next();
                return a;
            }

            //////////
            
            public boolean add(Integer bsonNode) {
                throw new RuntimeException("Error: cannot add multiple vertices into a read-only vertex");
            }

            
            public boolean remove(Object o) {
                throw new RuntimeException("Error: cannot add multiple vertices into a read-only vertex");
            }

            
            public boolean addAll(Collection<? extends Integer> c) {
                throw new RuntimeException("Error: cannot add multiple vertices into a read-only vertex");
            }

            
            public boolean retainAll(Collection<?> c) {
                throw new RuntimeException("Error: cannot add multiple vertices into a read-only vertex");
            }

            
            public boolean removeAll(Collection<?> c) {
                throw new RuntimeException("Error: cannot add multiple vertices into a read-only vertex");
            }

            
            public void clear() {
                throw new RuntimeException("Error: cannot add multiple vertices into a read-only vertex");
            }
        };
    }

    
    public int size() {
        int vSize = (int)ba.size();
        for (int i=0; i<vSize; i++) {
            vSize += getRawVertexById(i).outgoingA.size();
        }
        return vSize;
    }
    
    public boolean prop(AbstractNode left, AbstractNode right) {
        return isEdge(left.getId().hashCode(),right.getId().hashCode());
    }
    
    public boolean isEdge(Integer src, Integer dst) {
        BSONHolder3 srcH = getRawVertexById(src), dstH = getRawVertexById(dst);
        int srcSize = (int)srcH.outgoingA.size(), dstSize = (int)dstH.ingoingA.size();
        if (srcSize<dstSize) {
            for (int i=0; i<srcSize; i++)
                if (srcH.outgoingA.get(i)==dst)
                    return true;
            return false;
        } else {
            for (int i=0; i<dstSize; i++)
                if (srcH.ingoingA.get(i)==src)
                    return true;
            return false;
        }
    }


}
