/*
 * INeighGraph.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.intermediateTo2;

import it.giacomobergami.algorithms.RelationshipCache;
import it.giacomobergami.graphs.abstractgraph.AbstractGraph;
import it.giacomobergami.graphs.abstractgraph.AbstractNode;
import it.giacomobergami.graphs.abstractgraph.GeneralEdge;
import it.giacomobergami.graphs.abstractgraph.VertexStoreStrategy;
import it.giacomobergami.graphs.nodes.GoogleGraphVertex;
import it.giacomobergami.graphs.nodes.neigh.StrategyRB;
import it.giacomobergami.graphs.pointers.GraphBulkBinPointer;
import it.giacomobergami.graphs.pointers.UniqueGenerator;
import it.giacomobergami.utils.I;
import org.apache.jcs.JCS;
import org.apache.jcs.access.CacheAccess;
import org.apache.jcs.access.exception.CacheException;

import java.util.*;
import java.util.function.Supplier;

/**
 * Putting the forward and backward adjacency inside each vertex (reducing the seek operations)
 */
public  abstract class INeighGraph<V extends INeighVertex> extends AbstractGraph<V> {

    //private IAdjacency<AbstractNode,MultiHashAdjacencyMap> forwardAdjacenecy,backwardAdjacency;
    private UniqueGenerator ug;
    public CacheAccess neighCache;

    public INeighGraph() {
        this(VertexStoreStrategy.StoreVertexByUniqueId);
    }

    public INeighGraph(VertexStoreStrategy uniqueElementStrategy) {
        ug = new UniqueGenerator();
        try {
            neighCache = JCS.getInstance("default");
        } catch (CacheException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public Collection<INeighVertex> getNeighbours(INeighVertex x) {
        return new Collection<INeighVertex>() {

            INeighVertex src = x;
            RelationshipCache cache = new RelationshipCache(100000);

            @Override
            public int size() {
                return src.neigh.getSize();
            }

            @Override
            public boolean isEmpty() {
                return src.neigh.isEmpty();
            }

            @Override
            public boolean contains(Object o) {
                throw new RuntimeException("MessyCode");

                //return cache.get(src,(GoogleGraphVertex)o);
            }

            @Override
            public Iterator<INeighVertex> iterator() {
                return new Iterator<INeighVertex>() {
                    Iterator<INeighVertex> it = src.neigh.iterator();

                    @Override
                    public boolean hasNext() {
                        return it.hasNext();
                    }

                    @Override
                    public INeighVertex next() {
                        return it.next();
                    }
                };
            }

            @Override
            public Object[] toArray() {
                Object[] a = new Object[size()];
                int i = 0;
                Iterator<INeighVertex> it = iterator();
                while (it.hasNext()) a[i++] = it.next();
                return a;
            }

            @Override
            public <T> T[] toArray(T[] a) {
                int i = 0;
                Iterator<INeighVertex> it = iterator();
                while (it.hasNext()) a[i++] = (T) it.next();
                return a;
            }

            @Override
            public boolean add(INeighVertex googleGraphVertex) {
                cache.put(x, googleGraphVertex);
                return src.neigh.addNeighbour(googleGraphVertex);
            }

            @Override
            public boolean remove(Object o) {
                cache.remove(x, (GoogleGraphVertex) o);
                return src.neigh.removeNeighbour((GoogleGraphVertex) o);
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                for (Object x : c) if (!contains(x)) return false;
                return true;
            }

            @Override
            public boolean addAll(Collection<? extends INeighVertex> c) {
                boolean toret = true;
                for (INeighVertex x : c) toret = toret && add(x);
                return toret;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                boolean toret = true;
                for (Object x : c) toret = toret && remove(x);
                return toret;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                ArrayList<Object> torem = new ArrayList<>();
                for (INeighVertex x : new I<>(iterator())) if (!c.contains(x)) torem.add(x);
                return removeAll(torem);
            }

            @Override
            public void clear() {
                src.neigh = new StrategyRB(src);
            }

            @Override
            public boolean equals(Object o) {
                if (!(o instanceof Collection)) return false;
                return containsAll((Collection) o);
            }

            @Override
            public int hashCode() {
                final int prime = 17;
                int result = 3;
                for (INeighVertex k : new I<>(iterator())) {
                    result = result * prime + k.hashCode();
                }
                return result;
            }
        };
    }

    @Override
    public GeneralEdge addEdge(INeighVertex src, INeighVertex dst) {
        return src.addNeighbour(dst) ? new GeneralEdge(src,dst) : null;
    }

    @Override
    public boolean removeEdge(INeighVertex src, INeighVertex dst) {
        try {
            neighCache.remove(new GraphBulkBinPointer(src,dst));
        } catch (CacheException e) {
        }
        return src.removeNeighbour(dst);
    }


    @Override
    public int size() {
        int count = 0;
        for (INeighVertex a : getVertices()) {
            count += (a.neigh.sizeOutgoing()+1);
        }
        return count;
    }

    @Override
    public <T> Optional<T> startTransaction(Supplier<T> f) {
        return Optional.of(f.get()); //No transactions
    }

    @Override
    public void close() { }


    @Override
    public boolean prop(AbstractNode left, AbstractNode right) {
        return isEdge((V)left,(V)right);
    }

    @Override
        public String toString() {
        return "";
    }

}
