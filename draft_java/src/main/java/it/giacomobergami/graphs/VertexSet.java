/*
 * VertexSet.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs;

import it.giacomobergami.graphs.abstractgraph.AbstractNode;
import it.giacomobergami.graphs.abstractgraph.VertexStoreStrategy;
import it.giacomobergami.utils.datastructures.trees.iterator.IteratorOfValues;
import it.giacomobergami.utils.datastructures.trees.iterator.TreeKeyIterator;
import it.giacomobergami.utils.datastructures.trees.iterator.TreeMultiMapValuePointerIterator;
import it.giacomobergami.utils.datastructures.trees.rbtree.RBTree;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by vasistas on 16/04/16.
 */
public class VertexSet<E extends AbstractNode> implements Set<E> {

    private final boolean isStack;
    public RBTree<Integer,E> tree;
    private boolean storeWithSearchHash;
    private VertexStoreStrategy strategy;

    /**
     *  @param isStack if this parameter is true, the overflow list doesn't check if the element already exists
     *                (assuming unique insertions) but adds the element at the beginning of the list (it behaves
     *                as a stack). Otherwise, the list is scanned linearly and the elment is added
     * @param hashPolicy if this parameter is true, stores the element using the .searchhash method, and uses the
     * @param strategy Points out the possible strategy that could be adopted to store the vertex in the current VertexSet.
     *                 It affects the size of the tree data structure and the length of the overflow lists
     */
    public VertexSet(boolean isStack, boolean hashPolicy, VertexStoreStrategy strategy) {
        this.isStack = isStack;
        this.storeWithSearchHash = hashPolicy;
        this.strategy = strategy;
        this.tree = new RBTree<>(isStack);
    }

    public static <E extends AbstractNode> VertexSet<E> createSetfromRBTree(RBTree<Integer,E> tree, boolean hashPolicy) {
        VertexSet<E> toret = new VertexSet<>(tree.isStack,hashPolicy,hashPolicy ? VertexStoreStrategy.StoreVertexByHashingSchema : VertexStoreStrategy.StoreVertexByUniqueId);
        toret.tree = tree; //Setting the tree manually
        return toret;
    }

    public  static int evaluateIndexingStrategy(VertexStoreStrategy strategy, AbstractNode n) {
        switch (strategy) {
            case StoreVertexByFullValueHash:
                return n.hashCode();
            case StoreVertexByHashingSchema:
                return n.searchhash();
            case StoreVertexByUniqueId:
                return n.id.hashCode();
            default:
                throw new UnsupportedOperationException("Current Strategy is not recognized: "+strategy.name());
        }
    }

    @Override
    public int size() {
        return tree.getSize();
    }

    @Override
    public boolean isEmpty() {
        return tree.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        E elem = (E)o;
        return tree.contains(storeWithSearchHash ? elem.searchhash() : elem.hashCode(),elem);
    }

    @Override
    public Iterator<E> iterator() {
        return new IteratorOfValues<>(tree.treeIterator());
    }

    @Override
    public Object[] toArray() {
        Object[] a =  new Object[size()];
        Iterator<E> it = iterator();
        int i = 0;
        while (it.hasNext()) {
            a[i++] = it.next();
        }
        return a;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        Iterator<E> it = iterator();
        int i = 0;
        while (it.hasNext()) {
            a[i++] = (T)it.next();
        }
        return a;
    }

    @Override
    public boolean add(E e) {
        return tree.insert(storeWithSearchHash ? e.searchhash() : e.hashCode(),e);
    }

    @Override
    public boolean remove(Object o) {
        E elem = (E)o;
        return tree.delete(storeWithSearchHash ? elem.searchhash() : elem.hashCode(),elem);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) if (!contains(o)) return false;
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean toret = true;
        for (E x : c) toret = toret && add(x);
        return toret;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        RBTree<Integer,E> hash = new RBTree<>(isStack);
        boolean toret = true;
        for (Object x : c)
            if (contains(x)) {
                E y = (E)x;
                hash.insert(y.searchhash(),y);
            }
            else toret = false;
        tree = hash;
        return toret;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean toret = true;
        for (Object x : c) toret = toret && remove(x);
        return toret;
    }

    @Override
    public void clear() {
        this.tree = new RBTree<>(isStack);
    }

    public TreeMultiMapValuePointerIterator<Integer, E> treePointerIterator() {
        return tree.treeIterator();
    }

    public Iterator<Integer> getHashes() {
        return new TreeKeyIterator<>(tree.root);
    }
}
