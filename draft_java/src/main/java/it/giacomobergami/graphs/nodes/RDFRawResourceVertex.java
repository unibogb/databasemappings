/*
 * RDFRawResourceVertex.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.nodes;

import it.giacomobergami.graphs.abstractgraph.AbstractGraph;
import it.giacomobergami.graphs.abstractgraph.AbstractNode;
import it.giacomobergami.graphs.nodes.GoogleGraphVertex;
import it.giacomobergami.utils.datastructures.HashableHashSet;
import it.giacomobergami.utils.datastructures.HashableTreeMap;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Defines the raw RDF vertex, as defined in the RDF data model (that is only a resource identifier)
 */
public class RDFRawResourceVertex extends AbstractNode {

    private URI resourceID;
    private HashableTreeMap<String,String> fake;
    private HashSet<String> fakeSchema;
    private HashableHashSet<String> fakeType;
    public final static String URI_fake_attribute = "UID";

    public URI getResourceID() {
        return resourceID;
    }

    public static String resourceURItoResourceName(URI res) {
        return  (res.getFragment()==null) ? Paths.get(res.getPath()).getFileName().toString() : res.getFragment();
    }

    public RDFRawResourceVertex(String resourceID) throws URISyntaxException {
        this(new URI(resourceID),null);
    }

    public RDFRawResourceVertex(AbstractNode u,Set<String> hashingScheme) throws URISyntaxException {
        this("http://jackbergus.alwaysdata.net/nodes/" + AbstractGraph.jointype(u) + "/" + AbstractGraph.joinschema(u) + "#" + (u.getAttribute(URI_fake_attribute)));
    }
    public RDFRawResourceVertex(URI resourceID) {
        this(resourceID,null);
    }

    public RDFRawResourceVertex(URI resourceID,Set<String> hashingScheme) {
        super(hashingScheme, null);
        this.resourceID = resourceID;
        fake = new HashableTreeMap<>();
        fake.put(URI_fake_attribute,resourceID.toString());
        fakeSchema = new HashSet<>();
        fakeSchema.add(URI_fake_attribute);
        fakeType = new HashableHashSet<>();
        String type = resourceID.toString().replace(resourceURItoResourceName(resourceID),"");
        //if (type.endsWith("#")) type = type.replace("#","");
        fakeType.add(type);
    }

    @Override
    public String getAttribute(String attr) {
        return resourceID.toString();
    }

    @Override
    public void updateAll(Map<String, String> externMap) {
        throw new UnsupportedOperationException("Error: RDF vertices are IDs, and hence are immutable value structures (updateAll)");
    }


    @Override
    public void setAttribute(String attr, String value) {
        throw new UnsupportedOperationException("Error: RDF vertices are IDs, and hence are immutable value structures (setAttribute)");
    }

    @Override
    public Map<String,Comparable> cloneAttributes() {
        return (Map<String,Comparable>)fake.clone();
    }

    @Override
    public Set<String> getVertexSchema() {
        return fakeSchema;
    }

    @Override
    public HashableTreeMap<String,String> getValues() {
        return fake;
    }

    @Override
    public HashableHashSet<String> getType() {
        return fakeType;
    }

    @Override
    public boolean removeNeighbour(AbstractNode dst) {
        throw new UnsupportedOperationException("Error: RDF vertices are IDs, and hence are immutable value structures (setAttribute)");
    }

    @Override
    public boolean hasNeighbour(AbstractNode right) {
        throw new UnsupportedOperationException("Error: RDF vertices are IDs, and hence are immutable value structures (setAttribute)");

    }


}
