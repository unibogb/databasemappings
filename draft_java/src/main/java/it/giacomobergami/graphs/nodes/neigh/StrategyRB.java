/*
 * StrategyRB.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.nodes.neigh;

import it.giacomobergami.graphs.VertexSet;
import it.giacomobergami.graphs.abstractgraph.VertexStoreStrategy;
import it.giacomobergami.graphs.nodes.GoogleGraphVertex;
import it.giacomobergami.graphs.intermediateTo2.INeighVertex;
import it.giacomobergami.utils.datastructures.sets.UnidirectionalStack;
import java.util.Iterator;


public class StrategyRB implements INeigh {
    private final INeighVertex master;
    private VertexSet<INeighVertex> neigh, incoming;
    private UnidirectionalStack<INeighVertex> fastIterator;


    public StrategyRB(INeighVertex master) {
        this.master = master;
        neigh = new VertexSet<>(true,false, VertexStoreStrategy.StoreVertexByUniqueId);
        incoming = new VertexSet<>(true,false, VertexStoreStrategy.StoreVertexByUniqueId);
        fastIterator = new UnidirectionalStack<>();
    }

    @Override
    @Deprecated
    public void addBackEdge(INeighVertex originalMaster) {
        incoming.add(originalMaster);
    }

    @Override
    public int sizeIngoing() {
        return incoming.size();
    }

    @Override
    public Iterator<INeighVertex> iteratorIngoing() {
        return incoming.iterator();
    }

    @Override
    public int sizeOutgoing() {
        return neigh.size();
    }

    @Override
    public Iterator<INeighVertex> iteratorOutgoing() {
        return neigh.iterator();
    }

    @Override @Deprecated
    public void deleteBackEdge(INeighVertex master) {
        incoming.remove(master);
    }

    @Override
    public int getSize() {
        return fastIterator.getSize();
    }

    @Override
    public boolean isEmpty() {
        return fastIterator.getSize()==0;
    }

    @Override
    public boolean addNeighbour(INeighVertex av) {
        boolean toret = neigh.add(av);
        if (toret) av.neigh.addBackEdge(master);
        fastIterator.add(av);
        return toret;
    }

    @Override
    public boolean removeNeighbour(INeighVertex dst) {
        boolean toret = neigh.remove(dst);
        if (toret) ((GoogleGraphVertex) dst).neigh.deleteBackEdge(master);
        fastIterator.remove(dst);
        return toret;
    }

    @Override
    public boolean hasNeighbour(INeighVertex dst) {
        return (neigh.size() < dst.neigh.sizeIngoing()) ? neigh.contains(dst) : dst.neigh.hasIncoming(master);
    }

    @Override
    public boolean hasIncoming(INeighVertex src) {
        return incoming.contains(src);
    }

    @Override
    public Iterator<INeighVertex> iterator() {
        return fastIterator.iterator();
    }
}
