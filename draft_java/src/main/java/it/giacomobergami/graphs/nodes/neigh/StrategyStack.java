/*
 * StrategyStack.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.nodes.neigh;

import it.giacomobergami.graphs.intermediateTo2.INeighVertex;
import it.giacomobergami.utils.datastructures.sets.UnidirectionalStack;

import java.util.Iterator;

/**
 * Created by vasistas on 01/05/16.
 */
public class StrategyStack implements INeigh {

    public UnidirectionalStack<INeighVertex> ingoing;
    public UnidirectionalStack<INeighVertex> outgoing;
    public final INeighVertex master;

    public StrategyStack(INeighVertex master) {
        this.master = master;
        ingoing = new UnidirectionalStack<>();
        outgoing = new UnidirectionalStack<>();
    }

    @Override
    public boolean addNeighbour(INeighVertex av) {
        if (outgoing.add(av)) {
            av.neigh.addBackEdge(master);
            return true;
        } else return false;
    }

    @Override
    public boolean removeNeighbour(INeighVertex dst) {
        if (outgoing.remove(dst)) {
            dst.neigh.deleteBackEdge(master);
            return true;
        } else return false;
    }

    @Override
    public boolean hasNeighbour(INeighVertex dst) {
        return (outgoing.size < dst.neigh.sizeIngoing()) ? outgoing.contains(dst) : dst.neigh.hasIncoming(master);
    }

    @Override
    public boolean hasIncoming(INeighVertex src) {
        return ingoing.contains(src);
    }

    @Override
    public void addBackEdge(INeighVertex originalMaster) {
        ingoing.add(originalMaster);
    }

    @Override
    public int sizeIngoing() {
        return ingoing.size;
    }

    @Override
    public Iterator<INeighVertex> iteratorIngoing() {
        return ingoing.iterator();
    }

    @Override
    public int sizeOutgoing() {
        return outgoing.size;
    }

    @Override
    public Iterator<INeighVertex> iteratorOutgoing() {
        return outgoing.iterator();
    }

    @Override
    public void deleteBackEdge(INeighVertex master) {
        ingoing.remove(master);
    }

    @Override
    public int getSize() {
        return outgoing.size;
    }

    @Override
    public boolean isEmpty() {
        return outgoing.size==0;
    }

    @Override
    public Iterator<INeighVertex> iterator() {
        return outgoing.iterator();
    }
}
