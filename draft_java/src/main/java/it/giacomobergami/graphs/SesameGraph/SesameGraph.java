/*
 * SesameGraph.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.SesameGraph;

import it.giacomobergami.graphs.abstractgraph.AbstractGraph;
import it.giacomobergami.graphs.intermediateToRDF.AbstractRDFGraph;
import it.giacomobergami.graphs.intermediateToRDF.RDFEdge;
import it.giacomobergami.graphs.nodes.RDFRawResourceVertex;
import it.giacomobergami.utils.datastructures.HashableHashSet;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.eclipse.rdf4j.sail.nativerdf.NativeStore;

import java.io.*;
import java.net.URISyntaxException;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Implements a Sesame Graph
 */
public class SesameGraph extends AbstractRDFGraph {

    private Repository repo;
    private ValueFactory f;
    public RepositoryConnection conn;
    private HashMap<String,RDFRawResourceVertex> map;

    public SesameGraph(File dataDir) {
        repo = new SailRepository(new NativeStore(dataDir,"spoc,posc,cosp"));
        repo.initialize();
        f = repo.getValueFactory();
        map = new HashMap<>();
        conn = repo.getConnection();
    }

    public SesameGraph() {
        repo = new SailRepository(new MemoryStore());
        repo.initialize();
        f = repo.getValueFactory();
        map = new HashMap<>();
        conn = repo.getConnection();
    }

    public SesameGraph(InputStream leftStream, String leftPath, InputStream rightStream, String rightPath) {
        repo = new SailRepository(new MemoryStore());
        repo.initialize();
        f = repo.getValueFactory();
        map = new HashMap<>();
        conn = repo.getConnection();
        try {
            conn.add(leftStream,leftPath, RDFFormat.TURTLE);
            conn.add(rightStream,rightPath, RDFFormat.TURTLE);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    protected IRI rawCreateIRI(String src) {
        return f.createIRI(src);
    }

    protected IRI createIRI(RDFRawResourceVertex src) {
        try {
            return rawCreateIRI(src.getResourceID().toString());
        } catch (Exception e) {
            String srcV = "http://jackbergus.alwaysdata.net/nodes/" + AbstractGraph.jointype(src) + "/" + AbstractGraph.joinschema(src) + "#" + src.getAttribute(RDFRawResourceVertex.URI_fake_attribute);
            return rawCreateIRI(srcV);
        }
    }

    protected RDFRawResourceVertex createResourceVertex(RDFRawResourceVertex src) {
        try {
            return new RDFRawResourceVertex(createIRI(src).toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean isEdge(RDFRawResourceVertex src, String label, RDFRawResourceVertex dst) {
        if (hasVertex(src) && hasVertex(dst)) {
            IRI iSrc = createIRI(src);
            IRI iDst = createIRI(dst);
            IRI prop = rawCreateIRI(label);
            return conn.getStatements(iSrc, prop, iDst).hasNext();
        } else return false;
    }

    @Override
    public boolean hasVertex(RDFRawResourceVertex src) {
        src = createResourceVertex(src);
        if (src==null) return false;
        return map.containsKey(createResourceVertex(src).getResourceID().toString());
    }

    @Override
    public RDFRawResourceVertex getVertex(RDFRawResourceVertex tmp) {
        tmp = createResourceVertex(tmp);
        if (tmp==null || !map.containsKey(tmp.getResourceID().toString())) return null;
        return map.get(tmp.getResourceID().toString());
    }

    @Override
    public Iterator<RDFEdge> getOutgoingEdges(RDFRawResourceVertex src) {
        if (hasVertex(src)) {
            IRI iSrc = createIRI(src);
            RepositoryResult<Statement> r  =  conn.getStatements(iSrc, null,null);
            return new Iterator<RDFEdge>() {
                @Override
                public boolean hasNext() {
                    return r.hasNext();
                }

                @Override
                public RDFEdge next() {
                    Statement rl = r.next();
                    try {
                        return new RDFEdge(rl.getSubject().stringValue(),rl.getPredicate().stringValue(),rl.getObject().stringValue());
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        System.exit(1);
                        return null;
                    }
                }
            };
        }
        return null;
    }

    @Override
    public Iterator<RDFEdge> getEdges() {
        RepositoryResult<Statement> r  =  conn.getStatements(null,null,null);
        return new Iterator<RDFEdge>() {
            @Override
            public boolean hasNext() {
                return r.hasNext();
            }

            @Override
            public RDFEdge next() {
                Statement rl = r.next();
                try {
                    return new RDFEdge(rl.getSubject().stringValue(),rl.getPredicate().stringValue(),rl.getObject().stringValue());
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    System.exit(1);
                    return null;
                }
            }
        };
    }



    @Override
    public RDFEdge addEdge(RDFRawResourceVertex src, String label, RDFRawResourceVertex dst) {
        if (hasVertex(src) && hasVertex(dst)) {
            IRI iSrc = createIRI(src);
            IRI iDst = createIRI(dst);
            IRI prop = rawCreateIRI(label);
            if (!isEdge(src,label,dst)) {
                conn.add(iSrc,prop,iDst);
                return new RDFEdge(src,label,dst);
            } else {
                Statement rl = conn.getStatements(iSrc, prop, iDst).next();
                try {
                    return new RDFEdge(rl.getSubject().stringValue(),rl.getPredicate().stringValue(),rl.getObject().stringValue());
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    System.exit(1);
                    return null;
                }
            }
        } else return null;
    }

    @Override
    public boolean removeEdge(RDFRawResourceVertex src, String label, RDFRawResourceVertex dst) {
        if (hasVertex(src) && hasVertex(dst)) {
            IRI iSrc = createIRI(src);
            IRI iDst = createIRI(dst);
            IRI prop = rawCreateIRI(label);
            conn.remove(conn.getStatements(iSrc, prop, iDst));
            return !isEdge(src, label, dst);
        } else return false;
    }

    @Override
    public RDFRawResourceVertex addVertex(RDFRawResourceVertex src) {
        if (src==null) return null;
        if (hasVertex(src)) {
            return getVertex(src);
        } else {
            RDFRawResourceVertex toret = createResourceVertex(src);
            map.put(toret.getResourceID().toString(),toret);
            return toret;
        }
    }

    @Override
    public void addVertices(Collection<? extends RDFRawResourceVertex> nodes) {
        for (RDFRawResourceVertex v : nodes) addVertex(v);
    }

    @Override
    public void removeVertex(RDFRawResourceVertex src) {
        if (hasVertex(src)) {
            IRI iSrc = createIRI(src);
            conn.remove(conn.getStatements(iSrc, null, null));
            map.remove(createResourceVertex(src).getResourceID().toString());
        }
    }

    @Override
    public HashableHashSet<RDFRawResourceVertex> getVertices() {
        return map.keySet().stream().map(key -> map.get(key)).collect(Collectors.toCollection(HashableHashSet::new));
    }

    @Override
    public int size() {
        int vsize = map.size();
        RepositoryResult<Statement> r = conn.getStatements(null, null, null);
        while (r.hasNext()) {
            r.next();
            vsize++;
        }
        return vsize;
    }

    @Override
    public <T> Optional<T> startTransaction(Supplier<T> f) {
        conn = repo.getConnection();
        Optional<T> toret;
        try {
            T l = f.get();
            //conn.commit();
            toret = Optional.of(l);
        } catch (Exception e) {
            e.printStackTrace();
            toret = Optional.empty();
        } finally {
            //conn.close();
        }
        return toret;
    }

    @Override
    public void close() {
        conn.close();
        repo.shutDown();
        repo = null;
        conn = null;
    }

    @Override
    public AbstractRDFGraph performConstructSPARQLquery(String s, int step, Writer fw) throws IOException {
        long t = System.nanoTime();
        GraphQuery gq = conn.prepareGraphQuery(QueryLanguage.SPARQL, s);
        GraphQueryResult gqr = gq.evaluate();
        Model results = QueryResults.asModel(gqr);
        t = System.nanoTime() - t;
        fw.write(step+","+t+"\n");
        fw.flush();
        return new SesameReadonlyQueryGraph(results,this);
    }

    @Override
    public void toTurtle(RDFWriter writer) {
        conn.prepareGraphQuery(QueryLanguage.SPARQL,
                "CONSTRUCT {?s ?p ?o } WHERE {?s ?p ?o } ").evaluate(writer);
    }

    @Override
    public void printTriples(PrintStream out) {
        conn.getStatements(null,null,null).asList().forEach(result->{
            out.println(result.getSubject().stringValue()+"-["+result.getPredicate().stringValue()+"]->"+result.getObject().stringValue());
        });
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof SesameGraph)) return super.equals(o);
        SesameGraph left = (SesameGraph)o;
        Iterator<RDFEdge> it = getEdges();
        Iterator<RDFEdge> it2 = left.getEdges();
        Set<RDFEdge> edgesSet = new HashSet<>();
        //The Sesame library returns all the edges in a string-ordered way
        while (it.hasNext()) {
            RDFEdge torem = it.next();
            edgesSet.add(torem);
        }
        while (it2.hasNext()) {
            RDFEdge torem = it2.next();
            if (!edgesSet.contains(torem))
                return false;
            else
                edgesSet.remove(torem);
        }
        return (edgesSet.isEmpty());
    }

}
