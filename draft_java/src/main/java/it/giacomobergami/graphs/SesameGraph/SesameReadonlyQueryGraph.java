/*
 * SesameReadonlyQueryGraph.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.SesameGraph;

import it.giacomobergami.graphs.intermediateToRDF.AbstractRDFGraph;
import it.giacomobergami.graphs.intermediateToRDF.RDFEdge;
import it.giacomobergami.graphs.nodes.RDFRawResourceVertex;
import it.giacomobergami.utils.datastructures.HashableHashSet;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.rio.RDFWriter;

import java.io.PrintStream;
import java.io.Writer;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created by vasistas on 02/04/16.
 */

public class SesameReadonlyQueryGraph extends SesameGraph {

    private Model queryResult;
    private SesameGraph master;

    public SesameReadonlyQueryGraph(Model queryResult, SesameGraph master) {
        this.queryResult = queryResult;
        this.master = master;
    }

    @Override
    public boolean isEdge(RDFRawResourceVertex src, String label, RDFRawResourceVertex dst) {
        IRI iSrc = createIRI(src);
        IRI iDst = createIRI(src);
        IRI prop = rawCreateIRI(label);
        return queryResult.contains(iSrc, prop, iDst);
    }

    @Override
    public boolean hasVertex(RDFRawResourceVertex src) {
        IRI iSrc = createIRI(src);
        return queryResult.contains(iSrc, null, null);
    }

    @Override
    public RDFRawResourceVertex getVertex(RDFRawResourceVertex tmp) {
        return (this.hasVertex(tmp)) ? super.getVertex(tmp) : null;
    }

    @Override
    public Iterator<RDFEdge> getOutgoingEdges(RDFRawResourceVertex src) {
        if (this.hasVertex(src)) {
            IRI iSrc = createIRI(src);
                Iterator<Statement> r = queryResult.filter(iSrc,null,null).iterator();
            return new Iterator<RDFEdge>() {
                @Override
                public boolean hasNext() {
                    return r.hasNext();
                }

                @Override
                public RDFEdge next() {
                    Statement rl = r.next();
                    try {
                        return new RDFEdge(rl.getSubject().stringValue(),rl.getPredicate().stringValue(),rl.getObject().stringValue());
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        System.exit(1);
                        return null;
                    }
                }
            };
        }
        return null;
    }

    @Override
    public Iterator<RDFEdge> getEdges() {
        Iterator<Statement> r = queryResult.filter(null,null,null).iterator();
        return new Iterator<RDFEdge>() {
            @Override
            public boolean hasNext() {
                return r.hasNext();
            }

            @Override
            public RDFEdge next() {
                Statement rl = r.next();
                try {
                    return new RDFEdge(rl.getSubject().stringValue(),rl.getPredicate().stringValue(),rl.getObject().stringValue());
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    System.exit(1);
                    return null;
                }
            }
        };
    }

    @Override
    public RDFEdge addEdge(RDFRawResourceVertex src, String label, RDFRawResourceVertex dst) {
        throw new UnsupportedOperationException("Cannot add an edge to a result: it falsifies the query.");
    }

    @Override
    public boolean removeEdge(RDFRawResourceVertex src, String label, RDFRawResourceVertex dst) {
        throw new UnsupportedOperationException("Cannot remove an edge to a result: it falsifies the query.");
    }

    @Override
    public RDFRawResourceVertex addVertex(RDFRawResourceVertex src) {
        throw new UnsupportedOperationException("Cannot add a vertex to a result: it falsifies the query.");
    }

    @Override
    public void addVertices(Collection<? extends RDFRawResourceVertex> nodes) {
        throw new UnsupportedOperationException("Cannot add vertices to a result: it falsifies the query.");
    }

    @Override
    public void removeVertex(RDFRawResourceVertex src) {
        throw new UnsupportedOperationException("Cannot remove a vertex to a result: it falsifies the query.");
    }

    @Override
    public HashableHashSet<RDFRawResourceVertex> getVertices() {
        return queryResult.subjects().stream().map(r -> {
            try {
                return new RDFRawResourceVertex(r.stringValue());
            } catch (URISyntaxException e) {
                e.printStackTrace();
                System.exit(1);
                return null;
            }
        }).collect(Collectors.toCollection(HashableHashSet<RDFRawResourceVertex>::new));
    }

    @Override
    public int size() {
        return queryResult.subjects().size()+queryResult.size();
    }

    @Override
    public <T> Optional<T> startTransaction(Supplier<T> f) {
        T elem = f.get();
        return elem==null ? Optional.empty() : Optional.of(elem);
    }

    @Override
    public void close() {
    }

    @Override
    public AbstractRDFGraph performConstructSPARQLquery(String s, int step, Writer fw) {
        throw new UnsupportedOperationException("Cannot perform a SPARQL query over a result by SESAME limitation.");
    }

    @Override
    public void toTurtle(RDFWriter writer) {
        throw new UnsupportedOperationException("Cannot perform a TTL serialization over a result by SESAME limitation.");
    }

    @Override
    public void printTriples(PrintStream out) {
        queryResult.forEach(result->{
            out.println(result.getSubject().stringValue()+"-["+result.getPredicate().stringValue()+"]->"+result.getObject().stringValue());
        });
    }

}
