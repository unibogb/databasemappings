/*
 * RDFEdge.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.intermediateToRDF;

import it.giacomobergami.graphs.nodes.RDFRawResourceVertex;

import java.net.URISyntaxException;

/**
 * Defines an edge for RDF Graphs
 */
public class RDFEdge {
    private String label;
    protected RDFRawResourceVertex src, dst;

    public RDFRawResourceVertex getSrc() {
        return src;
    }

    public RDFRawResourceVertex getDst() {
        return dst;
    }

    @Override
    public String toString() {
        return src.toString()+"-["+label+"]->"+dst.toString();
    }
    public RDFEdge(RDFRawResourceVertex src, String label, RDFRawResourceVertex dst) {
        this.label = label;
        this.src = src;
        this.dst = dst;
    }

    public RDFEdge(String src, String label, String dst) throws URISyntaxException {
        this(new RDFRawResourceVertex(src),label,new RDFRawResourceVertex(dst));
    }

    public String getLabel() {
        return label;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof RDFEdge)) return false;
        RDFEdge left = (RDFEdge)o;
        return src.equals(left.src) && dst.equals(left.dst) && label.equals(left.label);
    }

    @Override
    public int hashCode() {
        int result = label.hashCode();
        result = 31 * result + src.hashCode();
        result = 31 * result + dst.hashCode();
        return result;
    }
}
