/*
 * AbstractRDFGraph.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.intermediateToRDF;

import it.giacomobergami.graphs.nodes.RDFRawResourceVertex;
import it.giacomobergami.utils.datastructures.HashableHashSet;
import org.eclipse.rdf4j.rio.RDFWriter;

import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Defines a general graph model for RDF graphs (Jena, Sesame, etc.)
 */
public abstract class AbstractRDFGraph {

    public abstract boolean isEdge(RDFRawResourceVertex src, String label, RDFRawResourceVertex dst);
    public abstract boolean hasVertex(RDFRawResourceVertex src);
    public abstract RDFRawResourceVertex getVertex(RDFRawResourceVertex tmp);
    public abstract Iterator<RDFEdge> getOutgoingEdges(RDFRawResourceVertex src);
    public abstract Iterator<RDFEdge> getEdges();
    public abstract RDFEdge addEdge(RDFRawResourceVertex src, String label, RDFRawResourceVertex dst);
    public abstract boolean removeEdge(RDFRawResourceVertex src, String label, RDFRawResourceVertex dst);
    public abstract RDFRawResourceVertex addVertex(RDFRawResourceVertex src);
    public abstract void addVertices(Collection<? extends RDFRawResourceVertex> nodes);
    public abstract void removeVertex(RDFRawResourceVertex src);
    public abstract HashableHashSet<RDFRawResourceVertex> getVertices();

    public RDFRawResourceVertex addVertex(String url) {
        try {
            return addVertex(new RDFRawResourceVertex(url));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    public abstract int size();

    public abstract <T> Optional<T> startTransaction(Supplier<T> f );

    public abstract void close();

    /**
     * Returns the query as a subgraph
     * @param s
     * @param step
     * @param fw
     * @return
     */
    public abstract  AbstractRDFGraph performConstructSPARQLquery(String s, int step, Writer fw) throws IOException;


    @Override
    public boolean equals(Object o ){
        if (!(o instanceof AbstractRDFGraph))
            return false;
        AbstractRDFGraph right = (AbstractRDFGraph)o;
        HashableHashSet<RDFRawResourceVertex> l = getVertices();
        if (l.isEmpty())
            return right.getVertices().isEmpty();
        for (RDFRawResourceVertex v : l) {
            if (!right.hasVertex(v))
                return false;
            else {
                Iterator<RDFEdge> it = getOutgoingEdges(v);
                while (it.hasNext()) {
                    RDFEdge e = it.next();
                    if (!right.isEdge(e.getSrc(),e.getLabel(),e.getDst()))
                        return false;
                }
            }
        }
        return true;
    }

    public abstract void toTurtle(RDFWriter writer2);

    public abstract void printTriples(PrintStream out);
}
