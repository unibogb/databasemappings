package it.giacomobergami.graphs.JenaPostgresGraph;

import it.giacomobergami.graphs.intermediateToRDF.AbstractRDFGraph;
import it.giacomobergami.graphs.intermediateToRDF.RDFEdge;
import it.giacomobergami.graphs.nodes.RDFRawResourceVertex;
import it.giacomobergami.utils.datastructures.HashableHashSet;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.sdb.SDBFactory;
import org.apache.jena.sdb.Store;
import org.apache.jena.sdb.StoreDesc;
import org.apache.jena.sdb.sql.JDBC;
import org.apache.jena.sdb.sql.SDBConnection;
import org.apache.jena.sdb.store.DatabaseType;
import org.apache.jena.sdb.store.LayoutType;
import org.apache.jena.sparql.core.DatasetGraph;
import org.apache.jena.sparql.sse.SSE;
import org.apache.jena.tdb.TDB;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.eclipse.rdf4j.rio.RDFWriter;

import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Created by vasistas on 03/09/16.
 */
public class JenaPostgresGraph extends AbstractRDFGraph {

   public static final String defaultDirectory = "jenadb";
    Dataset ds;
    DatasetGraph dsg;
    RDFRawResourceVertex currentGraphName;
    Graph currentGraph;
    Model currentModel;

    private static Node idTONode(RDFRawResourceVertex src) {
        return src==null ? null : SSE.parseNode(src.getResourceID().toString());
    }

    private static RDFNode idTORDFNode(RDFRawResourceVertex src) {
        return src==null ? null : ResourceFactory.createResource(src.toString());
    }

    private static Node idTONode(String src) {
        return src==null? null : SSE.parseNode(src);
    }

    public JenaPostgresGraph(URI graphName) {
        TDB.getContext().setTrue(TDB.symUnionDefaultGraph) ;
        currentGraphName = new RDFRawResourceVertex(graphName);
        ds = TDBFactory.createDataset(defaultDirectory);
        dsg = ds.asDatasetGraph() ;
        currentGraph = dsg.getGraph(idTONode(currentGraphName));
        currentModel = ds.getNamedModel(currentGraphName.toString());
    }


    @Override
    public boolean isEdge(RDFRawResourceVertex src, String label, RDFRawResourceVertex dst) {
        return dsg.find(idTONode(currentGraphName),idTONode(src),idTONode(label),idTONode(dst)).hasNext();
    }

    @Override
    public boolean hasVertex(RDFRawResourceVertex src) {
        return currentModel.containsResource(idTORDFNode(src));
    }

    @Override
    public RDFRawResourceVertex getVertex(RDFRawResourceVertex tmp) {
        return hasVertex(tmp) ? tmp : null;
    }

    @Override
    public Iterator<RDFEdge> getOutgoingEdges(RDFRawResourceVertex src) {
        return new Iterator<RDFEdge>() {
            final ExtendedIterator<Triple> it = currentGraph.find(idTONode(src), null, null);
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }
            @Override
            public RDFEdge next() {
                Triple t = it.next();
                try {
                    return new RDFEdge(t.getMatchSubject().getURI(),t.getMatchPredicate().getName(),t.getMatchObject().getURI());
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    System.exit(1);
                    return null;
                }
            }
        };
    }

    @Override
    public Iterator<RDFEdge> getEdges() {
        return getOutgoingEdges(null);
    }

    @Override
    public RDFEdge addEdge(RDFRawResourceVertex src, String label, RDFRawResourceVertex dst) {
        try {
            currentGraph.add(new Triple(idTONode(src), idTONode(label), idTONode(dst)));
            return new RDFEdge(src,label,dst);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean removeEdge(RDFRawResourceVertex src, String label, RDFRawResourceVertex dst) {
        throw new UnsupportedOperationException("removeEdge");
    }

    @Override
    public RDFRawResourceVertex addVertex(RDFRawResourceVertex src) {
        //currentGraph.
        return null;
    }

    @Override
    public void addVertices(Collection<? extends RDFRawResourceVertex> nodes) {
        for (RDFRawResourceVertex x : nodes) addVertex(x);
    }

    @Override
    public void removeVertex(RDFRawResourceVertex src) {
        throw new UnsupportedOperationException("removeVertex");
    }

    @Override
    public HashableHashSet<RDFRawResourceVertex> getVertices() {
        throw new UnsupportedOperationException("getVertices");
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public <T> Optional<T> startTransaction(Supplier<T> f) {
        dsg.begin(ReadWrite.WRITE);
        Optional<T> toret =  Optional.empty();
        try {
            toret = Optional.of(f.get());
            dsg.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dsg.end();
        }
        return toret;
    }

    @Override
    public void close() {
        ds.close();
    }

    @Override
    public AbstractRDFGraph performConstructSPARQLquery(String s, int step, Writer fw) throws IOException {
        return null;
    }

    @Override
    public void toTurtle(RDFWriter writer2) {

    }

    @Override
    public void printTriples(PrintStream out) {

    }
}
