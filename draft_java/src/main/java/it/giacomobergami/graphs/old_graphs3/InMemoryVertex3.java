/*
 * InMemoryVertex3.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.old_graphs3;

import it.giacomobergami.filemaps.formats.BSONHolder3;
import it.giacomobergami.filemaps.formats.VertexValues;
import it.giacomobergami.graphs.intermediateTo2.INeighVertex;
import it.giacomobergami.graphs.pointers.GraphIdPointerLong;
import it.giacomobergami.utils.datastructures.HashableTreeMap;
import it.giacomobergami.utils.datastructures.iterators.MapIterator;

import java.util.*;

/**
 * Defines a new vertex, where the attribute schema is stored inside the graph and the attributes per vertex are fixed
 * and stored in a array
 */
public class InMemoryVertex3 extends INeighVertex {

    private InMemoryGraph3 g;
    public final String[] values;

    public InMemoryVertex3(Set<String> hashingScheme, long id, InMemoryGraph3 g) {
        super(hashingScheme, new GraphIdPointerLong(id),false);
        this.g = g;
        values = new String[this.g.attributeToPosition.size()]; //Has the same size of the number of attrs
        updateHashes();
    }

    public InMemoryVertex3(long id, InMemoryGraph3 g, String... toHash) {
        this(new LinkedHashSet<>(Arrays.asList(toHash)),id,g);
    }

    public InMemoryVertex3(long id, InMemoryGraph3 g, String[] toHash, String... values) {
        super(new LinkedHashSet<>(Arrays.asList(toHash)), new GraphIdPointerLong(id),false);
        this.g = g;
        this.values = values;
        updateHashes();
    }

    @Override
    public String getAttribute(String attr) {
        return values[g.attributeToPosition.get(attr)];
    }

    @Override
    public void updateAll(Map<String, String> externMap) {
        for (Map.Entry<String,String> e : externMap.entrySet()) {
            setAttribute(e.getKey(),e.getValue());
        }
        updateHashes();
    }

    @Override
    public void setAttribute(String attr, String value) {
        values[g.attributeToPosition.get(attr)] = value;
        updateHashes();
    }

    @Override
    public Map<String, Comparable> cloneAttributes() {
        Hashtable<String,Comparable> s = new Hashtable<>();
        for (Map.Entry<String,Integer> e : g.attributeToPosition.entrySet()) {
            s.put(e.getKey(),values[e.getValue()]);
        }
        return s;
    }

    @Override
    public Set<String> getVertexSchema() {
        return g.attributeToPosition.keySet();
    }

    @Override
    public HashableTreeMap<String, String> getValues() {
        HashableTreeMap<String,String> s = new HashableTreeMap<>();
        for (Map.Entry<String,Integer> e : g.attributeToPosition.entrySet()) {
            s.put(e.getKey(),values[e.getValue()]);
        }
        return s;
    }

    public BSONHolder3 compileRaw() {
        return new BSONHolder3(
                new VertexValues(values),
                BSONHolder3.serializeAdjacencyAsPointers(new MapIterator<INeighVertex,Integer>(neigh.iteratorIngoing()) { @Override  public Integer apply(INeighVertex o) {return o.getId().hashCode();}}, neigh.sizeIngoing()),

                BSONHolder3.serializeAdjacencyAsPointers(new MapIterator<INeighVertex,Integer>(neigh.iteratorOutgoing()) { @Override  public Integer apply(INeighVertex o) {return o.getId().hashCode();}}, neigh.sizeOutgoing()),getId().hashCode());
    }


}
