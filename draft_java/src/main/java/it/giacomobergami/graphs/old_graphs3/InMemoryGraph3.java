/*
 * InMemoryGraph3.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.old_graphs3;

import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.formats.HashListEntryFormat;
import it.giacomobergami.filemaps.formats.IndexHashPos;
import it.giacomobergami.filemaps.rw.Writer;
import it.giacomobergami.graphs.intermediateTo2.INeighGraph;
import it.giacomobergami.graphs.GoogleGraph.DoubleHashVertexSet;
import it.giacomobergami.graphs.abstractgraph.GeneralEdge;
import it.giacomobergami.graphs.pointers.GraphIdPointerLong;
import it.giacomobergami.graphs.pointers.Pointer;
import it.giacomobergami.utils.I;
import it.giacomobergami.utils.MultiMapValuePointer;
import it.giacomobergami.utils.datastructures.iterators.MapIterator;
import it.giacomobergami.utils.datastructures.pointers.RBTreePointer;
import it.giacomobergami.utils.datastructures.trees.iterator.IteratorOfIterator;
import it.giacomobergami.utils.datastructures.trees.iterator.IteratorOfValues;
import it.giacomobergami.utils.datastructures.trees.rbtree.RBTree;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by vasistas on 30/04/16.
 */
public class InMemoryGraph3 extends INeighGraph<InMemoryVertex3> {

    public final Hashtable<String,Integer> attributeToPosition;
    public RBTree<Integer,InMemoryVertex3> orderedTree;
    RBTree<Pointer,InMemoryVertex3> indexedTree;

    public InMemoryGraph3(String... schema) {
        super();
        this.attributeToPosition = new Hashtable<>();
        int i =0;
        for (String s : schema) attributeToPosition.put(s,i++);
        orderedTree = new RBTree<>(true);
        indexedTree = new RBTree<>(true);
    }

    @Override
    public boolean isEdge(InMemoryVertex3 src, InMemoryVertex3 dst) {
        return src.hasNeighbour(dst);
    }

    @Override
    public boolean hasVertex(InMemoryVertex3 src) {
        RBTreePointer<Pointer, InMemoryVertex3> tmp = indexedTree.lookup(src.getId());
        return tmp == null ? false : tmp.first().equals(src);
    }

    public RBTreePointer<Pointer, InMemoryVertex3> getVertexPointer(int id) {
        return indexedTree.lookup(new GraphIdPointerLong(id));
    }

    @Override
    public InMemoryVertex3 getVertex(InMemoryVertex3 tmp) {
        return hasVertex(tmp) ? tmp : null;
    }

    @Override
    public Iterator<GeneralEdge> getEdges() {
        return new IteratorOfIterator<>(new MapIterator<InMemoryVertex3,Iterator<GeneralEdge>>(new IteratorOfValues<>(indexedTree.treeIterator())) {
            @Override
            public Iterator<GeneralEdge> apply(InMemoryVertex3 inMemoryVertex3) {
                return inMemoryVertex3.getOutgoingEdges();
            }
        });
    }

    @Override
    public InMemoryVertex3 addVertex(InMemoryVertex3 src) {
        if (!hasVertex(src)) {
            orderedTree.insert(src.searchhash(),src);
            indexedTree.insert(src.getId(),src);
        }
        return src;
    }

    /**
     * If you are sure that the vertices in the graph already exist, then you can add them without checking.
     * Beware: in this way
     * @param src
     * @return
     */
    public InMemoryVertex3 addBulkVertex(InMemoryVertex3 src) {
        orderedTree.insert(src.searchhash(),src);
        indexedTree.insert(src.getId(),src);
        return src;
    }

    @Override
    public void addVertices(Collection<? extends InMemoryVertex3> nodes) {
        for (InMemoryVertex3 v : nodes) addVertex(v);
    }

    @Override
    public InMemoryVertex3 generateVertexObject(String type, Set<String> hashingScheme) {
        return null;
    }

    @Override
    public void removeVertex(InMemoryVertex3 src) {
        if (indexedTree.delete(src.getId(),src)) {
            orderedTree.delete(src.searchhash(),src);
        }
    }

    @Override
    public Set<InMemoryVertex3> getVertices() {
        return new Set<InMemoryVertex3>() {
            @Override
            public int size() {
                return indexedTree.getSize();
            }

            @Override
            public boolean isEmpty() {
                return indexedTree.isEmpty();
            }

            @Override
            public boolean contains(Object o) {
                return hasVertex((InMemoryVertex3)o);
            }

            @Override
            public Iterator<InMemoryVertex3> iterator() {
                return new IteratorOfValues<>(indexedTree.treeIterator());
            }

            @Override
            public Object[] toArray() {
                Object[] a =  new Object[size()];
                Iterator<InMemoryVertex3> it = iterator();
                int i = 0;
                while (it.hasNext()) {
                    a[i++] = it.next();
                }
                return a;
            }

            @Override
            public <T> T[] toArray(T[] a) {
                Iterator<InMemoryVertex3> it = iterator();
                int i = 0;
                while (it.hasNext()) {
                    a[i++] = (T)it.next();
                }
                return a;
            }

            @Override
            public boolean add(InMemoryVertex3 inMemoryVertex3) {
                return addVertex(inMemoryVertex3)!=null;
            }

            @Override
            public boolean remove(Object o) {
                boolean toret = contains(o);
                removeVertex((InMemoryVertex3)o);
                return toret;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                for (Object x : c) if (!contains(x)) return false;
                return true;
            }

            @Override
            public boolean addAll(Collection<? extends InMemoryVertex3> c) {
                boolean toret = true;
                for (InMemoryVertex3 x : c) toret = toret && add(x);
                return toret;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                RBTree<Integer,InMemoryVertex3> hash = new RBTree<>(true);
                RBTree<Pointer,InMemoryVertex3> index = new RBTree<>(true);
                boolean toret = true;
                for (Object x : c)
                    if (contains(x)) {
                        InMemoryVertex3 y = (InMemoryVertex3)x;
                        hash.insert(y.searchhash(),y);
                        index.insert(y.getId(),y);
                    }
                    else toret = false;
                orderedTree = hash;
                indexedTree = index;
                return toret;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                boolean toret = true;
                for (Object x : c) toret = toret && remove(x);
                return toret;
            }

            @Override
            public void clear() {
                orderedTree.clear();
                indexedTree.clear();
            }
        };
    }

    @Override @Deprecated
    public DoubleHashVertexSet<? extends DoubleHashVertexSet, ?, InMemoryVertex3> getRawVertices() {
        return null;
    }

    public static FileOutputStream createFile(File path, String filename) {
        File file = new File(path, filename);
        if (file.exists()) file.delete();
        try {
            return new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void store(File path) {
        System.out.print("Write MultiFile… ");
        long t = System.currentTimeMillis();
        Iterable<MultiMapValuePointer<Integer, InMemoryVertex3>> it = new I<>(orderedTree.treeIterator());
        TreeSet<IndexHashPos> indexFile = new TreeSet<>();

        FileOutputStream fop = createFile(path,"vertices.bin");
        Writer vertexWriter = new Writer(fop);

        FileOutputStream hashIndexFile = createFile(path,"hashes.index");
        Writer hashWriter = new Writer(hashIndexFile);

        try {
            //Counting the offset of the hashlist
            long hashCounter = 0;
            for (MultiMapValuePointer<Integer, InMemoryVertex3> ptr : new I<>(orderedTree.treeIterator())) {
                Integer hashValue = ptr.getKey();

                //Position of the single vertex locally to the hash offset
                //int vertexPositionLocallyToHash = 0;

                //Creating an entry on the hash list element
                new HashListEntryFormat(hashValue,hashCounter).serialize(hashWriter);
                hashIndexFile.flush();

                for (InMemoryVertex3 key : new I<>(ptr.iterator())) {
                    //Setting the position of the current vertex
                    indexFile.add(new IndexHashPos(key.id.hashCode(),hashValue,hashCounter));

                    //Serializing the vertices in the vertices file. Defining the offset of the next vertex
                    hashCounter += key.compileRaw().serialize(vertexWriter);
                }
                fop.flush();

                //Incrementing the offset globally for the hash element
                //hashCounter += (long)vertexPositionLocallyToHash;
            }
            hashIndexFile.close();
            fop.close();

            //Writing the index file using the inverse pointer strategy

            /**
             * TODO: alternative
             *
             */
            FileOutputStream indexHashPos = createFile(path,"id.index");
            FileMapsUtils.writeIteration(indexFile,new Writer(indexHashPos));
            indexHashPos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
