/*
 * RBTreeVSStrategy.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.GoogleGraph.vertexSets;

import it.giacomobergami.graphs.GoogleGraph.DoubleHashVertexSet;
import it.giacomobergami.graphs.VertexSet;
import it.giacomobergami.graphs.abstractgraph.AbstractNode;
import it.giacomobergami.graphs.abstractgraph.VertexStoreStrategy;
import it.giacomobergami.utils.MultiMapValuePointer;

import java.util.Collection;
import java.util.Iterator;

/**
 * Uses the re-defined RBSet for storing the vertices. It duplicates the memory usage for storing vertices for two
 * different purposes
 */
public class RBTreeVSStrategy<N extends AbstractNode> extends DoubleHashVertexSet<RBTreeVSStrategy,Integer,N> {

    private VertexSet<N> map; // one that uses the searchHashes
    public VertexSet<N> elements; // the actual ids
    private int count;
    private boolean strategy;

    /**
     *
     * @param vertexIterationStrategy   Defines the strategy for iterating all over the nodes. If true, then
     *                                  uses the tree with hashing values, otherwise the one used for indexing the
     *                                  actual vertices
     * @param uniqueElementStrategy     Defines the strategy for indexing the vertices in the unique vertex identification
     *                                  element. It does not change the strategy for the hash-join data structure, that
     *                                  remains @{VertexStoreStrategy.StoreVertexByHashingSchema}
     */
    public RBTreeVSStrategy(boolean vertexIterationStrategy, VertexStoreStrategy uniqueElementStrategy) {
        this.strategy = vertexIterationStrategy;
        map = new VertexSet<>(true,true, VertexStoreStrategy.StoreVertexByHashingSchema);
        elements = new VertexSet<>(true,false, uniqueElementStrategy);
        count = 0;
    }



    @Override
    public Iterator<MultiMapValuePointer<Integer, N>> hashPointerIterator() {
        return map.treePointerIterator();
    }

    @Override
    public Iterator<Integer> getHashes() {
        return map.getHashes();
    }

    @Override
    public Iterator<N> getVertices() {
        return elements.iterator();
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return elements.contains(o);
    }

    @Override
    public Iterator<N> iterator() {
        return strategy ? map.iterator() : elements.iterator();
    }

    @Override
    public Object[] toArray() {
        return strategy ? map.toArray() : elements.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return strategy ? map.toArray(a) : elements.toArray(a);
    }

    @Override
    public boolean add(N abstractNode) {
        if (!elements.contains(abstractNode)) {
            elements.add(abstractNode);
            map.add(abstractNode);
            count++;
            return true;
        } else return false;
    }

    @Override
    public boolean remove(Object o) {
        if (elements.contains(o)) {
            elements.remove(o);
            map.remove(o);
            count--;
            return true;
        } else return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object x : c) if (!contains(x)) return false;
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends N> c) {
        boolean toret = true;
        for (N x : c) toret = toret && add(x);
        return toret;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean toret = true;
        for (Object x : c) toret = toret && remove(x);
        return toret;
    }

    @Override
    public void clear() {
        map.clear();
        elements.clear();
    }
}
