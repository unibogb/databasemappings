/*
 * CacheWithPolicy.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.GoogleGraph.caches;

import it.giacomobergami.graphs.nodes.GoogleGraphVertex;
import it.giacomobergami.graphs.pointers.GraphBulkBinPointer;
import org.apache.jcs.JCS;
import org.apache.jcs.access.CacheAccess;
import org.apache.jcs.access.exception.CacheException;

/**
 * Created by vasistas on 20/04/16.
 */
public class CacheWithPolicy {
    public final CacheAccess neighCache;
    public final CachePolicy policy;

    public CacheWithPolicy(CachePolicy policy) {
        CacheAccess neighCache1;
        try {
            neighCache1 = JCS.getInstance("default");
        } catch (CacheException e) {
            e.printStackTrace();
            System.exit(1);
            neighCache1 = null;
        }
        neighCache = neighCache1;
        this.policy = policy;
    }

    public boolean isEdge(GoogleGraphVertex src, GoogleGraphVertex dst) {
        GraphBulkBinPointer ptr = new GraphBulkBinPointer(src,dst);
        Boolean result = (Boolean) neighCache.get(ptr);
        if (result!=null) return result;
        else switch (policy) {
            case Neighbours: {
                return false; // In the second cache policy, if it isn't there, it is not an edge
            }
            case IsNeighbour: {
                //In the first cache policy, i do set the fact that it belongs to here either if it is present or not
                boolean toset = src.hasNeighbour(dst);
                try {
                    neighCache.put(ptr,toset);
                } catch (CacheException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
                return toset;
            }
            default:
                throw new RuntimeException("Unexpected Policy");
        }
        //return /* TODO vertices.contains(src) && */src.hasNeighbour(dst);
    }



}
