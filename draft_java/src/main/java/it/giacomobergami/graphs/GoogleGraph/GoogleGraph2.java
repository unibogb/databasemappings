/*
 * GoogleGraph2.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.GoogleGraph;

import it.giacomobergami.algorithms.RelationshipCache;
import it.giacomobergami.graphs.GoogleGraph.vertexSets.RBTreeVSStrategy;
import it.giacomobergami.graphs.abstractgraph.AbstractGraph;
import it.giacomobergami.graphs.abstractgraph.AbstractNode;
import it.giacomobergami.graphs.abstractgraph.GeneralEdge;
import it.giacomobergami.graphs.abstractgraph.VertexStoreStrategy;
import it.giacomobergami.graphs.nodes.GoogleGraphVertex;
import it.giacomobergami.graphs.intermediateTo2.INeighVertex;
import it.giacomobergami.graphs.nodes.neigh.StrategyRB;
import it.giacomobergami.graphs.pointers.GraphBulkBinPointer;
import it.giacomobergami.graphs.pointers.UniqueGenerator;
import it.giacomobergami.utils.I;
import org.apache.jcs.JCS;
import org.apache.jcs.access.CacheAccess;
import org.apache.jcs.access.exception.CacheException;

import java.util.*;
import java.util.function.Supplier;

/**
 * Putting the forward and backward adjacency inside each vertex (reducing the seek operations)
 */
public class GoogleGraph2 extends AbstractGraph<GoogleGraphVertex> {

    //private IAdjacency<AbstractNode,MultiHashAdjacencyMap> forwardAdjacenecy,backwardAdjacency;
    private RBTreeVSStrategy vertices;
    private UniqueGenerator ug;
    public CacheAccess neighCache;

    public GoogleGraph2() {
        this(VertexStoreStrategy.StoreVertexByUniqueId);
    }

    public GoogleGraph2(VertexStoreStrategy uniqueElementStrategy) {
        vertices = new RBTreeVSStrategy(true,uniqueElementStrategy);
        ug = new UniqueGenerator();
        try {
            neighCache = JCS.getInstance("default");
        } catch (CacheException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public Iterator<Integer> getAllHashes() {
        return vertices.getHashes();
    }
/*
    public Iterable<AbstractNode> getClashingHashVertices(AbstractNode n) {
        return vertices.getSearchHash(n.searchhash());
    }

    public Iterable<AbstractNode> getClashingHashIndexes(int n) {
        return vertices.getSearchHash(n);
    }
*/
    public Iterator<AbstractNode> VertexSet() {
        return vertices.getVertices();
    }

    @Override
    public boolean isEdge(GoogleGraphVertex src, GoogleGraphVertex dst) {
        GraphBulkBinPointer ptr = new GraphBulkBinPointer(src,dst);
        Boolean result = (Boolean) neighCache.get(ptr);
        if (result!=null) return result;
        else {
            boolean toset = src.hasNeighbour(dst);
            try {
                neighCache.put(ptr,toset);
            } catch (CacheException e) {
                e.printStackTrace();
                System.exit(1);
            }
            return toset;
        }
        //return /* TODO vertices.contains(src) && */src.hasNeighbour(dst);
    }

    @Override
    public boolean hasVertex(GoogleGraphVertex src) {
        return vertices.contains(src);
    }

    @Override
    public GoogleGraphVertex getVertex(GoogleGraphVertex tmp) {
        return (vertices.contains(tmp)) ? null : tmp;
    }


    @Override
    public Collection<GoogleGraphVertex> getNeighbours(GoogleGraphVertex x) {
        return new Collection<GoogleGraphVertex>() {

            GoogleGraphVertex src = x;
            RelationshipCache cache = new RelationshipCache(100000);

            @Override
            public int size() { return src.neigh.getSize(); }

            @Override
            public boolean isEmpty() { return src.neigh.isEmpty(); }

            @Override
            public boolean contains(Object o) {
                throw new RuntimeException("MessyCode");

                //return cache.get(src,(GoogleGraphVertex)o);
            }

            @Override
            public Iterator<GoogleGraphVertex> iterator() { return new Iterator<GoogleGraphVertex>() {
                Iterator<INeighVertex> it = src.neigh.iterator();
                @Override
                public boolean hasNext() {
                    return it.hasNext();
                }

                @Override
                public GoogleGraphVertex next() {
                    return (GoogleGraphVertex)it.next();
                }
            }; }

            @Override
            public Object[] toArray() {
                Object[] a = new Object[size()];
                int i= 0;
                Iterator<GoogleGraphVertex> it = iterator();
                while (it.hasNext()) a[i++] = it.next();
                return a;
            }

            @Override
            public <T> T[] toArray(T[] a) {
                int i= 0;
                Iterator<GoogleGraphVertex> it = iterator();
                while (it.hasNext()) a[i++] = (T)it.next();
                return a;
            }

            @Override
            public boolean add(GoogleGraphVertex googleGraphVertex) {
                cache.put(x,googleGraphVertex);
                return src.neigh.addNeighbour(googleGraphVertex);
            }

            @Override
            public boolean remove(Object o) {
                cache.remove(x,(GoogleGraphVertex)o);
                return src.neigh.removeNeighbour((GoogleGraphVertex)o);
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                for (Object x : c) if (!contains(x))return false; return true;
            }

            @Override
            public boolean addAll(Collection<? extends GoogleGraphVertex> c) {
                boolean toret = true;
                for (GoogleGraphVertex x : c) toret = toret && add(x);
                return toret;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                boolean toret = true;
                for (Object x : c) toret = toret && remove(x);
                return toret;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                ArrayList<Object> torem = new ArrayList<>();
                for (GoogleGraphVertex x : new I<>(iterator())) if (!c.contains(x)) torem.add(x);
                return removeAll(torem);
            }

            @Override
            public void clear() { src.neigh = new StrategyRB(src); }

            @Override
            public boolean equals(Object o) {
                if (!(o instanceof Collection)) return false;
                return containsAll((Collection)o);
            }

            @Override
            public int hashCode() {
                final int prime = 17;
                int result = 3;
                for( GoogleGraphVertex k : new I<>(iterator())) {
                    result = result * prime + k.hashCode();
                }
                return result;
            }
        };
    }

    @Override
    public Iterator<GeneralEdge> getEdges() {
        return EdgeIteratorIterator.edgeIterator(vertices.elements.tree.root);
    }

    @Override
    public GeneralEdge addEdge(GoogleGraphVertex src, GoogleGraphVertex dst) {
        return src.addNeighbour(dst) ? new GeneralEdge(src,dst) : null;
    }

    @Override
    public boolean removeEdge(GoogleGraphVertex src, GoogleGraphVertex dst) {
        try {
            neighCache.remove(new GraphBulkBinPointer(src,dst));
        } catch (CacheException e) {
        }
        return src.removeNeighbour(dst);
    }

    @Override
    public GoogleGraphVertex addVertex(GoogleGraphVertex src) {
        vertices.add(src);
        return src;
    }

    @Override
    public  void addVertices(Collection<? extends GoogleGraphVertex> nodes) {
        vertices.addAll(nodes);
    }

    @Override
    public GoogleGraphVertex generateVertexObject(String type, Set<String> hashingScheme) {
        return new GoogleGraphVertex(type,hashingScheme,ug.next());
    }

    @Override
    public void removeVertex(GoogleGraphVertex src) {
        vertices.remove(src);
    }

    @Override
    public Set<GoogleGraphVertex> getVertices() {
        return vertices;
    }

    public int vSize() {
        return vertices.size();
    }

    @Override
    public int size() {
        int count = 0;
        for (GoogleGraphVertex a : getVertices()) {
            count += (a.neigh.sizeOutgoing()+1);
        }
        return count;
    }

    @Override
    public <T> Optional<T> startTransaction(Supplier<T> f) {
        return Optional.of(f.get()); //No transactions
    }

    @Override
    public void close() { }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GoogleGraph2)) return super.equals(o);

        GoogleGraph2 that = (GoogleGraph2) o;
        return vertices != null ? vertices.equals(that.vertices) : that.vertices == null;

    }

    @Override
    public DoubleHashVertexSet<? extends DoubleHashVertexSet, ?, GoogleGraphVertex> getRawVertices() {
        return vertices;
    }


    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (vertices != null ? vertices.hashCode() : 0);
        return result;
    }

    @Override
    public boolean prop(AbstractNode left, AbstractNode right) {
        return isEdge((GoogleGraphVertex) left,(GoogleGraphVertex)right);
    }

    @Override
        public String toString() {
        return "";
    }

}
