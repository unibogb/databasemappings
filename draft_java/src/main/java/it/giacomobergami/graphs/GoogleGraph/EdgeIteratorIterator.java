/*
 * EdgeIteratorIterator.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.graphs.GoogleGraph;

import it.giacomobergami.graphs.abstractgraph.AbstractNode;
import it.giacomobergami.graphs.abstractgraph.GeneralEdge;
import it.giacomobergami.graphs.intermediateTo2.INeighVertex;
import it.giacomobergami.utils.datastructures.trees.iterator.AbstractTreeIterator;
import it.giacomobergami.utils.datastructures.trees.iterator.IteratorOfIterator;
import it.giacomobergami.utils.datastructures.trees.rbtree.RBTree;
import it.giacomobergami.utils.datastructures.trees.rbtree.RedBlackNode;

import java.util.Iterator;

/**
 * Created by vasistas on 17/04/16.
 */
public class EdgeIteratorIterator extends AbstractTreeIterator<AbstractNode,Object,Iterator<GeneralEdge>> {
    private EdgeIteratorIterator(RedBlackNode<AbstractNode, Object> current) {
        super(current, null);
    }
    @Override
    protected Iterator<GeneralEdge> transformCurrentStep(RBTree<AbstractNode, Object> root, RedBlackNode<AbstractNode, Object> current) {
        return new Iterator<GeneralEdge>() {
            AbstractNode src = current.key;
            Iterator<INeighVertex> it = ((INeighVertex)current.key).neigh.iterator();
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }
            @Override
            public GeneralEdge next() {
                return new GeneralEdge(src,it.next());
            }
        };
    }
    public static Iterator<GeneralEdge> edgeIterator(RedBlackNode<AbstractNode, Object> current) {
        return new IteratorOfIterator<>(new EdgeIteratorIterator(current));
    }

}
