/*
 * DoubleHashVertexSet.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */




package it.giacomobergami.graphs.GoogleGraph;

import it.giacomobergami.graphs.abstractgraph.AbstractNode;
import it.giacomobergami.utils.MultiMapValuePointer;

import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by vasistas on 16/04/16.
 */
public abstract class DoubleHashVertexSet
        <Extending extends DoubleHashVertexSet,
        K extends Comparable<K>,
        N extends AbstractNode>

        implements Set<N>, Comparable<Extending> {



    /**
     * Scans the data structure efficiently (by hash)
     * @return
     */
    public abstract Iterator<MultiMapValuePointer<K, N>> hashPointerIterator();

    /**
     * Returns the hashes for the
     * @return
     */
    public abstract Iterator<Integer> getHashes();

    public abstract Iterator<N> getVertices();

    @Override
    public int hashCode() {
        final int prime = 17;
        int result = 3;
        for( AbstractNode k : this ) {
            result = result * prime + k.hashCode();
        }
        return result;
    }

    @Override
    public int compareTo(Extending o) {
        if (o==null)
            return 1;
        int leftSize = size();
        int rightSize = o.size();
        if (leftSize==0 && rightSize==0)
            return 0;
        else if (leftSize==0)
            return -1;
        else if (rightSize==0)
            return 1;
        AbstractNode leftMin = (AbstractNode) Collections.min(this);
        AbstractNode rightMin = (AbstractNode)Collections.min(o);
        int min = (leftMin.compareTo(rightMin));
        if (min!=0)
            return min;
        AbstractNode leftMax = (AbstractNode)Collections.max(this);
        AbstractNode rightMax = (AbstractNode)Collections.max(o);
        int max = leftMax.compareTo(rightMax);
        if (max!=0)
            return -max;
        return leftSize - rightSize;
    }
}
