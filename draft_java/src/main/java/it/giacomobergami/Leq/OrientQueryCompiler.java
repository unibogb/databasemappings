/*
 * OrientQueryCompiler.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package it.giacomobergami.Leq;

import it.giacomobergami.graphs.abstractgraph.AbstractNode;
import it.giacomobergami.utils.logic.BinaryPredicate;

import java.util.*;
import java.util.stream.Collectors;

public  class OrientQueryCompiler {
        final public BinaryPredicate<AbstractNode,AbstractNode> prop;
        final private Set<String> hashScheme;
        private boolean lr;

        private static String neqRule(String property) {
            return ("out")+"."+property+"1<>"+("in")+"."+property+"2";
        }

        private static String reconstruct(String property) {
            //in.Organization2 as Organization2
            return "in."+property+"2 as "+property+"2, out."+property+"1 as "+property+"1 ";
        }

        private static String neqRule(String src_dst,Collection<String> property) {
            StringBuilder sb = new StringBuilder();
            Iterator<String> it = property.iterator();
            while (it.hasNext()) {
                sb.append(neqRule(it.next()));
                if (it.hasNext()) sb.append(" OR ");
            }
            return sb.toString();
        }

        private static String reconstruct(Collection<String> property) {
            StringBuilder sb = new StringBuilder();
            Iterator<String> it = property.iterator();
            while (it.hasNext()) {
                sb.append(reconstruct(it.next()));
                if (it.hasNext()) sb.append(", ");
            }
            return sb.toString();
        }

        public OrientQueryCompiler(String... args) {
            this(false,args);
        }

        public Set<String> mapLeft() {
            return lr ? hashScheme.stream().map(x->x+"1").collect(Collectors.toSet()) : hashScheme;
        }

        public Set<String> mapRight() {
            return lr ? hashScheme.stream().map(x->x+"2").collect(Collectors.toSet()) : hashScheme;
        }

        public OrientQueryCompiler(boolean lr, String... args) {
            this.lr = lr;
            Set<String> hashSet1 = new HashSet<>();
            hashSet1.addAll(Arrays.asList(args));
            this.hashScheme = hashSet1;
            this.prop =  (left, right) -> hashSet1.stream().allMatch(y -> left.getAttribute(y+(lr ? "1" : "")).equals(right.getAttribute(y+(lr ? "2" : ""))));
        }

        public List<String> generateCypherQuery() {
            ArrayList<String> inlinea = new ArrayList<>();
            inlinea.add("create class Joins extends E");
            inlinea.add("create edge Joins from (select * from L) to (select * from R)");
            inlinea.add("delete edge Joins where ("+neqRule("src",hashScheme)+")");
            inlinea.add("create class Vertices3 extends V");
            inlinea.add("insert into Vertices3 from select "+reconstruct(hashScheme)+", in.@rid as id2, out.@rid as id1 from Joins");
            inlinea.add("create class Edges3 extends E");
            inlinea.add("create edge Edges3 from (select * from Vertices3 where id1.outE(\"relations\").in in (select id1 from Vertices3) and id2.outE(\"relations\").in in (select id2 from Vertices3)) to (select * from Vertices3 where id1.inE(\"relations\").out in (select id1 from Vertices3) and id2.inE(\"relations\").out in (select id2 from Vertices3))");
            inlinea.add("delete edge Edges3 where (in.id1 not in out.id1.outE(\"relations\").in or in.id2 not in out.id2.outE(\"relations\").in)");
            //Get the graph, represented as the sole vertices
            inlinea.add("select * from Vertices3");
            return inlinea;
        }

    public static void main(String args[]) {
        OrientQueryCompiler tester = new OrientQueryCompiler(true, new String[]{"Organization","Year"});
        for (String s : tester.generateCypherQuery()) System.out.println(s);
        System.exit(1);

    }

    }
