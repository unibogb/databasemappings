package generateRawData;

import com.google.inject.Guice;
import com.google.inject.Injector;
import it.giacomobergami.data.GeneratorConfiguration;
import it.giacomobergami.graphs.old_graphs3.InMemoryGraph3;
import it.giacomobergami.graphs.old_graphs3.InMemoryVertex3;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by vasistas on 29/06/16.
 */
public class csvCreate {

    public static void main(String args[]) throws IOException {
        InMemoryGraph3 g = new InMemoryGraph3("Ip","Organization","Year"); //Schema
        FileWriter fw = new FileWriter("VerticesAttribute.txt");
        fw.write("Ip,Organization,Year\n");
        Injector injector = Guice.createInjector( new GeneratorConfiguration());
        VertexGenerator3bis gen = VertexGenerator3bis.getInstance();
        while (gen.hasNext()) {
            gen.next(fw);
        }
        fw.close();
    }

}
