/*
 * VertexGenerator3bis.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */

package generateRawData;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import it.giacomobergami.data.GenerateIpLoc;
import it.giacomobergami.data.GenerateOrganization;
import it.giacomobergami.data.GenerateYear;
import it.giacomobergami.data.GeneratorConfiguration;
import it.giacomobergami.graphs.old_graphs3.InMemoryGraph3;
import it.giacomobergami.graphs.old_graphs3.InMemoryVertex3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

/**
 * This class generates the raw data that is used for performing the EquiJoin queries
 */
public class VertexGenerator3bis implements Iterator<InMemoryVertex3> {

    final GenerateIpLoc ip;
    final GenerateOrganization org;
    final GenerateYear ye;
    private InMemoryGraph3 g ;
    private final int upTo;
    private int count;

    public static int multipleHashString(String... stringsToHash) {
        final int prime = 17;
        int result = 3;
        for( String s : stringsToHash )
        {
            result = result * prime + s.hashCode();
        }
        return result;
    }

    @Inject
    public VertexGenerator3bis(@Named("vertices.upTo") int upTo)  {
        this.upTo = upTo;
        ip = GeneratorConfiguration.getInstance().getInstance(GenerateIpLoc.class);
        org = GeneratorConfiguration.getInstance().getInstance(GenerateOrganization.class);
        ye = GeneratorConfiguration.getInstance().getInstance(GenerateYear.class);
        count = 0;
    }

    private static VertexGenerator3bis self = null;
    public static VertexGenerator3bis getInstance() {
        if (self==null)
            self = GeneratorConfiguration.getInstance().getInstance(VertexGenerator3bis.class);
        return self;
    }

    public boolean hasNext() {
        return count < upTo;
    }

    public final static String[] hashingScheme = new String[]{"Organization","Year"};

    public void  next(FileWriter fw) {
        if (!hasNext()) {
            try {
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            String ORG = org.next();
            String Y = ye.next();
            try {
                fw.write(ip.next().ip+","+ORG+","+Y+"\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
            //BSONHolder3 v = new BSONHolder3(IpOrgYear,id,hash);
            count++;
        }
    }

    public InMemoryVertex3 next() {
        if (!hasNext()) {
            return null;
        } else {
            String ORG = org.next();
            String Y = ye.next();
            //String IpOrgYear[] = new String[]{ip.next().ip,ORG,Y};
            int id = count;
            int hash = multipleHashString(ORG,Y);
            InMemoryVertex3 w = new InMemoryVertex3(id,g,hashingScheme,   ip.next().ip,ORG,Y);
            if (hash!=w.searchhash()) {
                throw new RuntimeException(hash+"!="+w.searchhash());
            }
            //BSONHolder3 v = new BSONHolder3(IpOrgYear,id,hash);
            count++;
            return w;
        }
    }

    public InMemoryGraph3 getG() {
        return g;
    }

    public void setG(InMemoryGraph3 g) {
        this.g = g;
    }
}
