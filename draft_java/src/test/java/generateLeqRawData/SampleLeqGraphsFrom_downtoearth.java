package generateLeqRawData;/*
 * SampleGraphsFrom_downtoearth.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



import it.giacomobergami.Paths;
import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.Sample;
import it.giacomobergami.filemaps.buffers.BufferArray;
import it.giacomobergami.filemaps.buffers.LongMMFile;
import it.giacomobergami.filemaps.formats.BSONHolder3;
import it.giacomobergami.filemaps.formats.BSONHolder4;
import it.giacomobergami.filemaps.formats.HashListEntryFormat;
import it.giacomobergami.filemaps.formats.IndexHashPos;
import it.giacomobergami.filemaps.rw.Writer;
import it.giacomobergami.graphs.old_graphs3.InMemoryGraph3;
import it.giacomobergami.graphs.readonly_graph.ReadonlyIntegerGraph;
import it.giacomobergami.utils.I;
import it.giacomobergami.utils.MultiMapValuePointer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.TreeSet;

/**
 * This class creates the graph operands, one part (either left or right) at a time for the LeqJoin
 * Use the configurations
 */
public class SampleLeqGraphsFrom_downtoearth {

    public void testReading() throws IOException {
        LongMMFile lmf = new LongMMFile(new File("/Users/vasistas/graphdb/leqtest/id.index"));
        BufferArray<IndexHashPos> ba = new BufferArray<>(IndexHashPos.class,lmf);
        LongMMFile vertices = new LongMMFile(new File("/Users/vasistas/graphdb/leqtest/vertices.bin"));

        IndexHashPos vertexPtr = ba.get(0);
        long actualPtr = vertexPtr.pos;
        BSONHolder3 vertex = BSONHolder3.unmarshall(vertices,actualPtr);

        System.out.print("Ingoing: ");
        for (int ingoing : new I<>(vertex.ingoingA.iterator())) {
            System.out.print(ingoing+", ");
        }
        System.out.println("");

        System.out.print("Outgoing: ");
        for (int outgoing : new I<>(vertex.outgoingA.iterator())) {
            System.out.print(outgoing+", ");
        }
        System.out.println("");

        System.out.print("Values: ");
        for (String val : new I<>(vertex.values.iterator())) {
            System.out.print(val+", ");
        }
        System.out.println("");
    }

    public final static String[] hashingScheme = new String[]{"Year"};

    public static void main(String[] args) throws IOException {
        Paths p = Paths.getInstance();
        ReadonlyIntegerGraph g = new ReadonlyIntegerGraph(new File(p.leqTestRaw("")), "Ip", "Organization", "Year");
        Sample sample = Sample.getInstance();
        sample.setG(g);
        //int currentStep = 10;
        long timeDeltaSum = 0;
        int steps[] = new int[]{2,5,10,100,1000,10000,100000,1000000};
        for (int i = 1; i < 7; i++) {
            timeDeltaSum += sample.next(steps[i-1]); // todo <<- here
            //currentStep *= 10;
            /*int idConversion[] = new int[g.getVertices().size()];
            Arrays.fill(idConversion, 0);*/
            //// At this step, all the idS are converted
            /*System.out.println("1. Converting to new ids");
            {
                int pos = 0;
                for (Integer vId : sample.V) idConversion[vId] = pos++;
            }*/

            String file = "graph" + i;
            File path = new File(p.leqTestRaw(p.operand+File.separator+file));

            /////System.out.print("Write MultiFile… ");
            long t = System.currentTimeMillis();
            Iterable<MultiMapValuePointer<Integer, BSONHolder4>> it = new I<>(sample.orderedTree.treeIterator());
            TreeSet<IndexHashPos> indexFile = new TreeSet<>();

            FileOutputStream fop = InMemoryGraph3.createFile(path, "vertices.bin");
            Writer vertexWriter = new Writer(fop);

            FileOutputStream hashIndexFile = InMemoryGraph3.createFile(path, "hashes.index");
            Writer hashWriter = new Writer(hashIndexFile);

            try {
                //Counting the offset of the hashlist
                long hashCounter = 0;
                for (MultiMapValuePointer<Integer, BSONHolder4> ptr : new I<>(sample.orderedTree.treeIterator())) {
                    Integer hashValue = ptr.getKey();

                    //Position of the single vertex locally to the hash offset
                    //int vertexPositionLocallyToHash = 0;

                    //Creating an entry on the hash list element
                    //System.out.println(hashValue+","+hashCounter);
                    new HashListEntryFormat(hashValue, hashCounter).serialize(hashWriter);
                    hashIndexFile.flush();

                    for (BSONHolder4 key : new I<>(ptr.iterator())) {
                        //Setting the position of the current vertex
                        int tmp = Integer.valueOf(key.values.get(2));
                        if (tmp!=hashValue) {
                            System.err.println("Error: year is not hash " + tmp + hashValue);
                            System.exit(1);
                        }
                        indexFile.add(new IndexHashPos(key.id, hashValue, hashCounter));

                        //Serializing the vertices in the vertices file. Defining the offset of the next vertex
                        hashCounter += key.serialize(vertexWriter);
                    }
                    fop.flush();

                    //Incrementing the offset globally for the hash element
                    //hashCounter += (long)vertexPositionLocallyToHash;
                }
                hashIndexFile.close();
                fop.close();

                //Writing the index file using the inverse pointer strategy

                FileOutputStream indexHashPos = InMemoryGraph3.createFile(path, "id.index");
                System.out.print(indexFile.size());
                FileMapsUtils.writeIteration(indexFile, new Writer(indexHashPos));
                indexHashPos.close();
                t = System.currentTimeMillis() - t;
                t += timeDeltaSum;
                System.out.println("… done (ms) "+ t);
            } catch (Throwable ttt) {
                ttt.printStackTrace();
            }

        }
    }

}
