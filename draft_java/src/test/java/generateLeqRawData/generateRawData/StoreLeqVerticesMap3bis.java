/*
 * StoreLeqVerticesMap3bis.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */



package generateLeqRawData.generateRawData;

import com.google.inject.Guice;
import com.google.inject.Injector;
import it.giacomobergami.Paths;
import it.giacomobergami.data.GeneratorConfiguration;
import it.giacomobergami.filemaps.FileMapsUtils;
import it.giacomobergami.filemaps.formats.HashListEntryFormat;
import it.giacomobergami.filemaps.formats.IndexHashPos;
import it.giacomobergami.filemaps.rw.ReadAdjacencyFile;
import it.giacomobergami.filemaps.rw.Writer;
import it.giacomobergami.graphs.old_graphs3.InMemoryGraph3;
import it.giacomobergami.graphs.old_graphs3.InMemoryVertex3;
import it.giacomobergami.graphs.pointers.Pointer;
import it.giacomobergami.utils.I;
import it.giacomobergami.utils.MultiMapValuePointer;
import it.giacomobergami.utils.datastructures.Pair;
import it.giacomobergami.utils.datastructures.pointers.RBTreePointer;
import it.giacomobergami.utils.datastructures.trees.rbtree.RBTree;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by vasistas on 24/04/16.
 */
public class StoreLeqVerticesMap3bis {

    Paths p;

    public StoreLeqVerticesMap3bis() {
        p = Paths.getInstance();
    }

    /**
     * Returns the whole vertex set
     * @return
     */
    public void load() {
        System.out.println("Using solution 3bis.");

        InMemoryGraph3 g = new InMemoryGraph3("Ip","Organization","Year"); //Schema

        Injector injector = Guice.createInjector( new GeneratorConfiguration());
        VertexLeqGenerator3bis gen = VertexLeqGenerator3bis.getInstance();
        gen.setG(g);
        Iterable<InMemoryVertex3> avsi = ()->gen;

        //Vertex Data Structure
        //RBTree<Integer,BSONHolder3> orderedTree = new RBTree<>(true);
        //RBTree<Integer,BSONHolder3> indexedTree = new RBTree<>(true);
        System.out.print("Storing vertices in Graph…");
        long t = System.currentTimeMillis();
        avsi.forEach(g::addBulkVertex);
        t = System.currentTimeMillis() - t;
        System.out.println(" done in "+(t/1000)+" seconds");

        System.out.print("Loading in/out edges… ");
        //Adding the edges to each vertex.
        ReadAdjacencyFile outgoingFile = new ReadAdjacencyFile("\t",p.soccmel);
        Pair<Integer,Set<Integer>> right = null;
        t = System.currentTimeMillis();
        if (right==null && outgoingFile.hasNext()) right = outgoingFile.next();
        while (outgoingFile.hasNext()) {
            int rightNow = right.first;
            RBTreePointer<Pointer, InMemoryVertex3> ptr = g.getVertexPointer(rightNow);
            int index = right.first;
            for (int outgoing : right.value) {
                RBTreePointer<Pointer, InMemoryVertex3> l = g.getVertexPointer(outgoing);

                if (l!=null)
                    ptr.first().addNeighbour(l.first());
            }
            right = outgoingFile.next();
        }
        t = System.currentTimeMillis() - t;
        System.out.println(" done in "+(t/1000)+" seconds");

        policyMultiFileWithIndex(g.orderedTree);

    }


    public static FileOutputStream createFile(String filename) {
        File file = new File(filename);
        if (file.exists()) file.delete();
        try {
            return new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }


    public void policyMultiFileWithIndex(RBTree<Integer,InMemoryVertex3> orderedTree) {
        System.out.print("Write MultiFile… ");
        long t = System.currentTimeMillis();
        Iterable<MultiMapValuePointer<Integer, InMemoryVertex3>> it = new I<>(orderedTree.treeIterator());
        TreeSet<IndexHashPos> indexFile = new TreeSet<>();

        FileOutputStream fop = createFile(p.leqTestRaw("vertices.bin"));
        Writer vertexWriter = new Writer(fop);

        FileOutputStream hashIndexFile = createFile(p.leqTestRaw("hashes.index"));
        Writer hashWriter = new Writer(hashIndexFile);

        try {
            //Counting the offset of the hashlist
            long hashCounter = 0;
            for (MultiMapValuePointer<Integer, InMemoryVertex3> ptr : new I<>(orderedTree.treeIterator())) {
                int hashValue = ptr.getKey();

                //Position of the single vertex locally to the hash offset
                //int vertexPositionLocallyToHash = 0;

                //Creating an entry on the hash list element
                new HashListEntryFormat(hashValue,hashCounter).serialize(hashWriter);
                hashIndexFile.flush();

                for (InMemoryVertex3 key : new I<>(ptr.iterator())) {
                    //Setting the position of the current vertex
                    if (Integer.valueOf(key.getAttribute("Year"))!=hashValue) {
                        System.out.println("ERROR: "+key.getAttribute("Year")+" is not "+hashValue);
                        System.exit(1);
                    }
                    indexFile.add(new IndexHashPos(key.id.hashCode(),hashValue,hashCounter));

                    //Serializing the vertices in the vertices file. Defining the offset of the next vertex
                    hashCounter += key.compileRaw().serialize(vertexWriter);
                }
                fop.flush();

                //Incrementing the offset globally for the hash element
                //hashCounter += (long)vertexPositionLocallyToHash;
            }
            hashIndexFile.close();
            fop.close();

            //Writing the index file using the inverse pointer strategy

            /**
             * TODO: alternative
             *
             */
            FileOutputStream indexHashPos = createFile(p.leqTestRaw("id.index"));
            FileMapsUtils.writeIteration(indexFile,new Writer(indexHashPos));
            indexHashPos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String args[]) {
        new StoreLeqVerticesMap3bis().load();
    }

}
