/*
 * BenchmarkJoinOverSamples.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */


package it.giacomobergami.Eq;


import it.giacomobergami.Paths;
import it.giacomobergami.algorithms.Joins;
import it.giacomobergami.utils.datastructures.Pair;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Performs some benchmarking over the proposed implementation
 */
public class BenchmarkJoinOverSamples {

    public static void main(String[] args) throws IOException {
        Paths p = Paths.getInstance();
        int times = 10;
        int steps[] = new int[]{2,5,10,100,1000,10000,100000,1000000};
        FileWriter writer = new FileWriter("youtube_join_my_results.csv", true);
        for (int t=1; t<=times; t++) {
            System.out.println("Experiment/Run No: "+t);
            for (int i=1; i<=steps.length; i++) {
                File pathLeft = new File(p.eqTestRaw("left"+File.separator+"graph"+i));
                File pathRight = new File(p.eqTestRaw("right"+File.separator+"graph"+i));

                ArrayList<Pair<String,String>> qRaw = new ArrayList<>();
                qRaw.add(new Pair<>("Organization1","Organization2"));
                qRaw.add(new Pair<>("Year1","Year2"));

                TreeMap<String,Integer> leftattributeToPosition = new TreeMap<>();
                leftattributeToPosition.put("Ip1",0);
                leftattributeToPosition.put("Organization1",1);
                leftattributeToPosition.put("Year1",2);

                TreeMap<String,Integer> rightattributeToPosition = new TreeMap<>();
                rightattributeToPosition.put("Ip2",0);
                rightattributeToPosition.put("Organization2",1);
                rightattributeToPosition.put("Year2",2);

                long mes = System.nanoTime();
                Joins.basicJoin(pathLeft,pathRight,qRaw,leftattributeToPosition,rightattributeToPosition);
                mes = System.nanoTime() - mes;
                //System.out.println("Time="+mes);
                writer.write(steps[i-1]+","+mes+"\n");
                //System.out.println("");
            }
            System.gc();
        }
        writer.close();
        System.out.println("Done.");
    }

}
