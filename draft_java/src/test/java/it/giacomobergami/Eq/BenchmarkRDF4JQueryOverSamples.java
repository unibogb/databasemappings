package it.giacomobergami.Eq;

import com.sun.org.apache.xerces.internal.impl.xpath.XPath;
import it.giacomobergami.graphs.SesameGraph.SesameGraph;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by vasistas on 14/06/16.
 */
public class BenchmarkRDF4JQueryOverSamples {

    public static void main(String args[]) throws IOException {

        //sg.printTriples(System.out);
        String l = "CONSTRUCT { \n" +
                "\t?newSrc <http://jackbergus.alwaysdata.net/graph> \"Result\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/edges/result> ?newDst; \n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year2> ?y2.\n" +
                "\t?newDst <http://jackbergus.alwaysdata.net/graph> \"Result\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year2> ?y4.\n" +
                "} WHERE {     \n" +
                "\t?src1 <http://jackbergus.alwaysdata.net/graph> \"L\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Id> ?id1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y1.\n" +
                "\t?src2 <http://jackbergus.alwaysdata.net/graph> \"R\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Id> ?id2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y2.\n" +
                "\tFILTER ( ( ?org1 = ?org2 ) && ( ?y1 =  ?y2 ) ) \n" +
                "\t\n" +
                "\tBIND (URI(CONCAT(\"http://jackbergus.alwaysdata.net/values/\",?id1,\"-\",?id2)) AS ?newSrc)\n" +
                "\t\n" +
                "\tOPTIONAL {\n" +
                "\t\t?src1 <http://jackbergus.alwaysdata.net/edges/edge> ?dst1.\n" +
                "\t\t?src2 <http://jackbergus.alwaysdata.net/edges/edge> ?dst2.\n" +
                "\t\t?dst1 <http://jackbergus.alwaysdata.net/graph> \"L\";     \n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Id> ?id3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y3.\n" +
                "\t\t?dst2 <http://jackbergus.alwaysdata.net/graph> \"R\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Id> ?id4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y4.\n" +
                "\t\tFILTER ( ( ?org3 = ?org4 ) && ( ?y3 = ?y4 ) )\n" +
                "\t\tBIND (URI(CONCAT(\"http://jackbergus.alwaysdata.net/values/\",?id3,\"-\",?id4)) AS ?newDst)\n" +
                "\t}\n" +
                "}";
        System.out.println(l);
        System.exit(1);
        FileWriter fw = new FileWriter("youtube_join_rdf4j_results.csv");
        fw.write("size,rdf4j\n");
        for (int times=0; times<10; times++) {
            int steps[] = new int[]{2, 5, 10, 100, 1000, 10000, 100000, 1000000};
            for (int pos = 1; pos < 7; pos++) {
                System.out.println("\tsize: " + steps[pos - 1]);
                SesameGraph sg = new SesameGraph(new File(pos + "rdf4j"));
                sg.performConstructSPARQLquery(l, steps[pos - 1], fw).close();
                sg.close();
                fw.flush();
            }
        }
    }

}
