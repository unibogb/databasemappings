/*
 * AggregateLeqSamplesInNeo4j.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */


package it.giacomobergami.Leq;


import it.giacomobergami.Paths;
import it.giacomobergami.filemaps.buffers.BufferArray;
import it.giacomobergami.filemaps.buffers.LongMMFile;
import it.giacomobergami.filemaps.formats.BSONHolder4;
import it.giacomobergami.filemaps.formats.IndexHashPos;
import it.giacomobergami.graphs.Neo4jGraph.Neo4JGraph;
import it.giacomobergami.graphs.abstractgraph.AbstractGraph;
import it.giacomobergami.graphs.abstractgraph.AbstractNode;
import it.giacomobergami.graphs.nodes.GoogleGraphVertex;
import it.giacomobergami.graphs.pointers.GraphIdPointerLong;
import it.giacomobergami.graphs.pointers.Pointer;
import it.giacomobergami.utils.I;
import it.giacomobergami.utils.datastructures.iterators.MapIterator;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Stores the left and right operands that have been sampled in the raw format in Neo4J
 */
public class AggregateLeqSamplesInNeo4j {

    public static final BSONHolder4 unserializer = new BSONHolder4();
    public static final HashSet<String> blankSet = new HashSet<>();
    public static final Pointer ptr = new GraphIdPointerLong(0L);

    /**
     * Returns the node as a vertex
     * @param left
     * @param file
     * @param inTransaction
     * @return
     */
    public static AbstractNode getOrCreateVertex(IndexHashPos left, LongMMFile file, AbstractGraph inTransaction, int side, int hop) {
        BSONHolder4 bulkVertexL = unserializer.unserialize(file,left.pos);
        GoogleGraphVertex vL = new GoogleGraphVertex("User",blankSet,ptr);
        vL.setAttribute("UID",(left.index+hop)+"");
        vL.setAttribute("graph",side==1 ? "L" : "R");
        vL.setAttribute("Ip"+side,bulkVertexL.values.get(0));
        vL.setAttribute("Organization"+side,bulkVertexL.values.get(1));
        vL.setAttribute("Year"+side,bulkVertexL.values.get(2));
        return inTransaction.addVertex(vL);
    }

    public static void main(String args[]) throws IOException {
        Paths p = Paths.getInstance();
        for (int pos=1; pos<6; pos++) {
            System.out.println("Storing graph i-th:"+pos);
            File pathLeft = new File(p.leqTestRaw("left"+File.separator+"graph"+pos));
            File pathRight = new File(p.leqTestRaw("right"+File.separator+"graph"+pos));
            Map<Set<String>,Set<String>> vertexLabelToAttributesToIndex = new HashMap<>(1);
            Set<String> type = new HashSet<>(1);
            type.add("User");
            Set<String> hashing = new HashSet<>(2);
            hashing.add("Year1");
            hashing.add("Year2");
            vertexLabelToAttributesToIndex.put(type,hashing);
            long t = System.currentTimeMillis();
            Neo4JGraph g = new Neo4JGraph("UID","r",false,vertexLabelToAttributesToIndex); // graphs are automatically stored in the current directory, with increasing numbers
            t = System.currentTimeMillis() - t;
            BSONHolder4 unserializer = new BSONHolder4();

            LongMMFile vLeft = new LongMMFile(new File(pathLeft, "vertices.bin")),
                    vRight = new LongMMFile(new File(pathRight, "vertices.bin"));

            LongMMFile lmfLeft = new LongMMFile(new File(pathLeft,"id.index")),
                    lmfRight = new LongMMFile(new File(pathRight,"id.index"));

            BufferArray<IndexHashPos> baLeft = new BufferArray<>(IndexHashPos.class,lmfLeft),
                    baRight = new BufferArray<>(IndexHashPos.class,lmfRight);

            long tt = System.currentTimeMillis();
            int max = g.startTransaction(() -> {
                int maxUID = -1;
                for (IndexHashPos left : baLeft) {
                    BSONHolder4 bulkVertexL = unserializer.unserialize(vLeft,left.pos);
                    //System.out.println("id="+left.index+" idB="+bulkVertexL.id);
                    AbstractNode anLeft = getOrCreateVertex(left,vLeft,g,1,0);
                    if (maxUID<left.index) maxUID = left.index;
                    // Allocating all the neighbours in order not to scan the file coninuously
                    Iterable<AbstractNode> Nu = new I<>(new MapIterator<Integer,AbstractNode>(bulkVertexL.outgoingA.iterator()) {
                        @Override
                        public AbstractNode apply(Integer o) {
                            IndexHashPos ptr = baLeft.get(o);
                            return getOrCreateVertex(ptr,vLeft,g,1,0);
                        }
                    });
                    for (AbstractNode anLeftp : Nu) {
                        g.addEdge(anLeft,anLeftp);
                    }
                }
                return maxUID;
            }).get();
            g.startTransaction(()-> {
                for (IndexHashPos right : baRight) {
                    BSONHolder4 bulkVertexR = unserializer.unserialize(vRight,right.pos);
                    AbstractNode anRight = getOrCreateVertex(right,vRight,g,2,max);
                    // Allocating all the neighbours in order not to scan the file coninuously
                    Iterable<AbstractNode> Nu = new I<>(new MapIterator<Integer,AbstractNode>(bulkVertexR.outgoingA.iterator()) {
                        @Override
                        public AbstractNode apply(Integer o) {
                            IndexHashPos ptr = baRight.get(o);
                            return getOrCreateVertex(ptr,vRight,g,1,0);
                        }
                    });
                    for (AbstractNode anRightp : Nu) {
                        g.addEdge(anRight,anRightp);
                    }
                }
                return 1;
            });
            tt = System.currentTimeMillis() - tt;
            System.out.println(pos+" in "+tt);
            g.close();
        }
    }

}
