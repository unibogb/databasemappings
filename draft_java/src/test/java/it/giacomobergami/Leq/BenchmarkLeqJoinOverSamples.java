/*
 * BenchmarkLeqJoinOverSamples.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */
package it.giacomobergami.Leq;

import it.giacomobergami.Paths;
import it.giacomobergami.algorithms.Joins;
import it.giacomobergami.utils.datastructures.Pair;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.TreeMap;

/**
 *  Performs some benchmarking over the proposed implementation
 */
public class BenchmarkLeqJoinOverSamples {

    public static void main(String[] args) throws IOException {
        Paths p = Paths.getInstance();
        FileWriter fw = new FileWriter("join_myLeq_results.csv");
        fw.write("size,proposed\n");
        for (int k=1; k<=10; k++ ) {
            for (int i = 1; i < 6; i++) {
                File pathLeft = new File(p.leqTestRaw("left" + File.separator + "graph" + i));
                File pathRight = new File(p.leqTestRaw("right" + File.separator + "graph" + i));
                TreeMap<String, Integer> leftattributeToPosition = new TreeMap<>();
                leftattributeToPosition.put("Ip1", 0);
                leftattributeToPosition.put("Organization1", 1);
                leftattributeToPosition.put("Year1", 2);

                TreeMap<String, Integer> rightattributeToPosition = new TreeMap<>();
                rightattributeToPosition.put("Ip2", 0);
                rightattributeToPosition.put("Organization2", 1);
                rightattributeToPosition.put("Year2", 2);

                long t = System.nanoTime();
                Joins.leqJoin(pathLeft, pathRight, new Pair<>("Year1", "Year2"), leftattributeToPosition, rightattributeToPosition);
                t = System.nanoTime() - t;
                fw.write(i+","+t+"\n");
                fw.flush();
            }
        }
        fw.close();
    }

}
