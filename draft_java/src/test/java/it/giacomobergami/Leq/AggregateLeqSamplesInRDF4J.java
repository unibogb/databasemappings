/*
 * AggregateLeqSamplesInRDF4J.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */

package it.giacomobergami.Leq;



import it.giacomobergami.Paths;
import it.giacomobergami.filemaps.buffers.BufferArray;
import it.giacomobergami.filemaps.buffers.LongMMFile;
import it.giacomobergami.filemaps.formats.BSONHolder4;
import it.giacomobergami.filemaps.formats.IndexHashPos;
import it.giacomobergami.graphs.SesameGraph.SesameGraph;
import it.giacomobergami.graphs.pointers.GraphIdPointerLong;
import it.giacomobergami.graphs.pointers.Pointer;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.nativerdf.NativeStore;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Stores the left and right operands that have been sampled in the raw format in RDF4J
 */
public class AggregateLeqSamplesInRDF4J {

    public static final BSONHolder4 unserializer = new BSONHolder4();
    public static final HashSet<String> blankSet = new HashSet<>();
    public static final Pointer ptr = new GraphIdPointerLong(0L);

    /*
    public static void main1(String args[]) {

        //sg.printTriples(System.out);
        String l = "CONSTRUCT { \n" +
                "\t?newSrc <http://jackbergus.alwaysdata.net/graph> \"Result\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/edges/result> ?newDst; \n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year2> ?y2.\n" +
                "\t?newDst <http://jackbergus.alwaysdata.net/graph> \"Result\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year2> ?y4.\n" +
                "} WHERE {     \n" +
                "\t?src1 <http://jackbergus.alwaysdata.net/graph> \"L\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Id> ?id1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y1.\n" +
                "\t?src2 <http://jackbergus.alwaysdata.net/graph> \"R\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Id> ?id2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year2> ?y2.\n" +
                "\tFILTER (( ?y1 <=  ?y2 ) ) \n" +
                "\t\n" +
                "\tBIND (URI(CONCAT(\"http://jackbergus.alwaysdata.net/values/\",?id1,\"-\",?id2)) AS ?newSrc)\n" +
                "\t\n" +
                "\tOPTIONAL {\n" +
                "\t\t?src1 <http://jackbergus.alwaysdata.net/edges/edge> ?dst1.\n" +
                "\t\t?src2 <http://jackbergus.alwaysdata.net/edges/edge> ?dst2.\n" +
                "\t\t?dst1 <http://jackbergus.alwaysdata.net/graph> \"L\";     \n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Id> ?id3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y3.\n" +
                "\t\t?dst2 <http://jackbergus.alwaysdata.net/graph> \"R\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Id> ?id4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y4.\n" +
                "\t\tFILTER ( ( ?y3 <= ?y4 ) )\n" +
                "\t\tBIND (URI(CONCAT(\"http://jackbergus.alwaysdata.net/values/\",?id3,\"-\",?id4)) AS ?newDst)\n" +
                "\t}\n" +
                "}";
    System.out.println(l);
        System.exit(1);
        for (int pos=1; pos<7; pos++) {
            SesameGraph sg = new SesameGraph(new File(pos+"rdf4j"));
            sg.performConstructSPARQLquery(l, steps[pos - 1], fw);
            sg.close();
        }
    }*/

    public static void main(String args[]) throws IOException {
        Paths p = Paths.getInstance();
        for (int pos=1; pos<6; pos++) {
            Repository repo = new SailRepository(new NativeStore(new File(pos+"rdf4j"),"spoc,posc,cosp"));
            repo.initialize();
            ValueFactory f = repo.getValueFactory();

            System.out.println("Storing graph i-th:"+pos);
            File pathLeft = new File(p.leqTestRaw("left"+File.separator+"graph"+pos));
            File pathRight = new File(p.leqTestRaw("right"+File.separator+"graph"+pos));
            long t = System.currentTimeMillis();
            t = System.currentTimeMillis() - t;
            BSONHolder4 unserializer = new BSONHolder4();

            LongMMFile vLeft = new LongMMFile(new File(pathLeft, "vertices.bin")),
                    vRight = new LongMMFile(new File(pathRight, "vertices.bin"));

            LongMMFile lmfLeft = new LongMMFile(new File(pathLeft,"id.index")),
                    lmfRight = new LongMMFile(new File(pathRight,"id.index"));

            BufferArray<IndexHashPos> baLeft = new BufferArray<>(IndexHashPos.class,lmfLeft),
                    baRight = new BufferArray<>(IndexHashPos.class,lmfRight);

            long tt = System.currentTimeMillis();

            int maxUID = -1;
            try (RepositoryConnection conn = repo.getConnection()) {
                for (IndexHashPos left : baLeft) {
                    BSONHolder4 bulkVertexL = unserializer.unserialize(vLeft,left.pos);
                    //System.out.println("id="+left.index+" idB="+bulkVertexL.id);
                    IRI vertex = f.createIRI("http://jackbergus.alwaysdata.net/values/" + left.index);
                    Literal id = f.createLiteral(""+left.index);
                    conn.add(vertex,f.createIRI("http://jackbergus.alwaysdata.net/property/Id"),id);

                    Literal ip = f.createLiteral(bulkVertexL.values.get(0));
                    conn.add(vertex,f.createIRI("http://jackbergus.alwaysdata.net/property/Ip1"),ip);

                    Literal or = f.createLiteral(bulkVertexL.values.get(1));
                    conn.add(vertex,f.createIRI("http://jackbergus.alwaysdata.net/property/Organization1"),or);

                    Literal ye = f.createLiteral(bulkVertexL.values.get(2));
                    conn.add(vertex,f.createIRI("http://jackbergus.alwaysdata.net/property/Year1"),ye);

                    Literal gr = f.createLiteral("L");
                    conn.add(vertex,f.createIRI("http://jackbergus.alwaysdata.net/graph"),gr);

                    Iterator<Integer> it = bulkVertexL.outgoingA.iterator();
                    while (it.hasNext()) {
                        IRI vertexdst = f.createIRI("http://jackbergus.alwaysdata.net/values/" + it.next());
                        conn.add(vertex,f.createIRI("http://jackbergus.alwaysdata.net/edges/edge"),vertexdst);
                    }

                    if (maxUID<left.index) maxUID = left.index;
                    // Allocating all the neighbours in order not to scan the file coninuously
                }
                conn.commit();
            }
            maxUID++;

            try (RepositoryConnection conn = repo.getConnection()) {
                for (IndexHashPos right : baRight) {
                    BSONHolder4 bulkVertexR = unserializer.unserialize(vRight, right.pos);
                    //System.out.println("id="+left.index+" idB="+bulkVertexL.id);
                    IRI vertex = f.createIRI("http://jackbergus.alwaysdata.net/values/" + (right.index + maxUID));
                    Literal id = f.createLiteral("" + right.index);
                    conn.add(vertex, f.createIRI("http://jackbergus.alwaysdata.net/property/Id"), id);

                    Literal ip = f.createLiteral(bulkVertexR.values.get(0));
                    conn.add(vertex, f.createIRI("http://jackbergus.alwaysdata.net/property/Ip2"), ip);

                    Literal or = f.createLiteral(bulkVertexR.values.get(1));
                    conn.add(vertex, f.createIRI("http://jackbergus.alwaysdata.net/property/Organization2"), or);

                    Literal ye = f.createLiteral(bulkVertexR.values.get(2));
                    conn.add(vertex, f.createIRI("http://jackbergus.alwaysdata.net/property/Year2"), ye);

                    Literal gr = f.createLiteral("R");
                    conn.add(vertex,f.createIRI("http://jackbergus.alwaysdata.net/graph"),gr);

                    Iterator<Integer> it = bulkVertexR.outgoingA.iterator();
                    while (it.hasNext()) {
                        IRI vertexdst = f.createIRI("http://jackbergus.alwaysdata.net/values/" + (it.next()+maxUID));
                        conn.add(vertex, f.createIRI("http://jackbergus.alwaysdata.net/edges/edge"), vertexdst);
                    }
                }
            }
            tt = System.currentTimeMillis() - tt;
            System.out.println(pos+" in "+tt);
        }
    }

}
