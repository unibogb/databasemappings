/*
 * BenchmarkLeqNeo4JQueryOverSamples.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */

package it.giacomobergami.Leq;

import it.giacomobergami.graphs.Neo4jGraph.Neo4JGraph;
import org.neo4j.graphdb.Result;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *  Performs some benchmarking over Neo4J
 */
public class BenchmarkLeqNeo4JQueryOverSamples {

    public static void main(String[] args) throws IOException {
        CypherLeqQueryCompiler tester = new CypherLeqQueryCompiler(true, new String[]{"Year"});
        //System.out.println(tester.generateCypherQuery());
        //System.exit(1);
        //String zeros = "0";
        FileWriter fw = new FileWriter("join_neo4jLeq_results.csv", true);
        for (int pos=0; pos<5; pos++) {
            //if (pos>0) zeros+="0";
            //System.out.println("1"+zeros+"x1"+zeros);
            System.out.println("Storing graph i-th:" + pos);
            Map<Set<String>, Set<String>> vertexLabelToAttributesToIndex = new HashMap<>(1);
            Set<String> type = new HashSet<>(1);
            type.add("User");
            Set<String> hashing = new HashSet<>(2);
            hashing.add("Year1");
            hashing.add("Year2");
            vertexLabelToAttributesToIndex.put(type, hashing);
            Neo4JGraph g = new Neo4JGraph("UID", "r", false, null,pos+""); //Getting the graph
            int cpos = pos;
            long t = g.startTransaction(()-> {
               Result r;
                //System.out.println(g.toString());
                //System.exit(3);
                long time_t = System.nanoTime();
                r = g.doGraphQuery(tester.generateCypherQuery());
                time_t = System.nanoTime() - time_t;
                try {
                    fw.write((cpos+1)+","+time_t+"\n");
                } catch (IOException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
                int count = 0;
                while (r.hasNext()) {
                    r.next();
                    count++;
                }
                System.out.println("Size:"+count);
                return time_t;
            }).get();
            g.close();
            fw.flush();
        }
        fw.close();
        //bgo.doGraphQuery(tester.generateCypherQuery())
    }

}
