/*
 * BenchmarkLeqSamplesInRDF4J.java
 * This file is part of databaseMappings
 *
 * Copyright (C) 2016 - Giacomo Bergami
 *
 * databaseMappings is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * databaseMappings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with databaseMappings. If not, see <http://www.gnu.org/licenses/>.
 */

package it.giacomobergami.Leq;

import it.giacomobergami.filemaps.formats.BSONHolder4;
import it.giacomobergami.graphs.SesameGraph.SesameGraph;
import it.giacomobergami.graphs.pointers.GraphIdPointerLong;
import it.giacomobergami.graphs.pointers.Pointer;
import org.apache.commons.io.output.WriterOutputStream;

import java.io.*;
import java.util.HashSet;

/**
 *  Performs some benchmarking over RDF4J
 */
public class BenchmarkLeqSamplesInRDF4J {

    public static final BSONHolder4 unserializer = new BSONHolder4();
    public static final HashSet<String> blankSet = new HashSet<>();
    public static final Pointer ptr = new GraphIdPointerLong(0L);


    public static void main(String args[]) throws IOException {

        //sg.printTriples(System.out);
        String l = "CONSTRUCT { \n" +
                "\t?newSrc <http://jackbergus.alwaysdata.net/graph> \"Result\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/edges/result> ?newDst; \n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year2> ?y2.\n" +
                "\t?newDst <http://jackbergus.alwaysdata.net/graph> \"Result\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year2> ?y4.\n" +
                "} WHERE {     \n" +
                "\t?src1 <http://jackbergus.alwaysdata.net/graph> \"L\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Id> ?id1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org1;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y1.\n" +
                "\t?src2 <http://jackbergus.alwaysdata.net/graph> \"R\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Id> ?id2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org2;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year2> ?y2.\n" +
                "\tFILTER (( ?y1 <=  ?y2 ) ) \n" +
                "\t\n" +
                "\tBIND (URI(CONCAT(\"http://jackbergus.alwaysdata.net/values/\",?id1,\"-\",?id2)) AS ?newSrc)\n" +
                "\t\n" +
                "\tOPTIONAL {\n" +
                "\t\t?src1 <http://jackbergus.alwaysdata.net/edges/edge> ?dst1.\n" +
                "\t\t?src2 <http://jackbergus.alwaysdata.net/edges/edge> ?dst2.\n" +
                "\t\t?dst1 <http://jackbergus.alwaysdata.net/graph> \"L\";     \n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Id> ?id3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip1> ?ip3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization1> ?org3;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year1> ?y3.\n" +
                "\t\t?dst2 <http://jackbergus.alwaysdata.net/graph> \"R\";\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Id> ?id4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Ip2> ?ip4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Organization2> ?org4;\n" +
                "\t      <http://jackbergus.alwaysdata.net/property/Year2> ?y4.\n" +
                "\t\tFILTER ( ( ?y3 <= ?y4 ) )\n" +
                "\t\tBIND (URI(CONCAT(\"http://jackbergus.alwaysdata.net/values/\",?id3,\"-\",?id4)) AS ?newDst)\n" +
                "\t}\n" +
                "}";
    //System.out.println(l);
        //System.exit(1);
        FileWriter fw = new FileWriter("join_rdf4jLeq_results.csv");
        fw.write("size,rdf4j\n");
        for (int times=0; times<10; times++) {
            for (int pos = 1; pos < 6; pos++) {
                SesameGraph sg = new SesameGraph(new File(pos + "rdf4j"));
                sg.performConstructSPARQLquery(l, pos, fw);
                sg.close();
            }
        }
        fw.close();
    }



}
